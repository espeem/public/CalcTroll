# Copyright (C) 2016 Mads Engelund, http://mads-engelund.net
import os
import argparse
from CalcTroll.Core.Submission.Workflow import Workflow

parser = argparse.ArgumentParser(\
"""Copy current version of CalcTroll to remote machine.
""")
args = parser.parse_args()
host = Workflow().host()
source = os.path.dirname(os.path.abspath(__file__))
source = os.sep.join(source.split(os.sep)[:-1]) + os.sep

cmd = 'rsync -Czrvc %s %s@%s:~/SOFTWARE/CalcTroll/' % (
        source,
        host.username(),
        host.server(),
        )
cmd += ' --include="*/" --include="*.py" --include="*.psf" --exclude="*"'
print cmd
os.system(cmd)
