import os
from os.path import join


replaced = "# Copyright"
replace = '# Written by Mads Engelund, 2017, http://espeem.com\n'
a = []
for root, dirs, files in os.walk('CalcTroll'):
    files = [f for f in files if f[-3:]=='.py']
    files = [f for f in files if f!='__init__.py']
    for f in files:
        filename = join(root, f)

        with open(filename, 'r') as f:
            lines = f.readlines()

        if lines[0][:len(replaced)] == replaced:
            lines[0] = replace

        with open(filename, 'w') as f:
            f.writelines(lines)
