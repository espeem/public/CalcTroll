**************************************************
Installation first steps.
**************************************************
Make the 'data' directory somewhere.
`mkdir data
Make a directory  to store your projects.
`mkdir Projects
Make a directory  to store your own plugins.
`mkdir UserPlugins
These are your persistent directories that remain after the docker image has 
terminated. The linking method described below will only work on Linux OS.
Working in Windows should possible but requires some docker knowledge. 

**************************************************
Installation and use of the production environment
**************************************************
Move to the 'prod' directory.
`cd <PATH-TO-CALCTROLL>/docker/prod
`docker build --tag calctroll
Will take a some time (downloading + compiling)
Now you can run this environment interactively using the command:
`docker run -it --mount type=bind,source=<PATH-TO-DATA>,target=/root/data --mount type=bind,source=<PATH-TO-PROJECTS>,target=/root/Projects --mount type=bind,source=<PATH-TO-USER-PLUGINS>,target=/root/Custom/UserPlugins --hostname docker calctroll 

******************************************************
Running calculations in the environment.
******************************************************
Copy some example script into the 'scripts' directory 
`cp /opt/CalcTroll/ExampleScripts/* scripts
The main executable to use is 'mysubmit'.
`mysubmit -d script.py
... (dry-run) will identify tasks that need to be executed and dependencies but execute nothing.
`mysubmit script.py
... (normal) will run all tasks with satisfied dependencies.
`mysubmit -l script.py
... (loop) will keep re-submitting the script untill no more tasks can be performed.
