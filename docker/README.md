This folder contain the automated scripts (Dockerfiles) that set up isolated environments (docker containers) with all nessecary executables and libraries. 

There are two versions 
'prod' - a production environment with a static version of the source code. 
'dev' - a development environment where the source code is meant to be linked so you can change it and 
        retain the changes. 

For installation navigate to the appropriate folder and follow instructions in the README

**************************************************
Installation first steps.
**************************************************
Make the 'data' directory somewhere.
`mkdir data
Make a directory  to store your scripts somewhere.
`mkdir scripts
These are your persistent directories that remain after the docker image has 
terminated. The linking method described below will only work on Linux OS.
Working in Windows should possible but requires some docker knowledge. 

**************************************************
Installation and use of the production environment
**************************************************
Move to the 'prod' directory.
`cd <PATH-TO-CALCTROLL>/docker/prod
`docker build --tag calctroll
Will take a some time (downloading + compiling)
Now you can run this environment interactively using the command:
`docker run -it --mount type=bind,source=<PATH-TO-DATA>,target=/root/data 
                --mount type=bind,source=<PATH-TO-SCRIPTS>,target=/root/scripts
                --hostname docker 
                calctroll
                
**************************************************
Installation and use of the development environment
**************************************************
Move to the 'dev' directory.
`cd <PATH-TO-CALCTROLL>/docker/dev
`docker build --tag calctroll
Will take a some time (downloading + compiling)
Now you can run this environment interactively using the command:
`docker run -it --mount type=bind,source=<PATH-TO-DATA>,target=/root/data 
                --mount type=bind,source=<PATH-TO-SCRIPTS>,target=/root/scripts
                --mount type=bind,source=<PATH-TO-CALCTROLL>,target=/opt/CalcTroll
                --hostname docker 
                calctroll

First time you run this environment you will also have to build the executables.
`cd /opt/CalcTroll
`python3 setup.py build

******************************************************
Running calculations in the environment.
******************************************************
Copy some example script into the 'scripts' directory 
`cp /opt/CalcTroll/ExampleScripts/* scripts
The main executable to use is 'mysubmit'.
`mysubmit -d script.py
... (dry-run) will identify tasks that need to be executed and dependencies but execute nothing.
`mysubmit script.py
... (normal) will run all tasks with satisfied dependencies.
`mysubmit -l script.py
... (loop) will keep re-submitting the script untill no more tasks can be performed.
