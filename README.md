CalcTroll is a framework for creating targeted atomic-scale simulation applications. 
Practically it is a series of utilities for setting up series time-demanding long-running calculations on a cluster, at current specifially focused on STM simulations with the SIESTA code. However, the idea is that any atomic-scale simulation workflow would be easily implemented.

The basic idea is to set up chains of calculations, linking the output of one run to the input of another. Some structures and calculations come predefined in the program, but in general a user will need to implement these, likely by subclassing some of the basic types defined in the code. Once created though, the plugins will live in a simple scripting interface.

Basic usecases would be:
1. Creating a workflow on-demand for someone else to use.
2. Capture and share your workflow for transparency with collaborators.

********************************
Installation:
********************************
We recommend installing a CalcTroll environment using Docker.
Instructions can be found in CalcTroll/docker/README.md

*******************************
Examples
*******************************
Examples can be found in the CalcTroll/Plugins folder, spread among the implemented code.
All example scripts are written as 'example_scriptname.py'.
When running python setup.py for the first time all 'example' scripts are copied to the ExampleScripts folder.
