# Written by Mads Engelund, 2023, http://espeem.com
# This plugin demonstrates how a user could interact with a specific
# remote host "Meluxina" using slurm scripts for submission and allowing the
# user to post to the queue.
import argparse
import os
from CalcTroll.Core.Relaxation.Task import RelaxationTask
from CalcTroll.Plugins.Servers.SlurmServer import SlurmServer



class Meluxina(SlurmServer):
    def __init__(
        self,
        project,
        data_path='data',
        ):
        self.__project = project
        super().__init__(
            server='login.lxp.lu',
            local_path='/project/scratch/' + project,
            data_path=data_path,
        )

    def submitParser(self):
        parser = argparse.ArgumentParser("ArgumentParser")
        parser.add_argument("filename")
        parser.add_argument("-d", "--dry_run",
                action='store_true',
                )
        parser.add_argument("-l", "--loop",
                action='store_true', default=False,
                )
        parser.add_argument("-s", "--skip_fail",
                action='store_true', default=True,
                )

        return parser

    def cleanAll(self):
        eeeeeeeeeeeeeeeeeeeee

    def utilities(self):
        return {}

    def header(self, run):
        n_valence = run.numberOfValenceElectrons()
        executable = run.executable()

        if executable == 'siesta':
            script = """#!/bin/bash -l
#SBATCH -N 1
#SBATCH --ntasks-per-node=64
#SBATCH --cpus-per-task=2
#SBATCH --time=10:00:00
#SBATCH --account=%s
#SBATCH --partition=cpu
#SBATCH --qos=default
            """ % self.__project
        elif executable == 'pw.x' and isinstance(run, RelaxationTask):
            script = """#!/bin/bash -l
#SBATCH -N 1
#SBATCH --ntasks-per-node=4
#SBATCH --time=10:00:00
#SBATCH --account=%s
#SBATCH --partition=gpu
#SBATCH --qos=default
            """ % self.__project

        elif executable == 'pw.x':
            script = """#!/bin/bash -l
#SBATCH -N 1
#SBATCH --ntasks-per-node=128
#SBATCH --cpus-per-task=1
#SBATCH --time=10:00:00
#SBATCH --account=%s
#SBATCH --partition=cpu
#SBATCH --qos=default
            """ % self.__project

        elif executable == 'pp.x':
            script = """#!/bin/bash -l
#SBATCH --ntasks=4
#SBATCH --cpus-per-task=32
#SBATCH --time=10:00:00
#SBATCH --account=%s
#SBATCH --partition=cpu
#SBATCH --qos=default
        """ % self.__project
            
        elif executable == 'yambo':
            cpus_per_task = 4
            script = """#!/bin/bash -l
#SBATCH --ntasks=32
#SBATCH --cpus-per-task=%d
#SBATCH --time=20:00:00
#SBATCH --account=%s
#SBATCH --partition=cpu
#SBATCH --qos=default
export OMP_NUM_THREADS=%d
        """ % (
            cpus_per_task, 
            self.__project,
            cpus_per_task,
            )
                
        elif executable == 'python':
            script = """#!/bin/bash -l
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=16
#SBATCH --time=10:00:00
#SBATCH --account=%s
#SBATCH --partition=cpu
#SBATCH --qos=default
        """ % self.__project
        else:
            raise Exception

        return script

    def scratchDirectory(self):
        return "/project/scratch/" + self.__project
    
    def methodScript(self, run):
        runfile = os.path.basename(run.runFile())
        tmp_outfile = 'TMP.out'
        outfile = os.path.basename(run.outFile())
        method = run.method()
        program = method.program()

        script = ''

        executable = run.executable()

        if executable == 'siesta':
            script += """
module load env/release
module load DummySiesta
""" 
            form = """$PROGRAM %s > %s"""        
        elif executable == 'pw.x'  and isinstance(run, RelaxationTask):
            script += """
module load env/staging/2023.1
module load QuantumESPRESSO/7.2-nvompic-2023a-NVHPC-23.7
module load SciPy-bundle/2023.07-gfbf-2023a-python-3.10.8
source /project/scratch/p200196/calc_env/bin/activate
export ASE_ESPRESSO_COMMAND="mpirun -n 4 pw.x -in PREFIX.pwi -npool 2 2>&1 | tee PREFIX.pwo"
""" 
            form = """$PROGRAM %s > %s"""
        
        elif executable == 'pw.x':
            script += """
module load env/release/2022.1
module load QuantumESPRESSO/7.1-foss-2022a
module load SciPy-bundle   
source /project/scratch/p200196/calc_env/bin/activate
export ASE_ESPRESSO_COMMAND="srun -n 128 pw.x -in PREFIX.pwi > PREFIX.pwo"
""" 
            form = """$PROGRAM %s > %s"""

        elif executable == 'pp.x':
            script += """
module load env/release/2022.1
module load QuantumESPRESSO/7.1-foss-2022a
module load SciPy-bundle   
source /project/scratch/p200196/calc_env/bin/activate
export ESPRESSO_PP_COMMAND="srun -n 16 pp.x"
"""
            form = """python %s > %s"""

        elif executable == 'yambo':
            script += """
module load env/staging/2022.1
module load Yambo/5.2.0-intel-2022a 
module load libfabric/1.15.1-GCCcore-11.3.0 
module load SciPy-bundle
source /project/scratch/p200196/calc_env/bin/activate
# export CUDA_VISIBLE_DEVICES=0,1,2,3
export YAMBO_EXECUTION_COMMAND='srun -verbose yambo'
"""
            form = """$PROGRAM %s > %s"""
        elif executable == 'python':
            script += """
module load env/release/2022.1 
module load SciPy-bundle
source /project/scratch/p200196/calc_env/bin/activate
"""
            form = """python %s > %s"""
        else:
            raise Exception("%s does not know how to run '%s'"%(self.__class__, executable))

        for command in run.makeCommands(form):
            script += command
            script += '\n'

        return script
    
    def setupScript(self, run):
        return ''
    
    def tearDownScript(self, run):
        script = """    
echo '--- END OF JOB ---'
"""
        return script