# Written by Mads Engelund, 2017, http://espeem.com
# This plugin demonstrates how a user could interact with a specific
# remote host "Oberon" using slurm scripts for submission and allowing the
# user to post to the queues "pams" and "all".
import argparse
import os
from CalcTroll.Plugins.Servers.SlurmServer import SlurmServer
from CalcTroll.Plugins.Programs.Siesta import Siesta

from CalcTroll import API
from CalcTroll.API import DEFAULT

class Oberon(SlurmServer):
    allowed_queues = ["pams", "all"]

    def submitParser(self):
        parser = argparse.ArgumentParser("ArgumentParser")
        parser.add_argument("filename")
        parser.add_argument("-d", "--dry_run",
                action='store_true',
                )
        parser.add_argument("-l", "--loop",
                action='store_true', default=False,
                )
        parser.add_argument("-s", "--skip_fail",
                action='store_true', default=True,
                )
        parser.add_argument("-n", "--nodes", default=1, type=int)
        parser.add_argument(
                "-q", "--queue",
                default=self.allowed_queues[0],
                choices=self.allowed_queues,
                )

        return parser

    def cleanAll(self):
        for i in range(1, 156):
            node = 'oberon%d'%i
            for path in ['/lscratch/', '/lscratch2/']:
                path = join(path, self.username())
                cmd = """ssh %s 'rm -rf %s/*'"""%(node, path)
                output = self.executeCommand(cmd, output=False)

    def utilities(self):
        if self.isRemote():
            return {'gnubands': '/home/mads/SOFTWARE/siesta-3.2-pl-5/Util/Bands/new.gnubands'}
        else:
            return {}

    def header(self, run):
        submit_type = run.submitType()
        partition = submit_type['queue']
        n_nodes = submit_type['nodes']

        if partition in ['all', 'pams']:
            days = 30
        else:
            days = 14

        if partition=='all':
            mem_pr_cpu = 2000
            cpu_pr_node = 8
        elif partition=='pams':
            mem_pr_cpu = 3900
            cpu_pr_node = 12

        ncpus = cpu_pr_node*n_nodes

        script = """#!/bin/bash
#SBATCH --nodes %d
#SBATCH --ntasks %d
#SBATCH --time %d-00:00:00
#SBATCH --mem-per-cpu %d
#SBATCH --job-name %s
#SBATCH --partition %s
#SBATCH --exclusive
#SBATCH --exclude=oberon[117,136,141]
"""%(n_nodes, ncpus, days, mem_pr_cpu, run.jobName(), partition)
        if n_nodes != 1:
            script +="#SBATCH --contiguous\n"

        return script

    def methodScript(self, run):
        runfile = os.path.basename(run.runFile())
        tmp_outfile = 'TMP.out'
        outfile = os.path.basename(run.outFile())
        method = run.method()
        program = method.program()

        script = ''

        if isinstance(program, Siesta):
            version = program.version()
            executable = run.executable()
            if executable is None:
                mess = '%s has no executable defined.' % run.name()
                raise NotImplementedError(mess)
            executable += '.optim'

            if version is None:
                version = ('release', '4.0b', 485)

            if version[0] == 'ts-scf':
                version = 'siesta/%s/%d.%d-ompi-i2015' % version
            elif version[0] == 'trunk':
                version = 'siesta/%s/%d-ompi-i2015' % version
            elif version[0] == 'release':
                version = 'siesta/%s/%s-%d-ompi-i2015' % version

            script = """# Write path to output file to slurm.out
echo OUTFILE=$TMPDIR/TMP.out

# Setup Siesta environment.
module use --append /home/pedro/local/modules
module purge
module load %s
""" % version
            script += """export OMP_NUM_THREADS=1
"""
            if 'siesta' in executable:
                script += """export SIESTA_COMMAND="mpirun %s""" % executable
                script += """ < ./%s > ./%s" """
                script += """
export SIESTA_PP_PATH=/home/engelund/SOFTWARE/CalcTroll/CalcTroll/Plugins/Programs/Siesta/pseudopotentials/UOR
export PYTHONPATH=$HOME/SOFTWARE/CalcTroll:$HOME/SOFTWARE/CalcTroll/ase
export SIESTA_UTIL_FRACTIONAL=fractional
export PROGRAM=/home/engelund/bin/python
"""
            elif 'tbtrans' in executable:
                script += """export PROGRAM=%s
""" % executable

            form = """$PROGRAM %s >& %s"""

        elif isinstance(program, Green):
            script += "# Setup environment for Green.\n"
            script += "echo OUTFILE=$TMPDIR/RUN.TMP/out\n"
            script += 'export GREEN_DIR=$HOME/SOFTWARE/Green_debug\n'
            script += "module use --append /home/pedro/local/modules\n"
            script += "module purge\n"
            script += "module load green/x-i2015\n\n"
            form = "rgreen -t 12 %s\n"
        else:
            raise Exception("%s does not know how to run %s"%(cls, program))

        for command in run.makeCommands(form):
            script += command
            script += '\n'

        return script
