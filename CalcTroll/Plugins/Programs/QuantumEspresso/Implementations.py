from CalcTroll import API
from CalcTroll.Core.Relaxation import Relaxed
from CalcTroll.Plugins.Analysis.EnergySpectrum import EnergySpectrum

from .Tasks import ASEEspressoRelaxation
from .EnergySpectrumTask import EnergySpectrumTask
from .QuantumEspresso import QuantumEspresso

class RelaxationImplementation(API.AnalysisImplementation):
    @classmethod
    def canHandle(cls, item):
        if not isinstance(item, Relaxed): return False
        if not isinstance(item.method(), QuantumEspresso): return False
        return True

    def makeTasks(self, items):
        tasks = []
        for item in items:
            if isinstance(item.system(), API.Collection):
                continue
            elif isinstance(item.system(), API.System):
                task = ASEEspressoRelaxation(item=item)
                tasks.append(task)
            else:
                eeeeeeeeeeeee

        return tasks
    

class EnergySpectrumImplementation(API.AnalysisImplementation):
    @classmethod
    def canHandle(cls, item):
        if not isinstance(item, EnergySpectrum): return False
        if not isinstance(item.method(), QuantumEspresso): return False
        if isinstance(item.system(), API.Collection): return False
        return True

    def makeTasks(self, items):
        tasks = []
        for item in items:
            if isinstance(item.system(), API.Collection):
                sub_systems = item.system().subSystems()
                for sub_system in sub_systems:
                    task = EnergySpectrumTask(relaxation_task=sub_system.tasks()[0], parameters=item.parameters())
                    tasks.append(task)
            elif isinstance(item.system(), API.System):
                task = EnergySpectrumTask(relaxation_task=item.system().tasks()[0], parameters=item.parameters())
                tasks.append(task)
            else:
                cccccccccccc

        return tasks

    def read(self, item, **kwargs):
        assert item in self.items()
        return self.tasks()[0].read(**kwargs)

class EnergySpectrumCollectionImplementation(API.AnalysisImplementation):
    @classmethod
    def canHandle(cls, item):
        if not isinstance(item, EnergySpectrum): return False
        if not isinstance(item.method(), QuantumEspresso): return False
        if not isinstance(item.system(), API.Collection): return False
        return True

    def makeTasks(self, items):
        tasks = []
        for item in items:
            if isinstance(item.system(), API.Collection):
                sub_systems = item.system().subSystems()
                for sub_system in sub_systems:
                    task = EnergySpectrumTask(relaxation_task=sub_system.tasks()[0], parameters=item.parameters())
                    tasks.append(task)
            else:
                cccccccccccc

        return tasks

    def read(self, item, **kwargs):
        assert item in self.items()
        return self.tasks()[0].read(**kwargs)