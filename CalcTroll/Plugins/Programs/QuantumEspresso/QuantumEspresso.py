# Written by Mads Engelund, 2023, http://espeem.com
import numpy as np
import os
import shutil
from os.path import join

from ase import io
from ase.units import Ry
from ase.constraints import FixAtoms
from ase.calculators.espresso import Espresso as ASEESPRESSO

from CalcTroll import API
from ase.units import Bohr, Hartree
from CalcTroll.Core.ParameterControl.ParameterControl import makeMinimalScript
from CalcTroll.ASEUtils.Atoms import Atoms
from CalcTroll.ASEUtils.Trajectory import write_traj
from CalcTroll.Plugins.Analysis.EnergySpectrum import EnergySpectrum


class QuantumEspresso(API.Program):
    def __init__(self,
            force_tolerance=0.04,
            spin='non-polarized',
            wavefunction_mesh_cutoff=30*Ry,
            density_mesh_cutoff=120*Ry,
            xc='PBE0',
            ):
        self.__nspin = {'non-polarized': 1, 'collinear': 2, 'non-collinear':4}[spin]
        self.__force_tolerance = force_tolerance
        self.__xc = xc.lower()
        self.__ase_calculator = ASEESPRESSO()

    def forceTolerance(self):
        return self.__force_tolerance

    def _calculator(self):
        return self.__ase_calculator

    def getDictionary(self):
        dictionary = dict(self._calculator().parameters)

        return dictionary

    def baseMethod(self):
        return self

    def executable(self):
        return 'pw.x'

    @classmethod
    def runFileEnding(cls):
        return 'py'

    @classmethod
    def outFileEnding(cls):
        return 'out'

    def neededFiles(self):
        return ['*.fdf', '*.py', '*.traj']

    def resultFiles(self):
        return []

    def checkIfFinished(self, filename, runfile=None):
        raise Exception

    def writeRelaxation(self,
              runfile,
              atoms,
              kpts,
              relax_cell,
              save_behaviour,
              ):
        path = os.path.dirname(runfile)
        ecutwfc = self['wavefunction_mesh_cutoff']/Ry
        ecutrho = self['density_mesh_cutoff']/Ry
        atoms = atoms.copy()
        del atoms.constraints
        for constraint in atoms.constraints:
            assert isinstance(constraint, FixAtoms)

        if self.__nspin == 1:
            atoms.set_initial_magnetic_moments(np.zeros(len(atoms)))

        atoms.purgeTags()
        symbols = np.unique(atoms.get_chemical_symbols())
        pseudo_dir = self.pseudoDir()
        pseudopotentials = [(symbol, '%s.upf' % symbol) for symbol in symbols]
        for symbol, pseudo in pseudopotentials:
            shutil.copy(os.path.join(pseudo_dir, pseudo), os.path.join(path, pseudo))

        filename = join(path, 'input.traj')
        write_traj(filename, atoms)

        c_string = 'calculator = '
        dictionary = self.getDictionary()
        dictionary['kpts'] = kpts
        dictionary['pseudopotentials'] = dict(pseudopotentials)
        dictionary['tprnfor'] = True
        dictionary['outdir'] = './SAVE'
        dictionary['pseudo_dir'] = './'
        dictionary['ecutwfc'] = ecutwfc
        dictionary['nspin'] = self.__nspin
        dictionary['ecutrho'] = ecutrho
        dictionary['input_dft'] = self.__xc
        dictionary['occupations'] = 'smearing'
        dictionary['degauss'] = 0.05
        dictionary['conv_thr']    = 1.E-8
        dictionary['mixing_beta'] = 0.7
        c_string += makeMinimalScript('Espresso', dictionary, detail_level=API.ALL) + '\n'
        path = os.path.dirname(runfile)
        eff_force_tolerance = self.forceTolerance()


        script =  """from CalcTroll.ASEUtils.Trajectory import myread_traj, write_traj, Trajectory
from ase import io
from ase.io.ulm import InvalidULMFileError
from ase.constraints import UnitCellFilter
from ase.optimize import FIRE, BFGS
from ase.calculators.espresso import Espresso
"""
        script += c_string
        script += """
try:
    print('Starting from previous geometry.')
    atoms = myread_traj('relax.traj', -1)
except (FileNotFoundError, InvalidULMFileError):
    print('Starting from scratch.')
    atoms = myread_traj('input.traj', -1)
atoms.set_calculator(calculator)
"""

        script += """
opt = BFGS(atoms)
traj = Trajectory('relax.traj', 'w', atoms)
opt.attach(traj)
opt.run(fmax=%.5f)

io.write('relax.xyz', atoms)
"""%eff_force_tolerance

        with open(runfile, 'w') as f:
            f.write(script)

    def writeInternalRelaxation(self,
              runfile,
              atoms,
              kpts,
              relax_cell,
              save_behaviour,
              ):
        path = os.path.dirname(runfile)
        ecutwfc = self['wavefunction_mesh_cutoff']/Ry
        ecutrho = self['density_mesh_cutoff']/Ry

        if len(kpts) == 2:
            kpts = list(kpts)
            kpts.append(1)

        atoms = atoms.copy()
        qe_force_unit = Hartree/Bohr
        for constraint in atoms.constraints:
            assert isinstance(constraint, FixAtoms)
        

        if self.__nspin == 1:
            atoms.set_initial_magnetic_moments(np.zeros(len(atoms)))

        atoms.purgeTags()
        symbols = np.unique(atoms.get_chemical_symbols())
        pseudo_dir = self.pseudoDir()
        pseudopotentials = [(symbol, '%s.upf' % symbol) for symbol in symbols]
        for symbol, pseudo in pseudopotentials:
            shutil.copy(os.path.join(pseudo_dir, pseudo), os.path.join(path, pseudo))

        filename = join(path, 'input.traj')
        write_traj(filename, atoms)


        c_string = 'calculator = '
        dictionary = self.getDictionary()
        if relax_cell:
            dictionary['calculation'] = 'vc-relax'
            dictionary['cell_dynamics'] = 'bfgs'
        else:
            dictionary['calculation'] = 'relax'
        dictionary['forc_conv_thr'] = self.forceTolerance()/qe_force_unit
        dictionary['kpts'] = kpts
        dictionary['pseudopotentials'] = dict(pseudopotentials)
        dictionary['tprnfor'] = True
        dictionary['outdir'] = './SAVE'
        dictionary['pseudo_dir'] = './'
        dictionary['ecutwfc'] = ecutwfc
        dictionary['nspin'] = self.__nspin
        dictionary['ecutrho'] = ecutrho
        dictionary['input_dft'] = self.__xc
        dictionary['occupations'] = 'smearing'
        dictionary['degauss'] = 0.05
        dictionary['conv_thr']    = 1.E-8
        dictionary['mixing_beta'] = 0.7

        c_string += makeMinimalScript('Espresso', dictionary, detail_level=API.ALL) + '\n'
        path = os.path.dirname(runfile)

        script =  """from CalcTroll.ASEUtils.Trajectory import myread_traj, write_traj, Trajectory
from ase import io
import subprocess
from ase.calculators.espresso import Espresso
from CalcTroll.Plugins.Programs.QuantumEspresso.Utilities import readAtomsFromOutput
"""
        script += c_string
        script += """
atoms = myread_traj('input.traj', -1)
try:
    atoms = readAtomsFromOutput(atoms, 'espresso.pwo')
except:
    print('Starting from scratch')
    atoms = myread_traj('input.traj', -1)
else:
    print('Starting from previous calculation')
"""
        script += """
calculator.write_input(atoms=atoms)
command = calculator.command.replace('PREFIX', calculator.prefix)
subprocess.run(command, shell=True)
final_atoms = readAtomsFromOutput(atoms, 'espresso.pwo')
io.write('relax.xyz', final_atoms)
write_traj('relax.traj', final_atoms)
"""

        with open(runfile, 'w') as f:
            f.write(script)

    def numberOfValenceElectrons(self, atoms, path):
        symbols = np.array(atoms.get_chemical_symbols())
        u_symbols = np.unique(symbols)
        count = dict([(symbol, (symbols==symbol).sum()) for symbol in u_symbols])
        total_valence = 0
        for symbol in u_symbols:
            filename = os.path.join(path, symbol + '.upf')
            with open(filename, 'r') as f:
                for line in f:
                    if 'z_valence' in line:
                        break

            valence = float(line.strip().split('"')[1].strip())
            total_valence += valence * count[symbol]

        return total_valence

    def pseudoDir(self):
        base_dir = os.path.join(os.path.dirname(__file__), 'pseudopotentials')
        if self.__xc in ['pbe', 'pbe0']:
            directory = os.path.join(base_dir, 'PseudoDojo/stringent')
        else:
            raise Exception

        return directory

    def writeOneShot(self,
            runfile,
            atoms,
            kpts,
            band_multiplier,
            save_behaviour={'wavefunctions':False},
            ):
        if len(kpts) == 2:
            kpts = list(kpts) + [1]
        path = os.path.dirname(runfile)
        ecutwfc = self['wavefunction_mesh_cutoff']/Ry
        ecutrho = self['density_mesh_cutoff']/Ry
        atoms = atoms.copy()
        del atoms.constraints
        if self.__nspin == 1:
            atoms.set_initial_magnetic_moments(np.zeros(len(atoms)))

        atoms.purgeTags()
        symbols = np.unique(atoms.get_chemical_symbols())
        pseudo_dir = self.pseudoDir()
        pseudopotentials = [(symbol, '%s.upf' % symbol) for symbol in symbols]
        for symbol, pseudo in pseudopotentials:
            shutil.copy(os.path.join(pseudo_dir, pseudo), os.path.join(path, pseudo))

        filename = join(path, 'input.traj')
        write_traj(filename, atoms)

        c_string = 'calculator = '
        dictionary = self.getDictionary()
        dictionary['kpts'] = kpts
        dictionary['pseudopotentials'] = dict(pseudopotentials)
        dictionary['tprnfor'] = True
        dictionary['outdir'] = './SAVE'
        dictionary['pseudo_dir'] = './'
        dictionary['ecutwfc'] = ecutwfc
        dictionary['nspin'] = self.__nspin
        dictionary['ecutrho'] = ecutrho
        dictionary['input_dft'] = self.__xc
        dictionary['occupations'] = 'smearing'
        dictionary['degauss'] = 0.05
        dictionary['conv_thr']    = 1.E-8
        dictionary['mixing_beta'] = 0.7
        nbnd = band_multiplier * self.numberOfValenceElectrons(atoms, path)/2
        dictionary['nbnd'] = int(np.ceil(nbnd))

        if save_behaviour['wavefunctions']:
            dictionary['wf_collect'] = True

        c_string += makeMinimalScript('Espresso', dictionary, detail_level=API.ALL) + '\n'
        path = os.path.dirname(runfile)


        script =  """from CalcTroll.ASEUtils.Trajectory import myread_traj, write_traj, Trajectory
from ase import io
from ase.calculators.espresso import Espresso
"""
        script += c_string
        script += """
atoms = myread_traj('input.traj', -1)
atoms.set_calculator(calculator)
atoms.get_total_energy()
"""

        with open(runfile, 'w') as f:
            f.write(script)
