import os
from os.path import join
from ase import io
from ase.constraints import FixAtoms
from ase.io.trajectory import Trajectory
from CalcTroll import API

from .Utilities import time


class ASEEspressoRelaxation(API.RelaxationTask):
    def __init__(
            self,
            item,
            ):

        API.RelaxationTask.__init__(
                self,
                item,
                )

    def explain(self):
        return API.Explanation(text, citations)

    def estimate(self):
        return effort*relaxation_steps

    def executable(self):
        return 'pw.x'

    def numberOfValenceElectrons(self):
        return self.system().fullyUnrelaxed().atoms().number_of_valence_electrons()

    def environmentCommands(self):
        runfile = os.path.basename(self.runFile())
        tmp_outfile = 'TMP.out'
        outfile = os.path.basename(self.outFile())
        cmds = []

        method = self.method()
        program = method.program()

        cmd = "export PROGRAM=python"
        cmds.append(cmd)

        return cmds

    def makeCommands(self, form):
        commands = self.environmentCommands()
        commands.append(form%(self.runFile(path=API.TAIL), 'TMP.out'))
        commands.append('mv TMP.out %s'%self.outFile(path=API.TAIL))
        commands.append('mv espresso.pwo %s' % self.espressoOutFile(path=API.TAIL))

        return commands

    def inputAtoms(self):
        atoms = self.system().atoms(parameters=self.parameters(), relaxed=False, initialized=True, constrained=True)

        return atoms

    def relaxation(self):
        if not self.isDone():
            return None

        i_filename = join(self.runDirectory(), 'input.traj')
        r_filename = join(self.runDirectory(), 'relax.traj')
        if not os.path.exists(r_filename):
            raise Exception("File %s missing" % r_filename)

        relaxation = io.read(i_filename, index=0), io.read(r_filename, index=-1)

        return relaxation

    def hasOpenBoundaries(self):
        return self.inputAtoms().hasOpenBoundaries()

    def _getLocalStatus(self):
        outfile = self.outFile(path=API.LOCAL)
        if not os.path.exists(outfile):
            return API.NOT_STARTED

        with open(outfile, 'r') as f:
             output = f.read()

        if 'RuntimeError:' in output:
            return API.FAILED
        else:
            return API.DONE

    def canDoInternalRelaxation(self, atoms):
        for constraint in atoms.constraints:
            if not isinstance(constraint, FixAtoms):
                return False
        return True

    def write(self):
        system = self.system()
        atoms = self.inputAtoms()
        kpts = system.parameters().kpts()
        method = self.method()
        io.write(join(self.runDirectory(), 'input.xyz'), atoms)

        if self.canDoInternalRelaxation(atoms):
            method.writeInternalRelaxation(
                runfile=self.runFile(),
                atoms=atoms,
                kpts=kpts,
                relax_cell=system.relaxCell(),
                save_behaviour=self.saveBehaviour(),
                )
        else:
            method.writeRelaxation(
                runfile=self.runFile(),
                atoms=atoms,
                kpts=kpts,
                relax_cell=system.relaxCell(),
                save_behaviour=self.saveBehaviour(),
                )

    def saveBehaviour(self):
        return {'hamiltonian': False, 'density_matrix':False}

    def espressoOutFile(self, path=API.TAIL):
        return join(self.runDirectory(path=path), 'relaxation.espresso.pwo')

    def neededFiles(self):
        return self.method().neededFiles()

    def resultFiles(self):
        return ['*.out']

    def _getLocalStatus(self):
        run_dir = self.runDirectory(path=API.LOCAL)
        outfile = self.outFile(path=API.LOCAL)
        if not os.path.exists(outfile):
            return API.NOT_STARTED

        if not os.path.exists(join(run_dir, 'relax.xyz')):
            return API.FAILED  

        if not os.path.exists(join(run_dir, 'relax.traj')):
            return API.FAILED           
    
        if not os.path.exists(join(run_dir, 'relaxation.espresso.pwo')):
            return API.FAILED 
        
        return API.DONE

    def time(self):
        outfile = self.espressoOutFile(path=API.LOCAL)

        return time(outfile)
