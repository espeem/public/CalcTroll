from datetime import datetime
import numpy as np

def time(outfile):
    start_ident = 'starts on '
    end_ident = 'was terminated on:'
    start_line = None
    end_line = None
    time_format = "%H:%M:%S  %d%b%Y"

    # Convert string to datetime object
    with open(outfile, 'r') as f:

        for line in f:
            if start_ident in line:
                start_line = line.strip()
                break

        for line in f:
            if 'was terminated on' in line:
                end_line = line.strip()
                break

    if start_line == None or end_line == None:
        raise ValueError

    time_str = start_line.split(start_ident)[1].strip()
    date, hours = time_str.split(' at ')
    hours = hours.replace(' ', '0')
    start_time = datetime.strptime(hours + '  ' + date, time_format)
    time_str = end_line.split(end_ident)[1].strip()

    hours, date = time_str.split('  ')
    hours = hours.replace(' ', '0')
    end_time = datetime.strptime(hours + '  ' + date, time_format)

    return end_time - start_time

def readAtomsFromOutput(atoms, filename):
    atoms = atoms.copy()
    cell_lines = None
    cell_start_found = False
    position_start_found = False
    with open(filename, 'r') as f:
        for line in f:
            if not cell_start_found:
                cell_start_found = 'CELL_PARAMETERS' in line
                if cell_start_found:
                    cell_lines = []
            else:
                line = line.strip()
                if line == '' or 'ATOMIC_POSITIONS' in line:
                    cell_start_found = False
                else:
                    cell_lines.append(line)

            if not position_start_found:
                position_start_found = 'ATOMIC_POSITIONS' in line
                if position_start_found:
                    position_lines = []
            else:
                line = line.strip()
                if line == '' or 'End final coordinates' in line:
                    position_start_found = False
                else:
                    position_lines.append(line.strip())

    if cell_lines is not None:
        cell = np.zeros((3, 3), float)
        for i in range(3):
            cell[i] = np.array(cell_lines[i].split(), float)
        atoms.set_cell(cell)

    positions = np.zeros((len(position_lines), 3), float)
    for i, line in enumerate(position_lines):
        symbol, x, y, z = line.split()[:4]
        print(symbol)
        assert atoms[i].symbol in symbol
        positions[i] = np.array([x, y, z])
    
    atoms.set_positions(positions)

    return atoms


def readEigenvalues(filename, read_bandnumbers=False):
    with open(filename, 'r') as f:
         lines = f.readlines()
    for i, line in enumerate(lines):
        if "reciprocal axes:" in line:
            break
    lines = lines[i+1:]

    reciprocal = np.zeros((3, 3), float)
    for i in range(3):
        line = lines[i].strip()
        reciprocal[:, i] = np.array(line.split()[-4:-1], float)
    lines = lines[i+1:]
    reciprocal = np.array(np.matrix(reciprocal)**(-1))
    
    for i, line in enumerate(lines):
        if "number of k points=" in line:
            break
    lines = lines[i+2:]
    n_kpts = int(line.split('=')[1].split()[0])
    
    kpts = np.zeros((n_kpts, 3))
    wk = np.zeros(tuple([n_kpts]))
    for i in range(n_kpts):
        line = lines[i].strip()
        line = line.split('=')
        kpts[i] = np.array(line[1].split(')')[0].split('(')[1].split(), float)
        wk[i] = float(line[2])
    lines = lines[i+2:]
    kpts = np.tensordot(reciprocal, kpts, (1, 1)).T

    for i, line in enumerate(lines):
        if "End of self-consistent calculation" in line or \
           "End of band structure calculation" in line:
            break
    lines = lines[i+1:]
        
    eigs = []
    r_kpts = []
    n_spins = 1

    for line in lines:
        line = line.strip()
        if line == '':
            continue

        if line[0] == 'k':
            line = line.split('=')[1].split('(')[0].strip()
            line = ' -'.join(line.split('-'))
            k = np.array(line.split(), float)
            r_kpts.append(k)
            continue

        if 'the Fermi energy is' in line:
            EF = float(line.split(' ')[-2])
            break

        line = line.split()
        if line[1] == 'SPIN':
            n_spins = 2
            continue

        for item in line:
            try:
                value = float(item)
            except ValueError:
                items = item.split('-')[1:]
                values = [-float(item) for item in items]
                eigs.extend(values)
            else:
                eigs.append(value)


    if n_spins == 2:
        assert len(r_kpts) == 2*len(kpts)
    elif n_spins == 1:
        assert len(r_kpts) == len(kpts)
        wk /= 2

    eigs = np.array(eigs, float)  
    eigs = np.reshape(eigs, (n_spins, len(kpts), -1))  
    eigs = np.transpose(eigs, (1, 2, 0))
    sh = eigs.shape

    if read_bandnumbers:
        return EF, kpts, wk, eigs, np.arange(sh[1])
    else:
        return EF, kpts, wk, eigs