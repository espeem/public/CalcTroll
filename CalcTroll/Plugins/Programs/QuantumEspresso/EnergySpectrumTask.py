import os
from os.path import join
from ase import io
from CalcTroll import API

from CalcTroll.Core.Utilities import generateHash
from .Utilities import time, readEigenvalues

class EnergySpectrumTask(API.Task):
    def __init__(
            self,
            relaxation_task,
            parameters,
            ):
        self.__r_task = relaxation_task
        self.__parameters = parameters
        path = relaxation_task.runDirectory()

        API.Task.__init__(
                self,
                path=path,
                method=relaxation_task.method(),
                inputs=[relaxation_task])

    def generateHash(self):
        h = super().generateHash()
        # print(h)
        # print(self.script(detail_level=API.ALL))
        return h
    
    def parameters(self):
        return self.__parameters

    def system(self):
        return self.__r_task.system()

    def explain(self):
        return API.Explanation(text, citations)

    def estimate(self):
        return effort*relaxation_steps


    def executable(self):
        return 'pw.x'

    def numberOfValenceElectrons(self):
        return self.system().fullyUnrelaxed().atoms().number_of_valence_electrons()

    def makeCommands(self, form):
        commands = ["export PROGRAM=python"]
        commands.append(form%(self.runFile(path=API.TAIL), 'TMP.out'))
        commands.append('mv TMP.out %s'%self.outFile(path=API.TAIL))
        commands.append('mv espresso.pwo %s' % self.espressoOutFile(path=API.TAIL))

        return commands

    def inputAtoms(self):
        atoms = self.system().atoms(parameters=self.parameters(), relaxed=True, initialized=True, constrained=True)
        atoms.positions -= atoms.get_celldisp()

        return atoms

    def hasOpenBoundaries(self):
        return self.inputAtoms().hasOpenBoundaries()

    def _getLocalStatus(self):
        outfile = self.outFile(path=API.LOCAL)
        if not os.path.exists(outfile):
            return API.NOT_STARTED

        with open(outfile, 'r') as f:
             output = f.read()

        if 'RuntimeError:' in output:
            return API.FAILED
        else:
            return API.DONE


    def write(self):
        system = self.system()
        atoms = self.inputAtoms()
        kpts = system.parameters().kpts()
        method = self.method()
        io.write(join(self.runDirectory(), 'input.xyz'), atoms)

        method.writeOneShot(
                runfile=self.runFile(),
                atoms=atoms,
                kpts=kpts,
                band_multiplier=2,
                )

    def espressoOutFile(self, path=API.TAIL):
        return join(self.runDirectory(path=path), 'analysis.espresso.pwo')

    def neededFiles(self):
        return self.method().neededFiles()

    def resultFiles(self):
        return ['*.out']

    def time(self):
        outfile = self.espressoOutFile(path=API.LOCAL)

        return time(outfile)
    
    def read(self, read_bandnumbers=False):
        outfile = self.espressoOutFile(path=API.LOCAL)

        return readEigenvalues(outfile, read_bandnumbers=read_bandnumbers)






