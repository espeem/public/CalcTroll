import os
import sys
from os.path import join
from ase import io
from CalcTroll.Core.Utilities import generateHash

from CalcTroll import API
from CalcTroll.Plugins.Programs.QuantumEspresso.Utilities import time, readEigenvalues


class SaveWavefunctions(API.Task):
    def __init__(
            self,
            item,
            relax_task,
            ):
        self.__r_task = relax_task
        self.__item = item
        path = relax_task.runDirectory()

        API.Task.__init__(
                self,
                path=path,
                method=item.method(),
                inputs=[relax_task])

    def time(self):
        outfile = self.espressoOutFile(path=API.LOCAL)

        return time(outfile)

    def directoryBaseName(self):
        name = self.className()
        m = self.method()
        max_multiplier = max(m['dielectric_band_number_multiplier'], m['selfenergy_band_number_multiplier'])
        h = generateHash('%2.2f_' % max_multiplier + self.parameters().script())
        path = "%s_%s" % (name, h)

        return path

    def system(self):
        return self.__item.system()

    def parameters(self):
        return self.__item.parameters()

    def explain(self):
        return API.Explanation(text, citations)

    def executable(self):
        return 'pw.x'

    def environmentCommands(self):
        runfile = os.path.basename(self.runFile())
        tmp_outfile = 'TMP.out'
        outfile = os.path.basename(self.outFile())
        cmds = []

        method = self.method()
        program = method.program()

        python_executable = sys.executable
        executable = self.executable()

        cmd = "export PROGRAM=python"
        cmds.append(cmd)

        return cmds

    def makeCommands(self, form):
        commands = self.environmentCommands()
        commands.append(form%(self.runFile(path=API.TAIL), 'TMP.out'))
        commands.append('mv TMP.out %s'%self.outFile(path=API.TAIL))
        commands.append('mv espresso.pwo %s' % self.espressoOutFile(path=API.TAIL))

        return commands

    def saveBehaviour(self):
        return {'wavefunctions': True}

    def inputAtoms(self):
        atoms = self.system().atoms(parameters=self.parameters(), relaxed=True, initialized=True, constrained=True)
        atoms.positions -= atoms.get_celldisp()

        return atoms

    def _getLocalStatus(self):
        outfile = self.outFile(path=API.LOCAL)
        if not os.path.exists(outfile):
            return API.NOT_STARTED

        with open(outfile, 'r') as f:
             output = f.read()

        if 'RuntimeError:' in output:
            return API.FAILED
        else:
            return API.DONE

    def numberOfValenceElectrons(self):
        path = self.runDirectory()
        atoms = self.inputAtoms()

        return self.method().numberOfValenceElectrons(atoms, path)


    def write(self):
        system = self.system()
        atoms = self.inputAtoms()
        kpts = system.parameters().kpts()

        method = self.method()
        dielectric_multiplier = method['dielectric_band_number_multiplier']
        selfenergy_multiplier = method['selfenergy_band_number_multiplier']
        band_multiplier = max(dielectric_multiplier, selfenergy_multiplier)
        io.write(join(self.runDirectory(), 'input.xyz'), atoms)

        method.baseMethod().writeOneShot(
                runfile=self.runFile(),
                atoms=atoms,
                kpts=kpts,
                band_multiplier=band_multiplier,
                save_behaviour=self.saveBehaviour(),
                )

    def readEigenvalues(self, path=API.LOCAL):
        return readEigenvalues(self.espressoOutFile(path=path))

    def espressoOutFile(self, path=API.TAIL):
        return join(self.runDirectory(path=path), 'analysis.espresso.pwo')

    def neededFiles(self):
        return self.method().neededFiles()

    def resultFiles(self):
        return ['*.out']

    def _getLocalStatus(self):
        outfile = self.outFile(path=API.LOCAL)
        if not os.path.exists(outfile):
            return API.NOT_STARTED

        outfile = self.espressoOutFile(path=API.LOCAL)
        if not os.path.exists(outfile):
            return API.FAILED

        with open(outfile, 'r') as f:
             output = f.readlines()

        for line in output:
            if 'JOB DONE' in line:
                return API.DONE
        
        return API.FAILED
