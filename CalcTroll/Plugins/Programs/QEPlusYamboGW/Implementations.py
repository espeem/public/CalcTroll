from CalcTroll import API
from CalcTroll.Plugins.Analysis.EnergySpectrum import EnergySpectrum
from CalcTroll.Plugins.Programs.QuantumEspresso.Implementations import RelaxationImplementation as QERelaxImp

from .SaveWavefunctions import SaveWavefunctions
from .EnergySpectrumTask import EnergySpectrumTask
from .QEPlusYamboGW import QEPlusYamboGW

class RelaxationImplementation(QERelaxImp):
    @classmethod
    def canHandle(cls, item):
        if not isinstance(item, API.Relaxed): return False
        if not isinstance(item.method(), QEPlusYamboGW): return False
        base_item = item.copy(method=item.method().baseMethod())
        return QERelaxImp.canHandle(base_item)

    def makeTasks(self, items):
        method = items[0].method()
        for item in items[1:]:
            assert method == item.method()
        base_method = method.baseMethod()
        items = [item.copy(method=base_method) for item in items]

        return QERelaxImp.makeTasks(self, items=items)


class EnergySpectrumImplementation(API.AnalysisImplementation):
    @classmethod
    def canHandle(cls, item):
        if not isinstance(item.method(), QEPlusYamboGW): return False
        if not isinstance(item, EnergySpectrum): return False
        
        return True    

    def makeTasks(self, items):
        tasks = []
        for item in items:
            relax_task = item.system().mainTask()
            task = SaveWavefunctions(item=item, relax_task=relax_task)
            tasks.append(task)
            task = EnergySpectrumTask(wf_task=task)
            tasks.append(task)

        return tasks

    def yamboTask(self):
        return self.tasks()[-1]

    def read(self, item, **kwargs):
        return self.yamboTask().read(**kwargs)
