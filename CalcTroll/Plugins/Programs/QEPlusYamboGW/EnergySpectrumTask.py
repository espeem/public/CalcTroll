import os
import re
import numpy as np
import subprocess
import sys
import shutil

from datetime import timedelta
from os.path import join
from ase import io
from ase.units import Ry, Bohr
from CalcTroll import API
from CalcTroll.Core.Utilities import generateHash, vectorLength
from .Utilities import parse_fermi_level, parse_band_data, readEnergyLevel


class EnergySpectrumTask(API.Task):
    def __init__(
            self,
            wf_task,
            e_range=(-10, 10),
            ):
        self.__wf_task = wf_task
        path = wf_task.runDirectory()
        self.__e_range = e_range
        self.__hash = None
        self.__bands = None

        API.Task.__init__(
                self,
                path=path,
                method=wf_task.method(),
                inputs=[wf_task])


    def hash(self):
        if self.__hash == None:
            self.__hash = generateHash(self.script())

        return self.__hash

    def wfTask(self):
        return self.__wf_task

    def runFileBasename(self):
        name = "%s_%s" % (self.className(), self.hash())
        return name + '.py'

    def outFileBasename(self):
        name = "%s_%s" % (self.className(), self.hash())
        return name + '.out'

    def directoryBaseName(self):
        return 'GW'

    def system(self):
        return self.wfTask().system()

    def parameters(self):
        return self.wfTask().parameters()

    def explain(self):
        return API.Explanation(text, citations)

    def executable(self):
        return 'yambo'

    def time(self):
        outfile = self.outFile(path=API.LOCAL)
        ident = '[Time-Profile]:'
        p_line = None

        # Convert string to datetime object
        with open(outfile, 'r') as f:
            for line in f:
                if ident in line:
                    p_line = line.strip()
                    break

        if p_line == None:
            raise ValueError

        time_difference_str = p_line.split(ident)[1].strip()
        if time_difference_str[-1] == 's':
            minutes, seconds = map(int, re.findall(r'\d+', time_difference_str))
            hours = 0
        elif time_difference_str[-1] == 'm':
            hours, minutes = map(int, re.findall(r'\d+', time_difference_str))
            seconds = 0

        time = timedelta(hours=hours, minutes=minutes, seconds=seconds)

        return time

    def makeCommands(self, form):
        indicator = self.hash()
        runfile = self.runFile(API.TAIL)
        outfile = self.outFile(API.TAIL)
        commands = []
        commands.append('export PROGRAM=python')
        commands.append('$PROGRAM %s' % runfile)
        commands.append('mv r-%s_HF_and_locXC_gw0_dyson_rim_cut_em1d_ppa_el_el_corr %s' % (indicator, outfile))

        return commands

    def numberOfValenceElectrons(self):
        return self.wfTask().numberOfValenceElectrons()
    
    def __determineBands(self):
        EF, kpts, wk, eigs = self.wfTask().readEigenvalues()
        e_min, e_max = self.__e_range
        take = np.logical_and(eigs > e_min + EF, eigs < e_max + EF)
        take = take.sum(axis=(0,2)) > 0
        bands = np.arange(len(take))[take]
        start, end = bands[0], bands[-1]

        return start, end

    def bandsToCalculate(self):
        if self.__bands == None:
            self.__bands = self.__determineBands()
        return self.__bands

    def inputAtoms(self):
        return self.wfTask().inputAtoms()
    
    def write(self):
        atoms = self.inputAtoms()
        max_length = vectorLength(atoms.cell, axis=1).max()
        indicator = self.hash()
        method = self.method()
        n_valence = self.numberOfValenceElectrons()
        start, end = self.bandsToCalculate()
        save_files1 = join(self.wfTask().runDirectory(), 'SAVE/pwscf.save/')
        save_files2 = self.runDirectory()

        nbands = int(n_valence/2)
        EXXXRLvcs = method['gw_exchange_cutoff']/Ry
        NGsBlkXp = method['gw_response_cutoff']/Ry
        BndsRnXp = nbands * method['dielectric_band_number_multiplier']
        GbndRnge = nbands * method['selfenergy_band_number_multiplier']
        replace_dict = {
                "EXXRLvcs" : "%.2f Ry" % EXXXRLvcs,
                "NGsBlkXp" :  "%.2f Ry" % NGsBlkXp,
                "BndsRnXp" :  "1|%d|" % BndsRnXp,
                "GbndRnge" :  "1|%d|" % GbndRnge,
                "SCEtresh": "0.005 eV",
                "CUTGeo"   :  '"sphere xyz"',
                "CUTRadius":  (max_length/2)/Bohr,
                "QPkrange" :  "|1|1|%d|%d|" % (start+1, end+1),
                }
        
        os.chdir(self.runDirectory())
        runfile = self.runFile(API.TAIL)
        save_files1 = join(self.__wf_task.runDirectory(), 'SAVE/pwscf.save/')
        save_files2 = self.runDirectory()
        shutil.copytree(save_files1, save_files2, dirs_exist_ok=True)

        exefile = runfile.split('.')[0] + '.in' 
        creation_command = "yambo -c -Q -X p -g n -p p -V qp -F %s" % exefile
        
        script = """
import subprocess
from CalcTroll.Plugins.Programs.QEPlusYamboGW.Utilities import replace_values
subprocess.run("p2y", shell=True, stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL)
subprocess.run("yambo", shell=True, stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL)
subprocess.run("%s", shell=True, stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL)
""" % creation_command

        script += """
replace_dict = %s
""" % (replace_dict)


        script += """
with open("%s", 'r') as f:
    script = f.read()
""" % exefile

        script += """
script = replace_values(script, replace_dict)
with open("%s", 'w') as f:
    f.write(script)
""" % exefile

        script += """
subprocess.run("$YAMBO_EXECUTION_COMMAND  -F %s -J %s > /dev/null 2>&1", shell=True, stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL)
        """ % (exefile, indicator)
        with open(runfile, 'w') as f:
            f.write(script)


    def neededFiles(self):
        return self.method().neededFiles()

    def resultFiles(self):
        return ['*.out']

    def _getLocalStatus(self):
        outfile = self.outFile(path=API.LOCAL)
        if not os.path.exists(outfile):
            return API.NOT_STARTED

        with open(outfile, 'r') as f:
            lines = f.readlines()
        lines.reverse()

        for line in lines:
            if 'Game summary' in line:
                return API.DONE

        return API.FAILED

    def read(self, read_bandnumbers=False):
        outfile = self.outFile(path=API.LOCAL)

        return readEnergyLevel(outfile=outfile, read_bandnumbers=read_bandnumbers)