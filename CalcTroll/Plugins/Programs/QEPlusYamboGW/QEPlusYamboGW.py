# Written by Mads Engelund, 2023, http://espeem.com
import numpy as np
import os
import re
import sys
from collections import OrderedDict
from os.path import join

from ase import io
from ase.units import Ry
from ase.calculators.calculator import all_changes
from ase.calculators.espresso import Espresso as ASEESPRESSO

from CalcTroll import API
from CalcTroll.API import DEFAULT, ALL, DONE, NOT_CONVERGED, FAILED, LOCAL
from CalcTroll.API import TAIL
from CalcTroll.Core.ParameterControl.ParameterControl import makeMinimalScript
from CalcTroll.Core.Utilities import vectorLength
from CalcTroll.ASEUtils.AtomUtilities import areAtomsEqual
from CalcTroll.ASEUtils.Atoms import Atoms
from CalcTroll.ASEUtils.Trajectory import write_traj

from CalcTroll.Plugins.Analysis.EnergySpectrum import EnergySpectrum
from CalcTroll.Plugins.Programs.QuantumEspresso import QuantumEspresso


class QEPlusYamboGW(API.Program):
    def __init__(self,
            spin='non-polarized',
            force_tolerance=0.04,
            wavefunction_mesh_cutoff=30*Ry,
            density_mesh_cutoff=120*Ry,
            gw_exchange_cutoff=10*Ry,
            gw_response_cutoff=1*Ry,
            dielectric_band_number_multiplier=2,
            selfenergy_band_number_multiplier=2,
            **kwargs):


        self.__base_method = QuantumEspresso(
                spin=spin,
                force_tolerance=force_tolerance,
                density_mesh_cutoff=density_mesh_cutoff,
                wavefunction_mesh_cutoff=wavefunction_mesh_cutoff,
                **kwargs)

    def baseMethod(self):
        return self.__base_method

    def getDictionary(self):
        dictionary = dict(self._calculator().parameters)

        return dictionary

    def executable(self):
        return 'pw.x'

    @classmethod
    def runFileEnding(cls):
        return 'py'

    @classmethod
    def outFileEnding(cls):
        return 'out'

    def neededFiles(self):
        return ['*.fdf', '*.py', '*.traj']

    def resultFiles(self):
        return []

    def checkIfFinished(self, filename, runfile=None):
        raise Exception

    def relaxedAtoms(self, runpath, pbc=None):
        filename = join(runpath, 'relax.traj')
        if not os.path.exists(filename):
            raise Exception("File %s missing" % filename)

        atoms = io.read(filename + '@-1')
        atoms = Atoms(atoms)

        return atoms

    def numberOfValenceElectrons(self, atoms, path):
        return self.baseMethod().numberOfValenceElectrons(atoms, path)

