import re
import numpy as np

P2 = [2, 4, 8, 16, 32, 64, 128, 256, 562, 1124, 2248]

def appropriateNodes(ask, target):
    assert target in P2
    if ask <= target:
        return ask in P2
    else:
        return ask//target == 0

def replace_values(text, replace_dict):
    lines = text.split('\n')
    blocks = []
    current_block = None
    replaced = []
    for i, line in enumerate(lines):
        stripped_line = line.strip()
        if stripped_line == '':
            continue
        elif stripped_line[0] == '#':
            continue
        elif stripped_line == "%":
            if current_block is not None:
                blocks.append((current_block, start_line, i))
                current_block = None
        elif '=' in line:
           key = line.split('=')[0].strip()
           if key in replace_dict:
               if key not in line:
                   raise ValueError(f"Key {key} not present in file.")
               lines[i] = line.replace(line, f"{key} = {replace_dict[key]}")
               replaced.append(key)
        elif stripped_line[0] == "%":
            test_line = stripped_line.split()
            first = test_line[0]
            if first == '%':
                key = test_line[1]
            else:
                key = first[1:]

            current_block = key
            start_line = i
        else:
            pass

    for key, start, end in blocks:
        if key in replace_dict:
            lines[start + 1:end] = [f"{replace_dict[key]}"]
            replaced.append(key)

    if not set(replaced) == set(replace_dict.keys()):
        print(replaced)
        print(replace_dict.keys())
        eeeeeeeeeeeeeee

    return '\n'.join(lines)


# Define a function to parse band data
def parse_band_data(line):
    # Regular expression pattern to match the band data
    pattern = r"B=\s*(\d+)\s+Eo=\s*([-+]?\d+\.\d+)\s+E=\s*([-+]?\d+\.\d+)"
    match = re.search(pattern, line)

    if match:
        band = int(match.group(1))
        Eo = float(match.group(2))
        E = float(match.group(3))

        return band, Eo, E
    else:
        return None

def parse_fermi_level(line):
    # Regular expression pattern to match the Fermi level
    pattern = r"Fermi Level\s*:\s*([-+]?\d+\.\d+)"
    match = re.search(pattern, line)

    if match:
        fermi_level = float(match.group(1))
        return fermi_level
    else:
        return None

def readEnergyLevel(outfile, read_bandnumbers):
    with open(outfile, 'r') as f:
        parse = parse_fermi_level
        for line in f:
            result = parse(line)
            if result != None :
                EF = result
                break

        some_found = False
        band_data = []
        for line in f:
            result = parse_band_data(line)
            if result != None:
                band_data.append(result)
                some_found = True
            elif some_found:
                break

    band_data = np.array(band_data)
    band_numbers = band_data[:, 0] - 1
   
    old_energies = band_data[:, 1]
    n1 = (old_energies < 0).sum() - 1
    n2 = n1 + 1
    e1, e2 = old_energies[n1], old_energies[n2]

    e_diff = abs(e2 - e1)
    w1, w2 = abs(e2)/e_diff, abs(e1)/e_diff

    new_energies = band_data[:, 2]
    diff = new_energies[:-1] - new_energies[1:]
    take = np.logical_and(abs(diff) > 10, band_numbers[:-1] > 1.5*band_numbers[n2])
    indices = np.arange(len(diff))[take]
    if len(indices) > 0:
        new_energies = new_energies[:indices[0] + 1]
        band_numbers = band_numbers[:indices[0] + 1]

    s_new_energies = sorted(new_energies)
    new_mid = w1*s_new_energies[n1] + w2*s_new_energies[n2]
    new_energies -= new_mid
    new_energies += EF

    # Hack since we cannont handle k-points yet.
    nk = 1
    kpts = [[0., 0., 0.]]
    wk = [1.]
    nbands = len(new_energies)
    nspins = 1
    eigs = np.reshape(new_energies, (nk, nbands, nspins))

    if read_bandnumbers:
        return EF, kpts, wk, eigs, band_numbers
    else:
        return EF, kpts, wk, eigs

