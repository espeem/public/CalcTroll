# Written by Mads Engelund, 2017, http://espeem.com
# Functions for reading with SIESTA output files and writing input files.

from ase.constraints import FixAtoms
import os
from os.path import join
import numpy as np
from scipy.io import netcdf_file
import gzip
import string
import time
from ase.atoms import Atoms, Atom
from ase.units import Bohr, Rydberg
from CalcTroll.Core.Utilities import vectorLength

def readTimeUsage(filename):
    start = None
    end = None
    with open(filename, 'r') as f:
        for line in f:
            if ">>" in line:
                b, line = line.split(':  ')
                line = line.strip()
                t = time.strptime(line, "%d-%b-%Y  %H:%M:%S")
                if b == ">> Start of run":
                    start = t
                elif b == ">> End of run":
                    end = t

    diff = time.mktime(end) - time.mktime(start)

    return diff



def readTotalEnergy(filename):
    # Find total energy from SIESTA stdout file
    with open(filename,'r') as f:
        lines = f.readlines()

    E = 0.0
    for line in lines:
        words = line.split()
        if 'Total' in words and '=' in words:
            E = float(words[-1])
            break
    return E

def readKpoints(label='./siesta'):
    filname = label + '.KP'
    with open(filname, 'r') as f:
        n_kpts = int(f.readline().strip())
        kpts = []
        weights = []
        for line in f:
            line = line.strip().split()
            kpt = np.array(list(map(float, line[1:4])))
            kpt *= (1/Bohr)
            weight =  float(line[4])
            kpts.append(kpt)
            weights.append(weight)

    return kpts, weights

def extractMullikenCharge(filename):
    read = False
    spin_index = 0
    values = None

    with open(filename, 'r') as f:
        for line in f:
            if read:
                line = line.strip()
                inputs = line.split()
                try:
                    index = int(inputs[0])
                except (IndexError, ValueError):
                    if 'siesta' in line:
                        read = False
                    elif line=='mulliken: Spin UP':
                        spin_index = 0
                    elif line=='mulliken: Spin DOWN':
                        spin_index = 1
                    elif line == '':
                        pass
                    elif 'Species' in line:
                        pass
                    elif 'Qatom' in line:
                        pass
                    elif 's' in line:
                        pass
                    elif 'p' in line:
                        pass
                    elif 'S' in line:
                        pass
                    elif 'P' in line:
                        pass
                    elif 'mulliken: Qtot' in line:
                        pass
                    else:
                        try:
                            float(inputs[0])
                        except ValueError:
                            read=False
                else:
                    index -= 1
                    value = float(inputs[1])
                    values[index, spin_index]  = value

            else:
                if 'initatomlists: Number of atoms' in line:
                    n_atoms = int(line.split()[-3])
                if 'Number of spin components' in line:
                    n_spins = int(line.split()[-1])
                if 'Atomic and Orbital Populations' in line:
                    read = True
                    if values is None:
                        values = np.zeros((n_atoms, n_spins), float)

    return values

def extractVoronoiCharge(filename):
    read = False
    values = []

    with open(filename, 'r') as f:
        for line in f:
            if read:
                inputs = line.split()
                try:
                    index = int(inputs[0])
                except (IndexError, ValueError):
                    if 'Atom #' in line:
                        pass
                    else:
                        read = False
                else:
                    values.append(float(inputs[1]))
            else:
                if 'Voronoi Net Atomic Populations:' in line:
                    read = True

    return np.array(values)

def extractHirshfeldCharge(filename):
    read = False
    values = []

    with open(filename, 'r') as f:
        for line in f:
            if read:
                inputs = line.split()
                try:
                    index = int(inputs[0])
                except (IndexError, ValueError):
                    if 'Atom #' in line:
                        pass
                    else:
                        read = False
                else:
                    values.append(float(inputs[1]))
            else:
                if 'Hirshfeld Net Atomic Populations:' in line:
                    read = True

    return np.array(values)

def readBandGap(siesta_bands):
    E_F = getFermiEnergy(siesta_bands)
    values = []
    with open(siesta_bands, 'r') as f:
        ticks = []
        tick_labels = []
        f.readline()
        f.readline()
        f.readline()
        f.readline()
        for line in f:
            line = line.strip().split()
            try:
                line = list(map(float, line))
            except ValueError:
                continue
            else:
                values.extend(line)

    values = np.array(values)

    valence = values[values < E_F].max()
    conduction = values[values > E_F].min()

    return valence, conduction

def readValenceBandEdge(siesta_bands):
    E_F = getFermiEnergy(siesta_bands)
    values = []
    with open(siesta_bands, 'r') as f:
        ticks = []
        tick_labels = []
        f.readline()
        f.readline()
        f.readline()
        f.readline()
        for line in f:
            line = line.strip().split()
            try:
                line = list(map(float, line))
            except ValueError:
                continue
            else:
                values.extend(line)

    values = np.unique(values)

    valence = values[values < E_F].max()
    conduction = values[values > E_F].min()

    return valence

def readBandGap(siesta_bands):
    E_F = getFermiEnergy(siesta_bands)
    values = []
    with open(siesta_bands, 'r') as f:
        ticks = []
        tick_labels = []
        f.readline()
        f.readline()
        f.readline()
        f.readline()
        for line in f:
            line = line.strip().split()
            try:
                line = list(map(float, line))
            except ValueError:
                continue
            else:
                values.extend(line)

    values = np.unique(values)

    valence = values[values < E_F].max()
    conduction = values[values > E_F].min()

    return valence, conduction

def makeDencharInput(span, shape, main_file=None, plot_wavefunctions=True, plot_charge=True):
    script = ''
    if main_file != None:
        script += '%include ' + main_file
    script += """
Denchar.CoorUnits Ang
Denchar.DensityUnits Ele/Ang**3
Denchar.TypeOfRun 3D
DencharPlotWaveFunctions %s
Denchar.PlotCharge %s """ % (plot_wavefunctions, plot_charge)

    script += """
Denchar.NumberPointsX %d
Denchar.NumberPointsY %d
Denchar.NumberPointsZ %d """ % tuple(shape)


    script += """
Denchar.MinX %.8f Ang
Denchar.MaxX %.8f Ang
Denchar.MinY %.8f Ang
Denchar.MaxY %.8f Ang
Denchar.MinZ %.8f Ang
Denchar.MaxZ %.8f Ang""" % (span[0][0], span[0][1], span[1][0], span[1][1], span[2][0], span[2][1])

    return script


def readPDOS(filename, target_indices=None):
    if isinstance(target_indices, int):
        target_indices = [target_indices]

    if not target_indices is None:
        target_indices = np.array(target_indices) + 1

    read = False
    energies = []
    with open(filename, 'r') as f:
        for line in f:
            if """/energy_values""" in line:
                break
            if read:
                energy = float(line.strip().split(' ')[0])
                energies.append(energy)

            if """energy_values""" in line:
                read = True
    energies = np.array(energies)

    read=False
    all_pdos = []
    pdos = []
    index = -1
    values = np.zeros((len(energies), 2))
    with open(filename, 'r') as f:
        for line in f:
            if read:
                line_list = line.strip().split(' ')
                line_list = [e for e in line_list if e!='']
                try:
                    line_values = list(map(float, line_list))
                except ValueError:
                    pass
                else:
                    if len(line_values) in [1, 2]:
                        pdos.append(line_values)

            if """atom_index""" in line:
                new_index = int(line.strip()[:-1].split(' ')[-1])
                read = (target_indices is None) or (new_index in target_indices)

                if index == new_index:
                    all_pdos.append(pdos)
                else:
                    if (target_indices is None) or (index in target_indices):
                        all_pdos = np.array(all_pdos)
                        values += all_pdos.sum(0)
                    all_pdos = []

                pdos = []
                index = new_index

        if (target_indices is None) or (index in target_indices):
            all_pdos = np.array(all_pdos)
            values += all_pdos.sum(0)

    return values, energies

def fdf2aseAtoms(fname):
    cell, xyz, snr, atomic_numbers, natoms = ReadFDFFile(fname)
    atomic_numbers = handleAtomicNumbers(atomic_numbers)
    atoms = Atoms(positions=xyz, cell=cell, numbers=atomic_numbers)

    return atoms

def handleAtomicNumbers(atomic_numbers):
    atomic_numbers = np.array(atomic_numbers)
    take = np.array(atomic_numbers) > 0
    atomic_numbers[take] = atomic_numbers[take]%200

    return atomic_numbers

def relaxASEFromXV(atoms, XV_filename):
    constraints = atoms.constraints

    xv_atoms = convertXVtoASE(XV_filename)

    if atoms.get_celldisp().shape == (3, 1):
        atoms.set_celldisp((0, 0, 0))

    xv_atoms.translate(atoms.get_celldisp())
    if len(constraints) == 1 and isinstance(constraints[0], FixAtoms):
        fixed = np.array(constraints[0].index)
        diff = atoms[fixed].positions - xv_atoms[fixed].positions
        l = vectorLength(diff, axis=1)
        if (l < 0.005).all():
            xv_atoms[fixed].positions = atoms[fixed].positions
        else:
            print(l)
            raise Exception

    atoms.positions = xv_atoms.positions
    atoms.cell = xv_atoms.cell

    return atoms


def convertASEtoXV(atoms, specie_dict, filename='siesta.XV'):
    vectors = atoms.get_cell()
    convFactor = 1/Bohr
    atomnumbers = atoms.get_atomic_numbers()
    speciesnumbers = [specie_dict[atomnumber] for atomnumber in atomnumbers]
    xyz = atoms.get_positions()
    with open(filename, 'w') as f:
        for i in range(3):
            line = '  '
            for j in range(3):
                line += string.rjust('%.9f'%(vectors[i][j]*convFactor),16)
            line += '     0.00  0.00  0.00 \n'
            f.write(line)
        # Write number of atoms (line 4)
        numberOfAtoms = len(speciesnumbers)
        f.write('     %i\n' %numberOfAtoms)
        # Go through the remaining lines
        for i in range(numberOfAtoms):
            line = '  %i' %speciesnumbers[i]
            line += '  %i  ' %atomnumbers[i]
            for j in range(3):
                line += string.rjust('%.9f'%(xyz[i][j]*convFactor),16)

            line += '     0.00  0.00  0.00 '
            f.write(line+'\n')

def convertXVtoASE(filename):
    result=ReadXVFile(filename=filename,InUnits='Bohr',OutUnits='Ang', ReadVelocity=False)
    unit_cell=np.array(result[0])
    numbers = np.array(result[2])
    numbers = handleAtomicNumbers(numbers)
    positions=np.array(result[3])
    atoms = Atoms(numbers=numbers, positions=positions, cell=unit_cell)


    return atoms

def convertXYZtoASE(filename):
    with open(filename,'r') as f:
        # Read number of atoms (line 4)
        n_atoms = string.atoi(string.split(f.readline())[0])

        # Read remaining lines
        atoms = Atoms()
        for line in f.readlines():
            if len(line)>5: # Ignore blank lines
                data = string.split(line)
                if len(data)==4:
                    symbol = data[0]
                    position = np.array([string.atof(data[1+j]) for j in range(3)])
                    atom = Atom(symbol=symbol, position=position)
                    atoms.append(atom)

    if len(atoms) != n_atoms:
        print('SiestaIO.ReadXYZFile: Inconstency in %s detected!' %filename)

    return atoms


def convertSTRUCTtoASE(filename):
    result=ReadSTRUCTFile(filename)
    unit_cell=np.array(result[0])
    numbers = np.array(result[2])%200
    scaled_positions=np.array(result[3])
    atoms = Atoms(numbers=numbers, scaled_positions=scaled_positions, cell=unit_cell)

    return atoms

Rydberg2eV = 13.6058
Bohr2Ang = 0.529177
Ang2Bohr = 1/Bohr2Ang

def ReadSTRUCTFile(filename):
    "Returns tuple (vectors,speciesnumber,atomnumber,xyz,[v,]) from an XV-file"
    try:
        file = open(filename,'r')
    except:
        file = gzip.open(filename+'.gz','r')

    # Read cell vectors (lines 1-3)
    vectors = []
    for i in range(3):
        data = string.split(file.readline())
        vectors.append([string.atof(data[j]) for j in range(3)])
    # Read number of atoms (line 4)
    numberOfAtoms = string.atoi(string.split(file.readline())[0])
    # Read remaining lines
    speciesnumber, atomnumber, xyz, V = [], [], [], []
    for line in file.readlines():
        if len(line)>5: # Ignore blank lines
            data = string.split(line)
            speciesnumber.append(string.atoi(data[0]))
            atomnumber.append(string.atoi(data[1]))
            xyz.append([string.atof(data[2+j]) for j in range(3)])
    file.close()
    if len(speciesnumber)!=numberOfAtoms:
        print('SiestaIO.ReadXVFile: Inconstency in %s detected!' %filename)
    return vectors,speciesnumber,atomnumber,xyz

def convertZMtoASE(filename):
    XV_filename = filename[:-2] + 'XV'
    result=ReadXVFile(filename=XV_filename,InUnits='Bohr',OutUnits='Ang',ReadVelocity=False)
    numbers = np.array(result[2])%200
    unit_cell, positions= ReadZMFile(filename=filename)
    atoms = Atoms(numbers=numbers, positions=positions, cell=unit_cell, pbc=[True]*3)

    return atoms

def ReadZMFile(filename):
    try:
        file = open(filename,'r')
    except:
        file = gzip.open(filename+'.gz','r')

    # Read cell vectors (lines 1-3)
    vectors = []
    for i in range(3):
        data = string.split(file.readline())
        vectors.append([string.atof(data[j]) for j in range(3)])

    # Read remaining lines
    speciesnumber, atomnumber, xyz, V = [], [], [], []
    for line in file.readlines():
        if len(line) < 5: # Ignore blank lines
            continue
        data = string.split(line)
        if len(data) < 2:
             break
        xyz.append([string.atof(data[j]) for j in range(3)])
    xyz = np.array(xyz)*Bohr
    vectors = np.array(vectors)*Bohr
    return vectors, xyz

def ReadXVFile(filename,InUnits='Bohr',OutUnits='Ang',ReadVelocity=False):
    "Returns tuple (vectors,speciesnumber,atomnumber,xyz,[v,]) from an XV-file"
    if (InUnits=='Bohr') and (OutUnits=='Ang'): convFactor = Bohr2Ang
    elif (InUnits=='Ang') and (OutUnits=='Bohr'): convFactor = Ang2Bohr
    elif (((InUnits=='Ang') and (OutUnits=='Ang')) \
       or ((InUnits=='Bohr') and (OutUnits=='Bohr'))): convFactor = 1
    else:pass
    try:
        file = open(filename,'r')
    except:
        file = gzip.open(filename+'.gz','r')

    # Read cell vectors (lines 1-3)
    vectors = []
    for i in range(3):
        data = str.split(file.readline())
        vectors.append([float(data[j])*convFactor for j in range(3)])
    # Read number of atoms (line 4)
    numberOfAtoms = int(str.split(file.readline())[0])
    # Read remaining lines
    speciesnumber, atomnumber, xyz, V = [], [], [], []
    for line in file.readlines():
        if len(line)>5: # Ignore blank lines
            data = str.split(line)
            speciesnumber.append(int(data[0]))
            atomnumber.append(int(data[1]))
            xyz.append([float(data[2+j])*convFactor for j in range(3)])
            V.append([float(data[5+j])*convFactor for j in range(3)])
    file.close()
    if len(speciesnumber)!=numberOfAtoms:
        print('SiestaIO.ReadXVFile: Inconstency in %s detected!' %filename)
    if ReadVelocity:
        return vectors,speciesnumber,atomnumber,xyz, V
    else:
        return vectors,speciesnumber,atomnumber,xyz

def WriteXVFile(filename,vectors,speciesnumber,atomnumber,xyz,\
                InUnits='Ang',OutUnits='Bohr',Velocity=[]):
    "Writes (vectors,speciesnumber,atomnumber,xyz,[V],) to the XV-file format"
    print('SiestaIO.WriteXVFile: Writing',filename)
    if (InUnits=='Bohr') and (OutUnits=='Ang'): convFactor = Bohr2Ang
    elif (InUnits=='Ang') and (OutUnits=='Bohr'): convFactor = Ang2Bohr
    elif (((InUnits=='Ang') and (OutUnits=='Ang')) \
       or ((InUnits=='Bohr') and (OutUnits=='Bohr'))): convFactor = 1
    else: print('SiestaIO.WriteXVFile: Unit conversion error!')
    file = open(filename,'w')
    # Write basis vectors (lines 1-3)
    for i in range(3):
        line = '  '
        for j in range(3):
            line += string.rjust('%.9f'%(vectors[i][j]*convFactor),16)
        line += '     0.00  0.00  0.00 \n'
        file.write(line)
    # Write number of atoms (line 4)
    numberOfAtoms = len(speciesnumber)
    file.write('     %i\n' %numberOfAtoms)
    # Go through the remaining lines
    for i in range(numberOfAtoms):
        line = '  %i' %speciesnumber[i]
        line += '  %i  ' %atomnumber[i]
        for j in range(3):
            line += string.rjust('%.9f'%(xyz[i][j]*convFactor),16)
        if len(Velocity)==0:
            line += '     0.00  0.00  0.00 '
        else:
            for j in range(3):
                line += string.rjust('%.9f'%(Velocity[i][j]*convFactor),16)
        file.write(line+'\n')
    file.close()

def ReadAXVFile(filename,MDstep,tmpXVfile="tmp.XV",ReadVelocity=False):
    "Read concatenated XV files from an MD simulation"
    # Determine the number of atoms in the supercell
    f = open(filename)
    [f.readline() for i in range(3)]
    Natoms = int(string.split(f.readline())[0])
    f.close()
    XVlines = Natoms + 4
    # Extract the XV file from MDstep and write tmpXVfile
    f = open(filename)
    g = open(tmpXVfile,'w')
    i = 0
    for line in f:
        if i >= MDstep*XVlines and i < (MDstep+1)*XVlines: g.write(line)
        if i == (MDstep+1)*XVlines: break
        i += 1
    f.close()
    g.close()
    # Read tmpXVfile corresponding to MDstep
    return ReadXVFile(tmpXVfile,InUnits='Bohr',OutUnits='Ang',ReadVelocity=ReadVelocity)

#--------------------------------------------------------------------------------
# ANI-format

def WriteANIFile(filename,Geom,Energy,InUnits='Ang',OutUnits='Ang'):
    " Write .ANI file from list of geometries with Energy=[E1,E2..]"
    print('SiestaIO.WriteANIFile: Writing',filename)
    if (InUnits=='Bohr') and (OutUnits=='Ang'): convFactor = Bohr2Ang
    elif (InUnits=='Ang') and (OutUnits=='Bohr'): convFactor = Ang2Bohr
    elif (((InUnits=='Ang') and (OutUnits=='Ang')) \
       or ((InUnits=='Bohr') and (OutUnits=='Bohr'))): convFactor = 1
    else: print('SiestaIO.WriteANIFile: Unit conversion error!')
    file = open(filename,'w')
    for ii, iGeom in enumerate(Geom):
        file.write('%i \n'%iGeom.natoms)
        file.write('%f \n'%Energy[ii])
        for iixyz in range(iGeom.natoms):
            file.write('%s %2.4f %2.4f %2.4f\n'%\
                       (PeriodicTable[iGeom.anr[iixyz]],\
                        convFactor*iGeom.xyz[iixyz][0],\
                        convFactor*iGeom.xyz[iixyz][1],\
                        convFactor*iGeom.xyz[iixyz][2]))
    file.close()

def ReadANIFile(filename,InUnits='Ang',OutUnits='Ang'):
    "Returns tuple (Geometry,Energy[Ry?],) from an ANI-file"
    print('SiestaIO.ReadANIFile: Reading',filename)
    if (InUnits=='Bohr') and (OutUnits=='Ang'): convFactor = Bohr2Ang
    elif (InUnits=='Ang') and (OutUnits=='Bohr'): convFactor = Ang2Bohr
    elif (((InUnits=='Ang') and (OutUnits=='Ang')) \
       or ((InUnits=='Bohr') and (OutUnits=='Bohr'))): convFactor = 1
    else: print('SiestaIO.ReadANIFile: Unit conversion error!')

    Energy, Geom = [], []

    file = open(filename,'r')
    newNN=file.readline()
    while newNN!='':
        NN=string.atoi(newNN)
        newG=MG.Geom()
        Energy.append(string.atof(file.readline()))
        for ii in range(NN):
            line=string.split(file.readline())
            xyz=[convFactor*string.atof(line[1]),
                 convFactor*string.atof(line[2]),\
                 convFactor*string.atof(line[3])]
            newG.addAtom(xyz,1,PeriodicTable[line[0]])
        Geom.append(newG)
        newNN=file.readline()
    file.close()
    return Geom,Energy

#--------------------------------------------------------------------------------
# Reading SIESTA FC ascii files

def ReadFCFile(filename):
    "Returns FC from an FC-file"
    print('SiestaIO.ReadFCFile: Reading',filename)
    file = open(filename,'r')
    # Read comment line (line 1)
    line = file.readline()
    if string.strip(line)!='Force constants matrix':
        print('SiestaIO.ReadFCFile: Inconstency in %s detected!' %filename)
    # Read remaining lines
    FC = []
    for line in file.readlines():
        data = string.split(line)
        FC.append([string.atof(data[j]) for j in range(3)])
    file.close()
    return FC

#--------------------------------------------------------------------------------
# Reading SIESTA Fortan binary files

def ReadFortranBin(file,type,num,printLength=False,unpack=True):
    "Reads Fortran binary data structures"
    import struct
    fmt = ''
    for i in range(num): fmt += type
    bin = file.read(struct.calcsize(fortranPrefix+fortranuLong+fmt+fortranuLong))
    if unpack:
        data = struct.unpack(fortranPrefix+fortranLong+fmt+fortranLong,bin)
        if printLength:
            print('SiestaIO.ReadFortranBin: %i bytes read' %data[0])
        if data[0]!=data[-1] or data[0]!=struct.calcsize(fortranPrefix+fmt):
            print('SiestaIO.ReadFortranBin: Error reading Fortran formatted binary file')
            kuk
        return data[1:-1]
    else:
        return bin

def sparse2matrix(indices,sparsearray):
    nao = len(indices)
    matrixarray = N.array((nao,nao),N.Float)
    for i in range(nao):
        for j in indices[i]:
            matrixarray[i,j] = sparsearray[i][j]
    return matrixarray

def ReadHSFile(filename):
    """
    Returns tuple (eF,H,S,ia1,istep) (in eV) from an HS-file
    for spin polarized calc, H = [Hs0, Hs1]
    """
    print('SiestaIO.ReadHSFile: Reading',filename)
    # Open binary Fortran file
    try:
        file = open(filename,'rb')
    except:
        file = gzip.open(filename+'.gz','rb')

    nao,nspin,maxhn,ia1,istep = ReadFortranBin(file,fortranLong,5)
    # Build sparse matrix index list
    numhg = ReadFortranBin(file,fortranLong,nao)
    list = []
    for i in range(nao):
        list.append(ReadFortranBin(file,fortranLong,numhg[i]))
    # Assume initially no k-point sampling
    kPointSampling = False
    # Read Hamiltonian H from sparse format
    for iSpin in range(nspin):
        tmpH = N.zeros((nao,nao),N.Float)
        for i in range(nao):
            tmp = ReadFortranBin(file,'d',numhg[i])
            for j in range(numhg[i]):
                k = list[i][j]-1 # Subtract 1 since indices begin with 0
                if k<nao:
                    tmpH[i,k] = tmp[j]
                else:
                    # With k-point sampling the elements k >= nao describe
                    # the couplings to neighboring cells
                    kPointSampling = True
        if iSpin==0:
            H=tmpH*Rydberg2eV          # no spin polarization, return H
        else:
            H=[H,tmpH*Rydberg2eV]      # spin polarized, return [Hs0, Hs1]
    # Read overlap matrix S from sparse format
    S = N.zeros((nao,nao),N.Float)
    for i in range(nao):
        tmp = ReadFortranBin(file,'d',numhg[i])
        for j in range(numhg[i]):
            k = list[i][j]-1 # Subtract 1 since indices begin with 0
            if k<nao:
                S[i,k] = tmp[j]
    # Read Fermi energy
    junk = ReadFortranBin(file,'d',2)[0] # qtot and temperature
    if kPointSampling:
        #  more junk for k-point sampling
        for ii in range(nao):
            for jj in range(numhg[ii]):
                junk = ReadFortranBin(file,'d',3)
    eF = ReadFortranBin(file,'d',1)[0]
    file.close()
    return eF*Rydberg2eV,H,S,ia1,istep


def ReadTSHSFile(filename):
    "Returns tuple of TSHS-file contents"
    print('SiestaIO.ReadTSHSFile: Reading',filename)
    # Open binary Fortran file
    file = open(filename,'rb')

    nua,nuotot,nspin,notot,maxnhtot = ReadFortranBin(file,fortranLong,5)
    isa = ReadFortranBin(file,fortranLong,nua)
    lasto = ReadFortranBin(file,fortranLong,nua+1)
    xa = ReadFortranBin(file,'d',3*nua)
    indxuo = ReadFortranBin(file,fortranLong,notot)

    numhg = []
    for i in range(nuotot):
        numhg.append(ReadFortranBin(file,fortranLong,1)[0])
    listh = []
    for i in range(nuotot):
        for j in range(numhg[i]):
            listh.append(ReadFortranBin(file,fortranLong,1)[0])
    Hsparse = []
    for i in range(nspin):
        for j in range(nuotot):
            for k in range(numhg[j]):
                Hsparse.append(ReadFortranBin(file,'d',1)[0])
    Ssparse = []
    for i in range(nuotot):
        for j in range(numhg[i]):
            Ssparse.append(ReadFortranBin(file,'d',1)[0])

    qtot,temp = ReadFortranBin(file,'d',2)
    efs = ReadFortranBin(file,'d',nspin)
    cell = ReadFortranBin(file,'d',3*3)
    gamma = ReadFortranBin(file,'b',1,printLength=False,unpack=False) # NB. Logical variable unpacked!

    # If not gamma point calculation, geometry is also written to *.TSHS-file
    #xij = []
    #for i in range(nuotot):
    #    for j in range(numhg[i]):
    #        print i,j
    #        xij.append(ReadFortranBin(file,'d',3,printLength=True))

    file.close()
    return nua,nuotot,nspin,notot,maxnhtot,isa,lasto,xa,indxuo,numhg,listh,\
           N.array(Hsparse),N.array(Ssparse),qtot,temp,efs,cell,gamma


def ReadOnlyS(filename):
    "Returns overlap matrix S from an onlyS-file"
    print('SiestaIO.ReadOnlyS: Reading',filename)
    # Open binary Fortran file
    file = open(filename,'rb')
    # Read dimensional information
    #   nao:     total number of atomic orbitals
    #   maxhn:   a max estimate of the number of nonzero elements
    nao,maxhn = ReadFortranBin(file,fortranLong,2)
    # Build sparse matrix index list
    numhg = ReadFortranBin(file,fortranLong,nao)
    indices = []
    for i in range(nao):
        tmp = ReadFortranBin(file,fortranLong,numhg[i])
        indices.append(N.array(tmp)-1) # Indices start from 0 in Python
    # Read overlap matrix S in sparse format
    S = []
    for i in range(nao):
        printDone(i,nao,'SiestaIO.ReadOnlyS')
        tmp = ReadFortranBin(file,'d',numhg[i])
        S.append(N.array(tmp,N.Float))
    file.close()
    return indices,S


def ReadWFSFile(filename):
    """
    TF/071008
    Returns the WF coefficients etc. from SIESTA systemlabe.WFS files
    (see siesta/utils/readwf.f for details)
    """
    file = open(filename,'rb')
    nk, = ReadFortranBin(file,'I',1)
    nspin, = ReadFortranBin(file,'I',1)
    nuotot, = ReadFortranBin(file,'I',1)
    #print nk, nspin, nuotot

    PSIvectors = []
    for iik in range(nk):
        for iispin in range(nspin):
            ik,k1,k2,k3 =  ReadFortranBin(file,'Iddd',1)
            #print ik,k1,k2,k3
            ispin, = ReadFortranBin(file,'I',1)
            nwflist, = ReadFortranBin(file,'I',1)
            #print ispin,nwflist
            for iw in range(nwflist):
                REpsi,IMpsi = [],[]
                indwf, = ReadFortranBin(file,'I',1)
                energy, = ReadFortranBin(file,'d',1)
                #print indwf, energy
                for jj in range(nuotot):
                    label = ''
                    for a in range(20): label += 'c'
                    out =  ReadFortranBin(file,'I'+label+'III'+label+'dd',1)
                    iaorb,lab1,j,iphorb,cnfigfio,lab2,repsi,impsi = out[0],out[1:21],out[21],out[22],out[23],out[24:44],out[44],out[45]
                    labelfis,symfio = '',''
                    for a in range(20):
                        labelfis += lab1[a]
                        symfio += lab2[a]
                    #print labelfis,symfio
                    REpsi.append(repsi),IMpsi.append(impsi)
                PSIvectors.append(N.array(REpsi)+1j*N.array(IMpsi))
    return nk,nspin,nuotot,nwflist,PSIvectors


def printDone(i,n,mess):
    # Print progress report
    if n>10:
        if i%int(n/10)==0:
            print(mess," : %2.0f %%" % ((100.0*i)/n,)," done.")

#--------------------------------------------------------------------------------
# MKL-format IO

def WriteMKLFile(filename,atomnumber,xyz,freq,vec,FCfirst,FClast):
    "Writes a MKL-file"
    print('SiestaIO.WriteMKLFile: Writing',filename)
    file = open(filename,'w')
    file.write('$MKL\n$COORD\n')
    for i in range(len(atomnumber)):
        line = str(atomnumber[i])
        for j in range(3):
            line += string.rjust('%.9f'%xyz[i][j],16)
        line +='\n'
        file.write(line)
    file.write('$END\n')
    if len(freq)>0:
        file.write('$FREQ\n')
        for i in range(len(freq)/3):
            file.write('C1 C1 C1\n')
            # Write frequencies
            line = ''
            for j in range(3):
                f = 1000*freq[3*i+j] # Write in meV
                if type(f)==type(1.0j): line += '%f '%f.real
                else: line += '%f '%f
            line += '\n'
            file.write(line)
            # Write modes
            for j in range(FCfirst-1):
                file.write('0 0 0 0 0 0 0 0 0\n')
            for j in range(FClast-FCfirst+1):
                line = ''
                for k in range(3):
                    line += '%f %f %f '%(vec[3*i+k][3*j],vec[3*i+k][3*j+1],vec[3*i+k][3*j+2])
                line += '\n'
                file.write(line)
            for j in range(FClast,len(xyz)):
                file.write('0 0 0 0 0 0 0 0 0\n')
        file.write('$END\n\n')
    file.close()

#--------------------------------------------------------------------------------
# XYZ-format IO

def WriteXYZFile(filename,atomnumber,xyz):
    "Writes atomic geometry in xyz-file format"
    print('SiestaIO.WriteXYZFile: Writing',filename)
    # Write file
    file = open(filename,'w')
    file.write(str(len(xyz)))
    file.write('\n\n')
    for i in range(len(xyz)):
        line = string.ljust(PeriodicTable[atomnumber[i]],5)
        for j in range(3):
            line += string.rjust('%.9f'%xyz[i][j],16)
        line +='\n'
        file.write(line)
    #file.write('\n')

#--------------------------------------------------------------------------------
# FDF format IO

def ReadFDFFile(infile):
    """ Reads an FDF file and gives the output values: pbc, xyz, snr, anr, natoms
        infile = FDF inputfile"""
    pbc = Getpbc(infile)
    xyz, snr = Getxyz(infile)
    anr = Getanr(infile, snr)
    natoms = Getnatoms(infile)
    if natoms != len(xyz):
        print('Error! natoms != len(xyz)')
    return pbc, xyz, snr, anr, natoms

def ReadFDFLines(infile):
    """ Returns an FDF file and all the %include files as split strings
        infile = input file"""
    head,tail =  os.path.split(infile)
    #print 'SiestaIO.ReadFDFLines: Reading', infile
    file = open(infile,'r')
    lines = []
    tmp = file.readline()
    while tmp != '':
        if len(tmp)>3 and len(string.split(tmp))!=0:
            tmp = string.split(tmp)
            if tmp[0] == '%include':
                tmp2 = ReadFDFLines(head+'/'+tmp[1])
                lines += tmp2
            else:
                lines.append(tmp)
                tmp = file.readline()
        else:
            tmp = file.readline()
    return lines


def Getnatoms(infile):
      """ Gives the number of atoms included in an FDF file
          infile = FDF input file"""
      natoms = GetFDFline(infile,'NumberOfAtoms')
      if natoms==None:
          natoms = GetFDFline(infile,'NumberOfAtoms:')
      return int(natoms[0])


def Getxyz(infile):
    """ Gives a list of the xyz posistions in a FDF file
        infile = FDF input file"""
    data = GetFDFblock(infile, 'Zmatrix')
    xyz = []
    snr = []
    if not data is None:
        while len(data[0])==1:
            data.pop(0)
        for i in range(len(data)):
            xyz.append([string.atof(data[i][j+1]) for j in range(3)])
            snr.append(string.atof(data[i][0]))
    else:
        data = GetFDFblock(infile, 'AtomicCoordinatesAndAtomicSpecies')
        for i in range(len(data)):
            xyz.append([string.atof(data[i][j]) for j in range(3)])
            snr.append(string.atof(data[i][3]))

    return xyz, snr


def Getpbc(infile):
    """ Gives a list of the lattice vectores in a FDF file
        infile = FDF input file"""
    data = GetFDFblock(infile,'LatticeVectors')
    pbc = []
    for i in range(len(data)):
        pbc.append([string.atof(data[i][j]) for j in range(3)])
    return pbc


def Getsnr(infile):
     """ Gives a list of the species numbers in a FDF file
         infile = FDF input file"""
     data = GetFDFblock(infile,'AtomicCoordinatesAndAtomicSpecies')
     snr = []
     for i in range(len(data)):
         snr.append(string.atoi(data[i][3]))
     return snr


def Getanr(infile, snr):
    """ Gives a list of the atomic numbers in a FDF file
        infile = FDF input file"""
    data = GetFDFblock(infile,'ChemicalSpeciesLabel')
    tmp = []
    table ={}
    for i in range(len(data)):
        tmp.append([string.atoi(data[i][j]) for j in range(2)])
        table[tmp[i][0]] = tmp[i][1]
    anr = []
    for i in range(len(snr)):
        anr.append(table[snr[i]])
    return anr

def Getkpts(infile):
    """ Gives a list of k-point setup in a FDF file
    infile = FDF input file"""
    data = GetFDFblock(infile,'kgrid_Monkhorst_Pack')
    kpts=[]
    for i in range(len(data)):
        kpts.append(int(data[i][i]))
    return tuple(kpts)

def GetFDFline(infile, KeyWord = ''):
    """ Finds a line and gives the value as a string
        infile = FDF input file
        KeyWord = line to find"""
    lines = ReadFDFLines(infile)
    for i in range(len(lines)):
        if lines[i][0]==KeyWord:
            return lines[i][1:]

def GetFDFblock(infile, KeyWord = ''):
    """Finds the walues in a block as strings
       infile = FDF input file
       KeyWord = block to find"""
    lines = ReadFDFLines(infile)
    data = []
    start = None
    for i in range(len(lines)):
        tmp = lines[i]
        if tmp[0] == '%block':
            if tmp[1] == KeyWord:
                start = i+1
                break

    if start is None:
        return None

    for i in range(len(lines)):
        tmp = lines[i+start]
        if tmp[0] != '%endblock':
            data.append(tmp)
        else: break
    return data

def WriteFDFblock(outfile, KeyWord,block):
    """write a block to file"""
    f=open(outfile,'a')
    f.write('\n')
    f.write(r'%'+'block %s\n'%KeyWord)
    for line in block:
        for element in line:
            f.write('%s\t'%element)
        f.write('\n')
    f.write(r'%'+'endblock %s\n'%KeyWord)
    f.close()
#--------------------------------------------------------------------------------

def getTotalEnergy(outfile):
    # Find total energy from SIESTA stdout file
    with open(outfile,'r') as f:
        lines = f.readlines()

    E = 0.0
    for line in lines:
        words = string.split(line)
        if 'Total' in words and '=' in words:
            E = string.atof(words[-1])
            break
    assert E != 0.0
    return E

def getFermiEnergy(outfile):
    # Read Fermi energy from SIESTA bands
    with open(outfile,'r') as f:
        Ef = float(f.readline().strip())
    return Ef

def ReadSiestaEIGfile(infile):
    # Read *EIG file and print eigenvalues with respect to eF.
    f = open(infile,'r')
    eF = float(string.split(f.readline())[0])
    f.readline() # Skip second line
    EIG = []
    eig = string.split(f.readline())[1:] # Skip first entry in third line
    EIG += eig
    for line in f.readlines():
        eig = string.split(line)
        EIG += eig
    for i in range(len(EIG)):
        print(float(EIG[i])-eF)
    f.close()

def ReadDOSFile(filename):
    # Reads SIESTA *.DOS files
    f = open(filename,'r')
    DOS = []
    for line in f.readlines():
        data = line.split()
        for i in range(len(data)):
            data[i] = float(data[i])
        DOS.append(data)
    f.close()
    return N.array(DOS)


#--------------------------------------------------------------------------------
# XML-format

import xml.dom.minidom as xml

def GetPDOSnspin(dom):
    # Read nspin
    node = dom.getElementsByTagName('nspin')[0] # First (and only) entry
    return int(node.childNodes[0].data)

def GetPDOSnorbitals(dom):
    # Read norbitals
    node = dom.getElementsByTagName('norbitals')[0] # First (and only) entry
    return int(node.childNodes[0].data)

def GetPDOSenergyValues(dom):
    # Read energy values
    node = dom.getElementsByTagName('energy_values')[0] # First (and only) entry
    data = node.childNodes[0].data.split()
    for i in range(len(data)):
        data[i] = float(data[i])
    return np.array(data)

def GetPDOSfromOrbitals(dom,index=[],atom_index=[],species=[],nlist=[],llist=[]):
    pdos = 0.0*GetPDOSenergyValues(dom)
    nodes = dom.getElementsByTagName('orbital')
    usedOrbitals = []
    for node in nodes:
        ok = True
        i = int(node.attributes['index'].value)
        ai = int(node.attributes['atom_index'].value)
        s = node.attributes['species'].value
        n = int(node.attributes['n'].value)
        l = int(node.attributes['l'].value)
        if i not in index and index!=[]:
            ok = False
        if ai not in atom_index and atom_index!=[]:
            ok = False
        if s not in species and species!=[]:
            ok = False
        if n not in nlist and nlist!=[]:
            ok = False
        if l not in llist and llist!=[]:
            ok = False
        if ok:
            usedOrbitals.append([i,ai,s,n,l])
            data = node.getElementsByTagName('data')[0] # First (and only) entry
            data = data.childNodes[0].data.split()
            for i in range(len(data)):
                data[i] = float(data[i])
            pdos += np.array(data)

    # Generate some output-related information
    if atom_index!=[]: print('... Atom indices =',atom_index)
    if species!=[]: print('... Species =',species)
    if nlist!=[]: print('... Allowed n quantum numbers =',nlist)
    if llist!=[]: print('... Allowed l quantum numbers =',llist)
    print('... Orbitals included =',len(usedOrbitals))
    atoms = []
    for orb in usedOrbitals:
        if orb[1] not in atoms: atoms.append(orb[1])
    print('... Atoms included =',len(atoms))
    return pdos,usedOrbitals

def ReadIonNCFile(filename,printnorm=False):
    """
    Reads a NetCDF file that describes the basis orbitals of a given species
    """
    class ion:
        pass

    file = netcdf_file(filename,'r')
    print('Reading Basis from %s' % filename)

    # General attributes
    ion.filename = filename
    ion.element = file.Element
    ion.label = file.Label
    ion.atomnum = file.Atomic_number[0]
    ion.numorb = file.Number_of_orbitals[0]

    # Variables
    ion.L = N.array(file.variables['orbnl_l'],N.Int)
    ion.N = N.array(file.variables['orbnl_n'],N.Int)
    ion.Z = N.array(file.variables['orbnl_z'],N.Int)
    ion.ispol = N.array(file.variables['orbnl_ispol'],N.Int)
    ion.delta = Bohr2Ang*N.array(file.variables['delta'],N.Float)
    ion.orb = N.array(file.variables['orb'],N.Float)
    ion.cutoff = N.array(file.variables['cutoff'],N.Float)

    print('   Element: %s   Atom number: %i,  L-orbs '% (ion.element,ion.atomnum), ion.L)
    for i in range(len(ion.L)):
        rr = ion.delta[i] * N.array(list(range(len(ion.orb[i]))),N.Float)
        ion.orb[i] = ion.orb[i]*(rr**ion.L[i])/(Bohr2Ang**(3./2.))
        rr = rr*Bohr2Ang
        # check normalization:
        if printnorm:
            print('   orb %i (L=%i),    Norm = %.6f'%(i,ion.L[i],N.sum(rr*rr*(ion.orb[i]**2))* ion.delta[i]))
    file.close()
    return ion

def ReadIonNCFiles(wildcard='*ion.nc'):
    """
    Reads all *ion.nc files in a folder
    """
    import glob
    ions = {}
    for ionnc in glob.glob(wildcard):
        ion = ReadIonNCFile(ionnc)
        ions[ion.atomnum] = ion
    return ions

def BuildBasis(XVfile,FirstAtom,LastAtom):
    """
    Builds the information for each basis orbital in the Hamiltonian
    """
    class basis:
        pass

    XVfile = os.path.abspath(XVfile)
    vectors,speciesnumber,atomnumber,xyz = ReadXVFile(XVfile)
    head,tail =  os.path.split(XVfile)
    ions = ReadIonNCFiles(head+'/*ion.nc')

    # Determine the basis dimension nn
    nn = 0
    for i in range(FirstAtom-1,LastAtom): # Python counts from zero
        an = atomnumber[i]
        nn += ions[an].numorb

    # Initiate basis variables
    basis.ii = N.zeros((nn,),N.Int)
    basis.L = N.zeros((nn,),N.Int)
    basis.M = N.zeros((nn,),N.Int)
    basis.atomnum = N.zeros((nn,),N.Int)
    basis.xyz = N.zeros((nn,3),N.Float)
    basis.delta = N.zeros((nn,),N.Float)
    basis.orb = []
    basis.coff = N.zeros((nn,),N.Float)

    # Describe each basis orbital
    iorb = 0
    for ii in range(FirstAtom-1,LastAtom):
        an = atomnumber[ii]
        ion = ions[an]
        for jj in range(len(ion.L)):
            for kk in range(-ion.L[jj],ion.L[jj]+1):
                basis.ii[iorb]=ii+1
                basis.atomnum[iorb]= an
                basis.L[iorb] = ion.L[jj]
                basis.M[iorb] = kk
                basis.xyz[iorb,:] = xyz[ii]
                basis.delta[iorb] = ion.delta[jj]
                basis.orb.append(ion.orb[jj,:])
                basis.coff[iorb] = (len(ion.orb[jj,:])-1)*ion.delta[jj]
                iorb = iorb+1

    return basis

def readEigenvalues(label='siesta', folder='./'):
    """ Read eigenvalues and Fermi level from the siesta.EIG file."""
    filename = join(folder, label + '.EIG')
    with open(filename, 'r') as f:
        E_F = float(f.readline().strip())
        nbands, nspins, nk = list(map(int, f.readline().split()))
        values = np.zeros((nk, nbands*nspins))

        band_index = 0
        for line in f:
            line = line.split()
            if len(line) == 0:
                continue

            try:
                int(line[0])
            except ValueError:
                new_values = list(map(float, line))
                advance = len(line)
                band_index += 10
            else:
                k_index = int(line[0]) - 1
                band_index = 0
                new_values = list(map(float, line[1:]))
                advance = len(new_values)

            values[k_index, band_index:band_index + advance] = new_values

        values = np.reshape(values, (nk, nspins, nbands))
        values = np.transpose(values, (0, 2, 1))

        return E_F, values

def plotBandStructure(filename, zero=None):
    print('plotBandStructure')
    from CalcTroll.Core.Submission.Workflow import WORKFLOW
    gnubands = WORKFLOW.host().utilities().get('gnubands')
    dirname = os.path.dirname(filename)
    if gnubands is None:
       raise Exception

    siesta_bands = filename
    gnuplot_bands = join(dirname, 'gnuplot.bands')
    xv_file = join(dirname, 'siesta.XV')
    assert os.path.exists(siesta_bands)
    cmd = '%s %s >%s'%(gnubands, siesta_bands, gnuplot_bands)
    os.system(cmd)
    i = 0
    while (not os.path.exists(gnuplot_bands)):
        time.sleep(1)
        i += 1
        print('sleep')

    plot = bandStructurePlot(siesta_bands, gnuplot_bands, zero=zero)

    os.remove(gnuplot_bands)

    return plot

def bandStructurePlot(siesta_bands, gnuplot_bands, zero=None):
    EF = getFermiEnergy(siesta_bands)
    print('open siesta.bands')
    with open(siesta_bands, 'r') as f:
        ticks = []
        tick_labels = []
        for line in f:
            line = line.strip().split('   ')
            if len(line) == 2:
                try:
                    float(line[1])
                except ValueError:
                    pass
                else:
                    continue

                tick = float(line[0])
                label = '$' + line[1][1:-1] + '$'
                if label == '$Gamma$':
                    label = '$\Gamma$'

                ticks.append(tick)
                tick_labels.append(label)

    with open(gnuplot_bands, 'r') as f:
        for line in f:
            if '#' not in line:
                break
            elif 'k_min, k_max' in line:
                k_max = float(line.strip().split(' ')[-1])
            elif 'Nspin' in line:
                line = line.strip().split(' ')
                line = [element for element in line if element!='']
                n_spins = int(line[-2])

        y_all = []
        xs = []
        ys = []
        test = []
        lines = []

        for line in f:
            if line=='\n':
                if len(ys)>0:
                    y0 = ys[0]
                    y_all.extend(ys)
                    lines.append((xs, np.array(ys)))
                xs = []
                ys = []
            else:
                line = line.strip().split(' ')
                line = [part for part in line if part!='']
                x, y = list(map(float, line))
                xs.append(x)
                ys.append(y)

    if zero is None:
        zero = 0

    up_lines = []
    down_lines = []
    if n_spins == 1:
        for xs, ys in lines:
            up_lines.append((xs, ys-zero))

    elif n_spins == 2:
        half = len(lines)/2
        for xs, ys in lines[half:]:
            down_lines.append((xs, ys-zero))
        for xs, ys in lines[:half]:
            up_lines.append((xs, ys-zero))

    return (up_lines, down_lines), (ticks, tick_labels), EF

def readTransmissionFile(filname):
    energies = []
    trans = []
    weights = []
    all_values = []
    values = []
    with open(filname, 'r') as f:
        f.readline()
        f.readline()
        f.readline()
        f.readline()
        for line in f:
            if '# kb' in line:
                weight = float(line.strip().split(' ')[-1])
                weights.append(weight)
            elif len(line.strip()) == 0:
                all_values.append(values)
                values = []
            else:
                values.append(list(map(float, line.strip().split('  '))))

    all_values.append(values)
    if len(weights) == 0:
        weights = [1.0]
    total = np.zeros(len(values), float)
    for weight, values in zip(weights, all_values):
        values = np.array(values)
        energies, trans = np.transpose(values)
        total += weight*trans
    energies = np.array(energies)

    return total, energies

def readNCDensity(density_nc, origin=(0, 0, 0)):
    origin = np.array(origin, dtype=float)/Bohr
    density, variables, cell = readGridNC(density_nc, origin=origin)
    variables = tuple([variable*Bohr for variable in variables])
    cell = np.array(cell)*Bohr
    density = np.array(density)*Bohr**(-3)

    return density, variables, cell

def readNCPotential(grid_nc, origin=(0, 0, 0)):
    origin = np.array(origin) / Bohr
    ep, r, cell = readGridNC(grid_nc, origin=origin)
    ep *= Rydberg
    r *= Bohr
    cell *= Bohr

    return ep, r, cell

def readGridNC(grid_nc, origin=(0, 0, 0)):
    origin = np.array(origin)
    f = netcdf_file(grid_nc, 'r', mmap=False)
    cell = np.array(f.variables['cell'][:], dtype=float)

    grid = f.variables['gridfunc'][:]

    order = np.array([0, 3, 2, 1])
    grid = np.transpose(grid, order)
    shape = np.array(grid.shape[-3:])

    abc = np.mgrid[0:shape[0]/float(shape[0]+1):shape[0]*1j,
                      0:shape[1]/float(shape[1]+1):shape[1]*1j,
                      0:shape[2]/float(shape[2]+1):shape[2]*1j,
                      ]
    x, y, z = np.tensordot(cell, abc, (0, 0))
    origin -= (cell.sum(axis=0)/(shape + 1))/2

    x -= origin[0]
    y -= origin[1]
    z -= origin[2]

    r = np.array([x, y, z])

    return grid, r, cell

def splitLine(line):
    line = line.strip().split(' ')
    line = [entry for entry in line if len(entry) > 0]

    return line

def hasCubeDensity(label='siesta'):
    try:
        readCubeDensity(label=label)
    except ImportError:
        return False

    return True

def readCubeDensity(label='siesta'):
    filename = label + '.RHO.cube'
    filenameUP = label + '.RHO.UP.cube'
    filenameDOWN = label + '.RHO.DOWN.cube'

    if os.path.exists(filenameUP):
        rho_0, variables, dh_cell = readCube(filenameUP)
        rho_1, variables, dh_cell = readCube(filenameDOWN)
        rho = np.array([rho_0, rho_1])

    elif os.path.exists(filename):
        rho, variables, dh_cell = readCube(filename)
    else:
        raise ImportError('File: %s does not exist' % filename)

    return rho, variables, dh_cell

def readCube(filename):
    with open(filename, 'r') as f:
        # Ignore two comment lines
        f.readline().strip()
        f.readline().strip()

        natoms, x0, y0, z0 = splitLine(f.readline())
        natoms = int(natoms)
        origin = np.array([float(x0), float(y0), float(z0)])*Bohr

        shape = []
        dh_cell = []
        for i in range(3):
            s, x, y, z = splitLine(f.readline())
            shape.append(int(s))
            dh_cell.append([float(x), float(y), float(z)])
        shape = tuple(shape)
        dh_cell = np.array(dh_cell)*Bohr

        # Skip atoms
        for i in range(natoms):
            f.readline()

        volume_data = []
        for line in f:
            line = list(map(float, splitLine(line)))
            volume_data.extend(line)

        volume_data = np.reshape(volume_data, shape)

    in_coords = np.mgrid[0:shape[0], 0:shape[1], 0:shape[2]]

    x, y, z = np.tensordot(dh_cell, in_coords, (0,0))
    x += origin[0]
    y += origin[1]
    z += origin[2]

    return volume_data, np.array((x, y, z)), dh_cell

def readCubeWavefunction(band, kn=1, spin=1, label='siesta', folder=None):
    sname = {1:'UP', 2:'DOWN'}[spin]
    filename = '%s.K%d.WF.%d.%s.' % (label, kn, band, sname)
    filenames = [filename + 'REAL.cube', filename + 'IMAG.cube']
    if folder is not None:
        filenames = [join(folder, filename) for filename in filenames]
    if spin==1 and not os.path.exists(filenames[0]):
        filenames = [filename.replace('.UP', '') for filename in filenames]

    wf_real, variables, dh_cell = readCube(filenames[0])
    wf_imag, variables, dh_cell = readCube(filenames[1])

    wf = wf_real + wf_imag*1j

    return wf, variables, dh_cell
