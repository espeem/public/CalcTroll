import os
import sys
import shutil
import numpy as np
from os.path import join
from ase.units import Ry, eV
from ase import io
from ase.io.trajectory import Trajectory

from CalcTroll import API
from CalcTroll.Core.Utilities import unique
from .SiestaIO import getFermiEnergy, readTimeUsage

meV = 0.001*eV

class ASESiestaElectronicStructure(API.Task):
    def __init__(
            self,
            system,
            method,
            parameters=API.DEFAULT,
            path=API.DEFAULT,
            identifier=API.DEFAULT,
            host=API.DEFAULT,
            inputs=tuple(),
            ):
        self.__system = system
        self.__method = method
        if parameters is API.DEFAULT:
            parameters=system.parameters()

        if path is API.DEFAULT:
            path = system.path()

        if identifier is API.DEFAULT:
            identifier = parameters.identifier()

        self.__parameters = parameters
        inputs = unique(list(inputs) + system.tasks())

        API.Task.__init__(
                self,
                path=path,
                identifier=identifier,
                host=host,
                inputs=inputs,
                )


    def parameters(self):
        return self.__parameters

    def executable(self):
        return {False:'siesta', True:'transiesta'}[self.hasOpenBoundaries()]

    def neededFiles(self):
        return ['*.traj', '*.fdf', '*.py', '*.psf', '*.DM']

    def resultFiles(self):
        return ['*.out', '*.XV', '*.EIG', '*.DM']

    def hasOpenBoundaries(self):
        return self.inputAtoms().hasOpenBoundaries()

    def system(self):
        return self.__system

    def method(self):
        return self.__method

    def estimate(self):
        NE = self.system().fullyUnrelaxed().atoms().number_of_valence_electrons()

        return float(NE**3)

    def numberOfValenceElectrons(self):
        return self.system().fullyUnrelaxed().atoms().number_of_valence_electrons()

    def environmentCommands(self):
        runfile = os.path.basename(self.runFile())
        tmp_outfile = 'TMP.out'
        outfile = os.path.basename(self.outFile())
        cmds = []

        method = self.method()
        program = method.program()

        python_executable = sys.executable
        executable = self.executable()
        pseudo_path = program.pseudoPath()

        cmd = "export SIESTA_PP_PATH=%s" % pseudo_path
        cmds.append(cmd)
        if self.numberOfValenceElectrons() < 20:
            cmd = """ASE_SIESTA_COMMAND='siesta < PREFIX.fdf > PREFIX.out'"""
            cmds.append(cmd)
        cmd = "export PROGRAM=%s" % python_executable
        cmds.append(cmd)

        return cmds

    def time(self):
        filename = join(self.runDirectory(), 'analysis.siesta.out')
        return readTimeUsage(filename)

    def kpts(self, atoms):
        kpts = np.ones(3, int)
        n_kpts = self.parameters().kpts()
        kpts[:len(n_kpts)] = n_kpts

        return kpts

    def makeCommands(self, form):
        commands = self.environmentCommands()
        if not isinstance(self.system(), API.Unrelaxed):
            dm_file = join(self.system().mainTask().runDirectory(), 'siesta.DM')
            commands.append('cp %s siesta.DM' % dm_file)
        commands.extend([
                form%(self.runFile(path=API.TAIL), 'TMP.out'),
                'mv TMP.out %s'%self.outFile(path=API.TAIL),
                'mv siesta.out analysis.siesta.out',
                'mv siesta.EIG analysis.siesta.EIG',
                'mv siesta.KP analysis.siesta.KP',
                ])

        return commands

    def fermiEnergy(self):
        return getFermiEnergy(join(self.runDirectory(), 'analysis.siesta.EIG'))

    def totalEnergy(self):
        return self.method().totalEnergy(self.runDirectory())

    def saveBehaviour(self):
        return {'hamiltonian': False, 'density_matrix':False}

    def inputAtoms(self):
        return self.system().atoms(parameters=self.parameters(), initialized=True, constrained=True)

    def write(self, additional_fdf=tuple()):
        additional_fdf = dict(additional_fdf)
        if not isinstance(self.system(), API.Unrelaxed):
            dm_file1 = join(self.system().mainTask().runDirectory(), 'siesta.DM')
            dm_file2 = join(self.runDirectory(), 'siesta.DM')
            if not dm_file1 == dm_file2:
                shutil.copy2(dm_file1, dm_file2)
        atoms = self.inputAtoms()
        kpts = self.kpts(atoms)
        fdf_parameters = self.method()._calculator().parameters['fdf_arguments']
        fdf_parameters['MaxSCFIterations'] = 1000
        for key, value in additional_fdf.items():
            fdf_parameters[key] = value

        with open(join(self.runDirectory(), 'SCRIPT'), 'w') as f:
            f.write(self.system().script())
        io.write(join(self.runDirectory(), 'input.xyz'), atoms)
        io.write('input.xyz', atoms)

        method = self.method().copy(fdf_arguments=fdf_parameters)

        method.writeOneShot(
                runfile=self.runFile(),
                atoms=atoms,
                kpts=kpts,
                save_behaviour=self.saveBehaviour(),
                )

    def _getLocalStatus(self):
        outfile = self.outFile(path=API.LOCAL)
        if not os.path.exists(outfile):
            return API.NOT_STARTED

        with open(outfile, 'r') as f:
             output = f.read()

        if 'RuntimeError:' in output:
            return API.FAILED
        else:
            return API.DONE

    def resultFilesForItem(self, item):
        return []

    def runFile(self, path=API.LOCAL):
        filename = 'ANALYSIS.' + self.method().runFileEnding()

        return join(self.runDirectory(path=path), filename)

    def outFile(self, path=API.LOCAL):
        filename = 'ANALYSIS.' + self.method().outFileEnding()

        return join(self.runDirectory(path=path), filename)





class FragmentOneShot(ASESiestaElectronicStructure):
    def __init__(
            self,
            item,
            part=0,
            ghosted=True,
            path=API.DEFAULT,
            ):
        self.__part = part
        self.__ghosted = ghosted
        if path is API.DEFAULT:
            path = item.path()

        ASESiestaElectronicStructure.__init__(
            self,
            system=item.system(),
            method=item.method(),
            path=path,
            identifier=None,
            )

    def write(self):
        atoms = self.inputAtoms()
        symbols = np.array(atoms.get_chemical_symbols())
        if self.__part == 0:
            keep_indices = self.system().moleculeIndices(atoms)
        else:
            keep_indices = self.system().slabIndices(atoms)

        if self.__ghosted:
            tags = np.ones(len(atoms), bool)
            tags[keep_indices] = 0
            symbols = np.unique(symbols[tags!=0])
            atoms.set_tags(tags)
            species = [Species(symbol=symbol, tag=1, ghost=True) for symbol in symbols]
        else:
            atoms = atoms[keep_indices]
            species = []

        kpts = self.kpts(atoms)

        method = self.method().copy(species=species)
        method.writeOneShot(
                runfile=self.runFile(),
                atoms=atoms,
                kpts=kpts,
                save_behaviour=self.saveBehaviour(),
                )

    def resultFiles(self):
        return ['*.out', '*.XV', '*.EIG', '*.DM']


class OpenSystemOneShot(ASESiestaElectronicStructure):
    def __init__(self,
            system,
            method,
            path=API.DEFAULT,
            identifier=API.DEFAULT,
            host=API.DEFAULT,
            ):
        inputs = system.tasks()
        ASESiestaElectronicStructure.__init__(self,
                system=system,
                method=method,
                path=path,
                identifier=identifier,
                host=host,
                inputs=inputs,
                )

    def saveBehaviour(self):
        return {'hamiltonian': True, 'density_matrix':False}

    def resultFiles(self):
        return ['*.out', '*.XV', '*.EIG', '*.TSHS', '*.fdf']




class ASESiestaCollectionRelaxation(API.RelaxationTask):
    def __init__(
            self,
            item,
            ):
        API.RelaxationTask.__init__(
                self,
                item,
                )

    def explain(self):
        basis_set = self.method().basisSet(full_description=True)
        energy_shift = self.method()['energy_shift']/meV
        xc = self.method()['xc']
        spin = self.method()['spin']
        spin = {'non-polarized': '', 'collinear': 'spin-polarized ', 'non-collinear': 'non-collinear spin', 'spin-orbit': 'full spin-orbit coupling'}[spin]
        citations =  [
                 "J. M. Soler, E. Artacho, J. D. Gale, A. Garcia, J. Junquera, P. Ordejon and D. Sanchez-Portal, J. Phys.:Condens. Matter, 2002, 14, 2745" ]

        if xc == 'PBE':
            xc = 'Perdew-Burke-Ernzerhof(PBE)'
            citations.append( "J. Perdew, K. Burke and M. Erzerhof, Phys. Rev. Lett., 1996, 77, 3865")

        force_tolerance = self.method()['force_tolerance']
        mesh_cutoff = self.method()['mesh_cutoff']/Ry
        t = (spin, basis_set, energy_shift, xc, mesh_cutoff, force_tolerance)
        return API.Explanation(
                "Geometric relaxations were performed using %sdensity-functional theory(DFT) using the SIESTA code.[*] We used a %s basis set with orbital radii defined using a %d meV energy shift. The %s exchange-correlation potential,[*] and a real-space grid equivalent to a %d Ry plane-wave cutoff. Forces were relaxed until forces were smaller than %.3f eV/Ang." % t,
                citations,
                )


    def runDirectory(self, path=API.LOCAL):
        base_run = API.RelaxationTask.runDirectory(self, path=path)

        if self.system().constraint() is None:
            path = base_run
        else:
            path =join(base_run, 'CCG_%s' % self.system().constraint().identifier())

        return path

    def executable(self):
        return 'siesta'

    def makeCommands(self, form):
        commands = self.environmentVariables()
        commands.append(form%(self.runFile(path=API.TAIL), 'TMP.out'))
        commands.append('mv TMP.out %s'%self.outFile(path=API.TAIL))
        commands.append('mv siesta.out %s' % self.siestaOutFile(path=API.TAIL))

        return commands

    def inputAtoms(self):
        atoms = self.system().atoms(parameters=self.parameters(), relaxed=False, initialized=True, constrained=True)

        return atoms

    def relaxation(self):
        if not self.isDone():
            return None

        pbc = self.system().pbc()
        atoms = self.method().relaxedAtoms(self.runDirectory(), pbc)

        return atoms

    def hasOpenBoundaries(self):
        return self.inputAtoms().hasOpenBoundaries()

    def _getLocalStatus(self):
        outfile = self.outFile(path=API.LOCAL)
        if not os.path.exists(outfile):
            return API.NOT_STARTED

        with open(outfile, 'r') as f:
             output = f.read()

        if 'RuntimeError:' in output:
            return API.FAILED
        else:
            return API.DONE


    def write(self):
        runfile = self.runFile()

        collection = self.system()
        kpts = collection.parameters().kpts()
        method = self.method()

        for i in range(len(collection)):
            atoms = collection.entry(i).atoms()
            atoms.purgeTags()
            inputfile = join(self.runDirectory(), 'input%d.traj' % i)
            write_traj(inputfile, atoms)

        c_string = 'calculator = ' + method.aseScript(kpts=kpts)
        path = os.path.dirname(runfile)
        eff_force_tolerance = method['force_tolerance']

        script =  """import os
from CalcTroll.ASEUtils.Trajectory import myread_traj, write_traj, Trajectory
from ase.constraints import UnitCellFilter
from ase.optimize import FIRE, BFGS
from ase import db
from ase.calculators.siesta.siesta import Siesta
from ase.calculators.siesta.parameters import Species
from CalcTroll.Plugins.Programs.Siesta.SiestaIO import convertXVtoASE
"""
        script += c_string
        script += """
for i in range(%d):""" % len(collection) + """
    relax_traj = 'relax%d.traj' % i
    if os.path.exists(relax_traj):
        continue
    atoms = myread_traj('input%d.traj' % i, -1)
    try:
        restart_atoms = myread_traj('relax.traj', -1)
    except:
        print('initializing from input.traj...')
    else:
        print('restarting from previous geometry...')
        atoms.positions = restart_atoms.positions
        atoms.cell = restart_atoms.cell
    atoms.set_calculator(calculator)
    opt = BFGS(atoms)
    traj = Trajectory('relax%d.traj' % i""" + """, 'w', atoms)
    opt.attach(traj)
    opt.run(fmax=%.5f)
"""%eff_force_tolerance

        with open(runfile, 'w') as f:
            f.write(script)

    def saveBehaviour(self):
        return {'hamiltonian': False, 'density_matrix':False}

    def siestaOutFile(self, path=API.TAIL):
        return join(self.runDirectory(path=path), 'relaxation.siesta.out')

    def neededFiles(self):
        return self.method().neededFiles()

    def resultFiles(self):
        return ['*.out', '*.XV', '*.EIG', '*.traj', '*.fdf']

    def _getLocalStatus(self):
        outfile = self.outFile(path=API.LOCAL)
        if not os.path.exists(outfile):
            return API.NOT_STARTED

        with open(outfile, 'r') as f:
             output = f.readlines()

        last_tolerance = 1000
        for line in output:
            line = line.split()
            if len(line) > 0:
                try:
                    last_tolerance = float(line[-1])
                except ValueError:
                    pass

        target_tolerance = self.method()['force_tolerance']
        if last_tolerance <= target_tolerance:
            return DONE
        else:
            return NOT_CONVERGED

class ASESiestaRelaxation(API.RelaxationTask):
    def __init__(
            self,
            item,
            ):
        API.RelaxationTask.__init__(
                self,
                item,
                )

    def explain(self):
        basis_set = self.method().basisSet(full_description=True)
        energy_shift = self.method()['energy_shift']/meV
        xc = self.method()['xc']
        spin = self.method()['spin']
        spin = {'non-polarized': '', 'collinear': 'spin-polarized ', 'non-collinear': 'non-collinear spin', 'spin-orbit': 'full spin-orbit coupling'}[spin]
        citations =  [
                 "J. M. Soler, E. Artacho, J. D. Gale, A. Garcia, J. Junquera, P. Ordejon and D. Sanchez-Portal, J. Phys.:Condens. Matter, 2002, 14, 2745" ]

        text = "Geometric relaxations were performed using %sdensity-functional theory(DFT) using the SIESTA code.[*] We used a %s basis set with orbital radii defined using a %d meV energy shift. The %s exchange-correlation potential,"
        if xc == 'PBE':
            xc = 'Perdew-Burke-Ernzerhof(PBE)'
            text += '[*]'
            citations.append( "J. Perdew, K. Burke and M. Erzerhof, Phys. Rev. Lett., 1996, 77, 3865")
        elif xc == 'LDA':
            xc = 'local density approximation (LDA)'
        else:
            print(xc)
            eeeeeeeeeeee

        force_tolerance = self.method()['force_tolerance']
        mesh_cutoff = self.method()['mesh_cutoff']/Ry
        t = (spin, basis_set, energy_shift, xc, mesh_cutoff, force_tolerance)
        text += " and a real-space grid equivalent to a %d Ry plane-wave cutoff. Forces were relaxed until forces were smaller than %.3f eV/Ang."
        text = text % t

        return API.Explanation(text, citations)

    def estimate(self):
        effort = ASESiestaElectronicStructure.estimate(self)
        relaxation_steps = 20

        return effort*relaxation_steps

    def runDirectory(self, path=API.LOCAL):
        path = API.RelaxationTask.runDirectory(self, path=path)

        return path

    def executable(self):
        return 'siesta'

    def numberOfValenceElectrons(self):
        return self.system().fullyUnrelaxed().atoms().number_of_valence_electrons()

    def environmentCommands(self):
        runfile = os.path.basename(self.runFile())
        tmp_outfile = 'TMP.out'
        outfile = os.path.basename(self.outFile())
        cmds = []

        method = self.method()
        program = method.program()

        python_executable = sys.executable
        executable = self.executable()
        pseudo_path = program.pseudoPath()

        cmd = "export SIESTA_PP_PATH=%s" % pseudo_path
        cmds.append(cmd)
        if self.numberOfValenceElectrons() < 20:
            cmd = """ASE_SIESTA_COMMAND='siesta < PREFIX.fdf > PREFIX.out'"""
            cmds.append(cmd)
        cmd = "export PROGRAM=%s" % python_executable
        cmds.append(cmd)

        return cmds

    def makeCommands(self, form):
        commands = self.environmentCommands()
        for name, force_tolerance, dm_tolerance, h_tolerance in self.preCalculations():
            fdf_name = name + '.' + self.method().runFileEnding()
            if os.path.exists(join(self.runDirectory(), fdf_name)):
                command = form%(fdf_name, 'TMP.out')
                commands.append(command)
                commands.append('mv TMP.out ' + name + '.out')

        commands.append(form%(self.runFile(path=API.TAIL), 'TMP.out'))
        commands.append('mv TMP.out %s'%self.outFile(path=API.TAIL))
        commands.append('mv siesta.out %s' % self.siestaOutFile(path=API.TAIL))

        return commands

    def inputAtoms(self):
        atoms = self.system().atoms(parameters=self.parameters(), relaxed=False, initialized=True, constrained=True)

        return atoms

    def relaxation(self):
        if not self.isDone():
            return None

        r_filename = join(self.runDirectory(), 'relax.traj')
        if not os.path.exists(r_filename):
            raise Exception("File %s missing" % r_filename)

        relaxation = Trajectory(r_filename)

        return relaxation

    def hasOpenBoundaries(self):
        return self.inputAtoms().hasOpenBoundaries()

    def _getLocalStatus(self):
        outfile = self.outFile(path=API.LOCAL)
        if not os.path.exists(outfile):
            return API.NOT_STARTED

        with open(outfile, 'r') as f:
             output = f.read()

        if 'RuntimeError:' in output:
            return API.FAILED
        else:
            return API.DONE

    def preCalculations(self):
        return [
                ('VERY_BAD', 1.0, 0.01, 1.0),
                ('BAD', 0.1, 0.001, 0.1),
                ('OK', 0.05, 0.0001, 0.01),
               ]

    def write(self):
        system = self.system()
        atoms = self.inputAtoms()
        kpts = system.parameters().kpts()
        method = self.method()
        io.write(join(self.runDirectory(), 'input.xyz'), atoms)

        for name, force_tolerance, dm_tolerance, h_tolerance in self.preCalculations():
            if force_tolerance < method['force_tolerance']:
                force_tolerance = method['force_tolerance']

            tmp_method = method.copy(force_tolerance=force_tolerance, dm_tolerance=dm_tolerance, h_tolerance=h_tolerance)
            ending = '.' + self.method().runFileEnding()
            filename = join(self.runDirectory(), name + ending)
            tmp_method.writeRelaxation(
                runfile=filename,
                atoms=atoms,
                kpts=kpts,
                relax_cell=self.system().relaxCell(),
                save_behaviour=self.saveBehaviour(),
                )

        method.writeRelaxation(
                runfile=self.runFile(),
                atoms=atoms,
                kpts=kpts,
                relax_cell=system.relaxCell(),
                save_behaviour=self.saveBehaviour(),
                )

    def saveBehaviour(self):
        return {'hamiltonian': False, 'density_matrix':False}

    def siestaOutFile(self, path=API.TAIL):
        return join(self.runDirectory(path=path), 'relaxation.siesta.out')

    def neededFiles(self):
        return self.method().neededFiles()

    def resultFiles(self):
        return ['*.out', '*.XV', '*.DM', '*.EIG', '*.traj', '*.fdf']

    def _getLocalStatus(self):
        outfile = self.outFile(path=API.LOCAL)
        if not os.path.exists(outfile):
            return API.NOT_STARTED

        with open(outfile, 'r') as f:
             output = f.readlines()

        last_tolerance = 1000
        for line in output:
            line = line.split()
            if len(line) > 0:
                try:
                    last_tolerance = float(line[-1])
                except ValueError:
                    pass

        target_tolerance = self.method()['force_tolerance']
        if last_tolerance <= target_tolerance:
            return API.DONE
        else:
            return API.NOT_CONVERGED

class SiestaRelaxation(API.RelaxationTask):
    def __init__(
            self,
            item,
            ):
        API.RelaxationTask.__init__(
                self,
                item,
                )

    def explain(self):
        basis_set = self.method().basisSet(full_description=True)
        energy_shift = self.method()['energy_shift']/meV
        xc = self.method()['xc']
        spin = self.method()['spin']
        spin = {'non-polarized': '', 'collinear': 'spin-polarized ', 'non-collinear': 'non-collinear spin', 'spin-orbit': 'full spin-orbit coupling'}[spin]
        citations =  [
                 "J. M. Soler, E. Artacho, J. D. Gale, A. Garcia, J. Junquera, P. Ordejon and D. Sanchez-Portal, J. Phys.:Condens. Matter, 2002, 14, 2745" ]

        text = "Geometric relaxations were performed using %sdensity-functional theory(DFT) using the SIESTA code.[*] We used a %s basis set with orbital radii defined using a %d meV energy shift. The %s exchange-correlation potential,"
        if xc == 'PBE':
            xc = 'Perdew-Burke-Ernzerhof(PBE)'
            text += '[*]'
            citations.append( "J. Perdew, K. Burke and M. Erzerhof, Phys. Rev. Lett., 1996, 77, 3865")
        elif xc == 'LDA':
            xc = 'local density approximation (LDA)'
        else:
            print(xc)
            eeeeeeeeeeee

        force_tolerance = self.method()['force_tolerance']
        mesh_cutoff = self.method()['mesh_cutoff']/Ry
        t = (spin, basis_set, energy_shift, xc, mesh_cutoff, force_tolerance)
        text += " and a real-space grid equivalent to a %d Ry plane-wave cutoff. Forces were relaxed until forces were smaller than %.3f eV/Ang."
        text = text % t

        return API.Explanation(text, citations)

    def estimate(self):
        effort = ASESiestaElectronicStructure.estimate(self)
        relaxation_steps = 20

        return effort*relaxation_steps

    def runDirectory(self, path=API.LOCAL):
        path = API.RelaxationTask.runDirectory(self, path=path)

        return path

    def executable(self):
        return 'siesta'

    def numberOfValenceElectrons(self):
        return self.system().fullyUnrelaxed().atoms().number_of_valence_electrons()

    def environmentCommands(self):
        runfile = os.path.basename(self.runFile())
        tmp_outfile = 'TMP.out'
        outfile = os.path.basename(self.outFile())
        cmds = []

        method = self.method()
        program = method.program()

        python_executable = sys.executable
        executable = self.executable()
        pseudo_path = program.pseudoPath()

        cmd = "export SIESTA_PP_PATH=%s" % pseudo_path
        cmds.append(cmd)
        if self.numberOfValenceElectrons() < 20:
            cmd = """ASE_SIESTA_COMMAND='siesta < PREFIX.fdf > PREFIX.out'"""
            cmds.append(cmd)
        cmd = "export PROGRAM=%s" % python_executable
        cmds.append(cmd)

        return cmds

    def makeCommands(self, form):
        commands = self.environmentCommands()
        for name, force_tolerance, dm_tolerance, h_tolerance in self.preCalculations():
            fdf_name = name + '.' + self.method().runFileEnding()
            if os.path.exists(join(self.runDirectory(), fdf_name)):
                command = form%(fdf_name, 'TMP.out')
                commands.append(command)
                commands.append('mv TMP.out ' + name + '.out')

        commands.append(form%(self.runFile(path=API.TAIL), 'TMP.out'))
        commands.append('mv TMP.out %s'%self.outFile(path=API.TAIL))
        commands.append('mv siesta.out %s' % self.siestaOutFile(path=API.TAIL))

        return commands

    def inputAtoms(self):
        parameters = self.parameters()
        atoms = self.system().atoms(parameters=parameters, relaxed=False, initialized=True, constrained=True)

        return atoms

    def relaxation(self):
        if not self.isDone():
            return None

        r_filename = join(self.runDirectory(), 'relax.traj')
        if not os.path.exists(r_filename):
            raise Exception("File %s missing" % r_filename)

        relaxation = Trajectory(r_filename)

        return relaxation

    def hasOpenBoundaries(self):
        return self.inputAtoms().hasOpenBoundaries()

    def _getLocalStatus(self):
        outfile = self.outFile(path=API.LOCAL)
        if not os.path.exists(outfile):
            return API.NOT_STARTED

        with open(outfile, 'r') as f:
             output = f.read()

        if 'RuntimeError:' in output:
            return API.FAILED
        else:
            return API.DONE

    def preCalculations(self):
        return [
                ('VERY_BAD', 1.0, 0.01, 1.0),
                ('BAD', 0.1, 0.001, 0.1),
                ('OK', 0.05, 0.0001, 0.01),
               ]

    def write(self):
        system = self.system()
        atoms = self.inputAtoms()
        kpts = system.parameters().kpts()
        method = self.method()
        with open(join(self.runDirectory(), 'SCRIPT'), 'w') as f:
            f.write(system.script())

        for name, force_tolerance, dm_tolerance, h_tolerance in self.preCalculations():
            tmp_method = method.copy(force_tolerance=force_tolerance, dm_tolerance=dm_tolerance, h_tolerance=h_tolerance)
            ending = '.' + self.method().runFileEnding()
            filename = join(self.runDirectory(), name + ending)
            tmp_method.writeInternalRelaxation(
                runfile=filename,
                atoms=atoms,
                kpts=kpts,
                relax_cell=self.system().relaxCell(),
                save_behaviour=self.saveBehaviour(),
                )

        method.writeInternalRelaxation(
                runfile=self.runFile(),
                atoms=atoms,
                kpts=kpts,
                relax_cell=system.relaxCell(),
                save_behaviour=self.saveBehaviour(),
                )

        with open(self.runFile(), 'a') as f:
            f.write("""print('CALCULATION FINISHED')""")


    def saveBehaviour(self):
        return {'hamiltonian': False, 'density_matrix':False}

    def siestaOutFile(self, path=API.TAIL):
        return join(self.runDirectory(path=path), 'relaxation.siesta.out')

    def neededFiles(self):
        return self.method().neededFiles()

    def resultFiles(self):
        return ['*.out', '*.XV', '*.EIG', '*.DM', '*.traj', '*.fdf', '.xyz']

    def _getLocalStatus(self):
        outfile = self.outFile(path=API.LOCAL)
        if not os.path.exists(outfile):
            return API.NOT_STARTED

        with open(outfile, 'r') as f:
             output = f.readlines()

        if output[-1].strip() == 'CALCULATION FINISHED':
            return API.DONE
        else:
            return API.NOT_CONVERGED

class TBTransRun(API.Task):
    def __init__(self,
                 transmission,
                 ):
        if hasattr(transmission.system(), 'mainTask'):
            path = transmission.system().mainTask().runDirectory()
        else:
            path = join(transmission.method().path(),
                        transmission.system().path())

        open_boundary = OpenSystemOneShot(
                system=transmission.system(),
                method=transmission.method(),
                path=path,
                identifier=None,
                )
        inputs = [open_boundary]
        self.__transmission = transmission
        API.Task.__init__(
                self,
                path=open_boundary.path(),
                identifier=open_boundary.identifier(),
                method=open_boundary.method(),
                host=open_boundary.host(),
                inputs=inputs,
                )

    def readData(self, item, **kwargs):
        files = os.listdir(self.runDirectory())
        start = 'siesta.TBT.AVTRANS'
        test = re.compile(start + '*')
        files = list(filter(test.search, files))
        results = OrderedDict()
        for filename in files:
            end = filename[len(start)+1:]
            fro, to = end.split('-')
            fro, to = [entry[3:] for entry in (fro, to)]
            filename = join(self.runDirectory(), filename)
            results[(fro, to)] = readTransmissionFile(filename)

        return results

    def neededFiles(self):
        return ['*.TSHS', '*.fdf']

    def resultFiles(self):
        return ['*.out', '*.TBT.*']

    def executable(self):
        return 'tbtrans'

    def hasOpenBoundaries(self):
        return True

    def energies(self):
        return self.__transmission.energies()

    def kpts(self):
        return self.__transmission.kpts()

    def write(self):
        energies = self.energies()
        kpts = np.ones(3)
        kpts[:len(self.kpts())] = self.kpts()
        script = """%include siesta.fdf
%block TBT.Contours
   tbt-1
%endblock
%block TBT.Contour.tbt-1
    part line""" + """
    from %.2f eV to %.2f eV
    delta %.3f eV
    method simpson-mix"""%(energies.start, energies.stop, energies.step) + """
%endblock

%block TBT.kgrid_Monkhorst_Pack""" + """
%d  0  0 0.0
0  %d  0 0.0
0  0  %d 0.0"""% tuple(kpts) + """
%endblock"""
        with open(self.runFile(), 'w') as f:
            f.write(script)

    def makeCommands(self, form):
        return [
                form % (self.runFile(path=API.TAIL), 'TMP.out'),
                'mv TMP.out %s' % self.outFile(path=API.TAIL),
                ]

    def runFile(self, path=API.LOCAL):
        filename = 'TRANSMISSION.fdf'
        return join(self.runDirectory(path=path), filename)

    def outFile(self, path=API.LOCAL):
        filename = 'TRANSMISSION.out'
        return join(self.runDirectory(path=path), filename)

    def _getLocalStatus(self):
        outfile = self.outFile(path=API.LOCAL)
        if not os.path.exists(outfile):
            return API.NOT_STARTED

        with open(outfile, 'r') as f:
             output = f.read()

        if 'RuntimeError:' in output:
            return API.FAILED
        else:
            return API.DONE
