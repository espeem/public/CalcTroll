import os
import numpy as np
from os.path import join
from CalcTroll import API

from CalcTroll.Core.Utilities import generateHash
from .SiestaIO import readEigenvalues, readKpoints
from .Tasks import ASESiestaElectronicStructure

class EnergySpectrumTask(ASESiestaElectronicStructure):
    def __init__(
            self,
            system,
            method=None,
            parameters=None,
            ):
        self.__parameters = parameters
        if isinstance(system, API.Relaxed):
            relaxation_task = system.tasks()[0]
            path = relaxation_task.runDirectory()
            inputs = [relaxation_task]
        elif isinstance(system, API.Unrelaxed):
            path = join(system.path(), method.path())
            inputs = []

        self.__hash = None

        ASESiestaElectronicStructure.__init__(
                self,
                system=system,
                method=method,
                parameters=parameters,
                path=path,
                inputs=inputs,
                )

    def parameters(self):
        return self.__parameters

    def hash(self):
        if self.__hash == None:
            self.__hash = generateHash(self.script())

        return self.__hash

    def runDirectory(self, path=API.LOCAL):
        path = API.RelaxationTask.runDirectory(self, path=path)

        return path

    def executable(self):
        return 'siesta'

    def numberOfValenceElectrons(self):
        return self.system().fullyUnrelaxed().atoms().number_of_valence_electrons()

    def explain(self):
        vac = self.parameters().padding()

        return API.Explanation("Final evaluation was performed with %d Ang of vacuum." % vac)

    def write(self):
        ASESiestaElectronicStructure.write(self, additional_fdf={'TimeReversalSymmetryForKpoints': False})

    def siestaOutFile(self, path=API.LOCAL):
        return join(self.runDirectory(path=path), 'analysis.siesta.out')

    def resultFiles(self):
        return [
                'analysis.siesta.out',
                'analysis.siesta.EIG',
                'analysis.siesta.KP',
                ]

    def read(self, path=API.LOCAL, read_bandnumbers=False, **kwargs):
        directory = self.runDirectory(path=path)
        filename = join(directory, 'analysis.siesta')

        EF, eigs = readEigenvalues(filename, **kwargs)
        kpts, wk = readKpoints(filename, **kwargs)
        sh = eigs.shape
        if read_bandnumbers:
            return EF, kpts, wk, eigs, np.arange(sh[1]) 
        else:
            return EF, kpts, wk, eigs

    def _getLocalStatus(self):
        status = ASESiestaElectronicStructure._getLocalStatus(self)
        if status is not API.DONE:
            return status

        for f in self.resultFiles():
            f = join(self.runDirectory(path=API.LOCAL), f)
            if not os.path.exists(f):
                return API.NOT_STARTED

        return API.DONE
