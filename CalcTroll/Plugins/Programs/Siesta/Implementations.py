from CalcTroll import API
from ase.constraints import FixAtoms
from CalcTroll.Core.Relaxation import Relaxed
from CalcTroll.Plugins.Analysis.EnergySpectrum import EnergySpectrum
from CalcTroll.Plugins.Analysis.Charges import Charges

from .Tasks import ASESiestaRelaxation, SiestaRelaxation
from .EnergySpectrumTask import EnergySpectrumTask
from .Siesta import Siesta

def shouldBeInternal(system):
    atoms = system.atoms()
    return all([isinstance(constraint, FixAtoms) for constraint in atoms.constraints])

class RelaxationImplementation(API.AnalysisImplementation):
    @classmethod
    def canHandle(cls, item):
        if not isinstance(item, Relaxed): return False
        if not isinstance(item.method(), Siesta): return False
        return True

    def makeTasks(self, items):
        tasks = []
        for item in items:
            if isinstance(item.system(), API.System):
                if shouldBeInternal(item.system()):
                    task = SiestaRelaxation(item=item)
                else:
                    task = ASESiestaRelaxation(item=item)
                tasks.append(task)

        return tasks

class EnergySpectrumImplementation(API.AnalysisImplementation):
    @classmethod
    def canHandle(cls, item):
        if not isinstance(item, EnergySpectrum): return False
        if not isinstance(item.method(), Siesta): return False
        return True

    def makeTasks(self, items):
        tasks = []
        for item in items:
            if isinstance(item.system(), API.System):
                task = EnergySpectrumTask(system=item.system(), method=item.method(), parameters=item.parameters())
                tasks.append(task)

        return tasks

    def read(self, item, **kwargs):
        assert item in self.items()
        return self.tasks()[0].read(**kwargs)

class ChargesImplementation(API.AnalysisImplementation):
    @classmethod
    def canHandle(cls, item):
        if not isinstance(item, Charges): return False
        if not isinstance(item.method(), Siesta): return False
        return True

    def makeTasks(self, items):
        tasks = []
        for item in items:
            if isinstance(item.system(), API.System):
                task = EnergySpectrumTask(system=item.system(), method=item.method(), parameters=item.parameters())
                tasks.append(task)

        return tasks

    def read(self, item, **kwargs):
        raise NotImplementedError