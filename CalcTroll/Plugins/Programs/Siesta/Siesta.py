# Written by Mads Engelund, 2017, http://espeem.com
import numpy as np
import os
import re
import sys
from collections import OrderedDict
from os.path import join

from ase import io
from ase.calculators.calculator import all_changes
from ase.calculators.siesta.siesta import Siesta as ASESIESTA
from ase.constraints import FixAtoms, FixedMode
from ase.units import Ry, eV
from ase import db as ASE_DB

from CalcTroll import API
from CalcTroll.API import DEFAULT, ALL, DONE, NOT_CONVERGED, FAILED, LOCAL
from CalcTroll.API import TAIL
from CalcTroll.Core.ParameterControl.ParameterControl import makeMinimalScript
from CalcTroll.Core.Utilities import vectorLength
from CalcTroll.ASEUtils.AtomUtilities import areAtomsEqual
from CalcTroll.ASEUtils.Atoms import Atoms
from CalcTroll.ASEUtils.Trajectory import write_traj


meV = 0.001*eV

FUNCTIONAL_SHORTHANDS = {
        ('LDA', 'PZ'): 'LDA',
        ('GGA', 'PBE'): 'PBE',
        ('GGA', 'PW91'): 'PW91',
        ('VDW', 'KBM'): 'OPTB88',
        }

SPIN_SHORTHANDS = {
        'non-polarized': 'F',
        'collinear': 'T',
        'non-collinear': 'NC',
        'spin-orbit': 'SO',
        }

VALENCE_DICT = {'Ge': 4, 'Si': 4, 'H': 1}

def makeIndicesGroup(indices):
    indices = np.unique(indices)

    groups=[]
    i=0
    while i<len(indices):
        j=i
        together=True
        while together and j+1<len(indices):
            if indices[j+1]-indices[j]!=1 or j+1==len(indices):
                break
            j+=1
        groups.append([indices[i],indices[j]])
        i=j+1

    return groups

def makeElectrodeConform(electrode):
    atoms = electrode.atoms()
    origin = -atoms.get_celldisp()
    atoms.positions += origin
    atoms.set_celldisp(np.zeros(3, float))

    return atoms

def uniqueElectrodes(regions):
    regions = [region for region in regions if region.isElectrode()]
    candidates = np.ones(len(regions), bool)
    matches = [[i] for i in range(len(regions))]
    for i, candidate in enumerate(regions):
        atoms_c = makeElectrodeConform(candidate)

    for i, candidate in enumerate(regions):
        atoms_c = makeElectrodeConform(candidate)
        for j, tester in enumerate(regions[i+1:]):
            atoms_t = makeElectrodeConform(tester)

            if areAtomsEqual(atoms_c, atoms_t):
                candidates[i + j + 1] = False
                matches[i].append(i + j + 1)

    result = []
    for i, region in enumerate(regions):
        if candidates[i]:
            names = [regions[j].name() for j in matches[i]]
            result.append((region, names))

    return result

class SOLUTION_METHOD:
    @classmethod
    def text(cls):
        raise Exception

class DIAGONALIZATION(SOLUTION_METHOD):
    @classmethod
    def text(cls):
        return 'SolutionMethod diagon\n'

class TRANSIESTA_SOLUTION_METHOD(SOLUTION_METHOD):pass

class TRIDIAGONAL(TRANSIESTA_SOLUTION_METHOD):
    @classmethod
    def text(cls):
        return 'SolutionMethod transiesta\n'

class MUMPS(TRANSIESTA_SOLUTION_METHOD):
    @classmethod
    def text(cls):
        return """SolutionMethod transiesta
TS.SolutionMethod MUMPS
TS.MUMPS.BlockingFactor 120
"""

class SiestaCalculator(ASESIESTA):
    def read_eigenvalues(self):
        pass

    def read_hsx(self):
        self.results['hsx'] = None

    def write_input(self, atoms, properties=None, system_changes=None):
        result = ASESIESTA.write_input(self, atoms=atoms, properties=properties, system_changes=system_changes)

        #if len(atoms.electrodes())>0:
        #    runfile = self.label + '.fdf'
        #    self.writeTS(runfile, atoms)

        return result

    def make_electrodes(self, atoms):
        unique_electrodes = uniqueElectrodes(atoms.electrodes())
        electodes = []
        for electrode, names in unique_electrodes:
            electrode_atoms = makeElectrodeConform(electrode)
            s_dir = electrode.semiInfiniteDirection()
            parameters = self.parameters.copy()
            label = '_'.join(names)
            label = 'EL_%s' % label
            parameters['label'] = label

            kpts = np.array(parameters['kpts'] )
            l = vectorLength(electrode.cell()[s_dir])
            k = int(np.ceil(400/l))
            kpts[s_dir] = k
            parameters['kpts'] = tuple(kpts)

            electrode_calculator = self.__class__(**parameters)
            electrode_atoms.set_calculator(electrode_calculator)
            electodes.append(electrode_atoms)

        return electodes

    def writeTS(self, path, atoms):
        with open(path, 'a') as f:
            f.write(self.writeSolutionMethod(atoms))
            f.write(self.writeChemicalPotentials(atoms))
            f.write(self.writeElectrodes(atoms))
            f.write(self.writeBuffer(atoms))
            f.write(self.writeContours(atoms))

    @classmethod
    def writeSolutionMethod(cls, atoms):
        if atoms.hasOpenBoundaries():
            return TRIDIAGONAL.text()
        else:
            return DIAGONALIZATION.text()

    @classmethod
    def writeChemicalPotentials(cls, atoms):
        electrodes = atoms.electrodes()
        potentials = np.array([el.voltage() for el in electrodes])

        script = """#Define all the different chemical potentials
"""

        script += "%block TS.ChemPots\n"
        for electrode in electrodes:
            script += 'U_%s\n'%electrode.name()
        script += "%endblock TS.ChemPots\n"
        for electrode in electrodes:
            script += '%block TS.ChemPot.U_' + electrode.name() +'\n'
            script += """mu %.8f eV""" % electrode.voltage() + """
  contour.eq
    begin
      c-%s
      t-%s
    end""" % (electrode.name(), electrode.name()) + """
%endblock

"""
        return script

    def writeElectrodes(self, atoms):
        electrodes = atoms.electrodes()
        names = [electrode.name() for electrode in electrodes]
        s_names = ['EL_' + str(name) for name in names]

        script = """%block TS.Elecs
""" + '\n'.join(s_names) + """
%endblock TS.Elecs

"""
        unique_electrodes = uniqueElectrodes(atoms.electrodes())
        unique_electrodes = [entry[1] for entry in unique_electrodes]

        for names in unique_electrodes:
            for name in names:
                electrode = atoms.region(name)
                sd = electrode.semiInfiniteDirection()
                pbc = electrode.pbc()[sd]
                semi_input = {(False, True): '+', (True, False):'-'}[pbc] + 'a' + str(sd+1)
                start = electrode.start() + 1
                name = electrode.name()
                TSHS_name = '_'.join(names)
                TSHS_name = 'EL_%s.TSHS'%TSHS_name
                s_name = 'EL_%s'%name
                cp_name = 'U_%s'%name
                script += """%block TS.Elec.""" + s_name + """
  TSHS %s
  chem-pot %s
  semi-inf-dir %s
  elec-pos begin %d"""%(TSHS_name, cp_name, semi_input, start) + """
%endblock TS.Elec.""" + s_name + """

"""

        return script

    @classmethod
    def writeContours(cls, atoms):
        script = """
TS.Contours.nEq.Eta   0.00001 eV"""
        script += """
# Define contours for contour integrals.
TS.Contours.Eq.Pole.N 10
"""

        for electrode in atoms.electrodes():
            script += """%block""" + """ TS.Contour.c-%s
  part circle
   from  -26.00000 eV + %.5f eV to -10. kT + %.5f eV
    points 70
     method g-legendre""" % (electrode.name(), electrode.voltage(), electrode.voltage()) + """
%endblock""" + """ TS.Contour.c-%s
""" % electrode.name()
            script += '%' + """block TS.Contour.t-%s
  part tail
   from prev to inf
    points 10
     method g-fermi""" % electrode.name() + """
%endblock""" + """ TS.Contour.t-%s
""" % electrode.name()

        potentials = np.array([el.voltage() for el in atoms.electrodes()])
        vdiff = potentials.max() - potentials.min()

        if vdiff > 0.0:
            script += """
%block TS.Contours.Bias.Window
  neq
%endblock TS.Contours.Bias.Window
%block TS.Contour.Bias.Window.neq
  part line
   from -|V|/2 to |V|/2
    delta 0.01 eV
     method simpson-mix
%endblock TS.Contour.Bias.Window.neq

%block TS.Contours.Bias.Tail
  neq-tail
%endblock TS.Contours.Bias.tail
%block TS.Contour.Bias.Tail.neq-tail
  part tail
   from 0. kT to 12. kT
    delta 0.01 eV
     method simpson-mix
%endblock TS.Contour.Bias.Tail.neq-tail

"""
        return script

    @classmethod
    def writeBuffer(cls, atoms):
        buffers = atoms.buffers()
        if len(buffers) == 0:
            return ''

        buffer_indices = []
        for buf in buffers:
            buffer_indices.extend(buf.indices())
        buffer_indices = np.array(buffer_indices) + 1

        groups = makeIndicesGroup(buffer_indices)
        if len(groups) > 0:
            script = """%block TS.Atoms.Buffer
"""
            for group in groups:
                script += 'atom from %d to %d\n'%(group[0],group[1])


            script += """%endblock TS.Atoms.Buffer
"""
        else:
            script = ''
        return script

class Species(API.Parameters):
    def __init__(
            self,
            symbol,
            basis_set='DZP',
            pseudopotential=None,
            tag=None,
            ghost=False,
            ):
        pass

class PAOBasisBlock(API.Parameters):
    """
    Representing a block in PAO.Basis for one species.
    """
    def __init__(self, block):
        """
        Parameters:
            -block : String. A block defining the basis set of a single
                     species using the format of a PAO.Basis block.
                     The initial label should be left out since it is
                     determined programatically.
                     Example1: 2 nodes 1.0
                               n=2 0 2 E 50.0 2.5
                               3.50 3.50
                               0.95 1.00
                               1 1 P 2
                               3.50
                     Example2: 1
                               0 2 S 0.2
                               5.00 0.00
                     See siesta manual for details.
        """
        assert isinstance(block, str)

class Siesta(API.Program):
    fdf_defaults = (
                    ('MinSCFIterations', 1),
                    ('MaxSCFIterations', 300),
                    ('DM.MixingWeight', 0.02),
                    ('DM.KickMixingWeight', 0.1),
                    ('DM.NumberPulay', 5),
                    ('DM.NumberKick', 50),
                    ('DM.MixSCF1', True),
                    ('DM.UseSaveDM', True),
                   )
    @staticmethod
    def findImplementation(item):
        return findImplementation(item)

    def __init__(self,
            mesh_cutoff=200*Ry,
            energy_shift=100*meV,
            basis_set='SZ',
            xc='LDA',
            force_tolerance=0.04,
            spin='non-polarized',
            dm_tolerance=0.0001,
            h_tolerance=0.01,
            version=None,
            species=tuple(),
            fdf_arguments=API.DEFAULT,
            ):
        kwargs = locals()
        kwargs['fdf_arguments'] = dict(self.fdf_defaults)
        if fdf_arguments is API.DEFAULT:
            pass
        elif isinstance(fdf_arguments, dict):
            kwargs['fdf_arguments'].update(dict(fdf_arguments))
        else:
            raise Exception

        kwargs.pop('self')

        kwargs.pop('force_tolerance')
        kwargs.pop('dm_tolerance')
        kwargs.pop('h_tolerance')
        kwargs.pop('version')

        kwargs['fdf_arguments']['SCF.DM.Tolerance'] = dm_tolerance
        kwargs['fdf_arguments']['SCF.H.Tolerance'] = (h_tolerance, 'eV')
        kwargs['fdf_arguments']['WriteMullikenPop'] = 1

        self.__basis_set = basis_set
        self.__version = version
        self.__ase_calculator = SiestaCalculator(**kwargs)

    def _calculator(self):
        return self.__ase_calculator

    def getDictionary(self):
        dictionary = dict(self.__ase_calculator.parameters)
        dictionary['fdf_arguments'] = dict(dictionary['fdf_arguments'])

        return dictionary

    def aseScript(self, kpts):
        dictionary = self.getDictionary()
        dictionary.pop('pseudo_qualifier')
        dictionary.pop('pseudo_path')
        dictionary.pop('label')
        dictionary.pop('atoms')
        dictionary['kpts'] = kpts
        string = makeMinimalScript('Siesta', dictionary, detail_level=API.ALL) + '\n'

        return string

    @classmethod
    def charges(cls, path):
        return extractMullikenCharge(join(path, 'siesta.out'))

    @classmethod
    def totalEnergy(cls, path, filename='siesta.out'):
        filename = join(path, filename)

        return readTotalEnergy(filename)

    @classmethod
    def getFermiEnergy(self, directory, label='siesta'):
        return getFermiEnergy(join(directory, label+'.EIG'))

    def executable(self):
        return 'siesta'

    def version(self):
        return self.__version

    def basisSet(self, full_description=False):
        if full_description:
            return {
                    'DZP': 'double-zeta polarized(DZP)',
                    'SZ': 'single-zeta polarized(SZ)',
                    }[self.__basis_set]
        else:
            return self.__basis_set

    def pseudoPath(self):
        return join(
                os.path.dirname(__file__),
                'pseudopotentials',
                'UOR',
                )

    def writeWaveFunctions(self, band_range=(1, 1)):
        method = self.copy()
        assert id(method) != id(self)
        parameters = method.__ase_calculator.parameters
        fdf_arguments = parameters['fdf_arguments']
        fdf_arguments['WFS.Write.For.Bands'] = True
        fdf_arguments['WFS.Band.Min'] = band_range[0] + 1
        fdf_arguments['WFS.Band.Max'] = band_range[1] + 1
        fdf_arguments['WriteDenchar'] = True
        fdf_arguments['COOP.Write'] = True
        fdf_arguments['TimeReversalSymmetryForKpoints'] = False
        method.__ase_calculator.set(**parameters)

        return method

    def writeDensity(self):
        method = self.copy()
        parameters = method.__ase_calculator.parameters
        fdf_arguments = parameters['fdf_arguments']
        fdf_arguments['SaveRho'] = True
        method.__ase_calculator.set(**parameters)

        return method

    def readDensity(self, label='siesta', d_type='nc'):
        if d_type == 'nc':
            dirname = os.path.dirname(label)
            filename = join(dirname, 'Rho.grid.nc')

            return readNCDensity(filename)
        else:
            raise ValueError

    @classmethod
    def runFileEnding(cls):
        return 'py'

    @classmethod
    def outFileEnding(cls):
        return 'out'

    def neededFiles(self):
        return ['*.fdf', '*.py', '*.traj']

    def resultFiles(self):
        return ['*.XV', '*.traj', '*.out', '*.EIG']

    def checkIfFinished(self, filename, runfile=None):
        if not os.path.exists(filename):
            return API.NOT_STARTED

        good = False
        with open(filename, 'r') as f:
            for line in f:
                good = 'BFGS' in line
        if good:
            final = float(line.split(' ')[-1])
            if final <= self['force_tolerance']:
                return DONE
            else:
                return NOT_CONVERGED
        else:
            return FAILED

    def makeSpecies(self, atoms):
        return []

    def writeRelaxation(self,
              runfile,
              atoms,
              kpts,
              relax_cell,
              save_behaviour,
              ):
        atoms = atoms.copy()

        net_charge = 0
        for doping in atoms.dopings():
            net_charge += doping.charge()

        # Remove electrodes to avoid transiesta relaxation (for now).
        atoms.purgeTags()

        species = self.makeSpecies(atoms)
        path = os.path.dirname(runfile)
        filename = join(path, 'input.traj')
        write_traj(filename, atoms)

        c_string = 'calculator = '
        dictionary = self.getDictionary()
        dictionary['fdf_arguments']['NetCharge'] = net_charge
        dictionary.pop('pseudo_qualifier')
        dictionary.pop('pseudo_path')
        dictionary.pop('label')
        dictionary.pop('atoms')
        dictionary['kpts'] = kpts
        dictionary['species'] = species
        c_string += makeMinimalScript('Siesta', dictionary, detail_level=API.ALL) + '\n'
        path = os.path.dirname(runfile)


        eff_force_tolerance = self['force_tolerance']


        script =  """from ase import io
from CalcTroll.ASEUtils.Trajectory import myread_traj, write_traj, Trajectory
from ase.constraints import UnitCellFilter
from ase.optimize import FIRE, BFGS
from ase.calculators.siesta.siesta import Siesta
from ase.calculators.siesta.parameters import Species, PAOBasisBlock
from CalcTroll.Plugins.Programs.Siesta.SiestaIO import relaxASEFromXV
"""
        script += c_string
        script += """
atoms = myread_traj('input.traj', -1)
try:
    atoms = relaxASEFromXV(atoms, 'siesta.XV')
except:
    print('initializing from input.traj...')
else:
    print('restarting from previous geometry...')
atoms.set_calculator(calculator)
"""

        if relax_cell:
            pbc = tuple(atoms.get_pbc())
            if pbc == (False, False, True):
                mask = (False, False, True, False, False, False)
            elif pbc == (True, False, False):
                mask = (True, False, False, False, False, False)
            elif pbc == (True, True, True):
                mask = None
            script += """
relax = UnitCellFilter(atoms, mask=%s)
""" % str(mask)

        else:
            script += "relax = atoms\n"

        script += """
opt = BFGS(relax)
traj = Trajectory('relax.traj', 'w', atoms)
opt.attach(traj)
opt.run(fmax=%.5f)
io.write('relax.xyz', atoms)
"""%eff_force_tolerance

        with open(runfile, 'w') as f:
            f.write(script)


    def writeInternalRelaxation(self,
              runfile,
              atoms,
              kpts,
              relax_cell,
              save_behaviour,
              ):
        atoms = atoms.copy()
        path = os.path.dirname(runfile)
        filename = join(path, 'input.traj')
        write_traj(filename, atoms)
        species = self.makeSpecies(atoms)
        filename = join(path, 'input.xyz')
        io.write(filename, atoms)

        dictionary = self.getDictionary()
        c_string = 'calculator = '
        if relax_cell:
            dictionary['fdf_arguments']['MD.VariableCell'] = True
            mini = np.inf
            for v in atoms.cell[atoms.pbc]:
                mini = min(vectorLength(v), mini)
            dictionary['fdf_arguments']['MD.PreconditionVariableCell'] = (mini, 'Ang')

        assert len(atoms.constraints) <= 1
        if len(atoms.constraints) == 1:
            constraint = atoms.constraints[0]
            constraint = ['atom %d' % (i + 1) for i in constraint.get_indices()]
            dictionary['fdf_arguments']['Geometry.Constraints'] = constraint
        dictionary['fdf_arguments']['MD.TypeOfRun'] = 'FIRE'
        dictionary['fdf_arguments']['MD.Steps'] = 500
        dictionary['fdf_arguments']['MD.MaxForceTol'] = (self['force_tolerance'], 'eV/Ang')
        dictionary.pop('pseudo_qualifier')
        dictionary.pop('pseudo_path')
        dictionary.pop('label')
        dictionary.pop('atoms')
        dictionary['kpts'] = kpts
        dictionary['species'] = species
        c_string += makeMinimalScript('Siesta', dictionary, detail_level=API.ALL) + '\n'
        path = os.path.dirname(runfile)
        filename = join(path, 'input.traj')

        script =  """from CalcTroll.ASEUtils.Trajectory import myread_traj, write_traj, Trajectory
from CalcTroll.Plugins.Programs.Siesta.SiestaIO import relaxASEFromXV
from CalcTroll.Plugins.Programs.Siesta.Siesta import SiestaCalculator as Siesta
from ase.calculators.siesta.parameters import Species, PAOBasisBlock
from ase import io
"""
        script += c_string
        script += """
atoms = myread_traj('input.traj', -1)

try:
    atoms = relaxASEFromXV(atoms, 'siesta.XV')
except:
    print('Starting from scratch')
    atoms = myread_traj('input.traj', -1)
else:
    print('Starting from previous calculation')

calculator.calculate(atoms=atoms)
final_atoms = relaxASEFromXV(atoms, 'siesta.XV')
io.write('relax.xyz', atoms)
write_traj('relax.traj', atoms)
"""

        with open(runfile, 'w') as f:
            f.write(script)

    def writeNEB(
            self,
            runfile,
            neb,
            kpts,
            ):
        dictionary = self.getDictionary()
        dictionary.pop('pseudo_qualifier')
        dictionary.pop('pseudo_path')
        dictionary.pop('label')
        dictionary.pop('atoms')
        dictionary['kpts'] = kpts
        c_string = 'calculator = '
        c_string += makeMinimalScript('Siesta', dictionary, detail_level=ALL) + '\n'
        path = os.path.dirname(runfile)

        n_images = len(neb.images)
        filename = join(path, 'neb.traj')
        write_traj(filename, neb.images)
        script =  """from CalcTroll.ASEUtils.Trajectory import myread_traj, write_traj, Trajectory
from ase import io
from ase.neb import NEB
from ase.optimize import FIRE
from CalcTroll.Plugins.Programs.Siesta.Siesta import SiestaCalculator as Siesta
"""
        script += """calculator = %s""" % c_string + """
kwargs = calculator.parameters.copy()
images = []
for i in range(%d):""" % n_images + """
    kwargs['label'] = 'siesta_%d' % i
    calculator = Siesta(**kwargs)
    image = myread_traj('neb.traj', i)
    image.set_calculator(calculator)
    image.get_total_energy()
    images.append(image)
""" + """
k=0.5
neb = NEB(images, k=k, climb=False)
qn = FIRE(neb, trajectory='neb.traj')
qn.run(fmax=%.3f)
""" % self['force_tolerance']


        with open(runfile, 'w') as f:
            f.write(script)

    def calculatorScript(
            self,
            atoms,
            kpts,
            species,
            min_iterations=True,
            save_behaviour=None,
            ):
        net_charge = 0
        for doping in atoms.dopings():
            net_charge += doping.charge()
        c_string = 'calculator = '
        dictionary = self.getDictionary()
        dictionary['fdf_arguments']['NetCharge'] = net_charge
        dictionary.pop('pseudo_qualifier')
        dictionary.pop('pseudo_path')
        dictionary.pop('label')
        dictionary.pop('atoms')
        if not min_iterations:
            dictionary['fdf_arguments']['MinSCFIterations'] = 1
        dictionary['kpts'] = kpts
        dictionary['species'] = species
        c_string += makeMinimalScript('Siesta', dictionary, detail_level=API.ALL) + '\n'

        return c_string

    def writeOneShot(self,
              runfile,
              atoms,
              kpts,
              save_behaviour=None,
              min_iterations=True,
              ):
        species = self.makeSpecies(atoms)
        c_string = self.calculatorScript(
                atoms,
                kpts=kpts,
                species=species,
                min_iterations=min_iterations,
                save_behaviour=save_behaviour,
                )
        path = os.path.dirname(runfile)
        filename = join(path, 'analysis.traj')
        write_traj(filename, atoms)

        script =  """from CalcTroll.ASEUtils.Trajectory import myread_traj, write_traj, Trajectory
from ase.constraints import UnitCellFilter
from CalcTroll.Plugins.Programs.Siesta import SiestaCalculator as Siesta
from ase.calculators.siesta.parameters import Species, PAOBasisBlock
from ase.calculators.siesta.parameters import Species
"""
        script += c_string
        script += """
atoms = myread_traj('analysis.traj', -1)
atoms.set_calculator(calculator)
"""

        script += """
atoms.get_total_energy()
"""

        with open(runfile, 'w') as f:
            f.write(script)

    def evaluateBandstructure(self, k_path):
        point, name = k_path[0]
        ka, kb, kc = point
        path = [(1, ka, kb, kc, name)]
        for point, name in k_path[1:]:
             ka, kb, kc = point
             path.append((100, ka, kb, kc, name))

        dictionary = self.getDictionary()
        dictionary['fdf_arguments']['BandLinesScale']='ReciprocalLatticeVectors'
        dictionary['fdf_arguments']['BandLines'] = path
        eeeeeeeeeeeeeeeeeeeeeee
        self.__ase_calculator.set(**dictionary)

    def constraintsString(self, constraints):
        string = 'constraints = []\n'
        for constraint in constraints:
            if isinstance(constraint, FixAtoms):
                import_string="from ase.constraints import FixAtoms\n"
                c_string = """FixAtoms(mask=%s)"""%np.array(constraint.index, int).tolist()
            elif isinstance(constraint, FixedMode):
                import_string="from ase.constraints import FixedMode\n"
                c_string = repr(constraint)
            else:
                eeeeeeeeeee
            string += import_string
            string += 'constraints.append(' + c_string + ')\n'

        return string
