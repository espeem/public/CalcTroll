# Written by Mads Engelund, 2017, http://espeem.com
# Defines input energy levels that cannot be defined before the electronic structure has been calculated.
# For instance, a user can specify the HOMO level of a molecule as an input before the calculation of the energy levels
# have been performed.
import numpy as np

from CalcTroll import API
from CalcTroll.Core.Utilities import isSpinPolarized
from CalcTroll.Plugins.Analysis import EnergySpectrum
from CalcTroll.Plugins.Analysis import BandStructure

TRIVIAL_ENERGY = 1e-2
HIGHEST_ALLOWED_ENERGY_LEVEL = 5

def convertEnergy(level, PDOS, EFs):
    if len(PDOS) <= 1:
        return level

    for i in range(len(PDOS) - 1):
        EF = EFs[i]
        i1, i2 = PDOS[i].system().referenceEnergyMatchIndices(PDOS[i].parameters())
        reference1 = PDOS[i].reference(indices=i1, EF=EF)
        reference2 = PDOS[i + 1].reference(indices=i2, EF=EF)

        level += reference1 - reference2

    return level


class EnergyLevel(API.NoTasksAnalysis):
    """ Abstract base class of energy specifications that cannot be determined before a calculations has been performed.
    """
    def __init__(self,
            offset=0.0,
            system=None,
            method=API.DEFAULT,
            parameters=API.DEFAULT,
            inputs=tuple(),
            ):
        self.__offset = offset
        self.__level = None

        API.NoTasksAnalysis.__init__(
                self,
                system=system,
                method=method,
                parameters=parameters,
                inputs=inputs,
                )

    def readData(self):
        if self.system() is None:
            raise ValueError("%s cannot be determined because no system was specified.")
        return self.level()

    def explain(self):
        return API.Explanation('')

    def level(self):
        if self.__level is None:
            self.__level = self._determine()

        return self.__level

    def offset(self):
        return self.__offset

    def _determine(self):
        """ Given the electronic eigenvalues, determine the energy level.

        :return A concrete energy (float)
        """
        raise NotImplementedError


class EnergyLevelFromEnergySpectrum(EnergyLevel):
    """ Abstract base class of energy specifications that cannot be determined before a calculations has been performed.
    """
    def __init__(self,
            offset=0.0,
            system=None,
            method=API.DEFAULT,
            parameters=API.DEFAULT,
            inputs=tuple(),
            ):
        self.__es = EnergySpectrum(
                system=system,
                method=method,
                parameters=parameters,
                )
        inputs = list(inputs) + [self.__es]

        EnergyLevel.__init__(
                self,
                system=system,
                method=method,
                parameters=parameters,
                inputs=inputs,
                )

    def energySpectrum(self):
        return self.__es


class BandGapLevel(EnergyLevelFromEnergySpectrum):
    def __init__(self, es=None, offset=0):
        EnergyLevelFromEnergySpectrum.__init__(self, es, offset)
        if es is not None:
            pdos = self.PDOS()[-1]
            bs = BandStructure(
                    system=pdos.system(),
                    method=pdos.method(),
                    parameters=pdos.parameters(),
                    )
            self.addInput(bs)
            self.__bs = bs

    def bandStructure(self):
        return self.__bs


class ValenceBandMaximum(BandGapLevel):

    def title(self):
        """
        :return A text representation of the energy level.
        """
        return 'VBM_%.2f' % self.offset()

    def _determine(self):
        """
        Given the electronic eigenvalues, determine the energy level.

        :return: A concrete energy (float)
        """
        """ Given the electronic eigenvalues, determine the energy level. """
        if not self.isDone():
            return None

        level = self.bandStructure().read()[-1]
        level = convertEnergy(level, self.PDOS())

        return level + self.offset()

class ConductionBandMinimum(BandGapLevel):
    """ Input parameter specifying the minimum of the conduction band."""

    def title(self):
        """
        :return A text representation of the energy level.
        """
        return 'CBM_%.2f' % self.offset()

    def _determine(self):
        """
        Given the electronic eigenvalues, determine the energy level.

        :return: A concrete energy (float)
        """
        """ Given the electronic eigenvalues, determine the energy level. """
        if not self.isDone():
            return None

        level = self.bandStructure().read()[0]
        level = convertEnergy(level, self.PDOS())

        return level + self.offset()


class MidGapLevel(EnergyLevelFromEnergySpectrum):
    """ Input parameter specifying the energy level in the middle of the gap."""

    def _determine(self):
        """
        Given the electronic eigenvalues, determine the energy level.

        :return: A concrete energy (float)
        """
        """ Given the electronic eigenvalues, determine the energy level. """
        if not self.isDone():
            return None
        es = self.energySpectrum()
        EF, kpts, wk, eigs = es.read()
        homo_eigs = sorted(eigs[eigs < EF + TRIVIAL_ENERGY])
        lumo_eigs = sorted(eigs[eigs > EF - TRIVIAL_ENERGY])

        level = (homo_eigs[-1] + lumo_eigs[0])/2.

        return level + self.offset()


class MO(EnergyLevelFromEnergySpectrum):
    def __init__(
            self,
            offset=0.0,
            include=API.DEFAULT,
            system=None,
            method=API.DEFAULT,
            parameters=API.DEFAULT,
            ):
        assert include in [True, False, API.DEFAULT]
        self.__include = include
        EnergyLevelFromEnergySpectrum.__init__(
            self,
            offset=offset,
            system=system,
            method=method,
            parameters=parameters,
            )

    def include(self):
        return self.__include

    def degeneracy(self, spin_resolved=False):
        es = self.energySpectrum()
        if es is None:
            return None

        EF, eigs = es.read()
        level = self._determine_normal()
        if isinstance(level, API.CalcTrollException):
            return level

        if not spin_resolved:
            return (abs(eigs - level) < TRIVIAL_ENERGY).sum()

        sp1 = (abs(eigs[:,:,0] - level) < TRIVIAL_ENERGY).sum()
        sp2 = (abs(eigs[:,:,1] - level) < TRIVIAL_ENERGY).sum()

        return sp1, sp2

    def _determine(self):
        if not self.include() in [True, False]:
            raise ValueError(
"To determine the level the include keyword must either be True/False. Got %s" % self.include())

        if self.include():
            return self._determine_include()
        else:
            return self._determine_normal()

    def _determine_normal(self):
        raise NotImplementedError

    def _determine_include(self):
        raise NotImplementedError

class LUMO(MO):
    """ Input parameter specifying the Lowest Unoccupied Molecular Orbital."""
    def __gt__(self, other):
        if isinstance(other, HOMO):
            return True

        return self.__plus > other.__plus

    def __init__(
            self,
            plus=0,
            offset=0.0,
            include=API.DEFAULT,
            highest_allowed=HIGHEST_ALLOWED_ENERGY_LEVEL,
            system=None,
            method=API.DEFAULT,
            parameters=API.DEFAULT,
            ):
        """
        :param plus: Integer specifying to chose either LUMO(plus=0) or a higher energy level LUMO+X(plus=X)
        :param offset: Adjust the target energy up or down, for instance 0.1 eV above LUMO level.
        """
        assert isinstance(plus, int) and plus>=0
        self.__plus = plus
        self.__highest_allowed = highest_allowed
        MO.__init__(
                self,
                offset=offset,
                include=include,
                system=system,
                method=method,
                parameters=parameters,
                )

    def higher(self):
        return self.copy(plus=self.__plus + 1)

    def lower(self):
        if self.__plus > 0:
            return self.copy(minus=self.__plus - 1)
        else:
            return HOMO(
                    system=self.system(),
                    method=self.method(),
                    parameters=self.parameters(),
                    minus=0,
                    offset=self.offset(),
                    )

    def title(self):
        """
        :return A text representation of the energy level.
        """
        if self.__plus != 0:
            return 'LUMO_+%d' % self.__plus
        else:
            return 'LUMO'

    def _determine_include(self):
        own_level = self._determine_normal()
        if isinstance(own_level,
                      API.InconsistentApproximation):
            return own_level

        other_level = self.higher()._determine_normal()
        if isinstance(other_level,
                      API.InconsistentApproximation):
            return self.__highest_allowed - TRIVIAL_ENERGY

        return (own_level + other_level)/2.0

    def _determine_normal(self):
        """ Given the electronic eigenvalues, determine the energy level.
        :return A concrete energy (float)
        """
        """ Given the electronic eigenvalues, determine the energy level. """
        if not self.isDone():
            return None

        es = self.energySpectrum()
        EF, kpts, wk, eigs = es.read()
        eigs = eigs.flatten()
        eigs = sorted(eigs[eigs > EF - TRIVIAL_ENERGY])
        eigs = findUniqueEnergies(eigs, TRIVIAL_ENERGY)
        eigs = np.array(eigs)
        eigs = eigs[eigs < self.__highest_allowed]

        try:
            level = eigs[self.__plus] + self.offset()
        except IndexError:
            this = LUMO(plus=self.__plus)
            if len(eigs) == 0:
                last = HOMO(minus=0)
            else:
                i = len(eigs) - 1
                last = LUMO(plus=i)

            mess = "%s was requested, but the highest available level is %s." % (this, last)
            return API.InconsistentApproximation(mess)

        return level


class HOMO(MO):
    """ Input parameter specifying the Highest Occupied Molecular Orbital."""
    def __gt__(self, other):
        if isinstance(other, LUMO):
            return False

        return self.__minus < other.__minus

    def __init__(
            self,
            minus=0,
            offset=0.0,
            include=API.DEFAULT,
            system=None,
            method=API.DEFAULT,
            parameters=API.DEFAULT,
            ):
        """
        :param minus: Integer specifying to chose either HOMO(minus=0) or a lower energy level HOMO-X(minus=X)
        :param offset: Adjust the target energy up or down, for instance 0.1 eV above HOMO level.
        """
        assert isinstance(minus, int) and minus>=0
        self.__minus = minus
        MO.__init__(
                self,
                system=system,
                method=method,
                parameters=parameters,
                offset=offset,
                include=include,
                )

    def higher(self):
        if self.__minus > 0:
            return self.copy(minus=self.__minus -1)
        else:
            return LUMO(
                    system=self.system(),
                    method=self.method(),
                    parameters=self.parameters(),
                    plus=0,
                    offset=self.offset(),
                    )

    def lower(self):
        return self.copy(minus=self.__minus + 1)

    def title(self):
        """
        :return A text representation of the energy level.
        """
        if self.__minus !=0:
            return 'HOMO_-%d' % self.__minus
        else:
            return 'HOMO'

    def _determine_include(self):
        own_level = self._determine_normal()
        if isinstance(own_level,
                      API.InconsistentApproximation):
            return own_level

        other_level = self.lower()._determine_normal()
        if isinstance(other_level,
                      API.InconsistentApproximation):
            return own_level - 10

        return (own_level + other_level)/2.0

    def _determine_normal(self):
        """
        Given the electronic eigenvalues, determine the energy level.

        :param es: An ElectronicSpectrum.
        :return: A concrete energy (float)
        """
        """ Given the electronic eigenvalues, determine the energy level. """
        if not self.isDone():
            print(self.system())
            print(self.inputs()[0].isDone())
            print(self.inputs()[0].tasks()[0].runDirectory())
            raise Exception

        es = self.energySpectrum()
        EF, kpts, wk, eigs = es.read()
        eigs = eigs.flatten()
        eigs = sorted(eigs[eigs < EF + TRIVIAL_ENERGY])
        eigs = findUniqueEnergies(eigs, TRIVIAL_ENERGY)
        eigs.reverse()
        try:
            level = eigs[self.__minus] + self.offset()
        except IndexError:
            mess = "HOMO(minus=%d) was requested, but the lowest available level is HOMO(minus=%d)." % (self.__minus, len(eigs) - 1)
            return API.InconsistentApproximation(mess)

        return level

def findUniqueEnergies(energies, delta):
    """
    Remove duplicate energies due to degeneracy.

    :param energies: Array of energies.
    :param delta: Energy difference that is considered trivial and due.
    :return: A new array with close energies removed.
    """
    energies = list(energies)
    i = 1
    while i < len(energies):
        if abs(energies[i] - energies[i - 1]) < delta:
            energies[i - 1] = (energies[i] + energies[i - 1])/2
            energies.pop(i)
        else:
            i += 1

    return energies
