# Written by Mads Engelund, 2017, http://espeem.com
import numpy as np
from ase.visualize import view
from CalcTroll.ASEUtils.Atoms import Atoms, Buffer
from CalcTroll.ASEUtils.AtomUtilities import groupAtoms

from CalcTroll.Plugins.Systems.Surface import Surface1D
from CalcTroll.Plugins.Systems.Ribbon import ArmchairRibbon, ZigzagRibbon
from CalcTroll import API
from CalcTroll.API import DEFAULT


class TerminatedArmchairRibbon(Surface1D):
    def __init__(self, n=7, parameters=DEFAULT):
        ribbon = ArmchairRibbon(n=n)
        atoms = ribbon.atoms()
        atoms, order = groupAtoms(atoms)[0]
        h_positions = atoms.positions + [0, 0, 1.09]
        extra_atoms = Atoms('H%d' % len(atoms), positions=h_positions)
        Surface1D.__init__(self, ribbon, extra_atoms=extra_atoms, parameters=parameters)

    def ribbon(self):
        return self.crystal()

    def passivateLowerSurface(self, atoms):
        lower_atoms, order = groupAtoms(atoms)[0]
        h_positions = lower_atoms.positions + [0, 0, -1.09]
        new_atoms = Atoms(
                'H%d' % len(lower_atoms),
                positions=h_positions,
                pbc=atoms.get_pbc(),
                cell=atoms.get_cell(),
                )
        new_atoms.setTag(name=self.name() + 'passivation', classtype=Buffer)
        new_atoms += atoms

        return new_atoms

class TerminatedZigzagRibbon(Surface1D):
    def __init__(self, n=7, parameters=DEFAULT):
        ribbon = ZigzagRibbon(n=n)
        atoms = ribbon.atoms()
        atoms, indices = groupAtoms(atoms)[0]
        h_positions = atoms.positions + [0, 0, 1.09]
        extra_atoms = Atoms('H%d' % len(atoms), positions=h_positions)

        Surface1D.__init__(self, ribbon, extra_atoms=extra_atoms, parameters=parameters)

    def ribbon(self):
        return self.crystal()

    def passivateLowerSurface(self, atoms):
        lower_atoms, order = groupAtoms(atoms)[0]
        take = np.array([symbol != 'H' for symbol in lower_atoms.get_chemical_symbols()], bool)
        lower_atoms = lower_atoms[take]

        h_positions = lower_atoms.positions + [0, 0, -1.09]
        new_atoms = Atoms(
                'H%d' % len(lower_atoms),
                positions=h_positions,
                pbc=atoms.get_pbc(),
                cell=atoms.get_cell(),
                )
        new_atoms.setTag(name=self.name() + 'passivation', classtype=Buffer)
        new_atoms += atoms

        return new_atoms
