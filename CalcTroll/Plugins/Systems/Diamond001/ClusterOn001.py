import numpy  as np
from CalcTroll.Plugins.Systems.ClusterOnSlab import ClusterOnSlab
from CalcTroll.Plugins.Systems.Diamond001.DanglingBonds import Diamond001Unpassivated, Diamond001ReconstructionState, InitialStateAtom, UNBUCKLED_p1x1
from CalcTroll.ASEUtils.AtomUtilities import findCopies
from CalcTroll import API

initial_state = Diamond001ReconstructionState(
    'BUCK',
    (
    InitialStateAtom((0, (0, 0)),  (0, 0,  0.0),  1) ,
    InitialStateAtom((1, (0, 0)),  (0, 0,  0.0),  1) ,
    InitialStateAtom((0, (-1, 1)), (0, 0,  0.0),  1) ,
    InitialStateAtom((1, (-1, 1)), (0, 0,  0.0),  1) ,
    InitialStateAtom((0, (-1,-1)), (0, 0,  0.0),  1) ,
    InitialStateAtom((1, (-1,-1)), (0, 0,  0.0),  1) ,
    InitialStateAtom((0, (0, 1)),  (0, 0, -0.4),  0) ,
    InitialStateAtom((1, (0, 1)),  (0, 0,  0.4),  0) ,
    InitialStateAtom((0, (0,-1)),  (0, 0, -0.4),  0) ,
    InitialStateAtom((1, (0,-1)),  (0, 0,  0.4),  0) ,
    InitialStateAtom((0, (0, 2)),  (0, 0,  0.4),  0) ,
    InitialStateAtom((1, (0, 2)),  (0, 0, -0.4),  0) ,
    InitialStateAtom((0, (0,-2)),  (0, 0,  0.4),  0) ,
    InitialStateAtom((1, (0,-2)),  (0, 0, -0.4),  0) ,
    InitialStateAtom((0, (-1, 0)), (0, 0, -0.4),  0) ,
    InitialStateAtom((1, (-1, 0)), (0, 0,  0.4),  0) ,
    InitialStateAtom((0, (-1, 2)), (0, 0, -0.4),  0) ,
    InitialStateAtom((1, (-1, 2)), (0, 0,  0.4),  0) ,
    InitialStateAtom((0, (-1,-2)), (0, 0, -0.4),  0) ,
    InitialStateAtom((1, (-1,-2)), (0, 0,  0.4),  0) ,
    InitialStateAtom((0, (1, 0)), (0, 0,  -0.4),  0) ,
    InitialStateAtom((1, (1, 0)), (0, 0,   0.4),  0) ,
    InitialStateAtom((0, (1, 1)), (0, 0,   0.4),  0) ,
    InitialStateAtom((1, (1, 1)), (0, 0,  -0.4),  0) ,
    InitialStateAtom((0, (1,-1)), (0, 0,   0.4),  0) ,
    InitialStateAtom((1, (1,-1)), (0, 0,  -0.4),  0) ,
    InitialStateAtom((0, (1, 2)), (0, 0,  -0.4),  0) ,
    InitialStateAtom((1, (1, 2)), (0, 0,   0.4),  0) ,
    InitialStateAtom((0, (1,-2)), (0, 0,  -0.4),  0) ,
    InitialStateAtom((1, (1,-2)), (0, 0,   0.4),  0) ,
    InitialStateAtom((0, (-2, 0)), (0, 0,  0.4),  0) ,
    InitialStateAtom((1, (-2, 0)), (0, 0, -0.4),  0) ,
    InitialStateAtom((0, (-2, 1)), (0, 0, -0.4),  0) ,
    InitialStateAtom((1, (-2, 1)), (0, 0,  0.4),  0) ,
    InitialStateAtom((0, (-2,-1)), (0, 0, -0.4),  0) ,
    InitialStateAtom((1, (-2,-1)), (0, 0,  0.4),  0) ,
    InitialStateAtom((0, (-2, 2)), (0, 0,  0.4),  0) ,
    InitialStateAtom((1, (-2, 2)), (0, 0, -0.4),  0) ,
    InitialStateAtom((0, (-2,-2)), (0, 0,  0.4),  0) ,
    InitialStateAtom((1, (-2,-2)), (0, 0, -0.4),  0) ,
    ),
    )

def setupInitialState(unbuckle):
    initial_state = []
    for item in unbuckle:
        dimer = [
            InitialStateAtom((0, item), (0, 0,  0),  1) ,
            InitialStateAtom((1, item), (0, 0,  0),  1) ,
            ]
        initial_state.extend(dimer)

    for ix in range(-2, 2):
        for iy in range(-2, 2):
            if (ix, iy) in unbuckle:
                continue

            sign = (-1)**((ix+iy)%2)
            r = sign*0.4
            dimer = [
                InitialStateAtom((0, (ix, iy)), (0, 0,  r),  0) ,
                InitialStateAtom((1, (ix, iy)), (0, 0, -r),  0) ,
                ]
            initial_state.extend(dimer)

    initial_state = Diamond001ReconstructionState(
            'BUCK',
            initial_state,
            )

    return initial_state



class ClusterOn001(ClusterOnSlab):
    def __init__(
            self,
            cluster,
            symbol='Si',
            origin=API.DEFAULT,
            displacement=(0,0,0),
            unbuckle=tuple(),
            name=API.DEFAULT,
            parameters=API.DEFAULT,
            ):
        initial_state = setupInitialState(unbuckle)
        slab = Diamond001Unpassivated(symbol, initial_state=UNBUCKLED_p1x1)

        ClusterOnSlab.__init__(
            self,
            cluster=cluster,
            slab=slab,
            origin=origin,
            initial_state=initial_state,
            name=name,
            parameters=parameters,
            )

    def subParameters(self, parameters=API.DEFAULT):
        parameters = self.defaultParameters(parameters)
        slab_parameters = parameters['slab_parameters']
        kpts = slab_parameters['kpts']
        vectors = slab_parameters['vectors']
        kpts = tuple(np.tensordot(kpts, vectors, (0, 1)))
        slab_parameters = slab_parameters.copy(kpts=kpts, vectors=((1, 0), (0, 1)))

        return (parameters['molecule_parameters'], slab_parameters)

    def applyInitializationConstraintsAndRelaxation(
            self,
            atoms,
            constraints=tuple(),
            initialized=True,
            relaxed=True,
            ):
        if initialized:
            diff_vector, initial_moments = self.makeAtomsInitialized(atoms)

        atoms = self.makeSubsystemsRelaxed(atoms)

        if constraints is not None:
            for constraint in constraints:
                atoms.set_constraint(constraint)

        if relaxed:
            atoms = self.makeAtomsRelaxed(atoms)

        if initialized:
            atoms.positions += diff_vector
            atoms.set_initial_magnetic_moments(initial_moments)

        return atoms
