# Written by Mads Engelund, 2017, http://espeem.com
import unittest
from ase.constraints import FixAtoms
from ase.visualize import view

from CalcTroll.Core.System import InitialStateAtom, InitialState
from CalcTroll.Core.Test.Dummies import TestRelaxationSystem
from CalcTroll.Core.Test.Case import CalcTrollTestCase
from CalcTroll.ASEUtils.Atoms import Atoms

from CalcTroll.Plugins.Systems.Surface.Surface3D import SurfaceParameters3D
from CalcTroll.Plugins.Systems.Diamond001 import Si001
from CalcTroll.Plugins.Systems.Diamond001.DanglingBonds import Diamond001Unpassivated
from CalcTroll import API

class Diamond001Test(CalcTrollTestCase):
    def testRelaxation(self):
        surface = Si001
        surface_parameters1 = surface.defaultParameters().copy(
                free_layers=0,
                bound_layers=1,
                vectors=((2, 0),(0, 2)),
                cell_size=50,
                )
        surface_parameters2 = surface_parameters1.copy(
                free_layers=0,
                bound_layers=1,
                vectors=((2, 0),(0, 4)),
                cell_size=50,
                )
        atoms = surface.atoms(parameters=surface_parameters1)
        crystal = surface.crystal()
        atoms = crystal.atoms()
        atoms.cell *= 1.1
        relaxation = atoms
        rcrystal = TestRelaxationSystem(crystal, relaxation)
        atoms = surface.atoms(parameters=surface_parameters1)
        surface = surface.createSystemWithRelaxedSubSystems([rcrystal])
        self.assertTrue(surface.hasRelaxedSubsystems())
        self.assertFalse(surface.isRelaxed())


if __name__=='__main__':
    unittest.main()
