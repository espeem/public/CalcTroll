# Written by Mads Engelund, 2017, http://espeem.com
from ase import io
import unittest
from CalcTroll.Core.Test.Case import CalcTrollTestCase
from CalcTroll.Core.Test.Dummies import TestRelaxationSystem
from ase.constraints import FixAtoms
from ase.visualize import view

from CalcTroll.Plugins.Systems.Defect.SurfacePointDefect import SurfacePointDefect
from CalcTroll.Plugins.Systems.Diamond001 import Ge001
from CalcTroll.Plugins.Systems.Diamond001.DanglingBonds import *

class DanglingBondsTest(CalcTrollTestCase):
    def testLineDefectConstruction(self):
        identifiers=[(2, (0, 0)), (3, (0, 0)), (-1, (0, 1))]
        periodicity = [[0, 2]]
        surface = Ge001
        defect = SurfaceLineDefect(surface=surface, identifiers=identifiers, periodicity=periodicity)
        pos1 = defect.removedAtoms(coordinates='orth_surface').get_positions().mean(0)
        pos = [pos1 + np.array([3,0,1]), pos1 + np.array([-3,0,1])]
        added_atoms = Atoms('Ag2', pos)
        name = 'Test'
        test = Diamond001LineDefect(
                name=name,
                symbol='Ge',
                identifiers=identifiers,
                added_atoms=added_atoms,
                periodicity=periodicity,
                )
        self.assertEqual(test.name(), name)

    def testPointDefectConstruction(self):
        identifiers=[(2, (0, 0)), (3, (0, 0)), ]
        surface = Ge001
        defect = SurfacePointDefect(
                surface=surface,
                identifiers=identifiers)
        pos1 = defect.removedAtoms(coordinates='orth_surface').get_positions().mean(0)
        pos = [pos1]
        name = 'Test'
        added_atoms = Atoms('Ag', pos)
        test = Diamond001PointDefect(
                name=name,
                symbol='Ge',
                identifiers=identifiers,
                added_atoms=added_atoms,
                )
        self.assertEqual(test.name(), name)


if __name__=='__main__':
    unittest.main()

