from ase import io
from CalcTroll.Core.Flags import DEFAULT
from CalcTroll.Core.Explanation import Explanation
from CalcTroll.Plugins.Systems.InputToAtoms import InputToAtoms
from CalcTroll.ASEUtils.RegularizeMolecule import regularizeMolecularDirections

class ChemSpider(InputToAtoms):
    def __init__(self,
                 chemspider_id,
                 name=DEFAULT,
                 ):
        InputToAtoms.__init__(
                self,
                single_input=chemspider_id,
                name=name,
                )

    def generateAtoms(self):
        filename = '%d.mol' % self.singleInput()
        atoms = io.read(filename)

        atoms = regularizeMolecularDirections(atoms)

        return atoms

    def explain(self):
        text = "The molecule was imported from the ChemSpider datadase[*] with ID, %d." % self.singleInput()
        ex = Explanation(text,
                ["Royal Chemical Society, ChemSpider: https://chemspider.com"])
        ex += InputToAtoms.explain(self)

        return ex
