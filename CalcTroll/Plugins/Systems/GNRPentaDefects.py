import numpy as np
from ase.constraints import FixAtoms, ModelSurfacePotential

from CalcTroll.Plugins.Systems.Crystal.Crystal1D import Crystal1D
from CalcTroll.Plugins.Systems.Molecule import Molecule
from CalcTroll.Plugins.Systems.Defect.LineDefect import LineDefect
from .PolyaromaticHydroCarbon import PolyaromaticHydroCarbon, passivate_atoms, passivate_partial
from CalcTroll.API import DEFAULT
from CalcTroll.ASEUtils.AtomUtilities import orderAtoms
from CalcTroll.ASEUtils.AtomUtilities import wrap
from CalcTroll.ASEUtils.Atoms import Atoms

class GNRRepeat(Crystal1D):
    def __init__(
            self,
            parameters=DEFAULT,
            ):
        indices = [(0, -1), (0, 0), (0, 1) ]
        poly = PolyaromaticHydroCarbon(
            indices=indices,
            passivate=False,
        )
        atoms = poly.atoms()
        del atoms.constraints
        atoms.set_pbc([True, False, False])
        atoms.rotate(v='z', a=-30)
        atoms = orderAtoms(atoms, (0, 1, 2))
        pos = atoms.get_positions()
        xmin, xmax,  = pos[:, 0].min(), pos[:, 0].max()
        ymin, ymax,  = pos[:, 1].min(), pos[:, 1].max()
        take = np.logical_or(pos[:, 1] > ymax - 0.01, pos[:, 1] < ymin + 0.01)
        atoms = passivate_partial(atoms, pos[take])
        xspan = xmax - xmin
        atoms.set_cell([[xspan*3/2, 0, 0], [0, 30, 0], [0, 0, 30]])
        atoms.positions[:, 0] += atoms.cell[0, 0]/2
        atoms = wrap(atoms, center=[0.5, 0, 0])
        atoms = orderAtoms(atoms, (2, 1, 0))

        Crystal1D.__init__(self, atoms=atoms)

class GNRDefectPeriod(LineDefect):
    def name(self):
        name = LineDefect.name(self)

        return name + '_%s_%s' % self.__types

    def __init__(
            self,
            types=(1, 1),
            parameters=DEFAULT,
            ):
        assert len(types) == 2
        assert types[0] in [1, 2]
        assert types[1] in [1, 2]
        self.__types = types
        crystal1d = GNRRepeat()
        atoms = crystal1d.atoms()
        take = np.array(atoms.get_chemical_symbols()) == 'H'
        indices = np.arange(len(take))[take]
        identifiers = [(0, i) for i in indices]

        r_pos = atoms.positions[take]
        plus_take = r_pos[:, 1] > 0
        minus_take = r_pos[:, 1] < 0

        added_atoms = Atoms()

        if plus_take.sum() == 2:
            r = r_pos[plus_take]
            added_c_pos = (r[0] + r[1])/2

            if types[0] == 1:
                added_h_pos = added_c_pos + (0, 0.9, 0.0)
                added_atoms += Atoms(['C', 'H'], np.array([added_c_pos, added_h_pos]))
            elif types[0] == 2:
                added_h_pos1 = added_c_pos + (0, 0.7, 0.5)
                added_h_pos2 = added_c_pos + (0, 0.7,-0.5)
                added_atoms += Atoms(['C', 'H', 'H'], np.array([added_c_pos, added_h_pos1, added_h_pos2]))

        if minus_take.sum() == 2:
            r = r_pos[minus_take]
            added_c_pos = (r[0] + r[1])/2
            if types[1] == 1:
                added_h_pos = added_c_pos + (0, -0.9, 0.0)
                added_atoms += Atoms(['C', 'H'], np.array([added_c_pos, added_h_pos]))
            elif types[1] == 2:
                added_h_pos1 = added_c_pos + (0,-0.7, 0.5)
                added_h_pos2 = added_c_pos + (0,-0.7,-0.5)
                added_atoms += Atoms(['C', 'H', 'H'], np.array([added_c_pos, added_h_pos1, added_h_pos2]))

        LineDefect.__init__(self, crystal1d=crystal1d, identifiers=identifiers, added_atoms=added_atoms)


class GNRDefect(Molecule):
    def name(self):
        name = Molecule.name(self)

        return name + '_%s_%s' % self.__types

    def __init__(
            self,
            types=(1, 1),
            parameters=DEFAULT,
            ):
        defect = GNRDefectPeriod(types=types)
        atoms = defect.atoms()
        atoms.set_pbc([False, False, False])
        atoms = passivate_atoms(atoms)
        atoms.positions[:, 2] -= 0.25 * abs(atoms.positions[:, 0])

        Molecule.__init__(self, atoms=atoms)
