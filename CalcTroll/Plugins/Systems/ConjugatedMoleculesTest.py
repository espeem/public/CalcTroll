# Written by Mads Engelund, 2017, http://espeem.com
import unittest
import numpy
from ase.visualize import view
from CalcTroll.Core.Test.Case import CalcTrollTestCase

from CalcTroll.Plugins.Systems.ConjugatedMolecules import *

class ConjugatedMoleculesTest(CalcTrollTestCase):
    def testConstruction(self):
        molecule = Decaphene()
        self.assertEqual(molecule.name(), 'Decaphene')

if __name__=='__main__':
    unittest.main()
