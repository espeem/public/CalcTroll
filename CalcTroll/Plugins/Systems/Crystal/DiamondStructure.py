# Written by Mads Engelund, 2017, http://espeem.com
from math import sqrt
import numpy as np
from numpy.linalg import norm
from numpy.linalg import inv as inverse
from ase.visualize import view
from ase.atoms import Atom
from CalcTroll.API import DEFAULT

from CalcTroll.ASEUtils.Atoms import Atoms
from CalcTroll.Plugins.Systems.Crystal import Crystal3D

class DiamondStructure(Crystal3D):
    def name(self):
        return self.symbol() + 'Alpha'

    def __init__(
            self,
            symbol,
            parameters=DEFAULT,
            ):
        a = LATTICE_CONSTANTS = {'C': 3.567, 'Si':5.431, 'Ge': 5.658}[symbol]
        atom1 = Atom(symbol, a*np.array([0,0,0])     )
        atom2 = Atom(symbol, a*np.array([1,1,1])/(4.))
        cell = np.array([[0,1,1],[1,0,1],[1,1,0]])*a/2.
        atoms = Atoms([atom1, atom2], cell=cell, pbc=[True]*3)
        self.__symbol = symbol

        Crystal3D.__init__(self, atoms=atoms, parameters=parameters)

    def symbol(self):
        return self.__symbol

    def dopants(self, dopant_level, suggestion=None):
        sign = np.sign(dopant_level)
        allowed_elements = {0:[self.material()], 1:['Ga','Al', 'B'], -1:['P']}[sign]
        if suggestion is None:
            dopant_element = allowed_elements[0]
        elif suggestion in allowed_elements:
            dopant_element = suggestion
        else:
            raise Exception

        return dopant_element

    def latticeConstant(self, relaxed=True):
        if relaxed:
            return float(self.__a)*self.relaxationFactor()
        else:
            return float(self.__a)

    def bandStructurePath(self):
        path = [
                ((0.000 , 0.000 , 0.000), '\Gamma'),
                ((0.000 , 0.500 , 0.500), 'X'),
                ((0.250 , 0.750 , 0.500), 'W'),
                ((0.500 , 0.500 , 0.500), 'L'),
                ((0.000 , 0.000 , 0.000), '\Gamma'),
                ((0.375 , 0.750 , 0.375), 'K'),
                ((0.000 , 0.500 , 0.500), 'X'),
                ((0.250 , 0.625 , 0.625), 'U'),
                ((0.375 , 0.750 , 0.375), 'K'),
                ((0.500 , 0.500 , 0.500), 'L'),
                ]
        return path

    def conventionalCell(self, coordinates='orth_crystal'):
        c = np.array([[-1, 1, 1], [1, -1, 1], [1, 1, -1]])
        pc = self.unitCell(coordinates=coordinates).T
        uc = (c @ pc).T

        return np.array(uc)

    def makeConventionalCell(self):
        atoms = self.toAseAtoms()
        unit_cell = atoms.get_cell()
        atoms = atoms.repeat((2, 2, 2))
        a = self.a()

        cell = np.array([[a, 0, 0], [0, a, 0], [0, 0, a]])
        atoms.set_cell(cell)

        atoms = wrap(atoms)
        atoms = removeCopies(atoms)

        return atoms

    def relaxationFactor(self):
        relaxation = self.relaxation()
        if relaxation is None:
            return np.identity(3)

        atoms = self.atomsTemplate(parameters=self.parameters())
        v_factor = relaxation.get_volume()/atoms.get_volume()
        l = v_factor**(1/3.)

        return np.identity(3)*l
