# Written by Mads Engelund, 2017, http://espeem.com
# Class describing 1-dimensional crystals such as:
# graphene nanoribbons,
# nanotubes,
# idealized mono-atomic metal wires,

import numpy as np
from CalcTroll.Core.Utilities import vectorLength
from CalcTroll.ASEUtils.Atoms import Atoms
from CalcTroll.ASEUtils.AtomUtilities import orderAtoms
from CalcTroll.ASEUtils.AtomUtilities import removeCopies
from CalcTroll.ASEUtils.AtomUtilities import makeAtomsRelaxed
from CalcTroll.Core.System.BasisChanger import BasisChanger, Transformation
from CalcTroll.Plugins.Systems.Crystal.Crystal import Crystal, CrystalParameters
from CalcTroll.Plugins.LengthScales import LengthScales
from CalcTroll import API
from CalcTroll.API import DEFAULT

class CrystalParameters1D(CrystalParameters):
    def __init__(
            self,
            kpts=DEFAULT,
            vectors=DEFAULT,
            padding=DEFAULT,
            ):
        pass

    def ignoreLongRangeCorrelation(self):
        if self['padding'] is DEFAULT:
            padding = LengthScales.defaults()['short_correlation_length']
            return self.copy(padding=padding)
        else:
            return self

    def neededKpts(self, cell):
        scales = LengthScales.defaults()
        kpts = np.ones(3, int)

        length=np.max(abs(cell[-1]))
        number=int(np.ceil(scales['correlation_length']/length))
        kpts[0] = number

        return kpts

class Crystal1D(Crystal):
    @classmethod
    def parameterClass(cls):
        return CrystalParameters1D

    def __init__(
            self,
            atoms,
            parameters=DEFAULT,
            ):
        assert all(atoms.pbc == np.array([True, False, False]))
        self.__atoms = Atoms(atoms)
        API.System.__init__(
                self,
                sub_systems=tuple(),
                parameters=parameters,
                )

    def setUpBasisChanger(self):
        atoms = self.atoms()
        cell = atoms.cell

        basis_changer = BasisChanger(
                transformations=[Transformation(to='orth_crystal', fro='crystal', trans_matrix=cell.transpose())]
                )
        return basis_changer

    def pbc(self):
        return [True, False, False]

    def defaultParameters(self, parameters=DEFAULT):
        if parameters is DEFAULT:
            parameters = CrystalParameters1D()

        vectors = parameters['vectors']
        if parameters['vectors'] is DEFAULT:
            vectors = ((1,),)

        kpts = parameters['kpts']
        if kpts is DEFAULT:
            kpts = parameters.neededKpts(self.__atoms.cell[self.__atoms.pbc])

        padding = parameters['padding']
        if padding is DEFAULT:
            padding = LengthScales.defaults()['short_correlation_length']

        parameters = parameters.copy(kpts=kpts, vectors=vectors, padding=padding)

        return parameters

    def inputAtoms(self):
        return self.__atoms.copy()

    def relaxationFactor(self):
        if self.relaxation() is None:
            return 1.0
        lr = vectorLength(self.relaxation().get_cell()[0])
        lu = vectorLength(self.__atoms.get_cell()[0])

        return lr/lu

    def atomsTemplate(self, parameters=DEFAULT):
        if parameters is DEFAULT:
            parameters = self.defaultParameters()

        vectors = np.array(parameters['vectors'])
        lengths = vectorLength(vectors, 1)
        assert lengths.all()

        atoms = self.__atoms.copy()
        pos = atoms.get_positions()
        size = np.array([pos[:, i].max() - pos[:,i].min() for i in range(3)])
        size = size + parameters['padding']

        repeats = (vectors[0][0], 1, 1)
        vectors = atoms.cell[0] * vectors[0][0]

        atoms = atoms.repeat(repeats)
        atoms.set_cell(size)
        atoms.cell[0] = vectors
        atoms = removeCopies(atoms)
        atoms = orderAtoms(atoms)
        atoms.set_celldisp((0, 0, 0))

        return atoms

    def makeAtomsRelaxed(self, atoms, tolerance=1):
        if self.relaxation() is not None:
            rf = self.relaxation().cell[0, 0]/self.__atoms.cell[0, 0]
            atoms.positions *= rf
            atoms.cell[0, 0] *= rf
            celldisp = atoms.get_celldisp()
            celldisp[0] *= rf
            atoms.set_celldisp(celldisp)
        atoms = makeAtomsRelaxed(atoms, self.relaxation(), tolerance=tolerance)

        return atoms

    def unitCell(self, coordinates='orth_crystal'):
        if self.isRelaxed():
            unit_cell = self.relaxation().get_cell()
        else:
            unit_cell = self.__atoms.get_cell()

        unit_cell = unit_cell[np.array(self.pbc())]

        return self.change(unit_cell, to=coordinates, fro='orth_crystal')

    def plotUnitCell(self, data=None, ax=None, direction='-z'):
        if data is None:
            atoms = self.atoms()
        else:
            atoms = data

        cell = atoms.get_cell()
        if direction in ['-y', '-z']:
            ax.axvline(x=-cell[0, 0]/2, color='blue', lw=3)
            ax.axvline(x=cell[0, 0]/2, color='blue', lw=3)

        return ax
