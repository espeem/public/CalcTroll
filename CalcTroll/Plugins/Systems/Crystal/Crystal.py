# Written by Mads Engelund, 2017, http://espeem.com
import numpy as np
from ase.constraints import FixAtoms, FixedPlane, FixedMode
from ase.atoms import Atoms as AseAtoms
from ase.visualize import view

from CalcTroll.Core.Visualizations import addMeasuringStick
from CalcTroll.Core.Visualizations import flatMatplotlibView
from CalcTroll.Plugins.LengthScales import LengthScales
from CalcTroll import API
from CalcTroll.API import DEFAULT

class CrystalParameters(API.Parameters):
    def __init__(self,
                 kpts=DEFAULT,
                 vectors=DEFAULT,
                 length_scales=LengthScales(),
                 ):
        if not kpts is DEFAULT:
            for kpt in kpts:
                int(kpt)

    def identifier(self):
        diag = [self['vectors'][i][i] for i in range(len(self['vectors']))]
        return [diag, self['kpts']]

    def kpts(self):
        return self['kpts']

    def neededKpts(self, cell):
        raise NotImplementedError

    def ignoreLongRangeCorrelation(self):
        lenth_scales = self['length_scales'].copy()
        short_length = self['length_scales']['short_correlation_length']
        lenth_scales = lenth_scales.copy(correlation_length=short_length)

        return self.copy(kpts=DEFAULT, length_scales=lenth_scales)

class Crystal(API.System):
    COORDINATES='orth_crystal'
    def relaxCell(self):
        return True

    def frame(self, data=None, border=API.DEFAULT, direction='-z'):
        if data is None:
            atoms = self.atoms()
        else:
            atoms = data

        if border is API.DEFAULT:
            border = 9

        pmin = atoms.positions.min(axis=0)
        pmax = atoms.positions.max(axis=0)

        pmin -= border
        pmax += border

        fc, sc = {'x':(1, 2), 'y':(0,2), 'z':(0, 1)}[direction[-1]]

        return np.array([pmin[[fc, sc]], pmax[[fc, sc]]])


