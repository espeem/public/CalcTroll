# Written by Mads Engelund, 2017, http://espeem.com
import numpy as np
from numpy.linalg import inv as inverse

from CalcTroll.Core.Utilities import convertFromBasis
from CalcTroll.Core.Utilities import vectorLength
from CalcTroll.ASEUtils.AtomUtilities import makeAtomsRelaxed
from CalcTroll.ASEUtils.AtomUtilities import removeCopies
from CalcTroll.ASEUtils.Atoms import Atoms
from CalcTroll.Core.System.BasisChanger import BasisChanger, Transformation

from .Crystal import Crystal, CrystalParameters
from .Utilities import findNeededRepetitions

from CalcTroll.API import DEFAULT

class CrystalParameters3D(CrystalParameters):
    def neededKpts(self, cell):
        scales = self['length_scales']
        kpts = np.ones(3, int)
        for i, vector in enumerate(cell):
             length=np.max(abs(vector))
             number=int(np.ceil(scales['correlation_length']/length))
             kpts[i] = number

        return kpts


class Crystal3D(Crystal):
    def __init__(
            self,
            atoms,
            parameters=DEFAULT,
            ):
        assert all(atoms.pbc == np.array([True, True, True]))
        self.__atoms = Atoms(atoms)

        Crystal.__init__(self, sub_systems=tuple(), parameters=parameters)

    def setUpBasisChanger(self):
        atoms = self.atoms()
        cell = atoms.cell

        basis_changer = BasisChanger(
                transformations=[Transformation(to='orth_crystal', fro='crystal', trans_matrix=cell.transpose())]
                )
        return basis_changer

    def unitCell(self, coordinates='orth_crystal'):
        atoms = self.__atoms.copy()
        if self.isRelaxed():
            atoms = self.makeAtomsRelaxed(atoms)
        unit_cell = atoms.cell

        if coordinates == 'orth_crystal':
            return unit_cell

        return self.change(unit_cell, to=coordinates, fro='orth_crystal')

    def conventionalCell(self, coordinates='orth_crystal'):
        raise NotImplementedError

    def normalVector(self, miller_indices):
        unit_cell = self.conventionalCell()
        inverse_cell = inverse((unit_cell).T).T

        normal_vector = miller_indices @ inverse_cell
        normal_vector /= vectorLength(normal_vector)

        return normal_vector

    def defaultParameters(self, parameters=DEFAULT):
        r_system = self.fullyUnrelaxed()
        if parameters is DEFAULT:
            parameters = CrystalParameters3D()

        kpts = parameters['kpts']
        if kpts is DEFAULT:
            kpts = parameters.neededKpts(r_system.unitCell())

        vectors = parameters['vectors']
        if vectors is DEFAULT:
            vectors = ((1,0,0), (0,1,0), (0,0,1))

        parameters = parameters.copy(kpts=kpts, vectors=vectors)

        return parameters

    def atomsTemplate(self, parameters=DEFAULT):
        if parameters is DEFAULT:
            parameters = self.parameterClass()()

        vectors = np.array(parameters['vectors'])

        lengths = vectorLength(vectors, 1)
        assert lengths.all()

        atoms = self.__atoms.copy()

        vectors = convertFromBasis(vectors, atoms.cell)
        repeats = findNeededRepetitions(atoms.cell, 1.5*lengths.max())

        atoms = atoms.repeat(repeats)
        atoms.cell[atoms.pbc] = vectors
        atoms.wrap()
        atoms = removeCopies(atoms)
        atoms.setTag('Reference', picker=None)
        atoms.set_celldisp(np.array([0, 0, 0]))

        padding = np.logical_not(atoms.pbc)
        assert padding.sum() == 0

        return atoms

    def makeAtomsRelaxed(self, atoms, tolerance=1.0):
        relaxation = self.relaxation()
        if relaxation is None:
            return atoms

        atoms.cell = self.applyCellRelaxationFactor(atoms.cell)
        atoms.positions = self.applyCellRelaxationFactor(atoms.positions)
        atoms._celldisp = self.applyCellRelaxationFactor([atoms._celldisp])[0]

        atoms = makeAtomsRelaxed(atoms, relaxation)

        return atoms

    def makeCellRelaxed(self, atoms):
        relaxation = self.relaxation()
        if relaxation == None:
            return atoms
        
        atoms.cell = self.applyCellRelaxationFactor(atoms.cell)
        atoms.positions = self.applyCellRelaxationFactor(atoms.positions)
        atoms._celldisp = self.applyCellRelaxationFactor([atoms._celldisp])[0]

        return atoms

    def applyCellRelaxationFactor(self, vectors):
        relax_atoms = self.relaxation()[-1]
        vectors = np.array(vectors)
        cell = np.array(self.__atoms.cell).T
        rcell = np.array(relax_atoms.cell).T

        factor = np.array(rcell @ inverse(cell))

        vectors = np.tensordot(factor, vectors, (1, 1)).T

        return vectors


    def pbc(self):
        return [True, True, True]

