import numpy as np
from CalcTroll import API


class Microstates(API.Collection):
    def __init__(self, systems, name=API.DEFAULT):
        assert len(systems) > 1
        
        for system in systems:
            assert isinstance(system, API.System)

        if name == API.DEFAULT:
           name = systems[0].name() + "_Microstates"

        API.Collection.__init__(
                self,
                systems=systems,
                name=name)
