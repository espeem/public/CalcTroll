from ase.atoms import Atom
from ase.atoms import Atoms as ASEAtoms
import numpy as np
from ase.visualize import view
import random

from CalcTroll.Core.Utilities import vectorLength
from CalcTroll.ASEUtils.Atoms import Atoms
from CalcTroll.ASEUtils.AtomUtilities import removeCopies, orderAtoms
from CalcTroll.Plugins.Systems.Molecule import Molecule

from CalcTroll import API
from CalcTroll.API import DEFAULT

ATOM_UNIT = Atoms(
    [
        Atom('C', (-1.2, -0.7 ,0.0,)),
        Atom('C', (-1.2,  0.7 ,0.0,)),
        Atom('C', ( 0.0,  1.4 ,0.0,)),
        Atom('C', ( 1.2,  0.7 ,0.0,)),
        Atom('C', ( 1.2, -0.7 ,0.0,)),
        Atom('C', ( 0.0, -1.4 ,0.0,)),
    ],
)

K_MOVES = [
        (-1, 0),
        ( 0, 1),
        ( 0,-1),
        ]

KP_MOVES = [
        ( 1, 0),
        ( 0, 1),
        ( 0,-1),
        ]

MOVES = {0:K_MOVES, 1:KP_MOVES}

def nextIndices(index):
    indices = np.array(MOVES[(index[0]+index[1])%2]) + np.array(index)
    indices = [tuple(index) for index in indices]

    return indices

def generateRandom(n=25):
    pah = [(0, 0)]
    while len(pah) < n:
        start_index = random.choice(pah)
        indices = nextIndices(start_index)
        indices = list(set(indices) - set(pah))
        if len(indices) == 0:
            continue
        index = random.choice(indices)
        pah.append(index)

    return frozenset(pah)

def generatePAHs(sample, n=10):
    if isinstance(n, int):
        n = [n]

    all_pahs = []
    r = n
    for j in range(sample):
        n = random.choice(r)
        pah = generateRandom(n=n)
        pahs = generateOneSizePAHs(100, pah)
        all_pahs.append(pahs[-1])

    return all_pahs

def addOnePAH(old_pah):
    new_index = old_pah[0]
    while new_index in old_pah:
        index = random.choice(old_pah)
        indices = nextIndices(index)
        new_index = random.choice(indices)

    new_pah = old_pah + [new_index]

    return frozenset(new_pah)

def generateOneSizePAHs(sample_size, pah):
    pahs = set([pah])
    for i in range(sample_size):
        pah = random.choice(list(pahs))
        new_pah = generateNext(pah)
        pahs.add(new_pah)

    pahs = [list(pah) for pah in list(pahs)]
    return pahs

def generateNext(old_pah):
    old_pah = list(old_pah)
    while True:
        r_index = random.choice(old_pah)
        if r_index == (0, 0):
            continue
        i_index = random.choice(old_pah)
        if i_index == r_index:
            continue

        next_pah = list(old_pah)
        indices = nextIndices(i_index)
        indices = list(set(indices) - set(old_pah))
        if len(indices) == 0:
            continue
        i_index = random.choice(indices)
        next_pah.remove(r_index)
        next_pah.append(i_index)

        r_indices = nextIndices(r_index)
        i_indices = nextIndices(i_index)

        created_bonds = len(set(next_pah).intersection(i_indices))
        removed_bonds = len(set(old_pah).intersection(r_indices))
        net_bonds = created_bonds - removed_bonds
        test = random.random()

        if test > np.exp(net_bonds*0.2):
            continue

        if not isConnected(next_pah):
            continue
        break

    return frozenset(next_pah)

def isConnected(pah):
    pah = list(pah)
    group = connectedGroup(0, pah)
    return group == set(pah)

def connectedGroup(index, pah):
    """ Find the group of indices that is adjacantly connected"""
    new_indices = set([pah[index]])
    pah = set(pah)
    indices = set([])
    while len(new_indices) > 0:
        indices.update(new_indices)
        new_indices = set([])
        for index in indices:
            new_indices.update(nextIndices(index))
        new_indices = pah.intersection(new_indices)
        new_indices = new_indices.difference(indices)

    return indices

class RandomPAHs(API.Collection):
    def __len__(self):
        return len(self.__pahs)

    def __init__(
            self,
            sample,
            n=10,
            seed=1,
            name="TEST",
            parameters=API.DEFAULT,
            ):
        random.seed(seed)
        self.__seed = seed
        self.__pahs = generatePAHs(sample=sample, n=n)
        self.__name = name
        self.__parameters = self.defaultParameters(parameters)
        self.__test_pah = PAH(self.__pahs[0], name=name, seed=seed)

    def path(self):
        return self.__test_pah.path()

    def defaultParameters(self, parameters):
        return PAH(self.__pahs[0]).defaultParameters(parameters)

    def subSystems(self):
        return []

    def setSubSystems(self, sub_systems):
        return self

    def parameters(self):
        return self.__parameters

    def subParameters(self, parameters):
        return []

    def entry(self, index):
        return PAH(self.__pahs[index])


class PAH(Molecule):
    def __init__(
            self,
            indices,
            seed=1,
            name=DEFAULT,
            parameters=DEFAULT,
    ):
        random.seed(seed)
        atoms = Atoms()
        indices = np.array(indices)
        angles = [i*np.pi/3 for i in range(3)]
        v = [1.4*np.array([np.cos(a), np.sin(a), 0.0]) for a in angles]
        ps = []
        for index in indices:
            p = index[0]*(v[1] + v[2])
            n, m = divmod(index[1] - index[0], 2)
            p += n*v[0] + (n + m)*v[1]
            ps.append(p)

        atoms = Atoms("C"*len(ps), ps)

        padding = 10
        maxi = atoms.positions.max(axis=0)
        mini = atoms.positions.min(axis=0)

        ran = maxi - mini + padding
        atoms.cell[0, 0] = ran[0]
        atoms.cell[1, 1] = ran[1]
        atoms.cell[2, 2] = ran[2]

        atoms = removeCopies(atoms)
        atoms = self.passivate(atoms)
        atoms.rattle(0.01, seed=seed)

        Molecule.__init__(self, atoms, name=name, parameters=parameters)

    def passivate(self, atoms):
        return passivate_atoms(atoms)


def passivate_atoms(atoms):
    symbols = np.array(atoms.get_chemical_symbols())
    pos = atoms.get_positions()[symbols=='C']

    return passivate_partial(atoms, pos)

def passivate_partial(atoms, pos, symbol='H'):
    a_pos = atoms.get_positions()
    tags = atoms.get_tags()
    hydrogens = []
    new_tags = []
    for i, p in enumerate(pos):
        diff = np.sqrt((abs(a_pos - p)**2).sum(axis=1))
        take1 = diff < 0.1
        take2 = np.logical_and(diff < 1.7, np.logical_not(take1))
        npos = a_pos[take2]
        if len(npos) == 2 and (take1).sum() == 1:
            av = npos.sum(axis=0)/2
            diff = p - av
            diff /= vectorLength(diff)
            hydrogens.append(p+diff)
            new_tags.append(tags[take1][0])

        elif len(npos) == 1 and (take1).sum() == 1:
            diff = npos[0] -p
            diff /= vectorLength(diff)
            a = 2*np.pi/3
            t = np.array([[np.cos(a), np.sin(a)], [-np.sin(a), np.cos(a)]])
            for i in range(2):
                diff[:2] = np.tensordot(t, diff[:2], (1, 0))
                hydrogens.append(p + diff)
                new_tags.append(tags[take1][0])

    hydrogens = np.array(hydrogens)
    hydrogens = Atoms(symbol*len(hydrogens), positions=hydrogens)
    hydrogens.set_tags(new_tags)
    atoms = atoms.copy()
    atoms += hydrogens

    return atoms
