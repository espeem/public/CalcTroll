from ase.atoms import Atom
from ase.atoms import Atoms as ASEAtoms
import numpy as np
from ase.visualize import view
import random

from CalcTroll.Core.Utilities import vectorLength
from CalcTroll.ASEUtils.Atoms import Atoms
from CalcTroll.ASEUtils.AtomUtilities import removeCopies, orderAtoms
from CalcTroll.Plugins.Systems.Molecule import Molecule
from CalcTroll.Plugins.Systems.PAH import passivate_atoms

from CalcTroll import API
from CalcTroll.API import DEFAULT

L=1.4

class FlatConstructor(object):
    def __init__(self, atom):
        self.__list = [atom]

    def addAtom(self, index, angle, atom_type, distance=L):
        v0 = self.__list[index].position
        angle = angle/180*np.pi
        x, y = np.cos(angle)*distance, np.sin(angle)*distance
        v1 = np.array((x, y, 0))
        position = v0 + v1
        atom = Atom(atom_type, position)

        self.__list.append(atom)

    def getAtoms(self):
        return Atoms(self.__list)


class LargeNanographene(Molecule):
    def __init__(
            self,
            parameters=DEFAULT,
            ):
        position = (1/np.tan(np.pi/6)*L/2, L/2, 0)
        atom = Atom('C', position)
        constructor = FlatConstructor(atom)
        constructor.addAtom(0, 30, 'N', L)
        constructor.addAtom(1,-30, 'C', L)
        constructor.addAtom(2, 45, 'C', L)
        constructor.addAtom(3,  0, 'C', L)
        constructor.addAtom(4,-45, 'C', L)
        constructor.addAtom(5, 15, 'C', L)
        constructor.addAtom(6, 75, 'C', L)
        constructor.addAtom(7,135, 'C', L)
        constructor.addAtom(8,195, 'C', L)
        constructor.addAtom(1, 90, 'C', L)
        constructor.addAtom(3,112, 'C', L)
        constructor.addAtom(8, 75, 'C', L)
        constructor.addAtom(12,135, 'C', L)
        constructor.addAtom(13,195, 'C', L)
        constructor.addAtom(14,255, 'C', L)

        atoms = constructor.getAtoms()

        atoms2 = atoms.copy()
        atoms2.positions[:, 1] = -atoms2.positions[:, 1]

        atoms.extend(atoms2)

        atoms1 = atoms.copy()
        atoms2 = atoms.copy()

        atoms1.rotate(a=120, v='z')
        atoms.extend(atoms1)

        atoms2.rotate(a=-120, v='z')
        atoms.extend(atoms2)

        atoms.rotate(a=30, v='z')
        atoms = passivate_atoms(atoms)

        Molecule.__init__(self, atoms)
