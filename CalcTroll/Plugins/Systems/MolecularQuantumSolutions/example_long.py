from CalcTroll.Interface import *

molecule = MolecularQuantumSolutions(12363)
method = Siesta(xc="PBE",
                mesh_cutoff=200 * Ry,
                spin='collinear',
                basis_set="DZP",
                )

molecule = Relaxed(method=method, system=molecule)

frame = save(molecule, 'molecule_topview.jpg', direction='-z')
