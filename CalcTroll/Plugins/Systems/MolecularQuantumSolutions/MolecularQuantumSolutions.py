from ase import io
from CalcTroll.Core.Flags import DEFAULT
from CalcTroll.Core.Explanation import Explanation
from CalcTroll.Plugins.Systems.InputToAtoms import InputToAtoms
from CalcTroll.ASEUtils.RegularizeMolecule import regularizeMolecularDirections

class MolecularQuantumSolutions(InputToAtoms):
    def __init__(self,
                 mqs_id,
                 name=DEFAULT,
                 ):
        InputToAtoms.__init__(
                self,
                single_input=mqs_id,
                name=name,
                )

    def generateAtoms(self):
        filename = 'MQS_%d.xyz' % self.singleInput()
        atoms = io.read(filename)

        atoms = regularizeMolecularDirections(atoms)

        return atoms

    def explain(self):
        text = "The molecule was imported from the Molecular Quantum Solutions datadase[*] with ID, %d." % self.singleInput()
        ex = Explanation(text,
                ["Molecular Quantum Solutions, Dashboard: https://dashboard.mqs.com"])
        ex += InputToAtoms.explain(self)

        return ex
