from CalcTroll.Interface import *

molecule = MolecularQuantumSolutions(12363)
method = Siesta(xc="PBE",
                mesh_cutoff=200 * Ry,
                spin='collinear',
                basis_set="DZP",
                )

molecule = Unrelaxed(system=molecule)

frame = save(molecule, 'molecule_unrelaxed_topview.jpg', direction='-z')
