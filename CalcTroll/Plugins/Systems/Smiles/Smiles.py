from os.path import join
import tempfile
import re
from ase import io
from ase.data import chemical_symbols
import numpy as np
from rdkit import Chem
from rdkit.Chem import AllChem

from CalcTroll import API
from CalcTroll.ASEUtils.AtomUtilities import orderAtoms
from CalcTroll.ASEUtils.Atoms import Atoms
from CalcTroll.ASEUtils.AtomUtilities import numberOfValenceElectrons
from CalcTroll.ASEUtils.RegularizeMolecule import regularizeMolecularDirections
from CalcTroll.Core.Flags import CalcTrollException, DEFAULT
from CalcTroll.Core.Explanation import Explanation

from CalcTroll.Plugins.Systems.InputToAtoms import InputToAtoms


chemical_symbols = list(chemical_symbols)
chemical_symbols.remove('X')

SMILES_DICT = {}


INPUTS = {
    'bonds' : ['-', '=', '#', '$', ':', '.', '/', '\\'],
    'ringstart' : ['%'],
    'parentheses' : ['(', ')'],
    'brackets' : ['[', ']'],
    'charge' : ['+', '-'],
    'digits' : [str(number) for number in range(10)],
    'organic' : ['B', 'C', 'N', 'O', 'S', 'P', 'F', 'I', 'Cl', 'Br'],
    'aromatic' : ['b', 'c', 'n', 'o', 's', 'p', 'se', 'as'],
    'element' : chemical_symbols,
    'chiral' : ['@'],
    }

SINGLE_INPUTS = []
for key in INPUTS:
    for entry in INPUTS[key]:
        SINGLE_INPUTS.extend(list(entry))
SINGLE_INPUTS = np.unique(SINGLE_INPUTS)

INPUTS['no_bracket_element'] = INPUTS['organic'] + INPUTS['aromatic']
INPUTS['no_bracket_element1'] = [entry for entry in INPUTS['no_bracket_element'] if len(entry) == 1]
INPUTS['no_bracket_element2'] = [entry for entry in INPUTS['no_bracket_element'] if len(entry) == 2]
INPUTS['no_bracket_first_of_two'] = [entry[0] for entry in INPUTS['no_bracket_element2']]

INPUTS['bracket_element'] = INPUTS['element'] + INPUTS['aromatic']
INPUTS['bracket_element1'] = [entry for entry in INPUTS['bracket_element'] if len(entry) == 1]
INPUTS['bracket_element2'] = [entry for entry in INPUTS['bracket_element'] if len(entry) == 2]
INPUTS['bracket_element_first_of_two'] = [entry[0] for entry in INPUTS['bracket_element2']]



SYMBOL_TO_MAX_NBONDS = {
        '.': 0,
        '-': 1,
        '\\': 1,
        '/': 1,
        '=': 2,
        '#': 3,
        '$': 4,
        }

AROMATIC_NBONDS = {
        ('B', 'B'): 1.5,
        ('B', 'C'): 1.5,
        ('B', 'N'): 1.5,
        ('B', 'P'): 1.5,
        ('C', 'C'): 1.5,
        ('C', 'N'): 1.5,
        ('C', 'P'): 1.5,
        ('C', 'N'): 1.5,
        ('N', 'N'): 1.5,
        ('N', 'P'): 1.5,
        ('P', 'P'): 1.5,
        }


MAX_VALENCE = {
    'B':3,
    'N':3,
    'C':4,
    'P':5,
    'O':2,
    'F':1,
    'I':1,
    'Cl':1,
    'Br':1,
    }



def validateParentheses(code):
    opn_counter = 0
    fail = False
    for l in code:
        if l == '(':
            opn_counter += 1
        if l == ')':
            if opn_counter == 0:
                fail = True
            opn_counter -= 1

    if not fail:
        fail = opn_counter != 0

    if fail:
        text = 'The Smiles code contains unmatched parentheses.'
        raise CalcTrollException(text)

    if code[0] == '(':
        text = 'The Smiles code may not start with a parenthesis.'
        raise CalcTrollException(text)

def validateNumbers(code):
    if code[0] in NUMBERS:
        text = 'The Smiles code may not start with a number.'
        raise CalcTrollException(text)

    matches = re.findall(r'([0-9])', code)
    if len(matches) == 0:
        return

    u_matches = np.unique(matches)
    for entry in u_matches:
        count = code.count(entry)
        if count != 2:
            text = 'The number %s does not occur exactly twice.' % entry
            raise CalcTrollException(text)

        between = code.split(entry)[1]
        matches = re.findall(r'([A-Z])', between)
        if len(matches) < 2:
            text = 'The ring number %s must occur with at least 2 atomic symbol in between.' % entry
            raise CalcTrollException(text)


def validateSequence(code):
    rep = createRepresentation(code)
    if rep[-1] == '-':
        text = "In '%s' the code ends with a bond: %s" % (code, code[-1])
    last_bond = None
    for i in range(len(rep) - 1):
        if rep[i] in SL_ATOMS:
            last_bond = None
        elif rep[i] in BONDS:
            if last_bond == None:
                last_bond = rep[i]
            else:
                text = "In '%s' two explicit bonds follow each other without an atom between at pos %d." % (code, i)
                raise CalcTrollException(text)

        if not rep[i+1] in ALLOWED_NEXT[rep[i]]:
            text = 'The symbol "%s" is not allowed to follow "%s".' % (code[i+1], code[i])
            raise CalcTrollException(text)


class Bonds(dict):
    def __setitem__(self, key, item):
        if key in self:
            text = 'A bond between position %d and %d is set several times.' % key
            raise CalcTrollException(text)
        if key[0] == key[1]:
            text = 'A bond is defined to the same atom positioned at %d' % key[0]
            raise CalcTrollException(text)

        dict.__setitem__(self, key, item)

class SmilesState(object):
    def __init__(self):
        self.aliases = {}
        self.closing_bonds = {}
        self.last_atoms = [None]
        self.last_atom = None
        self.atom_before_bracket = None
        self.last_bond = None
        self.opn_counter = 0
        self.explicit_count = {}
        self.atoms = {}
        self.bonds = Bonds()

    def openParen(self):
        self.opn_counter += 1
        if len(self.last_atoms) < self.opn_counter + 1:
            self.last_atoms.append(None)

    def closeParen(self, i):
        self.last_atoms[self.opn_counter] = None
        self.opn_counter -= 1
        if self.opn_counter < 0:
            text = "Position %d: Mismatched parenthesis" %i
            raise CalcTrollException(text)

    def addAtom(self, element, i):
        self.atoms[i] = element
        if self.last_atom is None:
            self.last_atom = i
            self.last_atoms[self.opn_counter] = i
            return

        last_atom = self.last_atoms[self.opn_counter]
        if last_atom is None:
            last_atom = self.last_atom

        if last_atom is None:
            last_atom = self.last_atom

        if self.last_bond is None:
            if element.islower() and self.atoms[last_atom].islower():
                self.last_bond = ':'
            else:
                self.last_bond = '-'

        self.bonds[(last_atom, i)] = self.last_bond
        self.last_atom = i
        self.last_atoms[self.opn_counter] = i
        self.last_bond = None

    def addBond(self, bond, i):
        if self.last_bond is not None:
            text = "Position %d: Two consequtive explicit bonds ('%s' and '%s') found. " % (i, self.last_bond, bond)
            raise CalcTrollException(text)

        self.last_bond = bond

    def addRingClosureNumber(self, number, i):
        if self.last_atom is None:
            text = "Position %d: Ring closure number before defining an atom." %i
            raise CalcTrollException(text)

        alias = self.aliases.get(number)
        opening = alias == None
        if opening:
            last_atom = self.last_atoms[self.opn_counter]
            if last_atom is None:
                text = "Position %d: Ring closure number before defining an atom." %i
                raise CalcTrollException(text)

            self.aliases[number] = last_atom
            self.closing_bonds[number] = self.last_bond

            if self.last_bond == None:
                self.last_bond = '-'

            self.last_bond = None
            self.aliases[number] = last_atom
        else:
            closure_bond = self.closing_bonds[number]
            if self.last_bond == None:
                pass
            elif closure_bond == None:
                closure_bond = self.last_bond
            else:
                if closure_bond != self.last_bond:
                    text = "Position %d: Ring closure bond defined as both ('%s' and '%s')" % (i, closure_bond, self.last_bond)
                    raise CalcTrollException(text)

            first_index = self.aliases[number]
            last_atom = self.last_atoms[self.opn_counter]

            if closure_bond == None:
                if self.atoms[first_index].islower() and self.atoms[last_atom].islower():
                    closure_bond = ':'
                else:
                    closure_bond = '-'

            if last_atom == None:
                text = "Position %d: Ring closure number at beginning of parenthesis." % (i)
                raise CalcTrollException(text)

            self.bonds[(first_index, last_atom)] = closure_bond

            self.last_bond = None
            del self.aliases[number]

    def addBracket(self, bracket, i):
        isotope, element, h_count, charge = interpretBracket(bracket)
        self.addAtom(element, i)
        self.explicit_count[i] = (h_count, charge)

    def finish(self):
        if self.opn_counter != 0:
            text = "%d parentheses were not closed at end of smiles code." % self.opn_counter
            raise CalcTrollException(text)

        if len(self.aliases.keys()) != 0:
            text = "Aromatic rings %s were opened but not closed." % list(self.aliases.keys())
            raise CalcTrollException(text)

        if self.last_bond != None:
            text = "A bond was not connected to a second atom."
            raise CalcTrollException(text)

        for key in self.atoms:
            value = self.atoms[key]
            if value[0].islower():
                self.atoms[key] = value.capitalize()

def interpretBracket(string):
    match = re.match(r'\d+', string)
    if match != None:
        isotope = match.group()
        j = len(isotope)
        string = string[j:]
        isotope = int(isotope)
    else:
        isotope = None

    if string[:2] in INPUTS['bracket_element2']:
        j = 2
    elif string[:1] in INPUTS['bracket_element1']:
        j = 1
    else:
        raise CalcTrollException()

    element = string[:j]
    string = string[j:]

    if string[:2] == '@@':
        string = string[2:]
    elif string[:1] == '@':
        string = string[1:]

    m_count = string.count('-')
    p_count = string.count('+')
    if (m_count, p_count) == (0, 0):
        sign = 0
        h_string = string
        charge_string = ''
    elif (m_count, p_count) == (1, 0):
        sign = -1
        h_string, charge_string = string.split('-')
    elif (m_count, p_count) == (0, 1):
        sign = 1
        h_string, charge_string = string.split('+')
    elif (m_count, p_count) == (2, 0):
        text = "Use of '++' in brackets depreceated."
        raise CalcTrollException(text)
    elif (m_count, p_count) == (0, 2):
        text = "Use of '--' in brackets depreceated."
        raise CalcTrollException(text)
    else:
        text = "Only one '+/-' sign allowed in bracket."
        raise CalcTrollException(text)

    if h_string == '':
        h_count = 0
    elif h_string == 'H':
        h_count = 1
    elif len(h_string) == 2 and h_string[0] == 'H' and h_string[1].isdigit():
        h_count = int(h_string[1])
    else:
        text = "Hydrogen count is incorrectly formatted."
        raise CalcTrollException(text)

    if charge_string == '':
        charge = sign
    elif len(charge_string) == 1 and charge_string[0].isdigit():
        charge = sign*int(charge_string)
    else:
        text = "Charge count is incorrectly formatted."
        raise CalcTrollException(text)

    return isotope, element, h_count, charge


def validateSyntax(code):
    validateInputs(code)

    code = str(code)

    state = SmilesState()
    if not code[0] in ['['] + INPUTS['no_bracket_element1'] + INPUTS['no_bracket_first_of_two']:
        text = "Position %d: '%s' at beginning of code." % (0, code[0])
        raise CalcTrollException(text)

    i = 0
    while i < len(code):
        symbol = code[i]
        end = i == len(code) - 1
        if symbol == '[':
            if end:
                text = "position -1: last symbol if '['."
                raise CalcTrollException(text)

            split = code[i:].split(']')
            if len(split) < 2:
                text = "Position %d: Unmatched '['"
                raise CalcTrollException(text)
            bracket = split[0][1:]

            state.addBracket(bracket, i)

            i += len(bracket) + 1


        elif symbol == ']':
            text = "Position %d: Unmatched ']'"
            raise CalcTrollException(text)

        elif symbol == '(':
            state.openParen()

        elif symbol == ')':
            state.closeParen(i)

            if code[i - 1] in ['('] + INPUTS['bonds']:
                text = "Position %d: '%s' before close parameter." % (i, code[i-1])
                raise CalcTrollException(text)

        elif symbol in INPUTS['bonds']:
            state.addBond(symbol, i)

        elif symbol.isalpha():
            if symbol in [entry[0] for entry in INPUTS['no_bracket_first_of_two']] and not end:
                test_element = code[i:i+2]
                if test_element in INPUTS['no_bracket_element2']:
                    state.addAtom(test_element, i)
                    i += 2
                    continue

            if symbol in INPUTS['no_bracket_element1']:
                state.addAtom(symbol, i)
                i += 1
                continue

            if not end:
                text = "Position %d: Neither '%s' nor '%s' recognized as elements in the organic subset." % (i, symbol, code[i:i+2])
            else:
                text = "Position %d: '%s' not recognized as an element in the organic subset." % (i, symbol)

            raise CalcTrollException(text)

        elif symbol in INPUTS['digits']:
            state.addRingClosureNumber(symbol, i)
            i += 1
            continue

        elif symbol == '%':
            if end:
                text = "'%' character found at end."
                raise CalcTrollException(text)
            match = re.match(r'\d+', code[i+1:])
            if match == None:
                text = "Position %d: '%' character found without subsequent digits."
                raise CalcTrollException(text)

            symbol = match.group()
            if len(symbol) == 1:
                text = "Position %d: " % i + "Character '%' used for single-digit ring closure number."
                raise CalcTrollException(text)
            elif len(symbol) > 2:
                symbol = symbol[:2]

            state.addRingClosureNumber(symbol, i)
            i += len(symbol) + 1
            continue
        else:
            text = "Position %d: Character %s not appropriate." % (i, symbol)
            raise CalcTrollException(text)

        i += 1

    state.finish()

    return state


def validateValence(code):
    state = validateSyntax(code)


    valence = {}
    for key in state.atoms.keys():
        valence[key] = 0

    for key in state.bonds:
        bond = state.bonds[key]
        if bond == ':':

            element_tuple = tuple(sorted([state.atoms[entry] for entry in key]))
            value = AROMATIC_NBONDS.get(element_tuple)
            if value == None:
                value = 1
        else:
            value = SYMBOL_TO_MAX_NBONDS[bond]

        i1, i2 = key
        valence[i1] += value
        valence[i2] += value

    errors = []
    for key in state.atoms:
        s = state.atoms[key]
        v = valence[key]
        max_valence = MAX_VALENCE.get(s)
        if max_valence != None and v > max_valence:
            errors.append((key, s, v, max_valence))

    if len(errors) == 0:
        return state

    text = "The maximum number of bonds in '%s' is exceeded:\n" % code
    for error in errors:
        text += "Smiles code position %d: '%s' atom - %d bonds, only %d allowed.\n" % (error)

    raise CalcTrollException(text)


def validateInputs(code):
    for i, symbol in enumerate(code):
        if not symbol in SINGLE_INPUTS:
            text = "Error Processing %s: The %s character at position %d is not used in Smiles syntax." % (code, symbol, i)
            raise CalcTrollException(text)

def validateRDKIT(state):
    UFF_ALLOWED = {'P', 'C', 'N', 'O', 'F', 'S', 'Cl', 'Br', 'I'}

    elements = set([state.atoms[key] for key in state.atoms.keys()])

    diff = elements.difference(UFF_ALLOWED)
    if len(diff) > 0:
        text = "The elements %s cannot be used by our pre-solver (RdKit-UFF)." % diff
        raise CalcTrollException(text)


def validateSmilesCode(code):
    state = validateValence(code)
    validateRDKIT(state)

    return state

def getValenceElectrons(code):
    state = validateSmilesCode(code)
    symbols = list(state.atoms.values())
    atoms = Atoms(symbols, [[0, 0, 0]]*len(symbols))

    return numberOfValenceElectrons(atoms)

class Smiles(InputToAtoms):
    def __init__(self,
                 smiles_code,
                 potential_strength=API.DEFAULT,
                 name=API.DEFAULT,
                 ):
        validateSmilesCode(smiles_code)
        InputToAtoms.__init__(
            self,
            single_input=smiles_code,
            potential_strength=potential_strength,
            name=name,
            )

    def generateAtoms(self):
        smiles_code = self.singleInput()
        atoms = SMILES_DICT.get(smiles_code)
        if atoms == None:
            atoms = SMILES2ASE(smiles_code)
            SMILES_DICT[smiles_code] = atoms.copy()
        else:
            atoms = atoms.copy()

        atoms = regularizeMolecularDirections(atoms)

        return atoms


    def explain(self):
        text = "The molecule was generated from the Smiles code, '%s', using the rd-kit library[*]." % self.singleInput()
        ex = Explanation(text,
                   ["RDKit: open-source chem-informatics package, http://www.rdkit.org"])
        ex += InputToAtoms.explain(self)

        return ex



def SMILES2ASE(smiles_code):
    # Creating mol object from SMILES and add H's
    mol = Chem.MolFromSmiles(smiles_code)
    try:
        mol_h = Chem.AddHs(mol)
    except TypeError:
        raise CalcTrollException("The SMILES code is not correctly formed.")

    # Number of conformers to be generated
    num_of_conformer=100
    max_iter=500

    # Default values for min energy conformer
    min_energy_UFF=10000
    min_energy_index_UFF=0
    min_energy_MMFF=10000
    min_energy_index_MMFF=0

    # Generate conformers (stored in side the mol object)
    cids = AllChem.EmbedMultipleConfs(mol_h, numConfs=num_of_conformer)
    ids = list(cids) #You can reach conformers by ids

    # copy molecule for UFF and MMFF optimizations
    mol_h_UFF=mol_h
    mol_h_MMFF=mol_h

    results_UFF = AllChem.UFFOptimizeMoleculeConfs(mol_h_UFF,maxIters=max_iter)
    results_MMFF = AllChem.MMFFOptimizeMoleculeConfs(mol_h_MMFF,maxIters=max_iter)

    # Search for the min energy conformer from results(tuple(is_converged,energy))
    for index, result in enumerate(results_MMFF):
        if(min_energy_MMFF>abs(result[1])):
            min_energy_MMFF=abs(result[1])
            min_energy_index_MMFF=index

    for index, result in enumerate(results_UFF):
        if(min_energy_UFF>result[1]):
            min_energy_UFF=result[1]
            min_energy_index_UFF=index

    # Compare UFF and MMFF min energies
    mol_to_save = Chem.Mol(mol_h_MMFF,False,min_energy_index_MMFF)
    if(min_energy_MMFF>min_energy_index_UFF):
        mol_to_save = Chem.Mol(mol_h_UFF,False,min_energy_index_UFF)
    else:
        mol_to_save = Chem.Mol(mol_h_MMFF,False,min_energy_index_MMFF)

    filename = join(tempfile.gettempdir(), 'mol.pdb')
    Chem.rdmolfiles.MolToPDBFile(mol_to_save, filename)
    atoms = io.read(filename)

    return atoms
