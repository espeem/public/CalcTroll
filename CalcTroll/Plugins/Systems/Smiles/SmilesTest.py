# Written by Mads Engelund, 2021, http://espeem.com
import unittest
from ase.atoms import Atoms
from CalcTroll import API
from CalcTroll.Core.Test.Case import CalcTrollTestCase
from CalcTroll.Core.Flags import CalcTrollException

from CalcTroll.Plugins.Systems.Smiles.Smiles import *

class SmilesTest(CalcTrollTestCase):
    def testConstruction(self):
        molecule = Smiles('C=C=C')
        self.assertEqual(molecule.pbc(), [False]*3)
        self.assertEqual(molecule.subSystems(), tuple())


    def testConstructionParenthesis(self):
        molecule = Smiles('CC(CC)CCC')
        self.assertEqual(molecule.pbc(), [False]*3)
        self.assertEqual(molecule.subSystems(), tuple())

        self.assertRaises(CalcTrollException, Smiles, '(CC)CC')
        self.assertRaises(CalcTrollException, Smiles, 'CC)C(C')
        self.assertRaises(CalcTrollException, Smiles, 'CC(CC)C(C')


    def testConstructionRing(self):
        molecule = Smiles('C1CCCC1')
        self.assertEqual(molecule.pbc(), [False]*3)
        self.assertEqual(molecule.subSystems(), tuple())

        self.assertRaises(CalcTrollException, Smiles, '1CCCCCC1')
        self.assertRaises(CalcTrollException, Smiles, 'C1CCC1CC1')
        self.assertRaises(CalcTrollException, Smiles, 'C1CCCCC')
        self.assertRaises(CalcTrollException, Smiles, 'C1C1CCCC')


    def testJunk(self):
        self.assertRaises(CalcTrollException, Smiles, 'dlsjlkajlfdjs')


    def testH(self):
        self.assertRaises(CalcTrollException, Smiles, 'CCCCCH')


    def testMessedUpSequence(self):
        self.assertRaises(CalcTrollException, Smiles, '(CC)N#CC')
        self.assertRaises(CalcTrollException, Smiles, 'CCN#-CC')
        self.assertRaises(CalcTrollException, Smiles, 'CCC(=)C')
        self.assertRaises(CalcTrollException, Smiles, 'CCC()C')
        self.assertRaises(CalcTrollException, Smiles, 'C0CC(0C)C')


    def testCisTrans(self):
        pos = Smiles("""F\C=C/F""").atoms().get_positions()
        pos = Smiles("""F\C=C\F""").atoms().get_positions()


    def testChiral(self):
        validateSyntax('[C@](C)OF')
        validateSyntax('[C@@](C)OF')

        self.assertRaises(CalcTrollException, validateSyntax, 'C@[C](C)OF')
        self.assertRaises(CalcTrollException, validateSyntax, 'C[@C](C)OF')


    def testTooManyBonds(self):
        molecule = Smiles('N1(N-N2)-N(C(CP2)-N1)C')
        self.assertEqual(molecule.pbc(), [False]*3)
        self.assertEqual(molecule.subSystems(), tuple())

        self.assertRaises(CalcTrollException, Smiles, 'N#N-N')
        self.assertRaises(CalcTrollException, Smiles, 'N1#(N#N2)-N(C(CP2)-N1)C')


    def testSimple(test):
        validateSyntax('CC')
        validateSyntax('CCO')
        validateSyntax('NCCCC')
        validateSyntax('CCCCN')
        validateSyntax('C=C')
        validateSyntax('C#N')
        validateSyntax('CC#CC')
        validateSyntax('CCC=O')
        validateSyntax('[Rh-](Cl)(Cl)(Cl)(Cl)$[Rh-](Cl)(Cl)(Cl)Cl')
        validateSyntax('C-C')
        validateSyntax('C-C-O')
        validateSyntax('C-C=C-C')
        validateSyntax('CCC(CC)CO')
        validateSyntax('CC(C)C(=O)C(C)C')
        validateSyntax('OCC(CCC)C(C(C)C)CCC')
        validateSyntax('C(C(C(C)))C')
        validateSyntax('OS(=O)(=S)O')


    def testRingClosure(self):
        validateSyntax('C1CCCCC1')
        validateSyntax('N1CC2CCCCC2CC1')
        validateSyntax('C1CCCCC1C1CCCCC1')
        validateSyntax('C1CCCCC1C2CCCCC2')
        validateSyntax('C12(CCCCC1)CCCCC2')

        self.assertRaises(CalcTrollException, validateSyntax, 'C12C2CCC1')
        self.assertRaises(CalcTrollException, validateSyntax, 'C12CCCC12')
        self.assertRaises(CalcTrollException, validateSyntax, 'C11')


    def testAdvancedRingClosure(self):
        validateSyntax('C%112(CCCCC%11)CCCCC2')

        self.assertRaises(CalcTrollException, validateSyntax, 'C%1CCCC%1')

    def testRingClosureBond(self):
        self.assertEqual(validateSyntax('C=1CCCCC=1').bonds[(0, 7)], '=')
        self.assertEqual(validateSyntax('C=1CCCCC1').bonds[(0, 7)], '=')
        self.assertEqual(validateSyntax('C1CCCCC=1').bonds[(0, 6)], '=')

        self.assertRaises(CalcTrollException, validateSyntax, 'C-1CCCCC=1')


    def testAromatic(self):
        validateSmilesCode('c1ccccc1')
        validateSmilesCode('C1=CC=CC=C1')
        validateSmilesCode('c1ccc2CCCc2c1')
        validateSmilesCode('C1=CC=CC(CCC2)=C12')
        validateSmilesCode('c1occc1')
        validateSmilesCode('C1OC=CC=1')
        validateSmilesCode('c1ccc1')
        validateSmilesCode('C1=CC=C1')


    def testBrackets(self):
        self.assertEqual(interpretBracket('U'), (None, 'U', 0, 0))
        self.assertEqual(interpretBracket('He'), (None, 'He', 0, 0))
        self.assertEqual(interpretBracket('Pb'), (None, 'Pb', 0, 0))
        self.assertEqual(interpretBracket('CH4'), (None, 'C', 4, 0))
        self.assertEqual(interpretBracket('ClH'), (None, 'Cl', 1, 0))
        self.assertEqual(interpretBracket('ClH1'), (None, 'Cl', 1, 0))
        self.assertEqual(interpretBracket('Cl-1'), (None, 'Cl', 0, -1))
        self.assertEqual(interpretBracket('OH1-'), (None, 'O', 1, -1))
        self.assertEqual(interpretBracket('OH-1'), (None, 'O', 1, -1))
        self.assertEqual(interpretBracket('Cu+2'), (None, 'Cu', 0, 2))
        self.assertEqual(interpretBracket('13CH4'), (13, 'C', 4, 0))
        self.assertEqual(interpretBracket('2H+'), (2, 'H', 0, 1))
        self.assertEqual(interpretBracket('238U'), (238, 'U', 0, 0))

        self.assertRaises(CalcTrollException, interpretBracket, 'dskdsjlfsja')
        self.assertRaises(CalcTrollException, interpretBracket, 'Cu++')
        self.assertRaises(CalcTrollException, interpretBracket, 'Cu4+44')
        self.assertRaises(CalcTrollException, interpretBracket, '2HeHe-1')


    def testAllElements(self):
        """ Test if any element either works or raises correct exception."""
        for element in chemical_symbols:
            if element in INPUTS['organic']:
                string = 'C' + element
            else:
                string = 'C[' + element + ']'
            try:
                Smiles(string)
            except CalcTrollException:
                pass

    def testValenceElectrons(self):
        self.assertEqual(getValenceElectrons('C1=CC=CC=C1'), 24)
        self.assertEqual(getValenceElectrons('N=N-N'), 15)



if __name__=='__main__':
    unittest.main()

