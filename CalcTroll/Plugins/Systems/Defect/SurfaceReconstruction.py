# Written by Mads Engelund, 2017, http://espeem.com
import numpy as np

from CalcTroll.Core.Utilities import vectorLength
from CalcTroll.ASEUtils.Atoms import Atoms
from CalcTroll.ASEUtils.AtomUtilities import removePeriodicallyEqualAtoms
from CalcTroll.ASEUtils.AtomUtilities import findIndices
from CalcTroll.ASEUtils.AtomUtilities import findCopies
from CalcTroll.ASEUtils.AtomUtilities import findPeriodicallyEqualAtoms
from CalcTroll.ASEUtils.AtomUtilities import makeAtomsRelaxed
from CalcTroll.ASEUtils.AtomUtilities import orderAtoms
from CalcTroll.Core.System.BasisChanger import BasisChanger, Transformation
from CalcTroll.Plugins.LengthScales import LengthScales
from CalcTroll.Plugins.Systems.Surface.Surface3D import SurfaceParameters3D, SurfaceClusterParameters3D
from CalcTroll.Plugins.Systems.Defect import SurfaceDefect
from CalcTroll.Plugins.Systems.Defect.SurfaceDefect import makeCenters, findAtoms

from CalcTroll import API
from CalcTroll.API import DEFAULT

class SurfaceReconstructionParameters(SurfaceParameters3D):
    def __init__(
            self,
            free_layers=DEFAULT,
            bound_layers=2,
            electrode_layers=0,
            vectors=DEFAULT,
            kpts=DEFAULT,
            cell_size=DEFAULT,
            length_scales=LengthScales(),
            passivated=True,
            ):
        pass

    def neededKpts(self, atoms):
        scales = self['length_scales']
        unit_cell = atoms.cell[atoms.pbc]
        kpts = np.ones(len(unit_cell))
        for i, vector in enumerate(unit_cell):
            length=np.max(abs(vector))
            number=int(np.ceil(scales['correlation_length']/length))
            kpts[i] = number

        return kpts

class SurfaceReconstruction(SurfaceDefect):
    @classmethod
    def parameterClass(cls):
        return SurfaceReconstructionParameters

    def __init__(
            self,
            surface,
            name=DEFAULT,
            identifiers=tuple(),
            added_atoms=DEFAULT,
            periodicity=((1, 0), (0, 1),),
            charge=0,
            initial_state=DEFAULT,
            parameters=DEFAULT,
            ):
        if name is DEFAULT:
            name = surface.name()

        assert np.array(periodicity).shape == (2, 2)
        SurfaceDefect.__init__(
                self,
                surface=surface,
                name=name,
                identifiers=identifiers,
                added_atoms=added_atoms,
                periodicity=periodicity,
                initial_state=initial_state,
                parameters=parameters,
                )

    def subParameters(self, parameters=API.DEFAULT):
        parameters = self.defaultParameters(parameters)
        parameter_cls = self.surface().parameterClass()
        if isinstance(parameters, SurfaceParameters3D):
            parameters = parameters.copy(vectors=((1,0), (0,1)), free_layers=API.DEFAULT, kpts=API.DEFAULT)
            parameters = parameter_cls(**parameters)
            parameters = self.surface().defaultParameters(parameters=parameters)
        elif isinstance(parameters, SurfaceClusterParameters3D):
            scales = parameters['length_scales']
            parameters = parameter_cls(length_scales=scales, vectors=((1, 0), (0, 1)))
            parameters = self.surface().defaultParameters(parameters=parameters)
        else:
            raise Exception

        return (parameters, )

    def defaultParameters(self, parameters=API.DEFAULT, centers=API.DEFAULT):
        if centers is API.DEFAULT:
            centers = ((0, 0, 0), )

        if parameters is DEFAULT:
            surface_parameters = self.surface().defaultParameters(centers=centers)
            parameters = SurfaceReconstructionParameters(**surface_parameters)


        parameters = self.defaultPeriodicParameters(parameters=parameters, centers=centers)

        return parameters

    def defaultPeriodicParameters(self, parameters=API.DEFAULT, centers=((0,0,0), )):
        surface = self.surface().fullyUnrelaxed()
        added_atoms, a_coordinates = self._addedAtoms()

        surface_parameters = self.surface().defaultParameters(parameters, centers=centers)
        surface_parameters = surface_parameters.toDictionary()
        surface_parameters.pop('kpts')
        surface_parameters.pop('vectors')

        if parameters is DEFAULT:
            parameters = SurfaceReconstructionParameters(**surface_parameters)
        else:
            parameter_dict = parameters.toDictionary()
            for key, value in surface_parameters.items():
                if parameter_dict.get(key) is None:
                    parameter_dict[key] = surface_parameters[key]

            parameters = SurfaceReconstructionParameters(**parameter_dict)

        vectors = parameters['vectors']
        if vectors is DEFAULT:
            new_centers = makeCenters(
                    surface=surface,
                    identifiers=self.identifiers(),
                    added_atoms=added_atoms,
                    periodicity=self.periodicity(),
                    )
            tmp_centers = centers
            centers = np.zeros((len(tmp_centers)+len(new_centers), 3), float)
            centers[:len(tmp_centers)] = tmp_centers
            centers[len(tmp_centers):] = new_centers

            s_parameters = self.surface().defaultParameters(
                    parameters=parameters,
                    centers=centers,
                    )
            vectors = np.array(s_parameters['vectors'])
            parameters = parameters.copy(vectors=vectors)

            if len(self.periodicity()) == 1:
                vp = self.periodicity()[0]
                v1, v2 = parameters['vectors']
                if (vectorLength(np.cross(vp, v1))==0):
                    v1 = vp
                elif (vectorLength(np.cross(vp, v2))==0):
                    v2 = vp
                else:
                    raise Exception

                vectors = np.array((v1, v2))
                parameters = parameters.copy(vectors=vectors)

        kpts = parameters['kpts']
        if kpts is DEFAULT:
            s_parameters = self.surface().defaultParameters(
                    parameters=parameters,
                    )
            kpts = s_parameters['kpts']
            parameters = parameters.copy(kpts=kpts)

        cell_size = parameters['cell_size']
        if cell_size is DEFAULT:
            parameters = parameters.copy(cell_size=45)

        return parameters

    def neededKpts(self, unit_cell, correlation_length):
        kpts=[]
        unit_cell = self.surface().change(unit_cell, fro='surface', to='orth_surface')
        for vector in unit_cell:
            vector = vector/self.surface().directionLenghtMultipliers()
            length=np.sqrt(np.sum(vector**2))
            number=int(np.ceil(correlation_length/length))
            kpts.append(number)

        return kpts

    def bandStructurePath(self):
        path = [
                ((0.0, 0.0, 0.0), '\\Gamma'),
                ((0.5,   0, 0.0), 'A'),
                ((0.5, 0.5, 0.0), 'AB'),
                ((0.0, 0.5, 0.0), 'B'),
                ((0.0, 0.0, 0.0), '\\Gamma'),
                ]
        return path
