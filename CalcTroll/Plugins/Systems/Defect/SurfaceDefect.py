# Written by Mads Engelund, 2017, http://espeem.com
import numpy as np
from ase.visualize import view
from CalcTroll import API
from CalcTroll.API import DEFAULT

from CalcTroll.ASEUtils.Atoms import Atoms
from CalcTroll.ASEUtils.AtomUtilities import orderAtoms
from CalcTroll.ASEUtils.AtomUtilities import findIndices
from CalcTroll.ASEUtils.AtomUtilities import removePeriodicallyEqualAtoms
from CalcTroll.ASEUtils.AtomUtilities import makeAtomsRelaxed
from CalcTroll.ASEUtils.AtomUtilities import cellCenter
from CalcTroll.Core.Visualizations import flatMatplotlibView
from CalcTroll.Core.Visualizations import addMeasuringStick
from CalcTroll.Plugins.Systems.Surface.Surface3D import convertShorthand, setCellAndPBC
from CalcTroll.Plugins.Systems.Defect import Defect
from CalcTroll.Core.System.BasisChanger import BasisChanger, Transformation

from CalcTroll.Plugins.Systems.Surface.Surface import setCellAndPBC

class SurfaceDefect(Defect):
    def __init__(
            self,
            surface,
            name=DEFAULT,
            identifiers=tuple(),
            added_atoms=DEFAULT,
            periodicity=tuple(tuple()),
            charge=0,
            initial_state=DEFAULT,
            parameters=DEFAULT,
            ):
        Defect.__init__(
                self,
                base_system=surface,
                name=name,
                identifiers=identifiers,
                added_atoms=added_atoms,
                periodicity=periodicity,
                charge=charge,
                initial_state=initial_state,
                parameters=parameters,
                )

    def convertIdentifierShorthand(self, identifiers):
        return convertShorthand(identifiers)

    def name(self):
        name = API.System.name(self)
        if self.charge() != 0:
            name += '_Q%.4f' % self.charge()

        return name

    def surface(self):
        return self.baseSystem()

    def setUpBasisChanger(self):
        surface = self.surface()
        extra_atoms = surface.extraAtoms(coordinates='orth_surface')
        crystal_atoms = surface.crystalAtoms(coordinates='orth_surface')
        added_atoms, a_coordinates = self._addedAtoms()
        identifiers = self.identifiers()
        periodicity = self.periodicity()


        transformations = []
        if len(identifiers) > 0:
            atoms = findAtoms(extra_atoms, crystal_atoms, identifiers, surface.cell('orth_surface'), periodicity)
            point = atoms.positions.mean(0)
            surface_transformation = Transformation(
                    to='orth_surface',
                    fro='orth_defect',
                    rotation_point=[point, [0,0,0]],
                    )
        elif len(added_atoms) > 0:
            point = added_atoms.get_positions().mean(axis=0)
            point[2] = added_atoms.get_positions()[:, 2].max()
            surface_transformation = Transformation(
                    to='orth_surface',
                    fro='orth_defect',
                    rotation_point=[point, [0,0,0]],
                    )
        else:
            surface_transformation = Transformation(
                    to='orth_surface',
                    fro='orth_defect',
                    )
        transformations.append(surface_transformation)
        basis_changer = BasisChanger(transformations=transformations)

        return basis_changer

    def atomsTemplate(self, parameters):
        system = self.fullyUnrelaxed()

        remove_atoms, coordinates = system._removedAtoms()
        added_atoms, coordinates = system._addedAtoms()

        surface = system.surface()
        atoms = surface.atomsTemplate(
                parameters=parameters,
                )
        atoms = removePeriodicallyEqualAtoms(remove_atoms, atoms)
        atoms += added_atoms

        if len(added_atoms) > 0:
            atoms.setRegion(name=self.name(), picker=slice(-len(added_atoms), None), cell=atoms.get_cell(), pbc=(False, False, False))

        atoms = system.change(
                atoms,
                to='orth_defect',
                fro='orth_surface',
                )
        atoms.wrap(center=0, eps=0.01)
        #atoms = orderAtoms(atoms)

        cell_center = cellCenter(atoms)
        atoms.set_celldisp(-cell_center)

        return atoms

    def makeSubsystemsInitialized(self, atoms):
        diff_vector = np.zeros(atoms.positions.shape)
        init_moments = np.zeros(len(atoms))

        for sub_system in self.subSystems():
            v, i = sub_system.makeAtomsInitialized(atoms)
            diff_vector += v
            init_moments += i

        return diff_vector, init_moments

    def makeSubsystemsRelaxed(self, atoms, tolerance=1.0):
        unrelaxed = self.fullyUnrelaxed()
        atoms = unrelaxed.change(atoms, fro='orth_defect', to='orth_surface')
        atoms = self.surface().makeSubsystemsRelaxed(
                atoms,
                tolerance=tolerance,
                )
        atoms = self.surface().makeAtomsRelaxed(atoms)
        atoms = self.change(atoms, fro='orth_surface', to='orth_defect')

        return atoms

    def _findAtoms(self,
            identifiers,
            ):
        surface = self.surface().fullyUnrelaxed()
        extra_atoms = surface.extraAtoms()
        crystal_atoms = surface.crystalAtoms(end_layer=1)
        unit_cell = surface.cell()
        added_atoms, coord = self._addedAtoms()
        added = np.array([isinstance(i, int) for i in identifiers])
        surface_identifiers = [i for i in identifiers if isinstance(i, tuple)]

        surface_atoms = findAtoms(
                extra_atoms,
                crystal_atoms,
                surface_identifiers,
                unit_cell,
                periodicity=self.periodicity(),
                )

        atoms = Atoms([], cell=extra_atoms.get_cell(), pbc=extra_atoms.get_pbc())
        for i in range(len(identifiers)):
            if added[i]:
                atoms.append(added_atoms[identifiers[i]])
            else:
                atoms.append(surface_atoms.pop(0))

        return atoms, 'orth_surface'

    def _addedAtoms(self):
        surface = self.surface().fullyUnrelaxed()
        extra_atoms = surface.extraAtoms()
        unit_cell = surface.unitCell()
        periodicity = self.periodicity()
        atoms = Atoms([], cell=extra_atoms.get_cell(), pbc=extra_atoms.get_pbc())
        base_atoms, coords = Defect._addedAtoms(self)
        atoms += base_atoms
        setCellAndPBC(atoms, periodicity)

        return atoms, 'orth_surface'

    def addedAtoms(
            self,
            initialized=True,
            relaxed=True,
            coordinates='orth_defect',
            ):
        atoms, base_coordinates = self._addedAtoms()
        atoms = self.fullyUnrelaxed().change(
                atoms,
                to='orth_defect',
                fro=base_coordinates,
                )

        atoms = self.applyInitializationConstraintsAndRelaxation(
                atoms=atoms,
                initialized=initialized,
                relaxed=relaxed,
                )
        atoms = self.change(atoms, to=coordinates, fro='orth_defect')

        return atoms

    def cell(self):
        cell = self.surface().cell('orth_surface')
        cell = self.change(cell, to='orth_defect', fro='orth_surface', difference=True)
        cell = np.dot(self.periodicity(), cell[:2])

        return cell

    def rattleAtoms(self, coordinates='orth_defect'):
        atoms = self.removedAtoms(coordinates)
        atoms += self.addedAtoms(coordinates)

        return atoms

    def initialStateIdentifiers(self, identifiers):
        return convertShorthand(identifiers)

    def findLayers(self,
            atoms,
            start_layer,
            end_layer,
            coordinates='orth_defect',
            ):
        atoms = self.change(atoms, to='orth_surface', fro=coordinates)
        return self.surface().findLayers(
                atoms,
                start_layer=start_layer,
                end_layer=end_layer
                )


    def bandStructurePath(self):
        path = [
                ((0.0, 0.0, 0.0), '\\Gamma'),
                ((0.5,   0, 0.0), 'A'),
                ((0.5, 0.5, 0.0), 'AB'),
                ((0.0, 0.5, 0.0), 'B'),
                ((0.0, 0.0, 0.0), '\\Gamma'),
                ]
        return path

    def dimension(self):
        return len(self.periodicity())

    def hasRelaxedSurface(self):
        return self.surface().isRelaxed()

    def findAtomIndices(self, identifiers, atoms, initialized=True, coordinates='orth_defect'):
        find_atoms = self.findAtoms(identifiers, initialized=initialized, coordinates=coordinates)

        indices = findIndices(find_atoms, atoms)

        return indices

    def _removedAtoms(self):
        atoms = self._findAtoms(self.identifiers())

        return atoms

    def removedAtoms(
            self,
            initialized=True,
            relaxed=True,
            coordinates='orth_defect',
            ):
        atoms, base_coordinates = self._removedAtoms()
        atoms = self.fullyUnrelaxed().change(
                atoms,
                to='orth_defect',
                fro=base_coordinates,
                )

        atoms = self.applyInitializationConstraintsAndRelaxation(
                atoms=atoms,
                initialized=initialized,
                relaxed=relaxed,
                )

        atoms = self.change(atoms, to=coordinates, fro='orth_defect')

        return atoms

    def removedAtomsIndices(self, atoms, coordinates='orth_defect'):
        return self.findAtomIndices(
                                    self.identifiers(),
                                    atoms,
                                    coordinates=coordinates,
                                    relaxed=relaxed,
                                    )

    def pbc(self):
        return (True, True, False)

    def jpg_save(
            self,
            filename,
            data=None,
            frame=None,
            border=API.DEFAULT,
            **kwargs):

        if data is None:
            data = self.read()

        return self.surface().jpg_save(
            filename=filename,
            data=data,
            frame=frame,
            border=border,
            **kwargs,
            )


    def frame(self, data=None, border=API.DEFAULT, direction='-z'):
        if data is None:
            atoms = self.atoms()
        else:
            atoms = data

        cell_center = cellCenter(atoms)
        pmin, pmax = -cell_center, cell_center

        if border is API.DEFAULT:
            border = 4

        pmin -= border
        pmax += border

        if direction == '-z':
            frame = np.array([pmin[:2], pmax[0:2]])
        elif direction == '-y':
            frame = np.array([[pmin[0], pmin[2]], [pmax[0], pmax[2]]])
        elif direction == '-x':
            frame = np.array([[pmin[1], pmin[2]], [pmax[1], pmax[2]]])
        else:
            raise Exception

        return frame

    def plot(self,
             data=None,
             ax=None,
             frame=None,
             border=4,
             color='white',
             size=0.55,
             ):
        if data is None:
            atoms = self.atoms()
        else:
            atoms = data

        if frame is None:
            frame = self.frame(data=data, border=border)

        self.plotUnitCell(data=atoms, ax=ax)

        atoms = atoms.repeat((4, 4, 1))
        move = -cellCenter(atoms)
        atoms.translate(move)

        flatMatplotlibView(atoms, ax, direction='-z', size=size)
        if frame is not None:
            ax.set_xlim((frame[0, 0], frame[1, 0]))
            ax.set_ylim((frame[0, 1], frame[1, 1]))
            addMeasuringStick(ax, color=color, size=5)

        ax.patch.set_facecolor('black')
        ax.set_xticks([])
        ax.set_yticks([])

        return frame

    def plotUnitCell(self, data=None, ax=None, direction='-z'):
        if data is None:
            data = data.read()

        return self.surface().plotUnitCell(data=data, ax=ax, direction=direction)


def makeCenters(surface, identifiers, added_atoms, periodicity):
    extra_atoms = surface.extraAtoms()
    crystal_atoms = surface.crystalAtoms(end_layer=1)
    unit_cell = surface.cell()
    atoms = findAtoms(
            extra_atoms,
            crystal_atoms,
            identifiers,
            unit_cell,
            periodicity,
            )
    atoms += added_atoms

    return atoms.get_positions()

def findAtoms(extra_atoms, crystal_atoms, identifiers, surface_unit_cell, periodicity):
    atoms = Atoms([], cell=extra_atoms.get_cell(), pbc=extra_atoms.get_pbc())

    for identifier in identifiers:
        number, cell = identifier

        if number >= 0:
            atom = extra_atoms.copy()[number]
            depth = 0
        elif number < 0:
            number = -(number + 1)
            depth, crystal_number = divmod(number, len(crystal_atoms))

            atom=crystal_atoms.copy()[crystal_number]
        else:
            raise Exception

        shift = -surface_unit_cell[2]*depth

        cell = (cell[0], cell[1], 0)
        shift += np.dot(cell, surface_unit_cell)

        atom.position += shift
        atoms += atom

    setCellAndPBC(atoms, periodicity)

    return atoms
