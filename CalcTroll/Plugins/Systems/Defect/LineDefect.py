# Written by Mads Engelund, 2017, http://espeem.com
import numpy as np

from CalcTroll import API
from CalcTroll.API import DEFAULT
from CalcTroll.Core.Utilities import vectorLength
from CalcTroll.ASEUtils.Atoms import Atoms
from CalcTroll.ASEUtils.AtomUtilities import removePeriodicallyEqualAtoms
from CalcTroll.ASEUtils.AtomUtilities import wrap
from CalcTroll.ASEUtils.AtomUtilities import orderAtoms
from CalcTroll.ASEUtils.AtomUtilities import cellCenter
from CalcTroll.Core.System.BasisChanger import BasisChanger, Transformation

from CalcTroll.Plugins.LengthScales import LengthScales
from CalcTroll.Plugins.Systems.Defect import Defect


class LineDefectParameters(API.Parameters):
    def __init__(
            self,
            vectors=DEFAULT,
            kpts=DEFAULT,
            padding=DEFAULT,
            ):
        pass

    def kpts(self):
        return self['kpts']

    def ignoreLongRangeCorrelation(self):
        if self['padding'] is DEFAULT:
            padding = LengthScales.defaults()['short_correlation_length']
            return self.copy(padding=padding)
        else:
            return self

    def neededKpts(self, vectors, unit_cell):
        scales = LengthScales.defaults()
        length = vectors[0][0]*vectorLength(unit_cell[0])
        kpts = np.ones(len(unit_cell), int)
        number=int(np.ceil(scales['correlation_length']/length))
        kpts[0] = number

        return kpts

class LineDefect(Defect):
    @classmethod
    def parameterClass(cls):
        return LineDefectParameters

    def subParameters(self, parameters=DEFAULT):
        if parameters is DEFAULT:
            parameters = self.parameters()

        parameters = self.defaultParameters(parameters)
        parameters = parameters.copy(vectors=((1,0),), kpts=DEFAULT)
        parameter_cls = self.crystal1d().parameterClass()

        return (parameter_cls(**parameters), )

    def crystal1d(self):
        return self.baseSystem()

    def setUpBasisChanger(self):
        crystal1d = self.crystal1d()
        crystal_atoms = crystal1d.atoms()
        added_atoms = self._addedAtoms()
        identifiers = self.identifiers()

        transformations = []
        if len(identifiers) > 0:
            atoms = findAtoms(crystal_atoms, identifiers)
            point = atoms.positions.mean(0)
            surface_transformation = Transformation(
                    to='orth_crystal',
                    fro='orth_defect',
                    rotation_point=[point, [0,0,0]],
                    )
        elif len(added_atoms) > 0:
            point = added_atoms.get_positions().mean(axis=0)
            point[2] = added_atoms.get_positions()[:, 2].max()
            surface_transformation = Transformation(
                    to='orth_crystal',
                    fro='orth_defect',
                    rotation_point=[point, [0,0,0]],
                    )
        else:
            surface_transformation = Transformation(
                    to='orth_crystal',
                    fro='orth_defect',
                    )
        transformations.append(surface_transformation)
        basis_changer = BasisChanger(transformations=transformations)

        return basis_changer

    def defaultParameters(self, parameters=DEFAULT, centers=((0,0,0), )):
        crystal1d = self.crystal1d().fullyUnrelaxed()
        added_atoms, coordinates = self._addedAtoms()

        if parameters is DEFAULT:
            parameters = crystal1d.defaultParameters()
            parameters = parameters.toDictionary()
            parameters.pop('kpts')
            parameters.pop('vectors')
            parameters = LineDefectParameters(**parameters)

        scales = LengthScales.defaults()
        s_length = scales['short_correlation_length']
        c_length = scales['correlation_length']
        unit_cell = crystal1d.unitCell()

        vectors = parameters['vectors']
        if vectors is DEFAULT:
            new_centers = makeCenters(
                    crystal1d=crystal1d,
                    identifiers=self.identifiers(),
                    added_atoms=added_atoms,
                    )
            tmp_centers = centers
            centers = np.zeros((len(tmp_centers)+len(new_centers), 3), float)
            centers[:len(tmp_centers)] = tmp_centers
            centers[len(tmp_centers):] = new_centers
            length = centers[:, 1].max() - centers[:, 1].min()
            length += s_length
            u_length = vectorLength(unit_cell[0])
            number=int(np.ceil(length/u_length)) * 2
            vectors = [[number]]

            parameters = parameters.copy(vectors=vectors)


        kpts = parameters['kpts']
        if kpts is DEFAULT:
            kpts = parameters.neededKpts(vectors, unit_cell)
            parameters = parameters.copy(kpts=kpts)

        padding = parameters['padding']
        if padding is DEFAULT:
            parameters = parameters.copy(padding=s_length)

        return parameters

    def __init__(
            self,
            crystal1d,
            name=DEFAULT,
            identifiers=tuple(),
            added_atoms=DEFAULT,
            initial_state=DEFAULT,
            parameters=DEFAULT,
            ):
        Defect.__init__(
                self,
                base_system=crystal1d,
                name=name,
                identifiers=identifiers,
                added_atoms=added_atoms,
                initial_state=initial_state,
                parameters=parameters,
                )

    def bandStructurePath(self):
        eeeeeeeeeeeeeeeeee
        path = [
                ((0.0, 0.0, 0.0), '\\Gamma'),
                ((0.5,   0, 0.0), 'A'),
                ((0.5, 0.5, 0.0), 'AB'),
                ((0.0, 0.5, 0.0), 'B'),
                ((0.0, 0.0, 0.0), '\\Gamma'),
                ]
        return path

    def atomsTemplate(self, parameters):
        remove_atoms, coordinates = self._removedAtoms()
        added_atoms, coordinates = self._addedAtoms()

        crystal = self.crystal1d()
        atoms = crystal.atomsTemplate(
                parameters=parameters,
                )
        atoms = removePeriodicallyEqualAtoms(remove_atoms, atoms)
        atoms += added_atoms
        self.change(atoms, to='orth_defect', fro='orth_crystal')
        atoms = wrap(atoms, center=[0.5, 0, 0])
        atoms = orderAtoms(atoms, (0, 1, 2))
        atoms.set_celldisp(-atoms.cell.sum(axis=1)/2)

        return atoms

    def makeSubsystemsRelaxed(self, atoms, tolerance=1.0):
        crystal = self.crystal1d()

        if crystal.relaxation() is not None:
            self.change(atoms, to='orth_defect', fro='orth_crystal')
            atoms = crystal.makeAtomsRelaxed(atoms)
            self.change(atoms, fro='orth_defect', to='orth_crystal')

        return atoms

    def _findAtoms(self,
            identifiers,
            ):
        crystal1d = self.crystal1d().fullyUnrelaxed()
        crystal_atoms = crystal1d.atoms()

        atoms = findAtoms(
                crystal_atoms,
                identifiers,
                )

        return atoms, 'orth_crystal'

    def pbc(self):
        return [True, False, False]

    def plotUnitCell(self, data=None, ax=None, direction='-z'):
        if data is None:
            data = data.read()

        return self.baseSystem().plotUnitCell(data=data, ax=ax, direction=direction)

    def frame(self, data=None, direction='-z', border=API.DEFAULT):
        if not self.isDone():
            return None

        if data is None:
            atoms = self.atoms()
        else:
            atoms = data

        cell_center = cellCenter(atoms)
        pmin, pmax = -cell_center, cell_center

        if border is API.DEFAULT:
            border = 4

        pmin -= border
        pmax += border

        return np.array([pmin, pmax])

    def plot(self,
             data=None,
             ax=None,
             frame=None,
             direction='-z',
             size=1.,
             bar=True,
             facecolor='black',
             ):
        if data is None:
            atoms = self.atoms()
        else:
            atoms = data

        cell = atoms.get_cell()
        atoms = atoms.repeat((3, 1, 1))
        atoms.translate(-cell[0])
        take1 = atoms.positions[:, 0] > frame[0, 0]
        take2 = atoms.positions[:, 0] < frame[1, 0]
        take = np.logical_and(take1, take2)
        atoms = atoms[take]
        atoms.set_cell(cell)
        Defect.plot(
                self,
                data=atoms,
                ax=ax,
                frame=frame,
                direction=direction,
                size=size,
                bar=bar,
                facecolor=facecolor,
                )

def makeCenters(crystal1d, identifiers, added_atoms):
    crystal_atoms = crystal1d.atoms()
    atoms = findAtoms(
            crystal_atoms,
            identifiers,
            )
    atoms += added_atoms

    return atoms.get_positions()

def findAtoms(crystal_atoms, identifiers):
    unit_cell = crystal_atoms.cell
    atoms = Atoms([])

    for identifier in identifiers:
        cell, number = identifier
        atom = crystal_atoms[number]
        atom.position += unit_cell[0]*cell
        atoms += atom

    return atoms
