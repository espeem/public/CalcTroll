# Written by Mads Engelund, 2017, http://espeem.com
import unittest

from CalcTroll.Core.System import InitialStateAtom, InitialState
from CalcTroll.Core.Test.Dummies import TestRelaxationSystem
from CalcTroll.Core.Test.Case import CalcTrollTestCase
from ase.constraints import FixAtoms

from CalcTroll.ASEUtils.Atoms import Atoms
from CalcTroll.Plugins.Systems.Surface.Surface3D import SurfaceParameters3D
from .SurfacePointDefect import SurfacePointDefect
from CalcTroll.API import view

class SurfacePointDefectTest(CalcTrollTestCase):
    pass


if __name__=='__main__':
    unittest.main()
