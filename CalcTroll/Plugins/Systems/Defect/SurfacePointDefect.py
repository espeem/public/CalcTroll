# Written by Mads Engelund, 2017, http://espeem.com
import numpy as np

from CalcTroll.ASEUtils.AtomUtilities import removePeriodicallyEqualAtoms
from CalcTroll.ASEUtils.AtomUtilities import findIndices
from CalcTroll.ASEUtils.AtomUtilities import findCopies
from CalcTroll.ASEUtils.AtomUtilities import cellCenter
from CalcTroll.ASEUtils.AtomUtilities import findPeriodicallyEqualAtoms
from CalcTroll.ASEUtils.AtomUtilities import makeAtomsRelaxed
from CalcTroll.ASEUtils.Atoms import Atoms
from CalcTroll.Core.Visualizations import flatMatplotlibView
from CalcTroll.Core.Visualizations import addMeasuringStick
from CalcTroll.Core.System.BasisChanger import BasisChanger, Transformation
from CalcTroll.Plugins.LengthScales import LengthScales
from CalcTroll.Plugins.Systems.Surface.Surface import setCellAndPBC
from CalcTroll.Plugins.Systems.Surface.Surface3D import SurfaceParameters3D, convertShorthand
from CalcTroll.Plugins.Systems.Defect import SurfaceDefect
from CalcTroll.Plugins.Systems.Defect.SurfaceDefect import makeCenters
from CalcTroll import API
from CalcTroll.API import DEFAULT


class SurfacePointDefectParameters(SurfaceParameters3D):
    def __init__(
            self,
            free_layers=DEFAULT,
            bound_layers=2,
            electrode_layers=0,
            vectors=DEFAULT,
            kpts=DEFAULT,
            cell_size=DEFAULT,
            length_scales=LengthScales(),
            passivated=True,
            ):
        pass

    def neededKpts(self, atoms):
        scales = self['length_scales']
        unit_cell = atoms.cell[atoms.pbc]
        kpts = np.ones(len(unit_cell))
        for i, vector in enumerate(unit_cell):
            length=np.max(abs(vector))
            number=int(np.ceil(scales['correlation_length']/length))
            kpts[i] = number

        return kpts

class SurfacePointDefect(SurfaceDefect):
    @classmethod
    def parameterClass(cls):
        return SurfacePointDefectParameters

    def subParameters(self, parameters=DEFAULT):
        if parameters is DEFAULT:
            parameters = self.parameters()

        parameters = parameters.copy(vectors=((1,0), (0,1)), kpts=DEFAULT)

        return (SurfacePointDefectParameters(**parameters), )

    def defaultParameters(self, parameters=DEFAULT, centers=((0,0,0), )):
        surface = self.surface().fullyUnrelaxed()
        added_atoms, coordinates = self._addedAtoms()

        surface_parameters = self.surface().defaultParameters()
        surface_parameters = surface_parameters.toDictionary()
        surface_parameters.pop('kpts')
        surface_parameters.pop('vectors')
        if parameters is DEFAULT:
            parameters = SurfacePointDefectParameters(**surface_parameters)
        else:
            parameter_dict = parameters.toDictionary()
            for key, value in surface_parameters.items():
                if parameter_dict.get(key) is None:
                    parameter_dict[key] = surface_parameters[key]

            parameters = SurfacePointDefectParameters(**parameter_dict)

        vectors = parameters['vectors']
        if vectors is DEFAULT:
            new_centers = makeCenters(
                    surface=surface,
                    identifiers=self.identifiers(),
                    added_atoms=added_atoms,
                    periodicity=self.periodicity(),
                    )
            tmp_centers = centers
            centers = np.zeros((len(tmp_centers)+len(new_centers), 3), float)
            centers[:len(tmp_centers)] = tmp_centers
            centers[len(tmp_centers):] = new_centers

            s_parameters = surface.defaultParameters(
                    parameters=parameters,
                    centers=centers,
                    )
            parameters = parameters.copy(vectors=s_parameters['vectors'])

        kpts = parameters['kpts']
        if kpts is DEFAULT:
            s_parameters = surface.defaultParameters(
                    parameters=parameters,
                    )
            kpts = s_parameters['kpts']
            parameters = parameters.copy(kpts=kpts)

        cell_size = parameters['cell_size']
        if cell_size is DEFAULT:
            parameters = parameters.copy(cell_size=45)

        return parameters

    def __init__(
            self,
            surface,
            name=DEFAULT,
            identifiers=tuple(),
            added_atoms=DEFAULT,
            charge=0,
            initial_state=tuple(),
            parameters=DEFAULT,
            ):
        SurfaceDefect.__init__(
                self,
                surface=surface,
                identifiers=identifiers,
                added_atoms=added_atoms,
                charge=charge,
                initial_state=initial_state,
                parameters=parameters,
                periodicity=tuple(tuple()),
                )

    def neededKpts(self, unit_cell, correlation_length):
        kpts=[]
        unit_cell = self.surface().change(unit_cell, fro='surface', to='orth_surface')
        for vector in unit_cell:
            vector = vector/self.surface().directionLenghtMultipliers()
            length=np.sqrt(np.sum(vector**2))
            number=int(np.ceil(correlation_length/length))
            kpts.append(number)

        return kpts

    def bandStructurePath(self):
        path = [
                ((0.0, 0.0, 0.0), '\\Gamma'),
                ((0.5,   0, 0.0), 'A'),
                ((0.5, 0.5, 0.0), 'AB'),
                ((0.0, 0.5, 0.0), 'B'),
                ((0.0, 0.0, 0.0), '\\Gamma'),
                ]
        return path
