# Written by Mads Engelund, 2017, http://espeem.com
import numpy as np
from CalcTroll import API
from CalcTroll.API import DEFAULT

from CalcTroll.ASEUtils.Atoms import Atoms
from CalcTroll.ASEUtils.AtomUtilities import orderAtoms
from CalcTroll.ASEUtils.AtomUtilities import findIndices
from CalcTroll.ASEUtils.AtomUtilities import removePeriodicallyEqualAtoms
from CalcTroll.ASEUtils.AtomUtilities import makeAtomsRelaxed
from CalcTroll.Core.System.BasisChanger import BasisChanger, Transformation

from CalcTroll.Plugins.Systems.Surface.Surface import setCellAndPBC

class Defect(API.System):
    COORDINATES='orth_defect'

    def __init__(
            self,
            base_system,
            name=DEFAULT,
            identifiers=tuple(),
            added_atoms=DEFAULT,
            periodicity=tuple(tuple()),
            charge=0,
            initial_state=DEFAULT,
            parameters=DEFAULT,
            ):
        self.__setIdentifiers(identifiers)
        if added_atoms is DEFAULT:
            added_atoms = Atoms([])
        self.__added_atoms = added_atoms
        self.__periodicity = np.array(periodicity)
        self.__charge = charge

        API.System.__init__(
                self,
                sub_systems=[base_system],
                name=name,
                initial_state=initial_state,
                parameters=parameters,
                )

    def baseSystem(self):
        return self.subSystems()[0]

    def convertIdentifierShorthand(self, identifiers):
        return identifiers

    def numbers(self):
        return [identifier[0] for identifier in self.identifiers()]

    def cells(self):
        return [identifier[1] for identifier in self.identifiers()]

    def __setIdentifiers(self, identifiers):
        self.__identifiers = self.convertIdentifierShorthand(identifiers)

    def identifiers(self):
        return self.__identifiers

    def _addedAtoms(self):
        return self.__added_atoms.copy(), self.baseSystem().COORDINATES

    def periodicity(self):
        return self.__periodicity.copy()

    def charge(self):
        return self.__charge

    def dimension(self):
        return len(self.periodicity())

    def findAtomIndices(self, identifiers, atoms, initialized=True, coordinates='orth_defect'):
        find_atoms = self.findAtoms(identifiers, initialized=initialized, coordinates=coordinates)
        indices = findIndices(find_atoms, atoms)

        return indices

    def findAtoms(self,
            identifiers,
            initialized=True,
            relaxed=True,
            coordinates='orth_defect',
            ):
        atoms, base_coordinates = self._findAtoms(identifiers)
        atoms = self.fullyUnrelaxed().change(
                atoms,
                to='orth_defect',
                fro=base_coordinates,
                )

        atoms = self.applyInitializationConstraintsAndRelaxation(
                atoms=atoms,
                initialized=initialized,
                relaxed=relaxed,
                )
        return self.change(atoms, to=coordinates, fro='orth_defect')

    def removedAtoms(self, coordinates='orth_defect'):
        atoms = self.findAtoms(self.identifiers(), coordinates=coordinates)

        return atoms

    def _removedAtoms(self):
        atoms = self.findAtoms(self.identifiers(), coordinates='orth_defect')

        return atoms, 'orth_defect'

    def removedAtomsIndices(self, atoms, coordinates='orth_defect'):
        return self.findAtomIndices(
                                    self.identifiers(),
                                    atoms,
                                    coordinates=coordinates,
                                    relaxed=relaxed,
                                    )
