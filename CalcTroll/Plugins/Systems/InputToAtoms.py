from CalcTroll.Core.Flags import DEFAULT
from CalcTroll import API
from .Molecule import Molecule

class InputToAtoms(Molecule):
    def __init__(self,
                 single_input,
                 potential_strength=API.DEFAULT,
                 name=DEFAULT,
                 ):
        self.__single_input = single_input
        Molecule.__init__(
                self,
                potential_strength=potential_strength,
                name=name,
                )

    def singleInput(self):
        return self.__single_input
