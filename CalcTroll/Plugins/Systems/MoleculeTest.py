# Written by Mads Engelund, 2017, http://espeem.com
import unittest
from ase.atoms import Atoms
from CalcTroll.Core.Test.Case import CalcTrollTestCase

from CalcTroll.Plugins.Systems.Molecule import *

class MoleculeTest(CalcTrollTestCase):
    def testConstruction(self):
        atoms = Atoms('CO', positions=[[0, 0, 0], [0, 0, 1]])
        molecule = Molecule(atoms=atoms)
        self.assertEqual(molecule.pbc(), [False]*3)
        self.assertEqual(molecule.subSystems(), tuple())

    def testAtoms(self):
        atoms = Atoms('CO', positions=[[0, 0, 0], [0, 0, 1]])
        molecule = Molecule(atoms=atoms)

        return_atoms = molecule.atoms()
        self.assertEqual(return_atoms.get_chemical_symbols(), atoms.get_chemical_symbols())
        self.assertArraysEqual(return_atoms.positions, atoms.positions)


if __name__=='__main__':
    unittest.main()

