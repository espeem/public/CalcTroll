import numpy as np
from ase import io
from os import path
from ase.constraints import FixAtoms
from CalcTroll import API

from CalcTroll.ASEUtils.RegularizeMolecule import regularizePart, SMALL
from CalcTroll.ASEUtils.AtomUtilities import cellCenter
from CalcTroll.ASEUtils.AtomUtilities import orderAtoms
from CalcTroll.ASEUtils.Atoms import Atoms
from CalcTroll.Core.Utilities import vectorLength

from .Cluster import Cluster, makeInitialization
from .MoleculeOnSlab import MoleculeOnSlab
from .Diamond001.Diamond001 import Diamond001
from .Diamond001.DanglingBonds import Diamond001Unpassivated, UNBUCKLED_p1x1
from .Defect.SurfaceReconstruction import SurfaceReconstruction

def pullOutMoleculeModel(filename):
    atoms = io.read(filename)
    if not isinstance(atoms, Atoms):
        atoms = Atoms(atoms)

    symbols = atoms.get_chemical_symbols()
    mask = [symbol == 'Si' for symbol in symbols]
    atoms = regularizePart(atoms, mask=mask)
    symbols = atoms.get_chemical_symbols()
    mask = [symbol == 'O' for symbol in symbols]
    rO = atoms[mask].get_positions()
    av = rO.sum(axis=0)/len(rO)
    atoms.translate(-av)
    take = atoms.positions[:, 2] > - 0.1
    atoms = atoms[take]

    rO -= av
    rH = rO + np.array([0, 0, -0.7])

    r = atoms.get_positions()
    atoms.set_cell(r.max(axis=0) + abs(r.min(axis=0)) + 10)
    atoms = orderAtoms(atoms, tolerance=SMALL, order=(2, 1, 0))

    return atoms

def passivateCluster(atoms):
    symbols = atoms.get_chemical_symbols()
    Omask = np.array([symbol == 'O' for symbol in symbols])
    rO = atoms.get_positions()[Omask]
    rH = rO + np.array([0, 0, -0.7])
    h_atoms = Atoms(positions=rH, symbols='H'*len(rH))
    atoms += h_atoms

    return atoms


class OxygenBindingCluster(Cluster):
    def __init__(self,
            filename,
            parameters=API.DEFAULT,
            ):
        head, tail = path.split(filename)
        name = tail.split('.')[0]
        self.__filename = filename
        self.__atoms = None
        Cluster.__init__(
                self,
                name=name,
                parameters=parameters)

    def _makeInputAtoms(self):
        atoms = io.read(self.__filename)
        if not isinstance(atoms, Atoms):
            atoms = Atoms(atoms)

        symbols = atoms.get_chemical_symbols()
        Omask = np.array([symbol == 'O' for symbol in symbols])
        rO = atoms[Omask].get_positions()

        r = atoms.get_positions()
        atoms.set_cell(r.max(axis=0) + abs(r.min(axis=0)) + 10)
        atoms = orderAtoms(atoms, tolerance=SMALL, order=(2, 1, 0))

        symbols = atoms.get_chemical_symbols()
        Omask = np.array([symbol == 'O' for symbol in symbols])
        Cmask = np.array([symbol == 'C' for symbol in symbols])
        rOs = atoms[Omask].get_positions()
        rCs = atoms[Cmask].get_positions()
        Cindices = np.arange(len(atoms))[Cmask]
        Oindices = np.arange(len(atoms))[Omask]
        fixed_indices = list(Oindices)
        for rO in rOs:
            argmin = vectorLength(rCs - rO, axis=1).argmin()
            fixed_indices.append(Cindices[argmin])

        self.__atoms = atoms
        self.__fixed_indices = fixed_indices

    def atoms(
            self,
            constrained=True,
            passivate=True,
            parameters=API.DEFAULT,
            ):
        if parameters is API.DEFAULT:
            parameters = self.parameters()
        parameters = self.defaultParameters(parameters)

        if self.__atoms is None:
            self._makeInputAtoms()

        atoms = self.__atoms.copy()
        maxi = atoms.positions.max(axis=0)
        mini = atoms.positions.min(axis=0)

        ran = maxi - mini + parameters.padding()
        atoms.cell[0, 0] = ran[0]
        atoms.cell[1, 1] = ran[1]
        atoms.cell[2, 2] = ran[2]
        atoms = self.makeAtomsRelaxed(atoms)

        cell_center = cellCenter(atoms)
        middle = (mini + maxi)/2
        cell_displacement = - (cell_center - middle)
        atoms.set_celldisp(cell_displacement)

        # Initialize mognetic moments.
        moments = makeInitialization(atoms)
        atoms.set_initial_magnetic_moments(moments)

        if not constrained:
            del atoms.constraints
        if constrained:
            symbols = atoms.get_chemical_symbols()
            if len(atoms.constraints) == 0:
                constraint = FixAtoms(indices=self.__fixed_indices)
                atoms.set_constraint(constraint)

        if passivate:
            atoms = passivateCluster(atoms)

        return atoms

    def makeAtomsRelaxed(self, atoms, tolerance=1):
        if self.isRelaxed():
            atoms.positions = self.relaxation().positions[:len(atoms)]

        return atoms


class OxygenBindingClusterOnSi001(MoleculeOnSlab):
    def __init__(
        self,
        filename,
        parameters=API.DEFAULT,
        ):
        cluster = OxygenBindingCluster(filename)
        slab = Diamond001Unpassivated('Si', initial_state=UNBUCKLED_p1x1)
        name = cluster.name() + 'On' + slab.name()

        MoleculeOnSlab.__init__(
                self,
                molecule=cluster,
                slab=slab,
                origin=[0.5, 0.0],
                name=name,
                parameters=parameters,
                )

    def atoms(self,
            constrained=True,
            parameters=API.DEFAULT,
            ):
        atoms = MoleculeOnSlab.atoms(self, constrained=constrained, parameters=parameters)

        # Initialize mognetic moments.
        moments = makeInitialization(atoms)
        atoms.set_initial_magnetic_moments(moments)

        return atoms
