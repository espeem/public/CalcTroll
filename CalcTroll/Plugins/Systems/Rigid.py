import numpy as np
from ase.atoms import Atom
from ase.atoms import Atoms as ASEAtoms
from CalcTroll.ASEUtils.Atoms import Atoms

from CalcTroll import API
from CalcTroll.API import DEFAULT
from CalcTroll.Plugins.Systems.Molecule import Molecule

class Cubane(Molecule):
    def __init__(self, name=DEFAULT, parameters=DEFAULT):
        positions = np.array([
            [0,       0,        0     ],
            [1,       0,        0     ],
            [0,       1,        0     ],
            [1,       1,        0     ],
            [0,       0,        1     ],
            [1,       0,        1     ],
            [0,       1,        1     ],
            [1,       1,        1     ],
            ])*1.4
        atoms = Atoms("CCCCCCCC", positions=positions)

        Molecule.__init__(self, atoms, name=name, parameters=parameters)
