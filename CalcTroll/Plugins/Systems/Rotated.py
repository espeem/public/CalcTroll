import numpy as np
from ase.atoms import Atom
from CalcTroll.Plugins.Systems.Molecule import Molecule

from CalcTroll.ASEUtils.Atoms import Atoms
from CalcTroll import API
from CalcTroll.API import DEFAULT

class Rotated(API.System):
    def __init__(self, system, angle=0):
        self.__angle = angle

        name = system.name() + '_Rotated_%d' % angle
  
        API.System.__init__(
                self,
                sub_systems=[system],
                name=name)

    def angle(self):
        return self.__angle

    def baseSystem(self):
        return self.subSystems()[0]

    def defaultParameters(self, parameters):
        return self.baseSystem().defaultParameters(parameters=parameters)

    def subParameters(self, parameters=API.DEFAULT):
        return [self.parameters()]

    def referenceSystem(self):
        return self.baseSystem().referenceSystem()

    def atoms(self, parameters=API.DEFAULT, constrained=True, initialized=True, relaxed=True):
        atoms = self.baseSystem().atoms(
                                        parameters=parameters,
                                        constrained=constrained,
                                        initialized=initialized,
                                        relaxed=relaxed,
                                        )

        atoms.rotate(a=self.angle(), v='z')

        if relaxed and self.isRelaxed():
            atoms = self.baseSystem().makeAtomsRelaxed(atoms, relaxation=self.relaxation())

        return atoms

    def frame(self, **kwargs):
        return self.baseSystem().frame(**kwargs)

    def plotUnitCell(self, **kwargs):
        return self.baseSystem().plotUnitCell(**kwargs)

    def jpg_save(self, filename, data, **kwargs):
        return self.baseSystem().jpg_save(filename, data, **kwargs)

    def pbc(self):
        return self.baseSystem().pbc()
