from ase.atoms import Atom
from CalcTroll import API
from CalcTroll.ASEUtils.Atoms import Atoms
from .Molecule import Molecule

class Water(Molecule):
    def __init__(self, parameters=API.DEFAULT):
        atoms = Atoms(
            [
            Atom('H', ( -1, 0,  0, )),
            Atom('O', (  0, 1,  0, )),
            Atom('H', (  1, 0,  0, )),
            ],)
        Molecule.__init__(self, atoms=atoms, parameters=parameters)



