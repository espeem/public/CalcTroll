import numpy as np
from CalcTroll import API


class RotateGroup(API.System):
    def __init__(self, system, tag=None, angle=0, parameters=API.DEFAULT):
        self.__angle = angle
        self.__tag = tag

        name = system.name() + '_Rotated_%d' % angle
        if parameters == API.DEFAULT:
            parameters=system.parameters()

        API.System.__init__(
                self,
                sub_systems=[system],
                parameters=parameters,
                name=name)

    def angle(self):
        return self.__angle

    def baseSystem(self):
        return self.subSystems()[0]

    def defaultParameters(self, parameters):
        return self.baseSystem().defaultParameters(parameters=parameters)

    def subParameters(self, parameters=API.DEFAULT):
        return [self.parameters()]

    def referenceSystem(self):
        return self.baseSystem().referenceSystem()

    def atoms(self, parameters=API.DEFAULT, constrained=True, initialized=True, relaxed=True):
        atoms = self.baseSystem().atoms(
                                        parameters=parameters,
                                        constrained=constrained,
                                        initialized=initialized,
                                        relaxed=relaxed,
                                        )

        if self.__tag == None:
            indices = np.arange(len(atoms))
        else:
            indices = atoms.tag(self.__tag).indices()

        r_atoms = atoms[indices]
        v = r_atoms[1].position - r_atoms[0].position
        r_atoms.rotate(a=self.angle(), v=v, center=r_atoms[0].position)
        atoms.positions[indices, :] = r_atoms.positions

        if relaxed and self.isRelaxed():
            atoms = self.baseSystem().makeAtomsRelaxed(atoms, relaxation=self.relaxation())

        return atoms

    def frame(self, **kwargs):
        return self.baseSystem().frame(**kwargs)

    def plotUnitCell(self, **kwargs):
        return self.baseSystem().plotUnitCell(**kwargs)

    def jpg_save(self, filename, data, **kwargs):
        return self.baseSystem().jpg_save(filename, data, **kwargs)

    def pbc(self):
        return self.baseSystem().pbc()
