import numpy as np
from ase.atoms import Atom
from ase.constraints import PullForce
from CalcTroll.Plugins.Systems.Molecule import Molecule

from CalcTroll.ASEUtils.Atoms import Atoms
from CalcTroll import API
from CalcTroll.API import DEFAULT

class Pulled(API.System):
    def __init__(self, system, tag, fext, name, parameters=API.DEFAULT):
        self.__tag = tag
        self.__fext = fext

        if parameters == API.DEFAULT:
            parameters=system.parameters()

        API.System.__init__(
                self,
                sub_systems=[system],
                parameters=parameters,
                name=name)

    def baseSystem(self):
        return self.subSystems()[0]

    def defaultParameters(self, parameters):
        return self.baseSystem().defaultParameters(parameters=parameters)

    def subParameters(self, parameters=API.DEFAULT):
        return [self.parameters()]

    def referenceSystem(self):
        return self.baseSystem().referenceSystem()

    def atoms(self, parameters=API.DEFAULT, constrained=True, initialized=True, relaxed=True):
        atoms = self.baseSystem().atoms(
                                        parameters=parameters,
                                        constrained=constrained,
                                        initialized=initialized,
                                        relaxed=True,
                                        )

        constraints = atoms.constraints
        indices = atoms.tag(self.__tag).indices()
        index = indices[-1]

        constraint = PullForce(index=index, fext=self.__fext)
        constraints.append(constraint)
        atoms.set_constraint(constraints)

        if relaxed and self.isRelaxed():
            atoms = self.baseSystem().makeAtomsRelaxed(atoms, relaxation=self.relaxation())

        return atoms

    def frame(self, **kwargs):
        return self.baseSystem().frame(**kwargs)

    def plotUnitCell(self, **kwargs):
        return self.baseSystem().plotUnitCell(**kwargs)

    def jpg_save(self, filename, data, **kwargs):
        del data.constraints
        return self.baseSystem().jpg_save(filename, data, **kwargs)

    def pbc(self):
        return self.baseSystem().pbc()

    def makeAtomsRelaxed(self, atoms, relaxation=None):
        return self.baseSystem().makeAtomsRelaxed(atoms, relaxation=relaxation)


class PullRelease(API.System):
    def __init__(self, system, tag, fext, name, parameters=API.DEFAULT):


        if parameters == API.DEFAULT:
            parameters=system.parameters()

        system = Pulled(system=system, tag=tag, fext=fext, name=name[:-2], parameters=parameters)

        API.System.__init__(
                self,
                sub_systems=[system],
                parameters=parameters,
                name=name)

    def baseSystem(self):
        return self.subSystems()[0]

    def defaultParameters(self, parameters):
        return self.baseSystem().defaultParameters(parameters=parameters)

    def subParameters(self, parameters=API.DEFAULT):
        return [self.parameters()]

    def referenceSystem(self):
        return self.baseSystem().referenceSystem()

    def atoms(self, parameters=API.DEFAULT, constrained=True, initialized=True, relaxed=True):
        atoms = self.baseSystem().atoms(
                                        parameters=parameters,
                                        constrained=constrained,
                                        initialized=initialized,
                                        relaxed=True,
                                        )

        constraints = [c for c in atoms.constraints if not isinstance(c, PullForce)]
        atoms.set_constraint(constraints)

        if relaxed and self.isRelaxed():
            atoms = self.baseSystem().makeAtomsRelaxed(atoms, relaxation=self.relaxation())

        return atoms


    def frame(self, **kwargs):
        return self.baseSystem().frame(**kwargs)

    def plotUnitCell(self, **kwargs):
        return self.baseSystem().plotUnitCell(**kwargs)

    def jpg_save(self, filename, data, **kwargs):
        return self.baseSystem().jpg_save(filename, data, **kwargs)

    def pbc(self):
        return self.baseSystem().pbc()
