# Written by Mads Engelund, 2017, http://espeem.com
import numpy as np
from ase.constraints import FixAtoms
from os.path import join
from CalcTroll.API import view

from CalcTroll.ASEUtils.Atoms import Atoms
from CalcTroll.Plugins.Systems.Defect import SurfacePointDefect
from CalcTroll.Plugins.Systems import Molecule
from CalcTroll.Plugins.Systems.Surface import Surface3D
from CalcTroll.Plugins.Systems.Crystal.BCC import BCC
from CalcTroll import API
from CalcTroll.API import DEFAULT


class Pyramid(Molecule):
    def __init__(
            self,
            symbol,
            crystal,
            miller_indices,
            positions,
            fixed=None,
            name=DEFAULT,
            parameters=DEFAULT,
            ):
        surface = Surface3D(crystal=crystal, miller_indices=miller_indices)
        atoms = Atoms(symbol*len(positions), positions)
        atoms = surface.change(atoms, fro='surface', to='orth_surface')
        pos = atoms.positions
        z_max = pos[:, 2].max()
        move = -pos[pos[:, 2] > z_max - 0.01].mean(axis=0)
        atoms.translate(move)

        if fixed is not None:
            atoms.set_constraint(FixAtoms(mask=fixed))

        Molecule.__init__(self,
                        atoms=atoms,
                        name=name,
                        parameters=parameters,
                        )


class PyramidBCC111(Pyramid):
    def name(self):
        if self.__apex:
            return "Pyramid-%d-BCC111" % self.__layers
        else:
            return "Pyramid-%d-TrimerBCC111" % self.__layers

    def __init__(
            self,
            symbol,
            apex=True,
            parameters=DEFAULT,
            ):
        layers = 5
        self.__apex = apex
        self.__layers = layers

        crystal = BCC(symbol)
        v1 = np.array([1, 0, 0])
        v2 = np.array([1, 1, 0])
        v3 = np.array([0, 0, 1])

        n1, n2, n3 = 0, 0, 0

        positions = []
        for n3 in range(layers):
            for n2 in range(layers):
                for n1 in range(layers):
                    if n1 + n2 + n3 < layers:
                        v = n1*v1 + n2*v2 + n3*v3
                        positions.append(v)

        if not apex:
            positions = positions[:-1]
        positions = np.array(positions)

        fixed = positions[:, 2] < 2.5

        miller_indices = (1, 1, 1,)
        Pyramid.__init__(self,
                        symbol=symbol,
                        crystal=crystal,
                        positions=positions,
                        miller_indices=miller_indices,
                        fixed=fixed,
                        parameters=parameters,
                        )


class PyramidBCC011(Pyramid):
    def name(self):
        if self.__apex:
            return "Pyramid-%d-BCC011" % self.__layers
        else:
            return "Pyramid-%d-TrimerBCC011" % self.__layers

    def __init__(
            self,
            symbol,
            apex=True,
            parameters=DEFAULT,
            ):
        layers = 5
        self.__apex = apex
        self.__layers = layers

        crystal = BCC(symbol)
        v1 = np.array([1, 0, 0])
        v2 = np.array([0, 1, 0])
        v3 = np.array([0, 0, 1])

        n1, n2, n3 = 0, 0, 0

        positions = []
        for n3 in range(layers):
            for n2 in range(layers - n3):
                for n1 in range(layers - n3):
                    v = n1*v1 + n2*v2 + n3*v3
                    if not (not self.__apex and n1==0 and n2==layers - n3 - 1):
                        positions.append(v)

        positions = np.array(positions)

        fixed = positions[:, 2] < 2.5

        miller_indices = (0, 1, 1,)
        Pyramid.__init__(self,
                        symbol=symbol,
                        crystal=crystal,
                        positions=positions,
                        miller_indices=miller_indices,
                        fixed=fixed,
                        parameters=parameters,
                        )
