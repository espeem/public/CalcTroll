import numpy as np
import random
from CalcTroll.Core.Utilities import vectorLength
from CalcTroll.Plugins.Systems.Diamond001.DanglingBonds import InitialStateAtom


def canCastToTnt(s):
    try:
        int(s)
        return True
    except ValueError:
        return False
    
def connections2identifiers(connections):
    assert len(connections) < 4
    new_connections = []
    for i, connect in enumerate(connections):
        identifiers = []
        for site in connect:
            c1, c2, side = site
            identifier = (side + 2, (c1, c2))
            identifiers.append(identifier)
        new_connections.append(identifiers)

    return identifiers, len(identifiers)

def connections2SiteIdentifiers(connections):
    assert len(connections) < 4
    new_connections = []
    for i, connect in enumerate(connections):
        identifiers = []
        for site in connect:
            c1, c2, side = site
            identifier = (side, (c1, c2))
        identifiers.append(identifier)
        new_connections.append(identifiers)

    return new_connections

def findFreeIdentifier(identifier, all_identifiers):
    number, cell = identifier
    x, y = cell

    if number == 2:
        test_number = 3
    elif number == 3:
        test_number = 2

    test = (test_number, cell)
    if not test in all_identifiers:
        return test

    test = (number, (x, y + 1))
    if not test in all_identifiers:
        return test

    test = (number, (x, y - 1))
    if not test in all_identifiers:
        return test

    if number == 2:
        test_number = 3
        test_cell = (x - 1, y)
    elif number == 3:
        test_number = 2
        test_cell = (x + 1, y)

    test = (test_number, test_cell)
    if not test in all_identifiers:
        return test

    raise Exception


def randomizePassivation(pass_identifiers, all_identifiers, seed):
    random.seed(a=seed, version=2)
    new_pass_identifiers = []
    for identifier in pass_identifiers:
        number, cell = identifier
        cell_x, cell_y = cell
        all_identifiers.remove(identifier)
        alternatives = []
        for number in (2, 3):
            for cell_x_plus in (-1, 0, 1):
                for cell_y_plus in (-1, 0, 1):
                    test_x = cell_x + cell_x_plus
                    test_y = cell_y + cell_y_plus

                    if not test_x in [0, 1]:
                        continue

                    if not test_y in [-1, 0, 1, 2]:
                        continue

                    alt_ident = (number, (test_x, test_y))

                    if not alt_ident in all_identifiers:
                        alternatives.append(alt_ident)

        alt_ident = random.choice(alternatives)
        new_pass_identifiers.append(alt_ident)
        all_identifiers.append(alt_ident)


    return new_pass_identifiers, all_identifiers

def setupInitialStateBulk(unbuckle, split_dimer, seed=None):
    initial_state = []
    splits = [entry[0] for entry in split_dimer]

    for i, item in enumerate(unbuckle):
        try:
            j = splits.index(i)
        except ValueError:
            split = 0.0
        else:
            split = split_dimer[j][1]

        dimer = [
            InitialStateAtom((0, item), (-split/2, 0,  0),  1) ,
            InitialStateAtom((1, item), (split/2, 0,  0),  1) ,
            ]
        initial_state.extend(dimer)

    xran = 3
    yran = 5

    xy = np.array([(ix, iy) for ix in range(-xran, xran + 1) for iy in range(-yran, yran+1)])
    dist = np.array([np.sqrt((ix*2)**2 + iy**2) for ix, iy in xy])
    argsort = np.argsort(dist)
    xy = xy[argsort]

    for ix, iy in xy:
        if (ix, iy) in unbuckle:
            continue

        if seed == None:
            sign = (-1)**((ix+iy)%2)
        else:
            sign = random.choice([-1, 1])

        r = sign*0.1
        dimer = [
            InitialStateAtom((0, (ix, iy)), (0, 0, -r),  0) ,
            InitialStateAtom((1, (ix, iy)), (0, 0,  r),  0) ,
            ]
        initial_state.extend(dimer)

    return initial_state


def setupInitialStateSites(identifiers, add=1.0):
    initial_state = []
    for iden in identifiers:
        initial_state.append(InitialStateAtom(iden, (0, 0, add),  0))

    return initial_state


def setupMoleculeInitialState(added_atoms):
    if len(added_atoms) == 0:
        return []
    symbols = added_atoms.get_chemical_symbols()
    last_big_index = [i for i in range(len(symbols)) if symbols[i] != 'H'][-1]
    extra_init = [InitialStateAtom(last_big_index, initial_moment=-1)]

    return extra_init


def generateConnections(nr_connections=3):
    connect1 = (0, 0, 0)
    if nr_connections == 1:
        return (connect1, )

    possible = []
    cand = np.array(connect1)
    for dx in [-1, 0, 1]:
        cand[0] = connect1[0] + dx
        for dy in [0, 1, 2]:
            cand[1] = connect1[1] + dy
            for s in [0, 1]:
                cand[2] = s
                possible.append(tuple(cand))
    possible.remove(connect1)
    connect2 = random.choice(possible)

    if nr_connections == 2:
        return (connect1, connect2)

    possible.remove(connect2)
    new_possible = []
    for c in possible:
        diff = np.array(c) - np.array(connect2)
        if abs(diff[0]) > 1:
            continue
        if abs(diff[1]) > 2:
            continue
        new_possible.append(c)

    connect3 = random.choice(new_possible)

    if nr_connections == 3:
        return (connect1, connect2, connect3)

    raise Exception


def treatInputConnections(connections, seed):
    if isinstance(connections, tuple):
        pass
    else:
        random.seed(a=seed, version=2)
        if connections == None:
            connections = random.choice([1, 2, 3])

        if connections in [1, 2, 3]:
            connections = generateConnections(connections)


    return connections

def treatInputAngle(angle, seed):
    if angle != None:
        return angle

    if seed == None:
        return 0

    random.seed(a=seed, version=2)
    return random.uniform(-180, 180)


def adjustCage(cage, r_site, mapping, r0):
    r0 = np.array(r0)
    take = np.arange(4)
    take[:len(mapping)] = np.array(mapping)

    l = len(r_site)
    k = 0.2
    j = 0
    while j < 100:
        r_cage = cage.positions[take]
        r_diff = r_cage[:l] - r_site
        d = vectorLength(r_diff, axis=1)
        r_unit = np.array([r_diff[i]/d[i] for i in range(l)])
        ws = 1/np.array(r0)
        Fabs = -k*(d - r0)*np.greater_equal(d  - r0, 0) - k*(d - r0*(2/3))*np.less_equal(d - r0*(2/3), 0)
        Fs = (Fabs*r_unit.T).T

        mean = r_cage.mean(axis=0)
        diff1 = r_site - mean
        diff2 = r_cage[:l] - mean
        diff1 = np.array([v/vectorLength(v) for v in diff1])
        pro = [np.dot(v1, v2) for v1, v2 in zip(diff1, diff2)]
        left = diff2 - (pro*diff1.T).T
        Fd = -0.5*k*left

        F = Fd + Fs

        Fmom = F.sum(axis=0)
        Fmom += k*np.array([0, 0, 0.75])
        r_diff = r_cage[:l] - mean
        dist = vectorLength(r_diff[0])
        torque = np.array([np.cross(v1, v2) for v1, v2 in zip(r_diff, F)]).sum(axis=0)
        t = vectorLength(torque)
        t_unit = torque/t

        a = (k*t/dist**2) * (180/np.pi)
        cage.rotate(a=a, v=t_unit, center=mean)
        cage.translate(Fmom)
        j += 1


def randomizeMoleculeConnected(m_specs, n_connections, seed=0):
    random.seed(a=seed, version=2)
    m_specs = m_specs.getPermutation()

    return m_specs

def randomizeDissociation(m_specs, n_connections, seed=0):
    random.seed(a=seed, version=2)
    groups = m_specs.groups()
    dissociate = m_specs.dissociate().copy()
    reassociate = m_specs.reassociate().copy()
    keys = dissociate.keys()
    for i in range(4):
        if i in keys:
            continue
        group = groups[i]
        if i < n_connections:
            start = 1
        else:
            start = 0
        n_diss = random.choice(range(start, len(group) - 1))

        dissociate[i] = n_diss

        if i in reassociate or i < n_connections or n_diss == 0:
            continue

        last = group[-n_diss]
        if last.symbol == 'H':
            continue

        re = random.choice([True, False])
        if re:
            reassociate.append(i)

    return m_specs.__class__(*groups, dissociate=dissociate, reassociate=reassociate)