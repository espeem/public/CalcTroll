import numpy as np
from CalcTroll.Core.Utilities import vectorLength

SiH_BOND_DISTANCE = 1.5

class Attachment(object):
    def __iter__(self):
        return iter(self.__connection)

    def __len__(self):
        return len(self.__connection)

    def __init__(self, surface, connection):
        self.__surface = surface
        self.__connection = connection

    def getHydrogenIdentifiers(self):
        identifiers = []
        for site in self:
            c1, c2, side = site
            identifier = (side + 2, (c1, c2))
            identifiers.append(identifier)

        return identifiers

    def findHydrogens(self):
        identifiers = self.getHydrogenIdentifiers()
        atoms = self.__surface.findAtoms(identifiers)

        return atoms

    def findSilicons(self):
        identifiers = []
        for site in self:
            c1, c2, side = site
            identifier = (side, (c1, c2))
            identifiers.append(identifier)
        atoms = self.__surface.findAtoms(identifiers)

        return atoms

    def getConnectionCoordinates(self, distance=1.3):
        atoms = self.findSilicons()
        r_pos = atoms.get_positions()
        if len(r_pos) == 1:
            return r_pos[0] + np.array([0, 0, SiH_BOND_DISTANCE])
        if len(r_pos) == 2:
            vector = r_pos[1] - r_pos[0]
            d = vectorLength(vector)
            vector /= d
            c = SiH_BOND_DISTANCE
            a = (d - distance)/2.
            b = np.sqrt(c**2 - a**2)

            return r_pos[0] + a*vector + np.array([0, 0, b])


class AttachmentList(object):
    def __getitem__(self, i):
        return self.__connections[i]

    def __len__(self):
        return len(self.__connections)

    def __iter__(self):
        return iter(self.__connections)

    def __init__(self, surface, connections):
        self.__connections = [Attachment(surface, connection) for connection in connections]

    def getHydrogenIdentifiers(self):
        identifiers = []
        for item in self:
            identifiers.extend(item.getHydrogenIdentifiers())

        return identifiers

    def findHydrogens(self):
        return [item.findHydrogens() for item in self]

    def findSilicons(self):
        return [item.findSilicons() for item in self]

    def getConnectionCoordinates(self):
        r_site = [item.getConnectionCoordinates() for item in self]

        return np.array(r_site)