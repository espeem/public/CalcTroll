from CalcTroll.ASEUtils.Atoms import AseAtom, Atoms
CH_BOND_DISTANCE = 1.1
SiH_BOND_DISTANCE = 1.5

SINGLE = Atoms([
              AseAtom('N',( 1.09*1.5,      0  ,   0)),
              AseAtom('F',(  -0.3633*1.5 ,      1.03*1.5  ,   0)),
              AseAtom('B',(  -0.3633*1.5 ,      -0.5138*1.5  ,   0.89*1.5)),
              AseAtom('O',(  -0.3633*1.5 ,      -0.5138*1.5  ,  -0.89*1.5)),
              AseAtom('C',( 0 ,     0  ,   0)),
              ])


CONNECT_TEST = Atoms([
              AseAtom('C',(  0.0 ,   0.0  ,  0.0)),
              AseAtom('H',(  0.0 ,   1.0  ,  0.5)),
              AseAtom('H',(  0.0 ,  -1.0  ,  0.5)),
              AseAtom('C',(  1.5 ,   0.0  ,  0.0)),
              AseAtom('H',(  1.5 ,   1.0  ,  0.5)),
              AseAtom('H',(  1.5 ,  -1.0  ,  0.5)),
              AseAtom('O',(  3.0 ,   0.0  ,  0.0)),
              AseAtom('H',(  4.0 ,   0.0  ,  0.0)),
              ])


CH3 = Atoms([
              AseAtom('C',( 0.0 ,   0.0  ,  0.0)),
              AseAtom('H',( 0.0 ,   1.0  ,  0.5)),
              AseAtom('H',( 0.0 ,  -1.0  ,  0.5)),
              AseAtom('H',( 1.0 ,   0.0  ,  0.0)),
              ],
              )