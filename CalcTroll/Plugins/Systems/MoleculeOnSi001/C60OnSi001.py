import numpy as np
from CalcTroll import API
from CalcTroll.Plugins.Systems.C60 import C60
from .MoleculeOnSi001 import MoleculeOnSi001
from CalcTroll.Plugins.Systems.Diamond001.DanglingBonds import Diamond001Reconstruction


class C60OnSi001(MoleculeOnSi001):
    def __init__(
        self,
        name="C60OnSi001",
        seed=None,
        ):  
        unbuckle = [(0, 0), (0, 1), (1, 0), (1, 1)]
        all_identifiers = [(3, (0, 0)), (3, (0, 1)), (2, (1, 0)), (2, (1, 1))]

        added_atoms = C60().atoms()
        added_atoms.rotate(v='y', a=90)
        added_atoms.rotate(v='z', a=60)
        ref1 = added_atoms.positions.mean(axis=0)
        ref1[2] = added_atoms.positions[:, 2].min()
        setup = Diamond001Reconstruction(symbol='Si', identifiers=[2, 3])
        test_atoms = setup.surface().findAtoms(all_identifiers)
        ref2 = test_atoms.positions.mean(axis=0)
        translate = ref2 - ref1
        added_atoms.translate(translate)
        
        MoleculeOnSi001.__init__(
            self,
            name=name,
            all_identifiers=all_identifiers,
            unbuckle=unbuckle,
            seed=seed,
            added_atoms=added_atoms,
            )