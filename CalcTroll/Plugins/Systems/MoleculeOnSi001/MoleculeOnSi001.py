import numpy as np

from ase.constraints import FixAtoms
from CalcTroll.ASEUtils.Atoms import Atoms
from CalcTroll import API


from CalcTroll.ASEUtils.Atoms import Atoms
from CalcTroll.Core.Utilities import vectorLength
from CalcTroll.Core.System.BasisChanger import BasisChanger, Transformation
from CalcTroll.ASEUtils.AtomUtilities import cellCenter
from CalcTroll.Plugins.Systems.Diamond001.DanglingBonds import Diamond001ReconstructionState, Diamond001Reconstruction
from CalcTroll.Plugins.Systems.Surface.Surface3D import SurfaceParameters3D, SurfaceClusterParameters3D, MoleculeOnlyParameters
from .Utilities import setupInitialStateBulk, setupMoleculeInitialState, canCastToTnt

class MoleculeOnSi001(Diamond001Reconstruction):

    def __init__(
        self,
        name,
        all_identifiers,
        unbuckle,
        seed,
        added_atoms,
        split_dimer=tuple(),
        ):
        self.__all_identifiers = all_identifiers
        self.__unbuckle = unbuckle
        self.__split_dimer = split_dimer
        self.__seed = seed
        self.__added_atoms = added_atoms

        Diamond001Reconstruction.__init__(
            self,
            symbol='Si',
            identifiers=[2, 3],
            added_atoms=added_atoms,
            name=name,
            )

    def defaultInitialState(self):
        initial_state_bulk = setupInitialStateBulk(self.__unbuckle, self.__split_dimer, self.__seed)
        initial_state_mol = setupMoleculeInitialState(self.__added_atoms)
        initial_state = Diamond001ReconstructionState(
            'BUCK',
            initial_state_bulk + initial_state_mol,
            )
        return initial_state

    def setUpBasisChanger(self):
        basis_changer = BasisChanger()
        added_atoms = self.surface().findAtoms(self.__all_identifiers)
        if len(added_atoms) == 0:
            mean = [0.0, 0.0, 0.0]
        else:
            r = added_atoms.get_positions()
            maxi, mini = r.max(axis=0), r.min(axis=0)
            mean = (maxi + mini)/2
            mean = r.mean(axis=0)

        transformation = Transformation(to='orth_surface', fro='orth_defect', rotation_point=[mean, [0,0,0]])
        basis_changer.addTransformation(transformation)
        basis_changer.include(self.surface().basisChanger())

        return basis_changer

    def defaultParameters(self, parameters=API.DEFAULT, centers=API.DEFAULT):
        if centers == API.DEFAULT:
            centers = self.addedAtoms().get_positions()

        if parameters == API.DEFAULT:
            surface_parameters = self.surface().defaultParameters(centers=centers)
            parameters = Diamond001Reconstruction.parameterClass()(**surface_parameters)

        if isinstance(parameters, SurfaceParameters3D):
            parameters = self.defaultPeriodicParameters(parameters=parameters, centers=centers)
        elif isinstance(parameters, SurfaceClusterParameters3D):
            pass
        elif isinstance(parameters, MoleculeOnlyParameters):
            pass
        else:
            raise Exception

        return parameters

    def atomsTemplate(self, parameters):
        if isinstance(parameters, SurfaceClusterParameters3D):
            atoms = self.__clusterAtomsTemplate(parameters)
        elif isinstance(parameters, SurfaceParameters3D):
            atoms = self.__periodicAtomsTemplate(parameters)
        elif isinstance(parameters, MoleculeOnlyParameters):
            atoms = self.__moleculeOnlyAtomsTemplate(parameters)
        else:
            print(parameters)
            eeeeeeeeeeeeee

        atoms.setTag('Added', picker=slice(-len(self.addedAtoms()), None))

        return atoms

    def __moleculeOnlyAtomsTemplate(self, parameters):
        if len(self.__all_identifiers) == 0:
            atoms = self.addedAtoms()
        else:
            length_scales = length_scales=parameters.lengthScales()
            periodic_parameters = SurfaceParameters3D(free_layers=0, length_scales=length_scales)
            periodic_parameters = self.defaultParameters(parameters=periodic_parameters)

            atoms = self.__periodicAtomsTemplate(periodic_parameters)
            split = -len(self.addedAtoms())
            a_indices = np.arange(len(atoms))
            atoms.setTag('InnerBoundary', picker=a_indices[split:])
            atoms.setTag('OuterBoundary', picker=a_indices[:split])

        atoms.set_pbc([False]*3)

        return atoms

    def __clusterAtomsTemplate(self, parameters):
        length_scales = parameters.lengthScales()
        length = length_scales['correlation_length']
        nearest = parameters.nearest()
        sl = (nearest + 1) * 3.0
        length_scales = length_scales.copy(pertubation_length=sl, short_correlation_length=sl, correlation_length=sl)
        periodic_parameters = SurfaceParameters3D(length_scales=length_scales)
        periodic_parameters = self.defaultParameters(parameters=periodic_parameters)
        vectors = np.array(periodic_parameters['vectors'])*2
        free_layers = periodic_parameters['free_layers'] + 4
        periodic_parameters = periodic_parameters.copy(vectors=vectors, free_layers=free_layers)

        atoms = self.__periodicAtomsTemplate(periodic_parameters)
        split = -len(self.addedAtoms())
        added = atoms[split:]
        atoms = atoms[:split]

        for i in range(nearest+1):
            diff = np.array([pos1 - atoms.positions for pos1 in added.positions])
            diff = vectorLength(diff, axis=2).min(axis=0)
            take = diff < 2.5
            added += atoms[take]
            atoms = atoms[np.logical_not(take)]
            
        split = take.sum()
        atoms = added

        take_1 = slice(0, -split)
        take_2 = slice(-split, None)
        a_indices = np.arange(len(atoms))
        atoms.setTag('InnerBoundary', picker=a_indices[take_1])
        atoms.setTag('OuterBoundary', picker=a_indices[take_2])

        maximum = atoms.positions.max(axis=0)
        minimum = atoms.positions.min(axis=0)

        width = maximum - minimum + length
        atoms.set_cell(width)
        
        atoms.set_pbc([False]*3)

        take_inner = atoms.tag('InnerBoundary').mask()
        take_outer = atoms.tag('OuterBoundary').mask()

        del atoms.constraints    

        distance = 2.5
        nearest = parameters['nearest']

        shell1_atoms = atoms[take_inner]
        shell2_atoms = atoms[take_outer]

        atoms = atoms[np.logical_not(take_outer)]
        a_indices = atoms.tag('InnerBoundary').indices()

        if nearest == 0:
            passivation_atoms = Atoms('H'*len(shell2_atoms), shell2_atoms.get_positions())
        else:
            passivation_atoms = Atoms()
            for i, pos in enumerate(shell1_atoms.positions):
                pos2 = shell2_atoms.positions
                diff_vector = pos2 - pos
                diff = vectorLength(diff_vector, axis=1)
                take = diff < distance
                vectors = diff_vector[take]
                if len(vectors) > 0:
                    vectors = np.array([v/vectorLength(v)*1.4 for v in vectors])

                    h_pos = pos + vectors
                    these_atoms = Atoms('H'*len(h_pos), h_pos)
                    these_atoms.setTag(a_indices[i])
                    passivation_atoms += these_atoms

        atoms += passivation_atoms
        mask = np.zeros(len(atoms), bool)
        mask[-len(passivation_atoms):] = 1
        atoms.set_constraint(FixAtoms(mask=mask))
        atoms.setTag('Passivation', slice(-len(passivation_atoms), None))

        maxi = atoms.positions.max(axis=0)
        mini = atoms.positions.min(axis=0)
        ran = maxi - mini + parameters.lengthScales()['correlation_length']
        atoms.cell[:,:] = 0
        atoms.cell[0, 0] = ran[0]
        atoms.cell[1, 1] = ran[1]
        atoms.cell[2, 2] = ran[2]

        cell_center = cellCenter(atoms)
        middle = (mini + maxi)/2
        cell_displacement = - (cell_center - middle)
        atoms.set_celldisp(cell_displacement)

        return atoms


    def __periodicAtomsTemplate(self, parameters=API.DEFAULT):
        atoms = Diamond001Reconstruction.atomsTemplate(self, parameters=parameters)
        db_identifiers = [(index-2, cell) for index, cell in self.__all_identifiers]
        indices = self.findAtomIndices(db_identifiers, atoms, initialized=False)
        atoms.setTag("Attachment Points", picker=indices)

        return atoms

    def makeSubsystemsRelaxed(self, atoms):
        pre_pos = atoms.get_positions()
        tags = [tag for tag in atoms.tags() if canCastToTnt(tag.name())]
        unrelaxed = self.fullyUnrelaxed()
        atoms = unrelaxed.change(atoms, fro='orth_defect', to='orth_surface')
        atoms = self.surface().makeSubsystemsRelaxed(
                atoms,
                )
        atoms = self.surface().makeAtomsRelaxed(atoms)
        atoms = self.change(atoms, fro='orth_surface', to='orth_defect')
        post_pos = atoms.get_positions()

        for tag in tags:
            i = int(tag.name())
            diff = post_pos[i] - pre_pos[i]
            indices = tag.indices()
            atoms.positions[indices] = pre_pos[indices] + diff

        return atoms
    
    def referenceSystem(self):
        ref_system = Diamond001Reconstruction(
            symbol='Si',
            identifiers=[2, 3],
            name='Si001Ref',
            )

        return ref_system

