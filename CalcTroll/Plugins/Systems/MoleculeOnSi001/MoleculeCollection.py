from .Fragments import *
from .MoleculeSpecs import MoleculeSpecs

MOLECULES = {
        'Test': MoleculeSpecs(
            g0=CONNECT_TEST,
            g4=CH3,
            dissociate={1:1, 2:1, 3:1, 4:0},
            cage=SINGLE,
        ),
        }