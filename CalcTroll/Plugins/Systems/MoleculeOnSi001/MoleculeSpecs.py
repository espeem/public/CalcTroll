import random
import numpy as np
from CalcTroll import API
from CalcTroll.ASEUtils.Atoms import Atoms

from .Fragments import SINGLE

class MoleculeSpecs(object):
    def __init__( self, g1=None, g2=None, g3=None, g4=None, g0=None, dissociate=None, reassociate=None, cage=API.DEFAULT):
        groups = []
        for i, g in enumerate([g1, g2, g3, g4]):
            if g == None:
                g = g0.copy()
            groups.append(g)
        self.__groups = groups
        if cage == API.DEFAULT:
            cage = SINGLE
        self.__cage = cage.copy()

        if dissociate != None:
            self.__dissociate = dissociate.copy()
        else:
            self.__dissociate = {}
        if reassociate != None:
            self.__reassociate = reassociate.copy()
        else:
            self.__reassociate = []

    def getCage(self):
        return self.__cage

    def reassociate(self):
        return self.__reassociate

    def groups(self):
        return self.__groups

    def dissociate(self):
        return self.__dissociate

    def getPermutation(self):
        shuffle = np.arange(0, 4)
        random.shuffle(shuffle)
        shuffle = list(shuffle)
        new_groups = [None]*4
        new_dissociate = {}
        new_reassociate = []

        for index in range(4):
            new_groups[shuffle[index]] = self.__groups[index]
            dissociate = self.__dissociate.get(index)
            if dissociate != None:
                new_dissociate[shuffle[index]] = dissociate

            if index in self.__reassociate:
                new_reassociate.append(shuffle[index])

        return self.__class__(*new_groups, dissociate=new_dissociate, reassociate=new_reassociate)

    def getAtoms(self, index):
        atoms = self.__groups[index].copy()
        dissociate = self.__dissociate.get(index + 1)
        if dissociate == None:
            atoms, end_atoms = atoms, Atoms([])
        elif dissociate == 0:
            atoms, end_atoms = atoms, Atoms([])
        else:
            atoms, end_atoms = atoms[:-dissociate], atoms[-dissociate:]

        if index in self.__reassociate:
            pos = np.array([0, 0, 0], float)
            pos[0] = atoms.positions[-1, 0] + 1.2
            atoms += Atoms('H', [pos])

        return atoms, end_atoms