import numpy as np
from CalcTroll import API
from CalcTroll.Core.Utilities import vectorLength
from CalcTroll.Core.Utilities import generateHash
from CalcTroll.ASEUtils.Atoms import Atoms

from CalcTroll.Plugins.Systems.Diamond001.DanglingBonds import Diamond001Reconstruction
from CalcTroll.Plugins.Systems.Diamond001.DanglingBonds import UNBUCKLED_p1x1
from .MoleculeOnSi001 import MoleculeOnSi001
from .MoleculeCollection import MOLECULES
from .Utilities import adjustCage, treatInputAngle, treatInputConnections, randomizePassivation
from .Attachment import AttachmentList


class CovalentOnSi001(MoleculeOnSi001):
    allowed_molecules = MOLECULES
    store = {}

    def path(self):
        script = self.script()
        h = generateHash(script)
        path = '%s_%s' % (self.__molecule_id, h)

        return path


    def __init__(
        self,
        name=API.DEFAULT,
        molecule_id='Caltrops',
        angle=None,
        connections=None,
        seed=None,
        ):
        connections = treatInputConnections(connections, seed)
        end_group_rotation = treatInputAngle(angle, seed)
        self.__molecule_id = molecule_id

        if name == API.DEFAULT:
            name = molecule_id

        system_key = (molecule_id, angle, connections, seed)
        if system_key not in self.store:
            m_specs = self.allowed_molecules[molecule_id]
            setup = Diamond001Reconstruction(
                symbol='Si',
                identifiers=[2, 3],
                initial_state=UNBUCKLED_p1x1,
                name=name,
                )

            surface = setup.surface()
            attachments = AttachmentList(surface, connections)

            added_atoms = attachments.findHydrogens()
            n_connect = len(attachments)

            r = [group.get_positions()[0] for group in added_atoms]
            ws = np.zeros(n_connect, float)
            n_pos = np.zeros(n_connect, int)
            for i in range(n_connect):
                atoms, end_atoms = m_specs.getAtoms(i)
                n_pos[i] = len(np.unique(atoms.positions[:, 0]))
            ws = 1/n_pos
            ws /= ws.sum()
            mean = np.tensordot(ws, r, (0, 0))
            translate = mean + np.array([0, 0, 2.0])

            if n_connect > 0:
                r = r - translate

            cage = m_specs.getCage()

            for i in range(n_connect, 4):
                atoms, end_atoms = m_specs.getAtoms(i)
                pos = cage.positions[i]
                atoms.rotate(a='x', v=pos)
                atoms.rotate(a=end_group_rotation, v=pos)
                atoms.translate(pos)
                atoms.setTag('EndGroup_%d' % i, picker=np.arange(len(atoms)))
                cage += atoms

            if n_connect == 0:
                should_be_down = cage.positions[:3].sum(axis=0)
            else:
                should_be_down = cage.positions[:n_connect].sum(axis=0)

            cage.rotate(a=should_be_down, v='-z')

            rc = cage.get_positions()[:4]
            for i in range(n_connect):
                if i == 0:
                    l =0
                else:
                    l = vectorLength(rc - r[i], axis=1).argmin()
                v1 = rc[l]
                v2 = r[i]
                a1 = np.arctan2(v1[0], v1[1])
                a2 = np.arctan2(v2[0], v2[1])
                da = (a2 - a1)
                da = ((da + np.pi) % (2*np.pi)) - np.pi
                da = da*(180/np.pi)
                adjust = -da/(i + 1)
                cage.rotate(a=adjust, v='z')
                rc = cage.get_positions()[:4]

            mapping = []
            r0 = []
            for i_r in range(n_connect):
                closest = vectorLength(rc - r[i_r], axis=1).argmin()
                atoms, end_atoms = m_specs.getAtoms(i_r)
                unique = np.unique(atoms.positions[:, 0])
                n_pos = len(unique)
                last_pos = unique[n_pos - len(attachments[i_r])]
                r0.append(last_pos)
                mapping.append(closest)

            cage.translate(translate)
            r_site = attachments.getConnectionCoordinates()
            if n_connect > 0:
                adjustCage(cage=cage, r_site=r_site, mapping=mapping, r0=r0)

            c_atoms = Atoms([])
            identifiers = attachments.getHydrogenIdentifiers()
            all_identifiers = list(identifiers)
            pass_identifiers = []
            all_end_atoms = []
            for i_r in range(n_connect):
                i_c = mapping[i_r]
                pos1 = r_site[i_r]
                cr = cage.positions[:4]
                cm = cr.mean(axis=0)
                pos2 = cage.positions[i_c]
                atoms, end_atoms = m_specs.getAtoms(i_r)
                unique = np.unique(atoms.positions[:, 0])
                n_pos = len(unique)
                l_attach = len(attachments[i_r])
                r_attach = attachments[i_r].findSilicons().get_positions()

                last_pos = unique[n_pos - l_attach]
                r = atoms.get_positions()
                take = r[:, 0] < last_pos + 0.1
                attach_atoms = atoms[np.logical_not(take)]
                atoms = atoms[take]
                attach_atoms.positions -= atoms.positions[-1]
                all_end_atoms.append(end_atoms)
                #identifier = findFreeIdentifier(identifiers[i_r], all_identifiers)
                #pass_identifiers.append(identifier)
                #all_identifiers.append(identifier)

                v = pos1 - pos2
                l1 = vectorLength(v)
                v = v/l1
                vt = np.array(v)
                vt[2] = 0

                l2 = r0[i_r]

                atoms.positions[:, 0] *= l1/l2
                if l2 > l1:
                    x = atoms.positions[:, 0]
                    h = np.sqrt(l2**2 - l1**2)/2
                    add = x*(2*h/l1)*(x<l1/2) + (2*h - 2*x*h/l1)*(x>=l1/2)
                    atoms.positions[:, 2] += add
                translate = pos2

                atoms.rotate(a='x', v=vt)
                atoms.rotate(a=vt, v=v)
                v2 = pos1 - cm
                v2 /= vectorLength(v2)
                v3 = v2 - np.dot(v2, v) * v
                v3 /= vectorLength(v3)
                z = np.array((0, 0, 1))
                v4 = z - np.dot(z, v) * v
                v4 /= vectorLength(v4)
                a = (180/np.pi)*np.arccos(np.dot(v4, v3))
                atoms.rotate(a=a, v=v)
                atoms.translate(translate)
                if l_attach > 1:
                    pos = atoms.positions[-1]
                    v = r_attach[1] - r_attach[0]
                    attach_atoms.rotate(a=np.array([1, 0, 0]), v=v)
                    attach_atoms.translate(pos)
                    atoms += attach_atoms

                c_atoms += atoms


            if seed != None:
                pass_identifiers, all_identifiers = randomizePassivation(pass_identifiers, all_identifiers, seed)

            #pass_atoms = setup.surface().findAtoms(pass_identifiers)
            added_atoms = Atoms()
            #for pass_atom, end_atoms in zip(pass_atoms, all_end_atoms):
            #    end_atoms.rotate(v='-y', a=90)
            #    diff = pass_atom.position - end_atoms[0].position
            #    end_atoms.translate(diff)
            #    added_atoms += end_atoms

            added_atoms += c_atoms
            cage = cage[4:]
            added_atoms += cage
            unbuckle = [cell for number, cell in all_identifiers]
            self.store[system_key] = (added_atoms, unbuckle, identifiers)
        else:
            added_atoms, unbuckle, identifiers = self.store[system_key]
            added_atoms = added_atoms.copy()
            identifiers = list(identifiers)

        MoleculeOnSi001.__init__(
            self,
            name=name,
            all_identifiers=identifiers,
            unbuckle=unbuckle,
            seed=seed,
            added_atoms=added_atoms,
            )