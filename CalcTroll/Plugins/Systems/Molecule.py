# Written by Mads Engelund, 2017, http://espeem.com
# Defines a molecule/cluster system - e.i a 0d collection of atoms.
import numpy as np
from ase.constraints import ModelSurfacePotential

from CalcTroll.ASEUtils.AtomUtilities import cellCenter
from CalcTroll.ASEUtils.Atoms import Atoms
from CalcTroll.Plugins.LengthScales import LengthScales
from CalcTroll import API
from CalcTroll.API import DEFAULT
from CalcTroll.Core.Visualizations import addMeasuringStick
from CalcTroll.Core.Visualizations import flatMatplotlibView


padding = LengthScales.defaults()['correlation_length']

# Initialize magnetic moment at one atom.
# The atom must be one of the heaviest in the molecule.
def makeInitialization(atoms):
    number = len(atoms)
    numbers = atoms.get_atomic_numbers()
    highest_number = numbers.max()
    highest_numbers = np.array([n==highest_number for n in numbers], dtype=bool)
    change = np.arange(number)[highest_numbers][0]

    moments = np.zeros(number)
    moments[change] = 1


    return moments


class MoleculeParameters(API.Parameters):
    def __init__(self, padding=DEFAULT):
        if padding is DEFAULT:
            padding = LengthScales.defaults()['correlation_length']
        self.__padding = padding

    def ignoreLongRangeCorrelation(self):
        if self['padding'] is DEFAULT:
            padding = LengthScales.defaults()['short_correlation_length']
            return self.copy(padding=padding)
        else:
            return self

    def padding(self):
        return self.__padding

    def kpts(self):
        return (1,1,1)

    def identifier(self):
        return self.padding()

class Molecule(API.System):
    @classmethod
    def parameterClass(cls):
        return MoleculeParameters

    def __len__(self):
        return len(self.__atoms)

    def defaultParameters(self, parameters=DEFAULT):
        if parameters is DEFAULT:
            result = MoleculeParameters()
        else:
            assert isinstance(parameters, MoleculeParameters)
            result = parameters

        return result

    def __init__(
            self,
            atoms=None,
            potential_strength=API.DEFAULT,
            name=DEFAULT,
            ):
        if potential_strength==API.DEFAULT:
            potential_strength = 1.0
        self.__strength = potential_strength
        self.__atoms = atoms
        if self.__atoms != None:
            self.__atoms = Atoms(self.__atoms)

        API.System.__init__(
                self,
                sub_systems=tuple(),
                name=name,
                )

    def generateAtoms(self):
        raise NotImplementedError

    def atoms(
            self,
            constrained=True,
            initialized=True,
            relaxed=True,
            parameters=DEFAULT,
            ):
        if self.__atoms == None:
            atoms = self.generateAtoms()
            if not isinstance(atoms, Atoms):
                atoms = Atoms(atoms)

            self.__atoms = atoms

        parameters = self.defaultParameters(parameters)

        atoms = self.__atoms.copy()
        maxi = atoms.positions.max(axis=0)
        mini = atoms.positions.min(axis=0)

        ran = maxi - mini + parameters.padding()
        atoms.cell[0, 0] = ran[0]
        atoms.cell[1, 1] = ran[1]
        atoms.cell[2, 2] = ran[2]
        if relaxed:
            atoms = self.makeAtomsRelaxed(atoms)

        cell_center = cellCenter(atoms)
        middle = (mini + maxi)/2
        cell_displacement = - (cell_center - middle)
        atoms.set_celldisp(cell_displacement)

        # Initialize mognetic moments.
        if initialized:
            moments = makeInitialization(atoms)
            atoms.set_initial_magnetic_moments(moments)

        if not constrained:
            del atoms.constraints
        if constrained:
            if len(atoms.constraints) == 0 and self.__strength > 0.0:
                constraint = ModelSurfacePotential(strength=self.__strength)
                atoms.set_constraint(constraint)

        return atoms

    def explain(self):
        return API.Explanation("""The molecule was rotated to a standard configuration based on the main axes of the moment of inertia. Ambiguities due to symmetry were resolved using the Pymatgen[*] implementation of Spglib[*].
The molecule was relaxed with a potential emulating a generic surface:
P[z] = %.2f * (2*(z+2)**(-3) - (z+2)**(-2)) (in units of [eV, Ang], minimum at z=0).""" % self.__strength,
        ["Pymatgen, structure manipulation package, https://pymatgen.org",
         "Spglib, Atsushi Togo, https://spglib.readthedocs.io"]
        )

    def pbc(self):
        return [False, False, False]

    def makeAtomsRelaxed(
            self,
            atoms,
            tolerance=1.0,
            relaxation=None,
        ):
        if relaxation == None:
            relaxation = self.relaxation()

        if self.isRelaxed():
            relax_atoms = self.relaxation()[-1]
            assert relax_atoms.get_chemical_symbols() == atoms.get_chemical_symbols()
            atoms.positions = relax_atoms.positions

        return atoms

    def plot(self,
             data=None,
             ax=None,
             frame=None,
             direction='-z',
             size=1.,
             bar=True,
             facecolor='black',
             ):
        if data is None:
            atoms = self.atoms()
        else:
            atoms = data

        size = size*0.4
        flatMatplotlibView(atoms, ax, direction=direction, size=size)
        if frame is not None:
            ax.set_xlim((frame[0, 0], frame[1, 0]))
            ax.set_ylim((frame[0, 1], frame[1, 1]))
            if bar:
                addMeasuringStick(ax, color='white', size=5)

        ax.patch.set_facecolor(facecolor)
        ax.set_xticks([])
        ax.set_yticks([])

    def frame(self, data=None, border=API.DEFAULT, direction='-z'):
        if data is None:
            atoms = self.atoms()
        else:
            atoms = data

        if border is API.DEFAULT:
            border = 9

        pmin = atoms.positions.min(axis=0)
        pmax = atoms.positions.max(axis=0)

        pmin -= border
        pmax += border

        fc, sc = {'x':(1, 2), 'y':(0,2), 'z':(0, 1)}[direction[-1]]

        return np.array([pmin[[fc, sc]], pmax[[fc, sc]]])

    def png_save(
            self,
            filename,
            data=None,
            **kwargs):
        return self.jpg_save(filename, data=data, **kwargs)

    def jpg_save(
            self,
            filename,
            data=None,
            frame=None,
            border=API.DEFAULT,
            direction='-z',
            **kwargs):
        from matplotlib import pylab as plt
        if data is None:
            data = self.read()
        if frame is None:
            frame = self.frame(
                data=data,
                border=border,
                direction=direction,
            )
        frame = np.array(frame)

        pmin, pmax = frame
        d = pmax - pmin
        dx = d[0]
        dy = d[1]
        ratio = dx/dy

        fig = plt.figure(frameon=False, figsize=(4*ratio, 4))
        fig.patch.set_facecolor('black')

        ax = plt.Axes(fig, [0., 0., 1., 1.])
        fig.add_axes(ax)

        self.plot(data=data,
                  ax=ax,
                  frame=frame,
                  direction=direction,
                  **kwargs)

        ax.set_xlim((pmin[0], pmax[0]))
        ax.set_ylim((pmin[1], pmax[1]))
        plt.savefig(filename)
        plt.close('all')

        return frame
