from ase.atoms import Atom
from ase.atoms import Atoms as ASEAtoms
import numpy as np
from ase.visualize import view

from CalcTroll.ASEUtils.Atoms import Atoms
from CalcTroll.ASEUtils.RegularizeMolecule import regularizeMolecularDirections

from CalcTroll import API
from CalcTroll.API import DEFAULT

from .Molecule import Molecule
from .PolyaromaticHydroCarbon import PolyaromaticHydroCarbon, passivate_atoms, passivate_partial

class GNR_MIN(Molecule):
    def __init__(
            self,
            name=DEFAULT,
            parameters=DEFAULT,
    ):
        indices = [(0, -1), (0, 0)]

        poly = PolyaromaticHydroCarbon(
            indices=indices,
        )
        atoms = poly.atoms()
        atoms = regularizeMolecularDirections(atoms)

        Molecule.__init__(self, atoms=atoms)

    def atoms(self, **kwargs):
        atoms = Molecule.atoms(self, **kwargs)
        la = len(atoms)
        moments = np.ones(len(atoms))
        moments[atoms.positions[:, 1] > 0] = -1
        atoms.set_initial_magnetic_moments(moments)

        return atoms


class GNR(Molecule):
    def name(self):
        return "GNR_%d" % self.__nor

    def __init__(
            self,
            number_of_repeats=1,
            name=DEFAULT,
            parameters=DEFAULT,
    ):
        self.__nor = number_of_repeats
        indices_unit = [
                (2, 0), (2, 1), (2, 2),
                (0, -1), (0, 0), (0, 1),
                (-2, -2), (-2, -1), (-2, 0),
        ]
        shift = (6, 3)

        indices = []
        for i in range(number_of_repeats):
            indices.extend(np.array(indices_unit) + i*np.array(shift))


        poly = PolyaromaticHydroCarbon(
            indices=indices,
        )
        atoms = poly.atoms()
        atoms = regularizeMolecularDirections(atoms)

        Molecule.__init__(self, atoms=atoms)

    def atoms(self, **kwargs):
        atoms = Molecule.atoms(self, **kwargs)
        la = len(atoms)
        moments = [1 for i in range(la//2)] + [-1 for i in range(la - la//2)]
        atoms.set_initial_magnetic_moments(moments)

        return atoms


class GNRPads(Molecule):
    def name(self):
        return "GNRPads_%d" % self.__nor

    def __init__(
            self,
            number_of_repeats=1,
            name=DEFAULT,
            parameters=DEFAULT,
    ):
        self.__nor = number_of_repeats
        indices_unit = [
                (2, 0), (2, 1), (2, 2),
                (0, -1), (0, 0), (0, 1),
                (-2, -2), (-2, -1), (-2, 0),
        ]
        shift = (6, 3)

        all_atoms = Atoms()
        for i in range(number_of_repeats):
            indices = np.array(indices_unit) + i*np.array(shift)

            poly = PolyaromaticHydroCarbon(indices=indices, passivate=False)
            atoms = poly.atoms()
            a1 = 20*np.pi/180
            a1 = (0.5 - i%2)*2*a1
            a2 = 30*np.pi/180
            atoms.rotate(a=a1, v=(np.cos(a2), np.sin(a2), 0))
            all_atoms.extend(atoms)
        atoms = all_atoms
        atoms = passivate_atoms(atoms)
        atoms = regularizeMolecularDirections(atoms)
        atoms.rattle(0.01)
        Molecule.__init__(self, atoms=atoms)


class GNROgliomer(Molecule):
    def name(self):
        return "GNROgliomer_%d" % self.__nor

    def __init__(
            self,
            number_of_repeats=1,
            name=DEFAULT,
            parameters=DEFAULT,
    ):
        self.__nor = number_of_repeats
        indices_unit = [
                (0, -1), (0, 0), (0, 1),
        ]
        shift = (2, 1)

        all_atoms = Atoms()
        for i in range(number_of_repeats*3):
            indices = np.array(indices_unit) + i*np.array(shift)

            poly = PolyaromaticHydroCarbon(indices=indices, passivate=False)
            atoms = poly.atoms()
            pos = [atoms.positions[1], atoms.positions[4]]
            if i%3 == 1:
                pos = [atoms.positions[1], atoms.positions[4]]
                atoms=passivate_partial(atoms, pos, symbol='F');

            a1 = 30
            a1 = (0.5 - i%2)*2*a1
            a2 = 30*np.pi/180
            atoms.rotate(a=a1, v=(np.cos(a2), np.sin(a2), 0))
            all_atoms.extend(atoms)
        atoms = all_atoms
        atoms = passivate_atoms(atoms)
        atoms = regularizeMolecularDirections(atoms)
        Molecule.__init__(self, atoms=atoms)


class SMALL_POLARIZED(Molecule):
    def __init__(
            self,
            name=DEFAULT,
            parameters=DEFAULT,
            potential_strength=API.DEFAULT,
    ):
        indices = [
                (0, -2), (-1, -2), (-2, -2),
                (-1, -1), (0, -1),
                (0, 0),
                (0, 1), (1, 1),
                (0, 2), (1, 2), (2, 2),
                  ]

        poly = PolyaromaticHydroCarbon(
            indices=indices,
        )
        atoms = poly.atoms()
        atoms = regularizeMolecularDirections(atoms)

        Molecule.__init__(self, atoms=atoms, potential_strength=potential_strength)











