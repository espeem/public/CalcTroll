# Written by Mads Engelund, 2017, http://espeem.com
# Defines a molecule/cluster system - e.i a 0d collection of atoms.
import numpy as np
from ase.constraints import FixAtoms, ModelSurfacePotential

from CalcTroll.ASEUtils.AtomUtilities import cellCenter
from CalcTroll.ASEUtils.Atoms import Atoms
from CalcTroll.Plugins.LengthScales import LengthScales
from CalcTroll.Plugins.Systems.Molecule import MoleculeParameters
from CalcTroll import API
from CalcTroll.API import DEFAULT
from CalcTroll.Core.Visualizations import addMeasuringStick
from CalcTroll.Core.Visualizations import flatMatplotlibView

padding = LengthScales.defaults()['correlation_length']

# Initialize magnetic moment at one atom.
# The atom must be one of the heaviest in the molecule.
def makeInitialization(atoms):
    number = len(atoms)
    numbers = atoms.get_atomic_numbers()
    highest_number = numbers.max()
    highest_numbers = np.array([n==highest_number for n in numbers], dtype=bool)
    change = np.arange(number)[highest_numbers][0]

    moments = np.zeros(number)
    moments[change] = 1


    return moments


class Cluster(API.System):
    def parameterClass(cls):
        return MoleculeParameters

    def __len__(self):
        return len(self.atoms())

    def defaultParameters(self, parameters=DEFAULT):
        if parameters is DEFAULT:
            result = MoleculeParameters()
        else:
            assert isinstance(parameters, MoleculeParameters)
            result = parameters

        return result

    def __init__(
            self,
            name=DEFAULT,
            parameters=DEFAULT,
            ):
        API.System.__init__(
                self,
                sub_systems=tuple(),
                name=name,
                parameters=parameters,
                )


    def pbc(self):
        return [False, False, False]

    def makeAtomsRelaxed(
            self,
            atoms,
            tolerance=1.0,
        ):
        raise NotImplementedError

    def plot(self,
             data=None,
             ax=None,
             frame=None,
             direction='-z',
             size=1.,
             bar=True,
             facecolor='black',
             ):
        if data is None:
            atoms = self.atoms()
        else:
            atoms = data

        size = size*0.4
        flatMatplotlibView(atoms, ax, direction=direction, size=size)
        if frame is not None:
            ax.set_xlim((frame[0, 0], frame[1, 0]))
            ax.set_ylim((frame[0, 1], frame[1, 1]))
            if bar:
                addMeasuringStick(ax, color='white', size=5)

        ax.patch.set_facecolor(facecolor)
        ax.set_xticks([])
        ax.set_yticks([])

    def frame(self, data=None, border=API.DEFAULT, direction='-z'):
        if data is None:
            atoms = self.atoms()
        else:
            atoms = data

        if border is API.DEFAULT:
            border = 9

        pmin = atoms.positions.min(axis=0)
        pmax = atoms.positions.max(axis=0)

        pmin -= border
        pmax += border

        fc, sc = {'x':(1, 2), 'y':(0,2), 'z':(0, 1)}[direction[-1]]

        return np.array([pmin[[fc, sc]], pmax[[fc, sc]]])

    def jpg_save(
            self,
            filename,
            data=None,
            frame=None,
            border=API.DEFAULT,
            direction='-z',
            **kwargs):
        from matplotlib import pylab as plt
        if data is None:
            data = self.read()
        if frame is None:
            frame = self.frame(
                data=data,
                border=border,
                direction=direction,
            )
        frame = np.array(frame)

        pmin, pmax = frame
        d = pmax - pmin
        dx = d[0]
        dy = d[1]
        ratio = dx/dy

        fig = plt.figure(frameon=False, figsize=(4*ratio, 4))
        fig.patch.set_facecolor('black')

        ax = plt.Axes(fig, [0., 0., 1., 1.])
        fig.add_axes(ax)

        self.plot(data=data,
                  ax=ax,
                  frame=frame,
                  direction=direction,
                  **kwargs)

        ax.set_xlim((pmin[0], pmax[0]))
        ax.set_ylim((pmin[1], pmax[1]))
        plt.savefig(filename)
        plt.close('all')

        return frame
