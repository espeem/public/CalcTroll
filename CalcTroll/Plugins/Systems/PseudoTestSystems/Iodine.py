import numpy as np

from CalcTroll import API
from CalcTroll.Plugins.Systems.Molecule import Molecule
from CalcTroll.Plugins.Systems.Smiles import Smiles
from CalcTroll.ASEUtils.Atoms import Atoms
from CalcTroll.ASEUtils.Atoms import AseAtom, Atoms


class IodineTerminatedChain(Molecule):
    def __init__(
            self,
            parameters=API.DEFAULT,
        ):
        smiles_code = 'C#CI'
        atoms = Smiles(smiles_code).atoms()
        atoms.rotate(v='y', a=90)
        Molecule.__init__(
            self,
            atoms=atoms,
            parameters=parameters,
        )

class IodineMethane(Molecule):
    def __init__(
            self,
            parameters=API.DEFAULT,
        ):
        smiles_code = 'CI'
        atoms = Smiles(smiles_code).atoms()
        atoms.rotate(v='y', a=90)
        Molecule.__init__(
            self,
            atoms=atoms,
            parameters=parameters,
        )
