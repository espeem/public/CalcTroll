import numpy as np
from CalcTroll import API

from CalcTroll.Core.Utilities import vectorLength

from CalcTroll.Plugins.Systems.Smiles import Smiles
from CalcTroll.Plugins.Systems.Molecule import Molecule
from CalcTroll.ASEUtils.Atoms import Atoms
from CalcTroll.ASEUtils.Atoms import AseAtom, Atoms

class GermaniumSP3(Molecule):
    def __init__(
            self,
            parameters=API.DEFAULT,
        ):
        smiles_code = 'CC(C)(C)C'
        atoms = Smiles(smiles_code).atoms()
        diff = vectorLength(atoms.positions, 1)
        argmin = diff.argmin()
        atoms.symbols[argmin] = 'Ge'
        Molecule.__init__(
            self,
            atoms=atoms,
            parameters=parameters,
        )

class GermaniumSP3Spin(Molecule):
    def __init__(
            self,
            parameters=API.DEFAULT,
        ):
        smiles_code = 'CC(C)(C)[CH2]'
        atoms = Smiles(smiles_code).atoms()
        diff = vectorLength(atoms.positions, 1)
        argmin = diff.argmin()
        atoms.symbols[argmin] = 'Ge'
        Molecule.__init__(
            self,
            atoms=atoms,
            parameters=parameters,
        )

class GermaniumReplacement(Smiles):
    def __init__(
            self,
            parameters=API.DEFAULT,
        ):
        name = self.className()
        Smiles.__init__(self, smiles_code=self.smiles_code, name=name)

    def atoms(self, *args, **kwargs):
        atoms = Smiles.atoms(self, *args, **kwargs)

        diff = vectorLength(atoms.positions, 1)
        argmin = diff.argmin()
        atoms.symbols[argmin] = 'Ge'
        v0 = np.array([0, 0, 1])
        v1 = atoms[argmin].position
        atoms.rotate(v=v0, a=v1)

        return atoms

class GermaniumAdamantane(GermaniumReplacement):
    smiles_code = 'C1C2CC3CC1CC(C2)C3'
