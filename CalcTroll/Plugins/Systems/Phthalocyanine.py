# Written by Mads Engelund, 2019, http://espeem.com
from ase.atoms import Atom
from ase.atoms import Atoms as ASEAtoms
from ase.constraints import FixAtoms
import numpy as np
from ase.visualize import view

from CalcTroll.Core.Utilities import vectorLength
from CalcTroll.ASEUtils.AtomUtilities import orderAtoms
from CalcTroll.ASEUtils.Atoms import Atoms
from CalcTroll.Plugins.Systems.Molecule import Molecule

from CalcTroll import API
from CalcTroll.API import DEFAULT


class Phthalocyanine(Molecule):
    def __init__(
            self,
            metal_atom='Sn',
            potential_strength=0.2,
            name=DEFAULT,
            ):

        if name is DEFAULT:
            name =  str(metal_atom) + 'Phthalocyanine'

        atom_unit = Atoms(
            [
                Atom('N', ( 0.0 , 1.6 ,0.0,)),
                Atom('C', ( 1.0 , 2.4 ,0.0,)),
                Atom('C', (-1.0 , 2.4 ,0.0,)),
                Atom('N', ( 2.0 , 2.0 ,0.0,)),
                Atom('C', ( 0.7 , 3.6 ,0.0,)),
                Atom('C', (-0.7 , 3.6 ,0.0,)),
                Atom('C', ( 1.4 , 4.8 ,0.0,)),
                Atom('C', (-1.4 , 4.8 ,0.0,)),
                Atom('C', ( 0.7 , 6.0 ,0.0,)),
                Atom('C', (-0.7 , 6.0 ,0.0,)),
            ],
        )
        atoms = atom_unit.copy()
        for i in range(3):
            atom_unit.rotate(v='z', a=90)
            atoms += atom_unit
        atoms = passivate_atoms(atoms)
        if metal_atom is not None:
            atoms.append(Atom(metal_atom, (0, 0, 1)))

        Molecule.__init__(
                self,
                atoms,
                potential_strength=potential_strength,
                name=name,
                )

class FixedSnPhthalocyanine(Phthalocyanine):
    def __init__(
            self,
            name=DEFAULT,
            parameters=DEFAULT,
            ):

        Phthalocyanine.__init__(
                self,
                potential_strength=1.0,
                name=name,
                parameters=parameters)

    def atoms(
            self,
            **kwargs
            ):
        atoms = Phthalocyanine.atoms(self, **kwargs)
        assert atoms.get_chemical_symbols()[-1] == 'Sn'

        atoms[-1].position[2] = -0.4
        mask = np.zeros(len(atoms))
        mask[-1] = True
        atoms.constraints.append(FixAtoms(mask=mask))

        return atoms


def passivate_atoms(atoms):
    symbols = np.array(atoms.get_chemical_symbols())
    pos = atoms.get_positions()[symbols=='C']

    return passivate_partial(atoms, pos)

def passivate_partial(atoms, pos, symbol='H'):
    a_pos = atoms.get_positions()
    tags = atoms.get_tags()
    hydrogens = []
    new_tags = []
    for i, p in enumerate(pos):
        diff = np.sqrt((abs(a_pos - p)**2).sum(axis=1))
        take1 = diff < 0.1
        take2 = np.logical_and(diff < 1.7, diff > 0.1)
        npos = a_pos[take2]
        if len(npos) == 2 and (take1).sum() == 1:
            av = npos.sum(axis=0)/2
            diff = p - av
            diff /= vectorLength(diff)
            hydrogens.append(p+diff)
            new_tags.append(tags[take1][0])

    hydrogens = np.array(hydrogens)
    hydrogens = Atoms(symbol*len(hydrogens), positions=hydrogens)
    hydrogens.set_tags(new_tags)
    atoms = atoms.copy()
    atoms += hydrogens

    return atoms

