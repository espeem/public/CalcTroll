from ase.atoms import Atom
from ase.atoms import Atoms as ASEAtoms
import numpy as np

from CalcTroll.Core.Visualizations import my_radii
from CalcTroll.ASEUtils.Atoms import Atoms
from CalcTroll.Plugins.Systems.Molecule import Molecule

from CalcTroll import API
from CalcTroll.API import DEFAULT

class Dimer(Molecule):
    def name(self):
        return self.symbol() + '2' + 'Dimer'

    def symbol(self):
        return self.__symbol

    def __init__(
            self,
            symbol='H',
            potential_strength=1.0,
    ):
        self.__symbol = symbol
        atoms = Atoms(symbol+'2')

        # Rough guess of the bond distance.
        r = [my_radii[e]*0.4 for e in atoms.get_atomic_numbers()]
        pos = [[-r[0], 0, 0], [r[1], 0, 0]]
        atoms.set_positions(pos)

        Molecule.__init__(
            self,
            atoms,
            potential_strength=potential_strength,
        )


class H2(Dimer):
    def __init__(self, potential_strength=1.0, parameters=DEFAULT):
        Dimer.__init__(self, 'H', potential_strength=potential_strength)

    def atoms(self, **kwargs):
        atoms = Dimer.atoms(self, **kwargs)
        atoms.set_initial_magnetic_moments([0]*len(atoms))

        return atoms


