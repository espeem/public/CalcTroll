# Written by Mads Engelund, 2017, http://espeem.com
import unittest
import numpy as np
from ase.visualize import view
from ase.atoms import Atom
from CalcTroll.Core.Test.Dummies import TestRelaxationSystem
from CalcTroll.Core.Test.Case import CalcTrollTestCase

from CalcTroll.ASEUtils.Atoms import Atoms
from CalcTroll.Plugins.Systems.Surface.Surface3D import Surface3D
from CalcTroll.Plugins.Systems.Crystal.Crystal3D import Crystal3D


class TestCrystal(Crystal3D):
    def normalVector(self, miller_indices):
        i, j, k = miller_indices
        normal_vector = np.array([i, j, k], float)
        normal_vector /= np.linalg.norm(normal_vector)

        return normal_vector

    def conventionalCell(self, coordinates='orth_crystal'):
        c = np.transpose(np.array([[-1, 1, 1], [1, -1, 1], [1, 1, -1]]))
        pc = np.matrix(np.transpose(self.unitCell(coordinates=coordinates)))
        uc = np.transpose(c*pc)

        return np.array(uc)


class SurfaceTest(CalcTrollTestCase):
    def setUp(self):
        a =  3.567
        atom1 = Atom('C', a*np.array([0,0,0])     )
        atom2 = Atom('C', a*np.array([1,1,1])/(4.))
        cell = np.array([[0,1,1],[1,0,1],[1,1,0]])*a/2.
        atoms = Atoms([atom1, atom2], cell=cell, pbc=[True]*3)
        self._crystal = TestCrystal(atoms)

        CalcTrollTestCase.setUp(self)

    def testConstruction(self):
        surface = Surface3D(crystal=self._crystal,
                          miller_indices=(0,0,1),
                          surface_cell=((2, 0), (0, 1)),
                          extra_atoms=Atoms('H', [[0, 0, 0.7]]),
                          )

        expected = [[ -1, 1, 0],
                    [  0, 0, 1],
                    [  0, 1, 0]]

        diff = surface.minimalCell('crystal') - expected
        self.assertAlmostEqual(sum(sum(abs(diff))), 0, 5)

        expected = [[ 2.52224989, 0.        , 0.        ],
                    [ 0.        , 2.52224989, 0.        ],
                    [ 1.26112494, 1.26112494, 1.7835    ]]

        diff = surface.minimalCell('orth_surface') - expected
        self.assertAlmostEqual(sum(sum(abs(diff))), 0, 5)

        expected = [[ 5.04449979,  0.        ,  0.        ],
                    [ 0.        ,  2.52224989,  0.        ]]

        diff = surface.unitCell('orth_surface') - expected
        self.assertAlmostEqual(sum(sum(abs(diff))), 0, 5)

        extra_atoms = surface.extraAtoms('orth_surface')
        self.assertEqual(len(extra_atoms), 1)

    def testSkew(self):
        surface_cell=((2, 1), (0, 1)),
        surface = Surface3D(crystal=self._crystal,
                            miller_indices=(0,0,1),
                            surface_cell=((2, 1), (0, 1)),
                            extra_atoms=Atoms('H', [[0, 0, 0.7]]),
                            )
        result = surface.unitCell('minimal_unit_cell')
        diff = result[:,:2] - np.array(surface_cell)[0]
        self.assertAlmostEqual(sum(sum(abs(diff))), 0, 5)

    def testPiecewise(self):
        surface = Surface3D(crystal=self._crystal,
                          miller_indices=(0,0,1),
                          surface_cell=((1, 0), (0, 1)),
                          )

        surface = surface.copy(surface_cell=((2, 1), (0,1)))
        expected = [[ 5.63992221,  0.         , 0.        ],
                    [ 1.12798444,  2.25596888 , 0.        ]]


        diff = np.array(surface.unitCell('orth_surface')) - np.array(expected)
        self.assertAlmostEqual(sum(sum(abs(diff))), 0, 5)

        surface = surface.copy(extra_atoms=Atoms('H', [[0, 0, 0.7]]))

        self.assertEqual(len(surface.extraAtoms('orth_surface')), 1)

    def testShiftOrigin(self):
        shift = [0.4, 0.5]
        surface = Surface3D(crystal=self._crystal,
                          miller_indices=(0,0,1),
                          surface_cell=((1, 0), (0, 1)),
                          extra_atoms=Atoms('H', [[0.2, 0.3, 0.7]]),
                          shift_origin=shift,
                          )

        one = surface.extraAtoms('from_top').get_positions()
        two = surface.extraAtoms('orth_surface').get_positions()

        diff = (two-one)[:,:2] - shift
        self.assertAlmostEqual(sum(sum(abs(diff))), 0, 5)

    def notestSetRelaxed(self):
        crystal = self._crystal.copy()
        surface = Surface3D(crystal=self._crystal,
                            miller_indices=(0,0,1),
                            surface_cell=((1, 0), (0, 1)),
                            extra_atoms=Atoms('H', [[0.2, 0.3, 0.7]]),
                            )
        surface_parameters1 = surface.defaultParameters().copy(
                free_layers=2,
                bound_layers=1,
                vectors=((2, 0),(0, 2)),
                cell_size=50,
                )
        surface_parameters2 = surface_parameters1.copy(
                free_layers=2,
                bound_layers=1,
                vectors=((2, 0),(0, 3)),
                cell_size=50,
                )
        surface_atoms = surface.atoms(parameters=surface_parameters1)

        atoms = crystal.atoms()
        atoms.cell *= 1.1
        atoms.positions *= 1.1
        new_crystal = TestRelaxationSystem(crystal, atoms)
        surface = surface.setSubSystems([new_crystal])
        new_surface_atoms = surface.atoms(parameters=surface_parameters1)
        self.assertArraysEqual(new_surface_atoms.cell[:2, :2], surface_atoms.cell[:2,:2]*1.1)
        self.assertEqual(new_surface_atoms.cell[2, 2], surface_atoms.cell[2,2])
        surface_atoms = new_surface_atoms.copy()
        surface_atoms.rattle(0.2)

        print(surface_atoms.positions)
        new_surface = TestRelaxationSystem(surface, surface_atoms)
        self.assertTrue(new_surface.isRelaxed())
        atoms = new_surface.atoms(parameters=surface_parameters2)
        expected = [[-1.38724, -4.16171, -1.96185],
                    [1.38724 , -4.16171, -1.96185],
                    [-1.38724, -1.38724, -1.96185],
                    [1.38724 , -1.38724, -1.96185],
                    [-1.38724, 1.38724 , -1.96185],
                    [1.38724 , 1.38724 , -1.96185],
                    [-1.02171, -3.41461, -1.41508],
                    [1.27478 , -2.97704, -0.91808],
                    [-1.2517 , 0.60929 , -0.77697],
                    [1.34208 , 0.01351 , -1.26587],
                    [-1.02171, 2.13434 , -1.41508],
                    [1.27478 , 2.57191 , -0.91808],
                    [-3.14083, -3.00977, 0.66155 ],
                    [0.07514 , -2.8946 , -0.05834],
                    [-2.89482, 0.37046 , -0.0027 ],
                    [-0.21154, 0.16451 , -0.24417],
                    [-3.14083, 2.53918 , 0.66155 ],
                    [0.07514 , 2.65435 , -0.05834],
                    [-1.27164, -3.80576, 1.07759 ],
                    [1.38199 , -3.83767, 1.58472 ],
                    [-1.20674, -1.363  , 1.10766 ],
                    [1.56533 , -0.82337, 1.40757 ],
                    [-1.27164, 1.74319 , 1.07759 ],
                    [1.38199 , 1.71128 , 1.58472 ]]


        exp_atoms = Atoms('C18H6', expected)
        self.assertAtomsEqual(exp_atoms, atoms)


if __name__=='__main__':
    unittest.main()
