# Written by Mads Engelund, 2017, http://espeem.com
import numpy as np
from ase.io import write

from ase.constraints import FixAtoms

from CalcTroll.ASEUtils.Atoms import Atoms
from CalcTroll.ASEUtils.AtomUtilities import cellCenter
from CalcTroll.ASEUtils.AtomUtilities import orderAtoms
from CalcTroll.ASEUtils.AtomUtilities import findIndices
from CalcTroll.ASEUtils.AtomUtilities import groupAtoms
from CalcTroll.ASEUtils.AtomUtilities import removeCopies
from CalcTroll.ASEUtils.AtomUtilities import findPeriodicallyEqualAtoms
from CalcTroll.ASEUtils.AtomUtilities import findCopies
from CalcTroll.ASEUtils.AtomUtilities import makeAtomsRelaxed
from CalcTroll.Core.Utilities import findOrthogonalVector

from CalcTroll.Core.Utilities import convertFromBasis, convertToIntegers, convertToBasis
from CalcTroll.Core.Utilities import vectorLength

from CalcTroll.Core.Visualizations import flatMatplotlibView
from CalcTroll.Core.System.BasisChanger import BasisChanger, Transformation
from CalcTroll.Core.Visualizations import addMeasuringStick
from CalcTroll.Plugins.Systems.Surface.Utilities import findOrthogonalBasisSet
from CalcTroll.Plugins.Systems.Crystal.Utilities import makeSurface
from CalcTroll.Plugins.Systems.Crystal.Utilities import makeSlabWithVectors
from CalcTroll.Plugins.Systems.Crystal.Utilities import fitBoxIntoBox
from CalcTroll.Plugins.LengthScales import LengthScales
from CalcTroll.Plugins.Systems.Surface.Surface import Surface, setCellAndPBC

from CalcTroll import API
from CalcTroll.API import DEFAULT

class MoleculeOnlyParameters(API.Parameters):
    def __init__(self,
                 length_scales=LengthScales(),
                 ):
        self.__ls = length_scales

    def lengthScales(self):
        return self.__ls

    def kpts(self):
        return (1, 1, 1)


class SurfaceParameters3D(API.Parameters):
    def __init__(self,
                 free_layers=DEFAULT,
                 bound_layers=2,
                 electrode_layers=0,
                 vectors=DEFAULT,
                 kpts=DEFAULT,
                 cell_size=DEFAULT,
                 length_scales=LengthScales(),
                 passivated=True,
                 ):
        pass

    def kpts(self):
        return self['kpts']

    def nLayers(self):
        return self['free_layers'] + self['bound_layers'] + self['electrode_layers']

    def identifier(self):
        l = []
        if self['free_layers'] is DEFAULT:
            raise Exception('Free layers not set')
        if not self.nLayers() is None:
            if self['free_layers'] is None:
                l.append(self.nLayers())
            else:
                l.append('%sof%s'%(self['free_layers'], self.nLayers()))

        for entry in [self['vectors'], self['kpts'], self['cell_size']]:
            if not entry is None:
                l.append(entry)

        return tuple(l)

    def repeatUnitCell(self, atoms):
        vectors = self['vectors']
        if vectors[0][1] == 0 and vectors[1][0] == 0:
            repeats = (vectors[0][0], vectors[1][1], 1)
        else:
            raise Exception
        atoms = atoms.repeat(repeats)

        return atoms

    def ignoreLongRangeCorrelation(self):
        lenth_scales = self['length_scales'].copy()
        short_length = self['length_scales']['short_correlation_length']
        lenth_scales = lenth_scales.copy(correlation_length=short_length)

        return self.copy(length_scales=lenth_scales)


class SurfaceClusterParameters3D(API.Parameters):
    def __init__(self, nearest=0, length_scales=LengthScales()):
        self.__ls = length_scales
        self.__nearest = nearest

    def lengthScales(self):
        return self.__ls

    def nearest(self):
        return self.__nearest
    
    def kpts(self):
        return (1, 1, 1)

    def ignoreLongRangeCorrelation(self):
        lenth_scales = self['length_scales'].copy()
        short_length = self['length_scales']['short_correlation_length']
        lenth_scales = lenth_scales.copy(correlation_length=short_length)

        return self.copy(length_scales=lenth_scales)


class Surface3D(Surface):
    """This object represents a surface of a 3-dimensional crystal. """
    @classmethod
    def parameterClass(cls):
        return SurfaceParameters3D

    def defaultParameters(
            self,
            parameters=DEFAULT,
            centers=((0,0,0),),
            ):
        if parameters is DEFAULT:
            parameters = self.parameterClass()()

        centers = np.array(centers)

        scales = parameters['length_scales']
        short_length = scales['short_correlation_length']
        long_length = scales['correlation_length']
        depth = scales.depth()

        in_equal_layers = self.inequivalentLayers()
        electrode_layers = parameters['electrode_layers']
        bound_layers = parameters['bound_layers']
        free_layers = parameters['free_layers']
        minimum = centers[:, 2].min()
        maximum = centers[:, 2].max()
        if bound_layers is DEFAULT:
            bound_layers = 2
        if electrode_layers is DEFAULT:
            electrode_layers = 0

        r_system = self.fullyUnrelaxed()
        if free_layers is DEFAULT:
            size = 2*depth - minimum
            n_layers = int(np.ceil(size/r_system.cell('orth_surface')[2,2]))
            n_layers += in_equal_layers 
            free_layers = n_layers - bound_layers - electrode_layers

        vectors = parameters['vectors']
        if vectors is DEFAULT:
            vectors = r_system.neededVectors(
                    short_length=short_length,
                    centers=centers,
                    )
        kpts = parameters['kpts']
        if kpts is DEFAULT:
            kpts = r_system.neededKpts(vectors, long_length)

        cell_size = parameters['cell_size']
        if cell_size is DEFAULT:
            cell_size = maximum - minimum
            cell_size += 2*depth
            cell_size += short_length


        parameters = parameters.copy(
                free_layers=free_layers,
                bound_layers=bound_layers,
                electrode_layers=electrode_layers,
                vectors=vectors,
                kpts=kpts,
                cell_size=cell_size,
                )

        return parameters

    def __init__(self,
                 crystal,
                 initial_state=DEFAULT,
                 miller_indices=(0,0,1),
                 surface_cell=((1, 0), (0, 1)),
                 extra_atoms=DEFAULT,
                 shift_origin=[0,0],
                 name=DEFAULT,
                 parameters=DEFAULT,
                 ):
        if extra_atoms is DEFAULT:
            extra_atoms = Atoms([])

        minimal_unit_cell  = makeSurface(crystal, miller_indices)

        self.__miller_indices = tuple(miller_indices)
        self.__minimal_unit_cell = minimal_unit_cell
        self.__surface_cell = surface_cell
        self.__extra_atoms = extra_atoms
        self.__shift_origin = shift_origin

        API.System.__init__(
                self,
                sub_systems=[crystal],
                initial_state=initial_state,
                name=name,
                parameters=parameters,
                )

    def setUpBasisChanger(self):
        crystal = self.crystal()
        minimal_unit_cell = self.__minimal_unit_cell
        extra_atoms = self.__extra_atoms
        shift_origin = self.__shift_origin

        surface_cell = self.__surface_cell
        cell = np.zeros((3, 3), dtype=float)
        cell[:2, :2] = surface_cell
        cell[2, 2] = 1
        surface_in_minimal = cell

        cell = convertFromBasis(cell, self.__minimal_unit_cell)
        cell = convertToIntegers(cell)
        cell =  convertFromBasis(cell, crystal.unitCell())
        orth_surface_basis = findOrthogonalBasisSet(cell)

        if len(extra_atoms) == 0:
            top = 0
        else:
            top = np.max(extra_atoms.get_positions()[:, 2])
        shift = list(shift_origin) + [top]

        transformations = [
            Transformation(to='minimal_unit_cell', fro='surface', trans_matrix=surface_in_minimal.transpose()),
            Transformation(to='orth_crystal', fro='crystal', trans_matrix=crystal.unitCell().transpose()),
            Transformation(to='crystal', fro='minimal_unit_cell', trans_matrix=minimal_unit_cell.transpose()),
            Transformation(to='from_top', fro='orth_surface', rotation_point=[[0,0,0], shift]),
            Transformation(to='orth_crystal',
                           fro='orth_surface',
                           trans_matrix=orth_surface_basis.transpose()),
            ]
        basis_changer = BasisChanger(transformations=transformations)

        return basis_changer

    def pbc(self):
        return [True, True, False]

    def _minimalUnitCell(self):
        return self.__minimal_unit_cell.copy()

    def minimalCell(self, coordinates='orth_surface'):
        return self.change(
                self._minimalUnitCell(),
                to=coordinates,
                fro='crystal',
                difference=True,
                )

    def _unitCell(self):
        unit_cell =np.dot(self._surfaceCell(), self.__minimal_unit_cell[:2])

        return unit_cell

    def unitCell(self, coordinates='orth_surface'):
        unit_cell = self._unitCell()
        unit_cell = self.change(
                unit_cell,
                to=coordinates,
                fro='crystal',
                difference=True,
                )
        return unit_cell

    def millerIndices(self):
        return tuple(self.__miller_indices)

    def outOfPlaneVector(self, coordinates='orth_surface'):
        return self.change(self.__minimal_unit_cell[2], to=coordinates, fro='crystal')

    def normalVector(self, coordinates='orth_surface'):
        unit_cell = np.array(self.crystal().conventionalCell())
        normal_vector = np.tensordot(unit_cell, self.millerIndices(), (0, 0))
        normal_vector /= vectorLength(normal_vector)
        return self.change(normal_vector, to=coordinates, fro='orth_crystal')

    def _cell(self):
        unit_cell = self._unitCell()
        out_of_plane = self._minimalUnitCell()[-1]
        cell = np.zeros((3,3), dtype=int)
        cell[:2] = unit_cell
        cell[2] = out_of_plane

        return cell

    def cell(self, coordinates='orth_surface'):
        cell = self._cell()

        return self.change(cell, fro='crystal', to=coordinates)

    def fullCell(self, parameters, coordinates='orth_surface'):
        vectors = np.array(parameters['vectors'])
        cell = np.zeros((3,3), dtype=float)
        cell[:2,:2] = self.change(vectors, fro='surface', to='orth_surface')
        cell[2,2] = parameters['cell_size']

        return self.change(cell, fro='orth_surface', to=coordinates)

    def _surfaceCell(self):
        return np.array(self.__surface_cell)

    def makeSubsystemsRelaxed(self, atoms, tolerance=1.0):
        if self.crystal().isRelaxed():
            atoms = self.change(atoms, fro='orth_surface', to='orth_crystal')
            atoms = self.crystal().makeAtomsRelaxed(atoms, tolerance=tolerance)
            atoms = self.change(atoms, to='orth_surface', fro='orth_crystal')

        return atoms

    def _extraAtoms(self):
        atoms = self.__extra_atoms.copy()
        cell = np.identity(3)
        cell[:2,:2] = self._surfaceCell()
        atoms.set_cell(cell)
        atoms.set_pbc(self.pbc())

        return atoms

    def extraAtoms(
            self,
            coordinates='orth_surface',
            initialized=True,
            relaxed=True,
            ):
        atoms = self._extraAtoms()
        atoms = self.fullyUnrelaxed().change(
                atoms,
                to='orth_surface',
                fro='minimal_unit_cell',
                )
        atoms = self.applyInitializationConstraintsAndRelaxation(
                atoms=atoms,
                initialized=initialized,
                relaxed=relaxed,
                )
        atoms = self.change(atoms, to=coordinates, fro='orth_surface')

        return atoms

    def _surfaceLayer(self, parameters):
        atoms = self._extraAtoms()
        atoms.set_pbc([True, True, False])
        atoms = parameters.repeatUnitCell(atoms)

        return atoms

    def surfaceLayer(
            self,
            parameters,
            coordinates='orth_surface',
            ):
        atoms = self._surfaceLayer(parameters=parameters)
        atoms = self.change(atoms, to=coordinates, fro='minimal_unit_cell')

        return atoms

    def constrainLayers(self, *args, **kwargs):
        mask = self.findLayers(*args, **kwargs)
        constraint = FixAtoms(mask=mask)

        return constraint

    def findLayers(self, atoms, start_layer=0, end_layer=1):
        c_atoms = self.crystalAtoms(start_layer=start_layer,
                                    end_layer=end_layer,
                                    sub_layers=True,
                                    )

        if len(c_atoms) == 0:
            mask = np.zeros(len(atoms), bool)
        else:
            zmin, zmax = c_atoms.positions[:,2].min(), c_atoms.positions[:,2].max()
            mask1 = atoms.positions[:, 2] > zmin - 1e-5
            mask2 = atoms.positions[:, 2] < zmax + 1e-5
            mask = np.logical_and(mask1, mask2)

        return mask

    def inequivalentLayers(self):
        atoms = self._crystalAtoms()
        groups = groupAtoms(atoms)

        return len(groups)

    def directionLenghtMultipliers(self):
        return np.array((1.0, 1.0))

    def neededVectors(self,
                      short_length,
                      centers=((0,0,0),),
                      ):
        centers = np.array(centers)
        max_centers = np.max(centers, axis=0)
        min_centers = np.min(centers, axis=0)
        extent = max_centers - min_centers

        v1, v2 = self.unitCell()
        l1 = vectorLength(v1)
        l2 = vectorLength(v2)

        size = abs(np.dot(extent + short_length, v1/l1))
        n1 = int(np.ceil(size/l1))

        v1t = np.array([v1[1], -v1[0], 0])
        v1t /= vectorLength(v1t)
        test_vector = np.dot(v2, v1t)*v1t
        lt = vectorLength(test_vector)
        size = abs(np.dot(extent+short_length, test_vector/lt))
        n2 = int(np.ceil(size/lt))

        vectors = np.array([(n1, 0), (0, n2)])

        return vectors

    def neededKpts(self, unit_cell, correlation_length):
        unit_cell = np.array(unit_cell)
        kpts=[]
        if unit_cell is None:
            unit_cell = np.array([[1,0], [0,1]])
        unit_cell = self.change(unit_cell, fro='surface', to='orth_surface')
        for vector in unit_cell:
            vector = vector/self.directionLenghtMultipliers()
            length=np.sqrt(np.sum(vector**2))
            number=int(np.ceil(correlation_length/length))
            kpts.append(number)

        return kpts

    def atomsTemplate(self, parameters):
        system = self.fullyUnrelaxed()
        electrode_sep = parameters.nLayers() - parameters['electrode_layers']
        c_atoms = system.crystalAtoms(
                          start_layer=electrode_sep,
                          end_layer=parameters.nLayers(),
                          vectors=parameters['vectors'],
                          minimal=True,
                          sub_layers=True,
                          )
        c_atoms += system.crystalAtoms(
                          start_layer=0,
                          end_layer=electrode_sep,
                          vectors=parameters['vectors'],
                          minimal=False,
                          sub_layers=True,
                          )
        atoms = c_atoms.copy()
        vectors = system.fullCell(parameters)
        v3 = np.zeros(3)
        v3[2] = -abs(atoms.cell[2][2])
        tmp_vectors = np.array([vectors[0], vectors[1], v3])
        atoms.set_cell(tmp_vectors)
        atoms.set_pbc([True, True, False])
        atoms.wrap()

        # Add outer surface layer.
        extra_atoms = system.surfaceLayer(parameters)
        atoms += extra_atoms
        atoms.setTag('Reconstruction', picker=slice(-len(extra_atoms), None))
        atoms.cell[:2,:2] = extra_atoms.cell[:2, :2]

        if parameters['passivated']:
            before = len(atoms)
            atoms = self.passivateLowerSurface(atoms)
            after = len(atoms)
            n_passivated = after - before

        # Make vaccum in the out-of-surface direction.
        atoms.cell[2] = np.array([0,0, parameters['cell_size']])

        if parameters['electrode_layers'] > 0:
            fro, to = parameters.nLayers() - parameters['electrode_layers'], parameters.nLayers()
            mask = system.findLayers(atoms, start_layer=fro, end_layer=to)
            cell = system.electrodeCell(parameters['electrode_layers'], parameters['vectors'])
            atoms.setRegion(
                    name=self.crystal().name(),
                    picker=mask,
                    cell=cell,
                    pbc=(True, True, (True, False)),
                    )
            mask = np.logical_not(mask)
            if mask.sum() > 0:
                atoms.setRegion(name=self.name(),
                                  picker=mask,
                                  cell=atoms.get_cell(),
                                  pbc=(True, True, False),
                                  )

        atoms.wrap(center=0, eps=0.01)
        atoms = orderAtoms(atoms)

        fro, to = parameters['free_layers'], parameters.nLayers()
        constrain_mask = system.findLayers(atoms, start_layer=fro, end_layer=to)
        constrain_indices = np.arange(len(atoms))[constrain_mask]
        constrain_indices = np.arange(constrain_indices[-1] + 1)
        atoms.set_constraint(FixAtoms(indices=constrain_indices))

        bulk_mask = system.findLayers(atoms, start_layer=0, end_layer=parameters.nLayers())
        atoms.setTag('ReferenceBulk', picker=bulk_mask)
        atoms.setTag('Reference', picker=slice(-n_passivated, None))


        cell_center = cellCenter(atoms)
        atoms.set_celldisp(-cell_center)

        return atoms

    def electrodeCell(self, electrode_layers, vectors):
        n = self.subLayersPrLayer()
        n, remain  = divmod(electrode_layers, n)
        assert remain == 0
        minimal = self.minimalCell()
        cell = minimal
        cell[2] *= n
        cell[2] = findOrthogonalVector(minimal[0], minimal[1], cell[2])

        return cell

    def subLayersPrLayer(self):
        x1 = self.crystalAtoms(
                start_layer=0,
                end_layer=1,
                sub_layers=False,
                )
        x2 = self.crystalAtoms(
                start_layer=0,
                end_layer=1,
                sub_layers=True,
                )
        result, remain = divmod(len(x1), len(x2))
        assert remain == 0

        return  result

    def _crystalAtoms(self,
                     start_layer=0,
                     end_layer=1,
                     vectors=((1,0),(0,1)),
                     sub_layers=False,
                     minimal=True,
                     ):
        sl = slice(start_layer, end_layer)
        if minimal:
            unit_cell = self._minimalUnitCell()
        else:
            unit_cell = self._cell()
        crystal = self.crystal().fullyUnrelaxed()

        catoms = crystal.atoms()
        catoms.purgeTags()
        atoms = makeSlabWithVectors(catoms, unit_cell, sl.stop)
        unit_cell = crystal.change(
                unit_cell,
                to='orth_crystal',
                fro='crystal',
                difference=True,)[:2]


        if len(atoms)==0:
            return atoms

        if minimal:
            vectors = unit_cell
        else:
            vectors = np.array(vectors)
            vectors = np.dot(unit_cell.transpose(), vectors.transpose()).transpose()
            repeats = fitBoxIntoBox(unit_cell, vectors)
            atoms = atoms.repeat(list(repeats) + [1])

        out_vector = atoms.cell[2]
        normal_vector = self.normalVector(coordinates='orth_crystal')
        pro = np.dot(normal_vector, out_vector)
        out_vector = pro*normal_vector

        vectors = np.array([vectors[0], vectors[1], out_vector])

        atoms.set_cell(vectors)
        atoms.set_pbc([True, True, False])
        atoms = removeCopies(atoms)
        atoms.wrap()
        if not self.name() == 'FCC111':
            atoms = orderAtoms(atoms, (2, 1, 0))

        if sub_layers:
            groups = groupAtoms(atoms)[sl]
            if len(groups) == 0:
                return Atoms([], cell=atoms.get_cell(), pbc=atoms.get_pbc())

            atoms, indices = groups[0]

            for atoms_group, indices in groups[1:]:
                atoms += atoms_group

        return atoms

    def crystalAtoms(
            self,
            start_layer=0,
            end_layer=1,
            vectors=((1,0),(0,1)),
            sub_layers=False,
            minimal=True,
            initialized=True,
            relaxed=True,
            coordinates='orth_surface',
            ):
        unrelaxed = self.fullyUnrelaxed()
        atoms = unrelaxed._crystalAtoms(
                start_layer=start_layer,
                end_layer=end_layer,
                vectors=vectors,
                sub_layers=sub_layers,
                minimal=minimal,
                )
        atoms = unrelaxed.change(atoms, to=coordinates, fro='orth_crystal')

        self.applyInitializationConstraintsAndRelaxation(
                atoms=atoms,
                initialized=initialized,
                relaxed=relaxed,
                )

        return self.change(atoms, to=coordinates, fro='orth_surface')

    def bandStructurePath(self):
        path = [
                ((0.0, 0.0, 0.0), '\\Gamma'),
                ((0.5, 0.0, 0.0), 'A'),
                ((0.5, 0.5, 0.0), 'AB'),
                ((0.0, 0.5, 0.0), 'B'),
                ((0.0, 0.0, 0.0), '\\Gamma'),
                ]
        return path

    def constrain(self, atoms):
        return atoms.copy()

    def _findAtoms(self, identifiers, initialized=True):
        pure_atoms = self._extraAtoms()
        atoms = Atoms([], cell=pure_atoms.get_cell(), pbc=pure_atoms.get_pbc())
        unit_cell = pure_atoms.cell

        for identifier in identifiers:
            number, cell = identifier

            if number >= 0:
                atom = pure_atoms.copy()[number]
                depth = 0
            elif number < 0:
                number = -(number + 1)
                depth, crystal_number = divmod(number, len(pure_atoms))

                atom=pure_atoms.copy()[crystal_number]
            else:
                raise Exception

            shift = -unit_cell[2]*depth

            cell = (cell[0], cell[1], 0)
            shift += np.dot(cell, unit_cell)

            atom.position += shift
            atoms += atom

        setCellAndPBC(atoms, periodicity=[])

        return atoms, 'minimal_unit_cell'

    def initialStateIdentifiers(self, identifiers):
        new_list = []
        for identifier in identifiers:
            if isinstance(identifier, int):
                new_list.append((identifier, (0, 0)))
            if isinstance(identifier, tuple) and len(identifier) == 2:
                new_list.append(identifier)

        return new_list

    def findAtomIndices(self, identifiers, atoms):
        find_atoms = self.findAtoms(identifiers)
        indices = findIndices(find_atoms, atoms)

        return indices

    def referenceEnergyMatchIndices(self, parameters):
        atoms = self.atomsTemplate(parameters)
        i1 = atoms.tag("ReferenceBulk").indices()
        i2 = None

        return i1, i2

    def plot(self,
             data=None,
             ax=None,
             frame=None,
             border=4,
             color='white',
             direction='-z',
             ):
        if data is None:
            atoms = self.atoms()
        else:
            atoms = data

        if frame is None:
            frame = self.frame(data=data, border=border)

        self.plotUnitCell(data=atoms, ax=ax, direction=direction)
        pbc = atoms.get_pbc()

        if not any(pbc):
            pass
        else:
            move = -atoms.cell[:2].sum(axis=0)
            repeat = (5, 5, 1)
            atoms = atoms.repeat(repeat)
            atoms.translate(move)

        flatMatplotlibView(atoms, ax, direction=direction, size=0.55)
        if frame is not None:
            ax.set_xlim((frame[0, 0], frame[1, 0]))
            ax.set_ylim((frame[0, 1], frame[1, 1]))
            addMeasuringStick(ax, color=color, size=5)

        ax.patch.set_facecolor('black')
        ax.set_xticks([])
        ax.set_yticks([])

        return frame

    def plotUnitCell(self, data=None, ax=None, direction='-z'):
        if data is None:
            atoms = self.atoms()
        else:
            atoms = data

        if isinstance(atoms, list):
            atoms = atoms[0]

        pbc = atoms.get_pbc()
        if not any(pbc):
            return None

        cell = atoms.get_cell()
        if direction == '-x':
            cell = cell[1:,1:]
        elif direction == '-y':
            cell = np.array([[cell[0, 0], cell[0, 2]], [cell[2, 0], cell[2, 2]]])
        elif direction == '-z':
            cell = cell[:2,:2]
        points = [
            -cell[0]/2 - cell[1]/2,
            cell[0]/2 - cell[1]/2,
            cell[0]/2 + cell[1]/2,
            -cell[0]/2 + cell[1]/2,
            ]
        x, y = np.transpose(points)
        ax.plot(
            x[0:2], y[0:2],
            x[1:3], y[1:3],
            x[2:4], y[2:4],
            [x[-1], x[0]], [y[-1], y[0]],
            color='blue',
            lw=3,
        )

    def frame(self, data=None, border=API.DEFAULT, direction='-z'):
        if data is None:
            atoms = self.atoms()
        else:
            atoms = data

        pbc = np.array(atoms.get_pbc())
        pmin = atoms.positions.min(axis=0)
        pmax = pmin + atoms.cell.sum(axis=0)
        pmax[np.logical_not(pbc)] = atoms.positions.max(axis=0)[np.logical_not(pbc)]

        if border is API.DEFAULT:
            border = 4

        pmin -= border
        pmax += border

        frame = np.array([pmin, pmax])

        if direction == '-x':
            frame = frame[:, 1:]
        elif direction == '-y':
            frame = frame[:, (True, False, True)]
        elif direction == '-z':
            frame = frame[:, :2]

        return frame

    def jpg_save(
            self,
            filename,
            data=None,
            frame=None,
            border=API.DEFAULT,
            direction='-z',
            **kwargs):
        from matplotlib import pylab as plt
        if data is None:
            data = self.read()
        if frame is None:
            frame = self.frame(
                data=data,
                border=border,
                direction=direction,
            )

        pmin, pmax = frame
        d = pmax - pmin
        dx = d[0]
        dy = d[1]
        ratio = dx/dy

        fig = plt.figure(frameon=False, figsize=(4*ratio, 4))
        fig.patch.set_facecolor('black')

        ax = plt.Axes(fig, [0., 0., 1., 1.])
        fig.add_axes(ax)

        self.plot(data=data,
                  ax=ax,
                  frame=frame,
                  direction=direction,
                  **kwargs)

        ax.set_xlim((pmin[0], pmax[0]))
        ax.set_ylim((pmin[1], pmax[1]))
        plt.savefig(filename)
        plt.close('all')

        return frame

def convertShorthand(identifiers):
    if identifiers is None:
        return []

    # Convert shorthand 3 -> (3, (0, 0))
    identifiers = [((identifier, (0, 0)) if isinstance(identifier, int) else identifier) for identifier in identifiers]

    return identifiers

