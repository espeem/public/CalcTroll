# Written by Mads Engelund, 2017, http://espeem.com
import os
from os.path import join
from math import pi
import numpy as np
from ase.visualize import view
from ase.constraints import FixAtoms

from CalcTroll.Plugins.Systems.Molecule import Molecule
from CalcTroll.Plugins.Systems.Surface import Surface
from CalcTroll.Core.System.BasisChanger import BasisChanger, Transformation
from CalcTroll.ASEUtils.AtomUtilities import makeAtomsRelaxed, orderAtoms
from CalcTroll import API
from CalcTroll.API import DEFAULT

from .MoleculeOnSlab import MoleculeOnSlabParameters


class ClusterOnSlab(API.System):
    COORDINATES='orth_system'

    @classmethod
    def parameterClass(cls):
        return MoleculeOnSlabParameters

    def _makeName(self):
        basename = self.baseName()
        name = ''
        if (self.displacement() != (0.,0.,0.)).any():
            name += 'S%.3f_%.3f_%.3f' % tuple(self.displacement())

        return name

    def baseName(self):
        return self.molecule().name() + 'On' + self.slab().name()

    def subParameters(self, parameters=DEFAULT):
        parameters = self.defaultParameters(parameters)

        return (parameters['molecule_parameters'], parameters['slab_parameters'])

    def defaultParameters(self, parameters=DEFAULT):
        slab = self.slab().fullyUnrelaxed()
        surface = self.surface().fullyUnrelaxed()
        cluster = self.cluster().fullyUnrelaxed()

        if parameters is DEFAULT:
            molecule_parameters = DEFAULT
            slab_parameters = DEFAULT
        elif isinstance(parameters,
                (slab.parameterClass(),
                 surface.parameterClass()),
                ):
            molecule_parameters = DEFAULT
            slab_parameters = slab.defaultParameters(parameters=parameters)
        else:
            molecule_parameters = parameters['molecule_parameters']
            slab_parameters = parameters['slab_parameters']

        centers = cluster.atoms().get_positions()

        molecule_parameters = cluster.defaultParameters(parameters=molecule_parameters)
        slab_parameters = slab.defaultParameters(parameters=slab_parameters, centers=centers)

        return MoleculeOnSlabParameters(
                molecule_parameters=molecule_parameters,
                slab_parameters=slab_parameters,
                )

    def __init__(
            self,
            cluster,
            slab,
            origin=DEFAULT,
            displacement=(0,0,0),
            initial_state=DEFAULT,
            name=DEFAULT,
            parameters=DEFAULT,
            ):
        if name == DEFAULT:
            name = self._makeName()
        if origin == DEFAULT:
            center = np.array([0, 0])

        sub_systems = [cluster, slab.surface()]
        self.__slab = slab
        self.__origin = origin
        self.__displacement = displacement

        API.System.__init__(
                self,
                sub_systems,
                initial_state=initial_state,
                name=name,
                parameters=parameters,
                )

    def displacement(self):
        return  np.array(self.__displacement, float)

    def setUpBasisChanger(self):
        basis_changer = BasisChanger()
        surface = self.surface()
        extra_atoms = surface.extraAtoms('orth_surface')
        point = extra_atoms.positions.mean(axis=0)
        cell = surface.minimalCell()
        point = cell[2]
        point[2] = extra_atoms.positions[:,2].max()
        cell = surface.unitCell()
        disp = np.tensordot(cell[:2], self.__origin, (0, 0))
        point -= disp
        transformation = Transformation(to='orth_surface', fro='orth_system', rotation_point=[point, [0,0,0]])
        basis_changer.addTransformation(transformation)
        basis_changer.include(self.slab().basisChanger())

        return basis_changer

    def slab(self):
        return self.__slab

    def surface(self):
        return self.subSystems()[1]

    def crystal(self):
        return self.slab().crystal()

    def cluster(self):
        return self.subSystems()[0]

    def findAtomIndices(self, identifiers, atoms):
        indices = self.clusterIndices(atoms)
        if identifiers is None:
            pass
        else:
            raise ValueError

        return indices

    def clusterIndices(self, atoms):
        return np.arange(len(atoms))[-len(self.cluster()):]

    def slabIndices(self, atoms):
        return np.arange(len(atoms))[:-len(self.cluster())]

    def atomsTemplate(self,
            constrained=True,
            parameters=DEFAULT,
            ):
        system = self.fullyUnrelaxed()
        if parameters is DEFAULT:
            parameters = self.parameters()

        parameters = system.defaultParameters(parameters)
        slab = system.slab()

        slab_parameters = parameters['slab_parameters']
        atoms = slab.atoms(
                constrained=constrained,
                initialized=True,
                parameters=slab_parameters,
                )
        atoms = system.change(atoms, to='orth_system', fro='orth_defect')
        atoms.wrap(center=0)
        atoms = orderAtoms(atoms)
        constraint_indices = atoms.constraints[0].get_indices()
        constraint_indices = np.arange(max(constraint_indices) + 1)
        del atoms.constraints
        atoms.set_constraint(FixAtoms(indices=constraint_indices))

        cluster_atoms = self.clusterAtomsTemplate(
                parameters=parameters['molecule_parameters'],
                constrained=False,
                )
        atoms += cluster_atoms

        if len(cluster_atoms) > 0:
            name = self.name().replace(os.sep, '_')
            atoms.setRegion(name=name, picker=slice(-len(cluster_atoms), None),
                              cell=atoms.get_cell(), pbc=[False, False, False])

        return atoms


    def makeSubsystemsRelaxed(self, atoms, tolerance=1.5):
        cluster_atoms = self.cluster().atoms(passivate=False)
        size = len(cluster_atoms)

        atoms = self.change(atoms, to='orth_surface', fro='orth_system')
        surface = self.surface()
        slab_atoms = atoms[:-size]
        slab_atoms = surface.makeAtomsRelaxed(slab_atoms, tolerance=tolerance)
        atoms.cell = slab_atoms.cell
        atoms.positions[:-size] = slab_atoms.positions

        atoms = self.change(atoms, fro='orth_surface', to='orth_system')

        if self.cluster().relaxation() is not None:
            atoms.positions[-size:] = cluster_atoms.positions

        return atoms

    def makeAtomsRelaxed(self, atoms, tolerance=1.5):
        atoms = self.makeSubsystemsRelaxed(atoms, tolerance=tolerance)
        relaxation = self.relaxation()

        if relaxation is None:
            return atoms

        size = len(self.cluster().atoms(passivate=False))
        slab_atoms = atoms[:-size]
        relaxed_slab_atoms = relaxation[:-size]
        atoms.positions[:-size] = makeAtomsRelaxed(slab_atoms, relaxed_slab_atoms, tolerance=tolerance).positions
        atoms.positions[-size:] = relaxation[-size:].positions

        return atoms


    def centers(self):
        atoms = self.cluster().atoms()
        positions = atoms.get_positions()
        atoms.rotate([0, 0, 1], a = pi/2.)
        more_positions = atoms.get_positions()

        return np.concatenate((positions, more_positions))

    def clusterAtomsTemplate(self, parameters, constrained=True):
        atoms = self.cluster().atomsTemplate(parameters=parameters, constrained=constrained, passivate=False)

        translation = self.displacement()
        atoms.translate(translation)

        return atoms

    def removedAtoms(self):
        r_atoms = self.slab().removedAtoms(coordinates='orth_surface')
        r_atoms = self.change(r_atoms, fro='orth_surface', to='orth_system')

        return r_atoms

    def pbc(self):
        return (True, True, False)

    def frame(self, data=None, border=3, direction='-z', **kwargs):
        return self.slab().frame(data=data, border=border, direction=direction, **kwargs)

    def plotUnitCell(self, data=None, ax=None, direction='-z'):
        if data == None:
            data = self.read()

        return self.slab().plotUnitCell(data=data, ax=ax, direction=direction)

    def _findAtoms(self, identifiers):
        found_atoms, coordinates = self.slab()._findAtoms(identifiers)
        found_atoms = self.change(found_atoms, to=self.COORDINATES, fro=coordinates)

        return found_atoms, self.COORDINATES

    def jpg_save(
            self,
            filename,
            data=None,
            frame=None,
            border=API.DEFAULT,
            direction='-z',
            **kwargs):

        if data is None:
            data = self.read()
        if frame is None:
            frame = self.frame(data=data, border=border, direction=direction)

        return self.slab().jpg_save(
            filename=filename,
            data=data,
            frame=frame,
            border=border,
            direction=direction,
            **kwargs,
            )

