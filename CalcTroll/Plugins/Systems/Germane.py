from ase.atoms import Atom
from CalcTroll.ASEUtils.Atoms import Atoms
from CalcTroll.Plugins.Systems.Molecule import Molecule
from CalcTroll import API

class Germane(Molecule):
    def __init__(
            self,
            name=API.DEFAULT,
        ):
        atoms = Atoms([
                       Atom('Ge', ( -0.0000 , -0.0000 ,  0.0000)),
                       Atom( 'H', (  1.5300 , -0.0000 ,  0.0000)),
                       Atom( 'H', ( -0.5100 ,  1.4425 ,  0.0000)),
                       Atom( 'H', ( -0.5100 , -0.7212 ,  1.2492)),
                       Atom( 'H', ( -0.5100 , -0.7212 , -1.2492)),
                       ])

        Molecule.__init__(
            self,
            atoms,
            name=name,
        )
