from CalcTroll.API import DEFAULT

from .PolyaromaticHydroCarbon import PolyaromaticHydroCarbon


class TriCycle(PolyaromaticHydroCarbon):
    def __init__(self, name=DEFAULT, parameters=DEFAULT):
        indices = [
            (3, 4),
            (1,  3), (2,  3), (3, 3), (4, 3),
            (-1,  2), (2,  2), (3, 2),
            (-3, 1), (-2,  1), (1, 1), (3, 1),
            (-3, 0), (-2, -0),  (-1, 0), (0, 0),
            (-4, -1), (-3, -1), (0, -1), (2, -1),
            (-3, -2), (0, -2), (1, -2),
            (-2, -3), (-1, -3), (0,  -3), (1, -3),
            (-1, -4),
        ]

        folds = \
            (1, 2, 15), (5, 9, 15), \
            (11, 7, 15), (19, 22, 15), \
            (20, 17, 15), (23, 24, 15),
        PolyaromaticHydroCarbon.__init__(
            self,
            indices,
            name=name,
            folds=folds,
            parameters=parameters,
        )
