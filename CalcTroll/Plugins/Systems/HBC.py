from ase.atoms import Atom
from ase.atoms import Atoms as ASEAtoms
import numpy as np
from ase.visualize import view
from ase.constraints import Hookean

from CalcTroll.ASEUtils.Atoms import Atoms
from CalcTroll.ASEUtils.AtomUtilities import removeCopies
from CalcTroll.Plugins.Systems.Molecule import Molecule

from CalcTroll import API
from CalcTroll.API import DEFAULT

from .PolyaromaticHydroCarbon import PolyaromaticHydroCarbon, CarbonRingChain, CarbonRingChain2
from .ConjugatedMolecules import triplicateEndGroup

class H2HBC(Molecule):
    def __init__(
            self,
            name=DEFAULT,
            parameters=DEFAULT,
    ):
        indices = [
            (1,2),
            (-1,1), ( 1,1), (2, 1),
            (-1,0), ( 0,0), ( 1,0),
            (-2,-1), (-1, -1), ( 0,-1), (1, -1),
            (-1, -2),
        ]
        folds=(
            (0, 2,-45),
            (1, 4, 45),
               )
        restoring_force = 0.005

        atoms = PolyaromaticHydroCarbon(
            indices,
            name=name,
            folds=folds,
            parameters=parameters,
        ).atoms()
        constraints = []
        for i, atom in enumerate(atoms):
            k = restoring_force*atom.mass
            constraint = Hookean(a1=i, a2=(0, 0,-1, 0),  k=k)
            constraints.append(constraint)

        atoms.set_constraint(constraints)

        Molecule.__init__(
            self,
            atoms,
            name=name,
            parameters=parameters,
        )


class HBC(Molecule):
    def __init__(
            self,
            name=DEFAULT,
            parameters=DEFAULT,
        ):
        indices = [
            (1,2),
            (-1,1), ( 1,1), (2, 1),
            (-1,0), ( 0,0), ( 1,0),
            (-2,-1), (-1, -1), ( 0,-1), (1, -1),
            (-1, -2),
        ]
        atoms = PolyaromaticHydroCarbon(
            indices,
            name=name,
            parameters=parameters,
        ).atoms()

        Molecule.__init__(
            self,
            atoms,
            name=name,
            parameters=parameters,
        )

class P1Closed(Molecule):
    def __init__(self, name=DEFAULT, parameters=DEFAULT):
       atoms = Atoms([
         Atom('C',  (4.6801 ,  -2.5916  , -1.3567  )),
         Atom('C',  (3.5684 ,  -2.5777  , -0.5653  )),
         Atom('C',  (2.6396 ,  -1.6719  , -0.7795  )),
         Atom('C',  (2.6905 ,  -0.7551  , -1.7788  )),
         Atom('C',  (3.7445 ,  -0.8436  , -2.5975  )),
         Atom('C',  (4.7677 ,  -1.7281  , -2.3934  )),
         Atom('C',  (1.6983 ,   0.0839  , -2.0176  )),
         Atom('C',  (1.8684 ,   1.3379  , -2.5158  )),
         Atom('C',  (0.8412 ,   2.2574  , -2.6320  )),
         Atom('C',  (-0.3927,    1.8478 ,  -2.1621 )),
         Atom('C',  (-0.6602,    0.5847 ,  -1.8303 )),
         Atom('C',  (0.4233 ,  -0.2827  , -1.8033  )),
         Atom('C',  (-1.9680,    0.2356 ,  -1.6582 )),
         Atom('C',  (-2.3331,   -0.9815 ,  -2.1750 )),
         Atom('C',  (-3.6384,   -1.3584 ,  -2.2790 )),
         Atom('C',  (-4.5463,   -0.5362 ,  -1.7587 )),
         Atom('C',  (-4.2186,    0.5810 ,  -1.1233 )),
         Atom('C',  (-2.9683,    0.9900 ,  -1.1525 )),
         Atom('C',  (-1.7526,    2.4549 ,   0.4411 )),
         Atom('C',  (-2.7906,    2.1887 ,  -0.4345 )),
         Atom('C',  (-3.5769,    3.2009 ,  -0.8022 )),
         Atom('C',  (-3.4256,    4.4079 ,  -0.1886 )),
         Atom('C',  (-2.4669,    4.6195 ,   0.7163 )),
         Atom('C',  (-1.6604,    3.6614 ,   1.0576 )),
         Atom('C',  (-0.6968,   -0.7958 ,   1.7390 )),
         Atom('C',  (-1.4072,    0.2667 ,   1.1891 )),
         Atom('C',  (-0.9199,    1.5127 ,   0.9365 )),
         Atom('C',  (0.3652 ,   1.7675  ,  1.2152  )),
         Atom('C',  (1.0970 ,   0.7502  ,  1.7139  )),
         Atom('C',  (0.5685 ,  -0.4458  ,  2.0910  )),
         Atom('C',  (-2.2131,   -4.6375 ,   2.5073 )),
         Atom('C',  (-1.1122,   -4.2029 ,   3.0839 )),
         Atom('C',  (-0.6899,   -2.9096 ,   2.9386 )),
         Atom('C',  (-1.2800,   -2.0325 ,   2.0306 )),
         Atom('C',  (-2.3319,   -2.6027 ,   1.3841 )),
         Atom('C',  (-2.8634,   -3.8084 ,   1.6568 )),
         Atom('F',  (3.7576 ,  -0.1765  , -3.7620  )),
         Atom('F',  (0.1446 ,  -1.5017  , -1.3072  )),
         Atom('F',  (-2.7020,   -0.0102 ,   0.9963 )),
         Atom('F',  (0.3746 ,  -2.6536  ,  3.6884  )),
         Atom('H',  (5.4372 ,  -3.3523  , -1.3343  )),
         Atom('H',  (3.4540 ,  -3.3624  ,  0.2408  )),
         Atom('H',  (1.8514 ,  -1.7488  , -0.0171  )),
         Atom('H',  (5.5883 ,  -1.7023  , -3.2320  )),
         Atom('H',  (2.8565 ,   1.7494  , -2.7269  )),
         Atom('H',  (0.9346 ,   3.3308  , -2.8572  )),
         Atom('H',  (-1.1935,    2.6301 ,  -2.1709 )),
         Atom('H',  (-1.5245,   -1.6565 ,  -2.6394 )),
         Atom('H',  (-3.8815,   -2.1786 ,  -2.8460 )),
         Atom('H',  (-5.5883,   -0.8055 ,  -1.8616 )),
         Atom('H',  (-5.0403,    1.0543 ,  -0.5707 )),
         Atom('H',  (-4.4099,    3.2139 ,  -1.5762 )),
         Atom('H',  (-4.0962,    5.3143 ,  -0.4473 )),
         Atom('H',  (-2.4319,    5.6841 ,   1.1543 )),
         Atom('H',  (-0.8925,    3.8765 ,   1.8200 )),
         Atom('H',  (0.7988 ,   2.6870  ,  0.8651  )),
         Atom('H',  (2.2685 ,   0.7250  ,  1.7577  )),
         Atom('H',  (1.2731 ,  -1.2031  ,  2.3576  )),
         Atom('H',  (-2.4271,   -5.6841 ,   2.7800 )),
         Atom('H',  (-0.5672,   -4.8893 ,   3.7620 )),
         Atom('H',  (-2.7761,   -2.1218 ,   0.5178 )),
         Atom('H',  (-3.6328,   -4.3159 ,   0.9937 )),
       ])
       Molecule.__init__(self, atoms, name=name, parameters=parameters)


class DBPP(PolyaromaticHydroCarbon):
    def __init__(self, name=DEFAULT, parameters=DEFAULT):
        indices = [
            (0, 0),
            (-1, 1), (0, 1), (1, 1), (2, 1),
            (1, 2), (2, 2), (3, 2), (4, 2),
            (3, 3),
            ]

        PolyaromaticHydroCarbon.__init__(
            self,
            indices=indices,
            name=name,
            parameters=parameters,
        )

class P1Open(Molecule):
    def __init__(self, name=DEFAULT, parameters=DEFAULT):
        atoms = Atoms([
           Atom(position=( 6.1892,   -5.8901,   -1.7970), symbol='C'),
           Atom(position=( 4.9486,   -6.3518,   -1.9924), symbol='C'),
           Atom(position=( 3.9188,   -5.4945,   -2.0693), symbol='C'),
           Atom(position=( 4.0612,   -4.1554,   -1.9416), symbol='C'),
           Atom(position=( 5.3301,   -3.7170,   -1.7640), symbol='C'),
           Atom(position=( 6.3685,   -4.5674,   -1.6965), symbol='C'),
           Atom(position=( 2.9772,   -3.3376,   -2.0118), symbol='C'),
           Atom(position=( 1.9182,   -3.6872,   -2.7726), symbol='C'),
           Atom(position=( 0.8365,   -2.9070,   -2.8933), symbol='C'),
           Atom(position=( 0.7759,   -1.7464,   -2.2312), symbol='C'),
           Atom(position=( 1.7818,   -1.3637,   -1.4202), symbol='C'),
           Atom(position=( 2.8698,   -2.1646,   -1.3424), symbol='C'),
           Atom(position=( 1.6730,   -0.1658,   -0.7899), symbol='C'),
           Atom(position=( 2.7707,    0.6084,   -0.9578), symbol='C'),
           Atom(position=( 2.7651,    1.9275,   -0.7366), symbol='C'),
           Atom(position=( 1.5961,    2.4913,   -0.4293), symbol='C'),
           Atom(position=( 0.5282,    1.7093,   -0.2215), symbol='C'),
           Atom(position=( 0.5378,    0.3550,   -0.2316), symbol='C'),
           Atom(position=(-1.6788,    0.1654,    0.7897), symbol='C'),
           Atom(position=(-0.5447,   -0.3562,    0.2297), symbol='C'),
           Atom(position=(-0.5367,   -1.7105,    0.2182), symbol='C'),
           Atom(position=(-1.6054,   -2.4914,    0.4260), symbol='C'),
           Atom(position=(-2.7735,   -1.9265,    0.7348), symbol='C'),
           Atom(position=(-2.7773,   -0.6077,    0.9577), symbol='C'),
           Atom(position=(-2.9763,    3.3364,    2.0214), symbol='C'),
           Atom(position=(-2.8721,    2.1645,    1.3493), symbol='C'),
           Atom(position=(-1.7847,    1.3623,    1.4224), symbol='C'),
           Atom(position=(-0.7760,    1.7429,    2.2310), symbol='C'),
           Atom(position=(-0.8333,    2.9022,    2.8953), symbol='C'),
           Atom(position=(-1.9147,    3.6837,    2.7795), symbol='C'),
           Atom(position=(-6.1872,    5.8916,    1.8227), symbol='C'),
           Atom(position=(-6.3678,    4.5693,    1.7201), symbol='C'),
           Atom(position=(-5.3297,    3.7180,    1.7823), symbol='C'),
           Atom(position=(-4.0599,    4.1552,    1.9564), symbol='C'),
           Atom(position=(-3.9161,    5.4939,    2.0862), symbol='C'),
           Atom(position=(-4.9456,    6.3520,    2.0146), symbol='C'),
           Atom(position=( 5.6412,   -2.4303,   -1.7283), symbol='F'),
           Atom(position=( 3.8016,   -1.8304,   -0.4610), symbol='F'),
           Atom(position=(-3.8069,    1.8333,    0.4701), symbol='F'),
           Atom(position=(-5.6418,    2.4316,    1.7451), symbol='F'),
           Atom(position=( 7.0452,   -6.5822,   -1.7370), symbol='H'),
           Atom(position=( 4.7782,   -7.4385,   -2.0781), symbol='H'),
           Atom(position=( 2.9314,   -5.9704,   -2.1884), symbol='H'),
           Atom(position=( 7.3918,   -4.1742,   -1.5707), symbol='H'),
           Atom(position=( 1.9069,   -4.6000,   -3.3904), symbol='H'),
           Atom(position=( 0.0090,   -3.2023,   -3.5610), symbol='H'),
           Atom(position=(-0.1108,   -1.1095,   -2.3956), symbol='H'),
           Atom(position=( 3.7012,    0.2421,   -1.4252), symbol='H'),
           Atom(position=( 3.6513,    2.5505,   -0.9426), symbol='H'),
           Atom(position=( 1.5157,    3.5902,   -0.3688), symbol='H'),
           Atom(position=(-0.3790,    2.2979,   -0.0365), symbol='H'),
           Atom(position=( 0.3696,   -2.3001,    0.0320), symbol='H'),
           Atom(position=(-1.5264,   -3.5904,    0.3643), symbol='H'),
           Atom(position=(-3.6602,   -2.5488,    0.9409), symbol='H'),
           Atom(position=(-3.7069,   -0.2412,    1.4267), symbol='H'),
           Atom(position=( 0.1105,    1.1049,    2.3918), symbol='H'),
           Atom(position=(-0.0035,    3.1955,    3.5610), symbol='H'),
           Atom(position=(-1.9007,    4.5953,    3.3990), symbol='H'),
           Atom(position=(-7.0429,    6.5845,    1.7671), symbol='H'),
           Atom(position=(-7.3918,    4.1770,    1.5972), symbol='H'),
           Atom(position=(-2.9280,    5.9689,    2.2029), symbol='H'),
           Atom(position=(-4.7741,    7.4385,    2.1019), symbol='H'),
        ])
        Molecule.__init__(self, atoms, name=name, parameters=parameters)

class P2Open(Molecule):
    def __init__(
            self,
            name=DEFAULT,
            parameters=DEFAULT,
        ):
        indices = [
            (-3, -3),
            (-2, -1),
            (0, 0),
            (1, 2),
            (0, 3),
            (1, 5),
            (2, 4),
        ]

        flourinate = (0, 3), (1, 4), (2, 1), (3, 0), (4, 3)
        raise_rings = {
            5: 0.9,
            6: 1.8,
        }
        atoms = CarbonRingChain(
            indices,
            name=name,
            flourinate=flourinate,
            raise_rings=raise_rings,
            parameters=parameters,
        ).atoms()

        Molecule.__init__(
            self,
            atoms,
            name=name,
            parameters=parameters,
        )


class P2Closed(CarbonRingChain):
    def __init__(self, name=DEFAULT, parameters=DEFAULT):
        indices = [
            (-3, 0),
            (-2, -1),
            (0, 0),
            (1, 2),
            (0, 3),
            (-2, 2),
            (-1, 1)
        ]

        flourinate = [
            (0, 3),
            (1, 2),
            (2, 1),
            (3, 0),
            (4, 5),
        ]
        raise_rings = {
            3: 0.2,
            4: 0.4,
            5: 1.3,
            6: 2.1,
        }
        CarbonRingChain.__init__(
            self,
            indices,
            name=name,
            raise_rings=raise_rings,
            flourinate=flourinate,
            parameters=parameters,
        )
