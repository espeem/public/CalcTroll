# Written by Mads Engelund, 2017, http://espeem.com
import unittest

from CalcTroll.Core.Test.Case import CalcTrollTestCase
from CalcTroll.API import view

from .TitaniumDiOxide011 import TitaniumDiOxide011

class TitaniumDiOxide011Test(CalcTrollTestCase):
    def testConstruction(self):
        surface = TitaniumDiOxide011()

        atoms = surface.atoms()

if __name__=='__main__':
    unittest.main()
