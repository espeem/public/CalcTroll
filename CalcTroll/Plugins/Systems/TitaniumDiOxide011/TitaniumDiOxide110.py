# Written by Mads Engelund, 2018, http://espeem.com
import numpy as np

from CalcTroll.Plugins.Systems.Surface.Surface3D import Surface3D
from CalcTroll.Plugins.Systems.Crystal import TitaniumDiOxide
from CalcTroll.ASEUtils.Atoms import Atoms
from CalcTroll import API
from CalcTroll.API import DEFAULT

class TitaniumDiOxide110(Surface3D):
    def __init__(
            self,
            parameters=DEFAULT,
            ):
        crystal = TitaniumDiOxide()
        surface = Surface3D(
            crystal=crystal,
            miller_indices=(1, 1, 0),
            surface_cell=[[1,0], [0, 1]],
        )


        p = np.array([[0.5, 0.0, 0.0]])
        p = surface.change(p, fro='minimal_unit_cell',
                                   to='orth_surface')
        p[0] += np.array([0, 0, 1.0])
        p = surface.change(p, to='minimal_unit_cell',
                                   fro='orth_surface')

        extra_atoms = Atoms('O', p)

        Surface3D.__init__(self,
                           crystal=surface['crystal'],
                           miller_indices=surface['miller_indices'],
                           surface_cell=surface['surface_cell'],
                           extra_atoms=extra_atoms,
                           parameters=parameters,
                           )


