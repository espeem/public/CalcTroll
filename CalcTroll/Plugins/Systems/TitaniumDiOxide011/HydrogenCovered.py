# Written by Mads Engelund, 2018, http://espeem.com
import numpy as np

from CalcTroll.Plugins.Systems.Surface.Surface3D import Surface3D
from CalcTroll.Plugins.Systems.Crystal import TitaniumDiOxide
from CalcTroll.ASEUtils.Atoms import Atoms
from CalcTroll import API
from CalcTroll.API import DEFAULT

class HydrogenCovered(Surface3D):
    def __init__(
            self,
            parameters=DEFAULT,
            ):
        crystal = TitaniumDiOxide()
        surface = Surface3D(
            crystal=crystal,
            miller_indices=(0, 1, 1),
            surface_cell=[[1,0], [0, 2]],
        )

        p = np.array([
            (0.25, 0.25 , 0),
            (0.75, 0.25 , 0),
            (0.25, 1.25 , 0),
            (0.75, 1.25 , 0),
            (0.125, 1.5 , 0),
            (0.675, 1.0 , 0),
            (0.0,-0.25 , 0),
            (0.5,-1.25 , 0),
            (0.0,-0.25 , 0),
            (0.5,-1.25 , 0),
        ])
        p = surface.change(p, fro='minimal_unit_cell',
                           to='orth_surface')
        p[0] += np.array([0, 0, 1])
        p[1] += np.array([0, 0, 1])
        p[2] += np.array([0, 0, 1])
        p[3] += np.array([0, 0, 1])
        p[4] += np.array([0, 0, 1.7])
        p[5] += np.array([0, 0, 1.7])
        p[6] += np.array([0, 0, 2])
        p[7] += np.array([0, 0, 2])
        p[8] += np.array([0, 0, 2.7])
        p[9] += np.array([0, 0, 2.7])
        p = surface.change(p, to='minimal_unit_cell',
                           fro='orth_surface')

        extra_atoms = Atoms('O'*(len(p)-6) + 'H2O2H2', p)

        Surface3D.__init__(self,
                           crystal=crystal,
                           miller_indices=(0, 1, 1),
                           extra_atoms=extra_atoms,
                           surface_cell=[[1,0], [0, 2]],
                           parameters=parameters,
                           )


