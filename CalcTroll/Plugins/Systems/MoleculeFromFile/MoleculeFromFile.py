import os
from ase import io
from CalcTroll.Plugins.Systems.InputToAtoms import InputToAtoms
from CalcTroll.Core.Flags import DEFAULT
from CalcTroll.ASEUtils.RegularizeMolecule import regularizeMolecularDirections

class MoleculeFromFile(InputToAtoms):
    def __init__(self,
                 filename,
                 regularize=True,
                 ):
        assert os.path.exists(filename)
        self.__regularize = regularize

        InputToAtoms.__init__(
                self,
                single_input=filename,
                )

    def generateAtoms(self):
        atoms = io.read(self.singleInput())

        if self.__regularize:
            atoms = regularizeMolecularDirections(atoms)

        return atoms
