from ase.atoms import Atom
from CalcTroll.ASEUtils.Atoms import Atoms
from CalcTroll.Plugins.Systems.Molecule import Molecule
from CalcTroll import API

class C60(Molecule):
    def __init__(
            self,
            name=API.DEFAULT,
    ):
        atoms = Atoms([
                       Atom('C', ( -3.22798000,-0.34813000,-0.56484000)),
                       Atom('C', ( -3.22792000, 0.35057000, 0.56703000)),
                       Atom('C', ( -2.81726000,-1.61451000,-0.56589000)),
                       Atom('C', ( -2.81549000,-0.21643000, 1.69873000)),
                       Atom('C', ( -2.81778000, 0.21755000,-1.69478000)),
                       Atom('C', ( -2.81720000, 1.61564000, 0.56718000)),
                       Atom('C', ( -2.40533000,-2.18058000, 0.56515000)),
                       Atom('C', ( -2.40516000,-1.48164000, 1.69814000)),
                       Atom('C', ( -2.40711000, 1.48255000,-1.69452000)),
                       Atom('C', ( -2.40720000, 2.18121000,-0.56284000)),
                       Atom('C', ( -2.15294000,-1.83060000,-1.69770000)),
                       Atom('C', ( -2.15314000,-0.69854000,-2.39573000)),
                       Atom('C', ( -2.15084000, 0.69741000, 2.39737000)),
                       Atom('C', ( -2.15140000, 1.83037000, 1.69837000)),
                       Atom('C', ( -1.33063000,-2.96160000, 0.56425000)),
                       Atom('C', ( -1.32911000,-1.83169000, 2.39736000)),
                       Atom('C', ( -1.33004000, 1.83242000,-2.39437000)),
                       Atom('C', ( -1.33071000, 2.96240000,-0.56311000)),
                       Atom('C', ( -1.07833000,-2.61166000,-1.69858000)),
                       Atom('C', ( -1.07710000,-0.34857000,-3.09486000)),
                       Atom('C', ( -1.07472000, 0.34733000, 3.09644000)),
                       Atom('C', ( -1.07505000, 2.61165000, 1.69809000)),
                       Atom('C', ( -0.66633000,-3.17774000,-0.56769000)),
                       Atom('C', ( -0.66466000,-2.74643000, 1.69662000)),
                       Atom('C', ( -0.66448000,-0.91791000, 3.09587000)),
                       Atom('C', ( -0.66561000, 0.91757000,-3.09492000)),
                       Atom('C', ( -0.66534000, 2.74613000,-1.69569000)),
                       Atom('C', ( -0.66497000, 3.17725000, 0.56804000)),
                       Atom('C', ( -0.00227000,-2.26165000,-2.39757000)),
                       Atom('C', ( -0.00240000,-1.12960000,-3.09576000)),
                       Atom('C', (  0.00240000, 1.12956000, 3.09577000)),
                       Atom('C', (  0.00226000, 2.26169000, 2.39763000)),
                       Atom('C', (  0.66497000,-3.17729000,-0.56804000)),
                       Atom('C', (  0.66534000,-2.74617000, 1.69570000)),
                       Atom('C', (  0.66561000,-0.91761000, 3.09493000)),
                       Atom('C', (  0.66445000, 0.91790000,-3.09595000)),
                       Atom('C', (  0.66463000, 2.74643000,-1.69670000)),
                       Atom('C', (  0.66630000, 3.17774000, 0.56761000)),
                       Atom('C', (  1.07506000,-2.61170000,-1.69808000)),
                       Atom('C', (  1.07481000,-0.34734000,-3.09646000)),
                       Atom('C', (  1.07710000, 0.34853000, 3.09487000)),
                       Atom('C', (  1.07829000, 2.61165000, 1.69850000)),
                       Atom('C', (  1.33069000,-2.96235000, 0.56316000)),
                       Atom('C', (  1.33004000,-1.83246000, 2.39438000)),
                       Atom('C', (  1.32910000, 1.83173000,-2.39731000)),
                       Atom('C', (  1.33063000, 2.96156000,-0.56425000)),
                       Atom('C', (  2.15150000,-1.83038000,-1.69839000)),
                       Atom('C', (  2.15082000,-0.69736000,-2.39732000)),
                       Atom('C', (  2.15314000, 0.69849000, 2.39574000)),
                       Atom('C', (  2.15294000, 1.83056000, 1.69771000)),
                       Atom('C', (  2.40717000,-2.18121000, 0.56276000)),
                       Atom('C', (  2.40710000,-1.48250000, 1.69457000)),
                       Atom('C', (  2.40516000, 1.48159000,-1.69813000)),
                       Atom('C', (  2.40534000, 2.18053000,-0.56515000)),
                       Atom('C', (  2.81727000,-1.61557000,-0.56715000)),
                       Atom('C', (  2.81782000,-0.21744000, 1.69472000)),
                       Atom('C', (  2.81547000, 0.21647000,-1.69867000)),
                       Atom('C', (  2.81726000, 1.61447000, 0.56590000)),
                       Atom('C', (  3.22791000,-0.35053000,-0.56697000)),
                       Atom('C', (  3.22793000, 0.34821000, 0.56481000)),
                       ])

        Molecule.__init__(
            self,
            atoms,
            name=name,
        )
