# Written by Mads Engelund, 2019, http://espeem.com
import unittest
import numpy
from ase.visualize import view
from CalcTroll.Core.Test.Case import CalcTrollTestCase

from CalcTroll.Plugins.Systems.Rotated import *

atoms = Atoms(
    [
        Atom('N', ( 0.0 , 1.6 ,0.0,)),
        Atom('C', ( 1.0 , 2.4 ,0.0,)),
        Atom('C', (-1.0 , 2.4 ,0.0,)),
        Atom('C', ( 0.7 , 3.6 ,0.0,)),
        Atom('C', (-0.7 , 3.6 ,0.0,)),
    ],
)

class RotatedTest(CalcTrollTestCase):

    def testConstruction(self):
        molecule = Molecule(atoms)
        rotated = Rotated(molecule, angle=90)

        test_atoms = atoms.copy()
        test_atoms.rotate(a=90, v='z')

        self.assertIsInstance(rotated, Rotated)
        self.assertAtomsEqual(rotated.atoms(), test_atoms)


if __name__=='__main__':
    unittest.main()
