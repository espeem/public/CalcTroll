# Written by Mads Engelund, 2017, http://espeem.com
import os
from os.path import join
import numpy as np
from numpy.linalg import norm

from CalcTroll import API

class LengthScales(API.Parameters):
    def __init__(self,
                 pertubation_length=3,
                 short_correlation_length=6,
                 correlation_length=30,
                 depth=None,
                 ):
        assert pertubation_length <= short_correlation_length
        assert short_correlation_length <= correlation_length

    def depth(self):
        depth = self['depth']
        if depth is None:
            depth = self['short_correlation_length']

        return depth

    def setKpts(self, atoms, correlation_length=None):
        if correlation_length is None:
            correlation_length = self.correlationLength()
        kpts=np.ones(3, dtype=np.int)
        for i, vector in enumerate(atoms.cell):
            if atoms.pbc[i]:
                length=np.max(abs(vector))
                number=int(np.ceil(correlation_length/length))
            else:
                number = 1
            kpts[i] = number

        return kpts
