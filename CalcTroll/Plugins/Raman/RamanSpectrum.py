import numpy as np
from ase.units import Ry, eV, invcm

from CalcTroll.Core.Analysis.Spectrum import Spectrum
from CalcTroll.Core.Plugin.Types import AnalysisProvider
from CalcTroll.Core.Task import AbstractHasTasks
from CalcTroll import API
from CalcTroll.API import DEFAULT
from CalcTroll.Plugins.LengthScales import LengthScales
from CalcTroll.Plugins.Programs.Siesta import Siesta

from .Siesta import RamanSiesta

DEFAULT_WIDTH = 0.01 * eV

def adjustFreqUnits(freqs, freq_unit='eV'):
    freqs = np.array(freqs, float)
    if freq_unit == 'eV':
        pass
    elif freq_unit == 'meV':
        freqs *= 1000
    elif freq_unit == 'cm-1':
        freqs /= invcm
    else:
        raise Exception

    return freqs


class RamanSpectrum(Spectrum):
    def findImplementation(self):
        assert isinstance(self.method(), Siesta)
        return RamanSiesta([self])

    def __init__(
            self,
            system=None,
            method=API.DEFAULT,
            parameters=API.DEFAULT,
            ):

        if method is API.DEFAULT:
            method = Siesta(
                        mesh_cutoff=200*Ry,
                        basis_set="DZP",
                        xc='PBE',
                        energy_shift=0.10*eV,
                        fdf_arguments={
                            'PAO.BasisType': 'split',
                            'PAO.SoftDefault': True,
                            },
                        )


        Spectrum.__init__(
                self,
                system=system,
                method=method,
                parameters=parameters,
                inputs=[],
                )

    def txt_save(self, filename, data, freq_unit='eV', **kwargs):
        freqs, ints = data
        freqs = adjustFreqUnits(freqs, freq_unit)

        with open(filename, 'w') as f:
            f.write('%10s\t%10s\n' % ('freq', 'intensity'))
            f.write('%10s\t%10s\n' % (freq_unit, 'A^4*amu-1'))
            for freq, intensity in zip(freqs, ints):
                f.write('%10.3f\t%10.3f\n' % (freq, intensity))

    def plot(
            self,
            data,
            ax=None,
            width=DEFAULT,
            freq_unit='eV',
            **kwargs):

        if width is DEFAULT:
            width = adjustFreqUnits([DEFAULT_WIDTH], freq_unit)[0]
        x, y = self.generateCurve(data, width, freq_unit)

        frame = self.frame(data, width=width, freq_unit=freq_unit)
        label = {'eV': '$eV$', 'meV': '$meV$', 'cm-1': '$cm^{-1}$'}[freq_unit]

        ax.plot(x, y, **kwargs)

        ax.set_ylabel('$A^{4}/amu$')
        #ax.set_xlim(*frame[0])
        #ax.set_ylim(*frame[1])

        return ax

    def frame(self, data, width=DEFAULT, freq_unit='eV'):
        if width is DEFAULT:
            width = adjustFreqUnits([DEFAULT_WIDTH], freq_unit)[0]
        x, y = self.generateCurve(data, width, freq_unit)

        return [[0, x.max()], [0, y.max()*1.1]]

    def generateCurve(self, data, width, freq_unit):
        df = width/100.
        from scipy.stats import norm
        freqs, ints = data
        freqs = adjustFreqUnits(freqs, freq_unit)

        m = 0
        for freq, intensity in zip(freqs, ints):
            m = max(m, norm.ppf(0.99999, freq, width))

        x = np.arange(0, m, df)
        y = np.zeros(len(x))
        for freq, intensity in zip(freqs, ints):
            y += intensity * norm.pdf(x, freq, width)

        return x, y

    def jpg_save(self, filename, data, **kwargs):
        from matplotlib import pylab as plt
        fig = plt.figure()
        ax = fig.gca()
        self.plot(data=data, ax=ax, **kwargs)
        plt.savefig(filename)
        plt.close('all')


class RamanSpectrumCollection(AnalysisProvider, AbstractHasTasks):
    def __init__(self, systems, method):
        inputs = [RamanSpectrum(system=system, method=method) for system in systems]

        super(AbstractHasTasks, self).__init__(inputs=inputs)

    def read(self, **kwargs):
        data = [i.read() for i in self.inputs()]

        return data

    def frame(self, data=None, width=DEFAULT, **kwargs):
        inputs = self.inputs()
        f = [[np.inf, -np.inf], [np.inf, -np.inf]]
        for i, inp in enumerate(self.inputs()):
            nf = inp.frame(data[i], width=width, **kwargs)
            f = [
                     [min(f[0][0], nf[0][0]),
                      max(f[0][1], nf[0][1]),
                     ],
                     [min(f[1][0], nf[1][0]),
                      max(f[1][1], nf[1][1]),
                     ],
                ]

        return f

    def jpg_save(self, filename, data, frame=None, width=DEFAULT, **kwargs):
        from matplotlib import pylab as plt
        fig = plt.figure()
        ax = fig.gca()
        for i, d in zip(self.inputs(), data):
            i.plot(data=d, ax=ax, label=i.system().name(), width=width, **kwargs)
        if frame is None:
            frame = self.frame(data, width=width, **kwargs)
        ax.set_xlim(*frame[0])
        ax.set_ylim(*frame[1])
        ax.legend()
        plt.savefig(filename)
        plt.close('all')

    def tasks(self):
        return []
