from CalcTroll.Interface import *

molecule = Acene(1)
method = Siesta(
            mesh_cutoff=200*Ry,
            basis_set="DZP",
            xc='PBE',
            spin='non-polarized',
            energy_shift=0.10*eV,
            fdf_arguments={
                'PAO.BasisType': 'split',
                'PAO.SoftDefault': True,
                },
            )

spectrum = RamanSpectrum(
        system=molecule,
        method=method,
        )
save(spectrum, 'raman.txt')
save(spectrum, 'raman.jpg')
