import os
from os.path import join
import numpy as np
from ase.units import eV
from CalcTroll import API
from CalcTroll.Plugins.Programs.Siesta import Siesta
from CalcTroll.Plugins.Programs.Siesta.Tasks import ASESiestaElectronicStructure


class RamanSiesta(API.AnalysisImplementation):

    def makeTasks(self, items):
        system = items[0].system()
        method = items[0].method()
        parameters = items[0].parameters()
        task1 = CalculateRaman(
                    system=system,
                    method=method,
                    parameters=parameters,
                    )

        return [task1]

    def read(self, item, **kwargs):
        return item.tasks()[0].read()


class CalculateRaman(ASESiestaElectronicStructure):
    def __init__(
            self,
            system,
            method,
            parameters=API.DEFAULT,
            ):
        fdf_arguments = {
            'MinSCFIterations': 1,
            'MaxSCFIterations': 300,
            'DM.MixingWeight': 0.01,
            'DM.KickMixingWeight': 0.1,
            'DM.NumberPulay': 4,
            'DM.NumberKick': 50,
            'DM.MixSCF1': True,
            'DM.UseSaveDM': True,
            'SCFMustConverge': False,
            'COOP.Write': True,
            'WriteDenchar': True,
            'SCF.DM.Tolerance': 0.0001,
            'XML.Write': True,
            'SCF.H.Tolerance': (0.01, 'eV'),
            'WriteMullikenPop': 1
        }
        method = method.copy(fdf_arguments=fdf_arguments)
        ASESiestaElectronicStructure.__init__(
                self,
                system=system,
                method=method,
                parameters=parameters,
                )

    def write(self):
        ASESiestaElectronicStructure.write(self)

        with open(self.runFile(), 'r') as f:
            old_script = f.read()

        script = """\
import numpy as np
from ase.calculators.siesta.siesta_lrtddft import RamanCalculatorInterface
from ase.vibrations.raman import StaticRamanCalculator
from ase.vibrations.placzek import PlaczekStatic
"""
        script += old_script

        script += """\
del atoms.constraints
atoms.set_calculator(calculator)

# Static Raman spectra
name = '%s'
omega = 10.0
rm = StaticRamanCalculator(atoms, RamanCalculatorInterface, name=name, delta=0.011,
                           exkwargs=dict(omega=omega, label="siesta", jcutoff=7, iter_broadening=0.15,
                                         xc_code='LDA,PZ', tol_loc=1e-6, tol_biloc=1e-7,
                                         krylov_options={"tol": 1.0e-4, "atol": 1.0e-4}))

# save dipole moments from DFT calculation in order to get
# infrared intensities as well
rm.ir = True
rm.run()

pz = PlaczekStatic(atoms, name=name)
e_vib = pz.get_energies()
with open('ram-summary.txt', 'w') as f:
    pz.summary(log=f)
""" % self.system().name()

        with open(self.runFile(), 'w') as f:
            f.write(script)

    def summaryFile(self, path=API.LOCAL):
        filename = 'ram-summary.txt'

        return join(self.runDirectory(path=path), filename)

    def read(self):
        summaryfile = self.summaryFile()

        with open(summaryfile, 'r') as f:
            lines = f.readlines()

        for line in lines:
            line = line.split()
            if line[0] == '#':
                multiplier = int(line[-1][1:-1].split('A^4')[0])
                break

        freqs = []
        ints = []
        for line in lines:
            line = line.split()
            try:
                int(line[0])
            except ValueError:
                continue
            freq, intensity = float(line[1]), float(line[3])
            freqs.append(freq)
            ints.append(intensity)

        freqs = np.array(freqs) * .001 * eV
        ints = np.array(ints) * multiplier

        return freqs, ints


    def _getLocalStatus(self):
        outfile = self.outFile(path=API.LOCAL)
        if not os.path.exists(outfile):
            return API.NOT_STARTED

        with open(outfile, 'r') as f:
             output = f.read()

        if 'RuntimeError:' in output:
            return API.FAILED

        summary = self.summaryFile(path=API.LOCAL)
        if not os.path.exists(summary):
            return API.FAILED

        return API.DONE
