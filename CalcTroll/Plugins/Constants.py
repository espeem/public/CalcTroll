# Written by Mads Engelund, 2017, http://espeem.com
import numpy as np
from ase.units import _hbar, _me, _e, J, s, kg, C

# Make constants in internal units
qe = _e*C
hbar = _hbar*(J*s)
me = _me*kg
