# Written by Mads Engelund, 2017, http://espeem.com
# Defines the request for the energy eigenvalues on a coarse k-grid (one sufficient for accurate forces).
from CalcTroll import API
from CalcTroll.Core.Relaxation import AbstractRelaxed, Relaxed

class EnergySpectrum(API.Analysis):
    def __init__(self,
                 system,
                 method=API.DEFAULT,
                 parameters=API.DEFAULT,
                 ):
        if system is None:
            pass
        else:
            if parameters == API.DEFAULT:
                parameters = system.defaultParameters()

            if not isinstance(system, AbstractRelaxed):
                system = Relaxed(
                    system,
                    method=method,
                    parameters=parameters,
                    )

            if method is API.DEFAULT:
                method = system.method()

        inputs = [system]
        API.Analysis.__init__(
                self,
                system=system,
                method=method,
                parameters=parameters,
                inputs=inputs,
                )


    def explain(self):
        return API.Explanation("")

    def txt_save(
            self,
            filename,
            data=None,
            **kwargs):
        lines = []
        EF, kpts, wk, levels = data
        levels = levels.flatten()
        levels.sort()
        for level in levels:
            string = "%8.2f" % level
            lines.append(string)

        with open(filename, 'w') as f:
            f.write('Fermi Level = %2.2f eV\n' % EF)
            for line in lines:
                f.write(line+'\n')
