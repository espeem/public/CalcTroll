# Written by Mads Engelund, 2020, http://espeem.com
from CalcTroll.ASEUtils.AtomUtilities import numberOfValenceElectrons
from CalcTroll.Core.Plugin.Types import AnalysisProvider

class ValenceElectrons(AnalysisProvider):
    def __init__(self, system):
        self.__system = system

    def isDone(self):
        return True

    def read(self):
        return numberOfValenceElectrons(self.__system.atoms())

    def txt_save(self, filename, data):
        with open(filename, 'w') as f:
            f.write("%d" % data)


