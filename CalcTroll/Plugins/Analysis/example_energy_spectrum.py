from CalcTroll.Interface import *

molecule = Dimer('H')
method = Siesta(xc="PBE",
                mesh_cutoff=200 * Ry,
                spin='collinear',
                basis_set="DZP",
                )

es = EnergySpectrum(method=method, system=molecule)
save(es, 'es.txt')