import numpy as np
from CalcTroll import API
from CalcTroll.Plugins.ReferenceEnergy import HOMO, LUMO, MidGapLevel
from CalcTroll.Core.Flags import CalcTrollException

class EnergyLevels(API.NoTasksAnalysis):
    def __init__(
        self,
        system=None,
        method=API.DEFAULT,
        parameters=API.DEFAULT,
        higher=HOMO(),
        lower=LUMO(),
        ):
        assert isinstance(lower, (HOMO, LUMO))
        assert isinstance(higher, (HOMO, LUMO))
        assert (higher > lower)
        if system != None:
            parameters = system.defaultParameters(parameters=parameters)
            system = API.Relaxed(system, method, parameters)

        self.__neutral = MidGapLevel(system=system, method=method, parameters=parameters)
        self.__lower = lower
        self.__higher = higher
        self._makeInputs(system, method, parameters)
        inputs = self.levels()

        API.NoTasksAnalysis.__init__(
                self,
                system=system,
                method=method,
                parameters=parameters,
                inputs=inputs,
            )

    def levels(self):
        return self.__levels

    def explain(self):
        return API.Explanation("")

    def _makeInputs(self, system, method, parameters):
        lower = self.__lower
        higher = self.__higher
        levels = [lower]
        while levels[-1].higher().script(detail_level=API.ALL) != higher.script(detail_level=API.ALL):
            levels.append(levels[-1].higher())
        levels.append(higher)

        self.__levels = [level.copy(system=system, method=method, parameters=parameters,include=False) for level in levels]

    def readData(self, **kwargs):
        neutral = self.__neutral.level()
        ens, deg = [], []
        for level in self.levels():
            ens.append(level.level() - neutral)
            deg.append(level.degeneracy())

        return ens, deg

    def txt_save(
            self,
            filename,
            data=None,
            **kwargs):
        ens, deg = data
        with open(filename, 'w') as f:
            for i, level in enumerate(self.levels()):
                f.write('%s\t%.3f eV %d\n' % (level.title(), ens[i], deg[i]))
