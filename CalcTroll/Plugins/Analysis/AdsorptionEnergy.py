# Written by Mads Engelund, 2017, http://espeem.com
from os.path import join
from CalcTroll import API

class AdsorptionEnergy(API.Analysis):
    def __init__(self,
                 system,
                 method,
                 ):
        self.__system = system
        self.__method = method
        tasks = [TotalEnergy(sub_system, method=method) for sub_system in system.subSystems()]
        tasks.append(TotalEnergy(system, method=method))
        API.HasInputs.__init__(
                self,
                inputs=system.tasks(),
                tasks=tasks,
                )
        self.__tasks = method.makeTasks(
                items=[self],
                path=join(system.mainTask().runDirectory(), 'BSSE'),
                shorthand=None,
                identifier=None,
                host=system.mainTask().host(),
                )

    def tasks(self):
        return self.__tasks

    def update(self):
        pass

    def path(self):
        return self.system().path()

    def system(self):
        return self.__system

    def method(self):
        return self.__method

    def basisSetSuperpositionError(self):
        if not self.isDone():
            return None

        tasks = self.tasks()
        shorthands = [task.shorthand() for task in tasks]
        error = 0
        error += tasks[shorthands.index('GhostSlab')].totalEnergy() - \
                tasks[shorthands.index('RemoveSlab')].totalEnergy()
        error += tasks[shorthands.index('GhostMolecule')].totalEnergy() - \
                 tasks[shorthands.index('RemoveMolecule')].totalEnergy()

        return error

    def read(self):
        if not self.isDone():
            return None

        combined = TotalEnergy(self.system(), method=self.method()).read()
        separate = 0
        for sub_system in self.system().subSystems():
            separate += TotalEnergy(sub_system, method=self.method()).read()

        combined -= self.basisSetSuperpositionError()

        return combined - separate

