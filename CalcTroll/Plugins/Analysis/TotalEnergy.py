from CalcTroll import API

class TotalEnergy(API.Analysis):
    def txt_save(self, filename, data, **kwargs):
        with open(filename, 'w') as f:
            f.write('E=%.4f eV' % data)
