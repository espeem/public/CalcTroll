from CalcTroll.Interface import *

molecule = Dimer('N')
method = Siesta()
level = HOMO(system=molecule, method=method, include=False)
print(level.read())
level = LUMO(system=molecule, method=method, include=False)
print(level.read())
