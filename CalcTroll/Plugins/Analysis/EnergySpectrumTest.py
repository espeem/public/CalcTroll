# Written by Mads Engelund, 2017, http://espeem.com
import unittest
from ase.atoms import Atoms
from CalcTroll.Core.Test.Case import CalcTrollTestCase
from CalcTroll.Core.Test.Dummies import TEST_ATOMS

from CalcTroll.Plugins.Analysis.EnergySpectrum import *
from CalcTroll.Plugins.Systems import Molecule
from CalcTroll.Plugins.Programs import Siesta

class TestCase(CalcTrollTestCase):
    def testConstruction(self):
        system = Molecule(atoms=TEST_ATOMS)
        EnergySpectrum(system=system)


if __name__=='__main__':
    unittest.main()

