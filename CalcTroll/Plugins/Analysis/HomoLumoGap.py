import numpy as np
from CalcTroll import API
from CalcTroll.Plugins.ReferenceEnergy import HOMO, LUMO, MidGapLevel
from CalcTroll.Core.Flags import CalcTrollException

class HomoLumoGap(API.NoTasksAnalysis):
    def __init__(
        self,
        system=None,
        method=API.DEFAULT,
        parameters=API.DEFAULT,
        ):
        self.__homo = HOMO(include=False, system=system, method=method, parameters=parameters)
        self.__lumo = LUMO(include=False, highest_allowed=10, system=system, method=method, parameters=parameters)

        inputs = [self.__homo, self.__lumo]

        API.NoTasksAnalysis.__init__(
                self,
                system=system,
                method=method,
                inputs=inputs,
            )

    def read(self, **kwargs):
        homo_level = self.__homo.readData(**kwargs)
        lumo_level = self.__lumo.readData(**kwargs)

        return lumo_level - homo_level
