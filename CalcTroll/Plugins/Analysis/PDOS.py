# Written by Mads Engelund, 2017, http://espeem.com
import numpy as np
from CalcTroll import API


class PDOS(API.Analysis):
    def reference(self, indices, EF):
        values, energies = self.read(target_indices=indices)
        if values.shape[1] == 2:
            values = values.sum(axis=1)
        below_fermi = energies < EF
        energies = energies[below_fermi]
        values = values[below_fermi]
        reference = (energies*values).sum()/values.sum()

        return reference

    def fermiEnergy(self):
        return self.tasks()[0].fermiEnergy()

    def timing(self):
        return self.tasks()[0].timing()

    def plot(self, data=None, ax=None, tag=None, **kwargs):
        if not self.isDone():
            return None

        #from matplotlib import pylab as plt
        atoms = self.system().atoms()
        eF = self.fermiEnergy()

        if tag != None:
            indices = atoms.tag(tag).indices()
        else:
            indices = np.arange(len(atoms))

        pdos, e = self.read(target_indices=indices)

        e -= eF

        ax.set_xlim((-5, 5))
        ax.set_ylim((0, 0.50))
        ax.set_xlabel("E/eV")
        ax.plot(e, pdos.sum(axis=1), **kwargs)

        return ax

    def jpg_save(self, filename, data=None, **kwargs):
        from matplotlib import pylab as plt
        if data is None:
            data = self.read()

        fig = plt.figure()

        ax = fig.gca()

        self.plot(data=data, ax=ax, **kwargs)

        plt.savefig(filename)
        plt.close('all')
