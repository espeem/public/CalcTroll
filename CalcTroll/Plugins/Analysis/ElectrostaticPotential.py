# Written by Mads Engelund, 2020, http://espeem.com
# Defines the request for the Hartree potential.
from CalcTroll import API

class ElectrostaticPotential(API.Analysis):
    def explain(self):
        return API.Explanation(
            "The electrostatic potential was calculated."
            )
