import numpy as np
from scipy.interpolate import RectBivariateSpline
from CalcTroll.Core.Utilities import gaussian, convolve


def smoothSTM(data, refine=2):
    image, variables = data
    X, Y = variables

    sh = np.array(image.shape)
    origin = X[0, 0], Y[0, 0]

    dx = X[1, 0] - X[0, 0]
    dy = Y[0, 1] - Y[0, 0]

    x = np.arange(sh[0])*dx
    y = np.arange(sh[1])*dy
    interp = RectBivariateSpline(x, y, image)

    x = np.arange(sh[0]*refine)*dx/refine
    y = np.arange(sh[1]*refine)*dy/refine

    image = interp(x, y)
    Y, X = np.meshgrid(y, x)
    X += origin[0]
    Y += origin[1]

    r = np.array([X, Y])

    image, r = gaussianBroaden(image, r, sigma=0.05)

    return image, r

def gaussianBroaden(image, r, sigma):

    center = np.array(image.shape, dtype=int)/2
    r0 = r[:, center[0], center[1]]
    kernel = gaussian(r, r0, sigma)
    kernel /= kernel.sum()
    result = convolve(image, kernel, center=center)

    return result.real, r
