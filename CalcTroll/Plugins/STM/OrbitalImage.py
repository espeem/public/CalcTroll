import numpy as np
from os.path import join

from CalcTroll import API
from CalcTroll.Plugins.ReferenceEnergy import HOMO, EnergyLevel, MO
from CalcTroll.Plugins.Analysis import EnergySpectrum
from CalcTroll.Core.Visualizations import padVariables, addMeasuringStick, addInfo

from .ElectronicImage import ElectronicImage
from .Probe import TersoffHamann
from .Grid.Grid import WFGrid
from .Utilities import wavefunctionTopography

class OrbitalImage(ElectronicImage):
    def __init__(
            self,
            system=None,
            method=API.DEFAULT,
            level=HOMO(),
            degeneracy_index=0,
            iso_value=API.DEFAULT,
            setpoint_current=API.DEFAULT,
            parameters=API.DEFAULT,
        ):

        inputs = []
        if isinstance(level, EnergyLevel):
            level = level.copy(
                system=system,
                method=method,
                parameters=parameters,
                )
            inputs.append(level)

        if isinstance(level, MO) and \
           level.include() is API.DEFAULT:
            level = level.copy(include=False)

        if iso_value is API.DEFAULT:
            if setpoint_current is API.DEFAULT:
                setpoint_current = 50
            iso_value = TersoffHamann().wavefunctionContourValue(setpoint_current)
        else:
            if setpoint_current is API.DEFAULT:
                pass
            else:
                raise ValueError("Both 'iso_value' and 'setpoint_current' defined. " + \
                                 "Please give only one of these variables.")

        self.__level = level
        self.__d = degeneracy_index
        self.__iso_value = iso_value


        ElectronicImage.__init__(
                self,
                system=system,
                method=method,
                parameters=parameters,
                inputs=inputs,
                )

    def _defineGrids(self):
        grids = [
            WFGrid(level=self.__level, index=self.__d)
            ]

        return grids

    def explain(self):
        return API.Explanation('')

    def binaryFile(self):
        return 'stm_images.nc'

    def level(self):
        return self.__level

    def title(self):
        return 'WF'

    def generate(self, grids):
        index = self._locateOwnGrids(grids)[0]

        data = wavefunctionTopography(
            grids,
            index=index,
            contour_value=self.__iso_value,
        )

        return data

    def plot(self, data, ax=None, frame=None, color='black', info=True, bar=True, **kwargs):
        ax.set_aspect('equal')
        ax.set_xticks([])
        ax.set_yticks([])

        if isinstance(data, API.CalcTrollException):
            text = data.className()
            ax.text(
                0.5,
                0.5,
                text,
                color='red',
                verticalalignment='center',
                horizontalalignment='center',
            )
            return None

        data, kwargs = self.treatDataForPlotting(data, **kwargs)

        image, variables = data

        X, Y = padVariables(variables)
        if frame is None:
            ax.set_xlim((X.min(), X.max()))
            ax.set_ylim((Y.min(), Y.max()))
        else:
            ax.set_xlim((frame[0, 0], frame[1, 0]))
            ax.set_ylim((frame[0, 1], frame[1, 1]))

        ax.pcolormesh(X, Y, image, **kwargs)
        if bar:
            addMeasuringStick(ax, color=color, size=5)

        level = self['level']
        if isinstance(level, EnergyLevel):
            text = level.title()
        else:
            text = '%.1f' % level

        text = self.title() + ' ' + text

        if info:
            addInfo(ax, text=text, color=color)

    def treatDataForPlotting(self, data, phase=False, **kwargs):
        image, variables = data
        take = image > -np.inf
        vmax = image[take].max()
        vmin = image[take].min()
        vmax = max([vmax, -vmin])
        vmin = -vmax

        image[np.logical_not(take)] = 0
        image[abs(image) < vmax*0.5] = 0

        image[image>0] = (image[image>0] - vmax/2)*2
        image[image<0] = (image[image<0] + vmax/2)*2

        if phase:
            image = -image

        data = image, variables

        return data, {'vmax':vmax, 'vmin':vmin, 'cmap':'seismic'}

    def selfContained(self):
        level = self.__level
        item = self.fixEnergyValues()

        if self.__d is API.ALL:
            d = level.degeneracy()
            if isinstance(d, API.InconsistentApproximation):
                return [d]

            new_items = []
            for i in range(d):
                new_item = item.copy(degeneracy_index=i)
                new_items.append(new_item)

            return new_items
        else:
            return [item]

    def fixEnergyValues(self):
        """
        Return a mode where the placeholder arguments for energies are replaced
        with numerical ones.
        """
        level = self.level()
        if isinstance(level, float):
            return self.copy()

        level = level.level()
        if isinstance(level, API.CalcTrollException):
            return level

        return self.copy(level=level)

    def readData(self, path):
        if self.__d is API.ALL:
            items = self.selfContained()
            read = []
            for item in items:
                if isinstance(item, API.InconsistentApproximation):
                   read.append(item)
                else:
                    read.append(item.readData(path))

            return read
        else:
            return super().readData(path)
