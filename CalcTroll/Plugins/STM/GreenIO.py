def stmDatToArray(filename):
    with open(filename, 'r') as f:
        f.readline()
        delta = []
        for i in range(2):
            line = list(map(float, [entry for entry in f.readline().strip().split(' ') if entry!=''][:2]))
            delta.append(line)

        shape = [entry for entry in f.readline().strip().split(' ') if len(entry)!=0]
        shape = list(map(int, shape[:2]))

        xm, ym = [delta[i][0] + np.arange(shape[i])*delta[i][1] for i in range(2)]

        Xm, Ym = np.meshgrid(xm, ym)

        collect = []
        for line in f:
            try:
                line = [entry for entry in line.strip().split(' ') if len(entry)>0]
                line = list(map(float, line))
            except ValueError:
                break
            else:
                collect.extend(line)

        Z = np.reshape(collect, shape)

        return Xm, Ym, Z

