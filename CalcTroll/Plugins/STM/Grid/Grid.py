import numpy as np
from ase.units import kB
from CalcTroll import API
from CalcTroll.Core.Flags import ALL
from CalcTroll.Plugins.ReferenceEnergy import EnergyLevel, MidGapLevel, HOMO, TRIVIAL_ENERGY

from CalcTroll.Plugins.STM.Utilities import integrated_weights, differential_weights, expandData
from CalcTroll.Plugins.STM.Utilities import integrated_weights_array, grid2Line
from CalcTroll.Plugins.STM.Probe import TersoffHamann

class Grid(API.Parameters):
    _np_type = None
    def __init__(self):
        self.__array = None
        self.__padding = None

    def padding(self):
        return self.__padding

    def r(self):
        return self.__r

    def array(self):
        return self.__array

    def range(self):
        raise NotImplementedError

    def weights(self, energies, spins, ws):
        raise NotImplementedError

    def weightKey(self):
        raise NotImplementedError

    def probe(self):
        raise NotImplementedError

    def expandData(self, var, r):
        if np.array(self.__periodic).any():
            rrrrrrrrrrrrrrr

        return expandData(var, r, padding=self.__padding)

    def makeNCDimensions(self, f):
        return self.dimensions()


class PointGrid(Grid):
    pass

class PointCurrentEnergyGrid(PointGrid):
    _np_type = np.float64

    def __init__(
            self,
            position=(0, 0),
            voltage_range=2,
            temperature=10,
            neutral_level=MidGapLevel(),
            voltage_broadening=0.01,
            probe=TersoffHamann(),
            ):
        self.__position = position
        self.__T = temperature
        self.__de = voltage_broadening
        self.__Vr = voltage_range
        self.__nl = neutral_level
        self.__p = probe

        PointGrid.__init__(self)

    def neutralLevel(self):
        return self.__nl

    def position(self):
        return self.__position

    def voltageBroadening(self):
        return self.__de

    def range(self):
        safety = 6*(self.__de + kB*self.__T)

        Vs = list(self.__Vr)
        for i in range(len(self.__Vr)):
           if isinstance(Vs[i], EnergyLevel):
               Vs[i] = Vs[i].level()
           else:
               Vs[i] = Vs[i] + self.neutralLevel()

        ran = np.array([Vs[0] - safety, Vs[1] + safety])

        return ran

    def probe(self):
        return self.__p

    def totalWeight(self, index):
        de = self.__energies[1] - self.__energies[0]

        return self.__weights[index].sum() * de

    def initiateWeights(self,
            energies,
            spins,
            kpts,
            ws,
            wk,
            ):
        w_e = integrated_weights_array(eigenvalues=energies,
                         voltages=self.__energies,
                         neutral_level=self['neutral_level'],
                         voltage_broadening=self['voltage_broadening'],
                         temperature=self['temperature'],
                         )

        return np.transpose(w_e * wk * ws)

    def calculateContribution(self, u, r, energy, w):
        I = abs(u)**2 / self.probe().densityPrCurrent()
        I_z, r = grid2Line(
            position=self.position(),
            values=I,
            r=r,
            )

        return np.multiply.outer(w, I_z)

    def dimensions(self):
        return ['z', 'e']

    def makeNCDimensions(self, f):
        sh = f.dimensions['e']
        e_start = f.variables['e_start'].getValue()
        de = f.variables['de'].getValue()
        self.__energies = e_start + np.arange(sh) * de

        return self.dimensions()


class ImageGrid(Grid):
    def dimensions(self):
        return ['z', 'x', 'y']


class WFGrid(ImageGrid):
    _np_type=np.complex64
    def __init__(
            self,
            level,
            index=0,
            ):
        self.__level = level
        self.__d = index
        ImageGrid.__init__(self)

    def level(self):
        return self.__level

    def probe(self):
        return TersoffHamann()

    def range(self):
        level = self.level()
        if isinstance(level, EnergyLevel):
            level = level.level()

        if isinstance(level, API.InconsistentApproximation):
            return level

        return np.array([level - TRIVIAL_ENERGY, level + TRIVIAL_ENERGY])

    def calculateContribution(self, u, r, energy, w, use_dask):
        return w * u

    def totalWeight(self, index):
        return self.__weights[index]

    def initiateWeights(self,
            energies,
            spins,
            kpts,
            ws,
            wk,
            ):
        level = self.level()
        if isinstance(level, EnergyLevel):
            level = level.level()

        take = abs(energies - level) < TRIVIAL_ENERGY
        indices = np.arange(len(energies))[take]
        spins = spins[take]

        # Sort degenerates according to spin.
        argsort = np.argsort(spins)
        indices = indices[argsort]

        weights = np.zeros(len(energies))

        d = self.__d
        if ws == 2:
            if len(indices) <= d:
                d = d - len(indices)

        if len(indices) > d:
            index = indices[d]
            weights[index] = 1

        return weights


    def weightKey(self):
        return (
            self['level'],
            self['index'],
            self.className(),
        )


class CurrentGrid(ImageGrid):
    _np_type = np.float64
    def __init__(
            self,
            voltage=2,
            temperature=10,
            neutral_level=MidGapLevel(),
            voltage_broadening=0.01,
            probe=TersoffHamann(),
            regime=API.THERMAL,
            ):
        self.__T = temperature
        self.__de = voltage_broadening
        self.__voltage = voltage
        self.__neutral_level = neutral_level
        self.__probe = probe
        self.__regime = regime

        ImageGrid.__init__(self)

    def regime(self):
        return self.__regime
    
    def voltage(self):
        return self.__voltage

    def neutralLevel(self):
        return self.__neutral_level

    def voltageBroadening(self):
        return self.__de

    def probe(self):
        return self.__probe

    def weightKey(self):
        return (
            self['voltage'],
            self['temperature'],
            self['neutral_level'],
            self['voltage_broadening'],
            self.className(),
        )

    def calculateContribution(self, u, r, energy, w, use_dask):
        cont =  w * abs(u)**2/self['probe'].densityPrCurrent()

        return cont


class IntegratedCurrentGrid(CurrentGrid):
    def initiateWeights(self,
            energies,
            ):
        w_e = integrated_weights(energies=energies,
                         voltage=self['voltage'],
                         neutral_level=self['neutral_level'],
                         voltage_broadening=self['voltage_broadening'],
                         temperature=self['temperature'],
                         )

        return w_e

    def range(self):
        safety = 6*(self['voltage_broadening'] + kB*self['temperature'])
        V = self.voltage()
        N = self.neutralLevel()
        if isinstance(N, EnergyLevel):
            N = N.level()

        if isinstance(V, EnergyLevel):
            V = V.level()
            if isinstance(V, API.CalcTrollException):
                return np.array([N, N])
        else:
            V += N

        ran = sorted([N, V])

        ran[0] -= safety
        ran[1] += safety

        return np.array(ran)


class DifferentialCurrentGrid(CurrentGrid):
    def initiateWeights(self,
            energies,
            spins,
            kpts,
            ws,
            wk,
            ):
        w_e = differential_weights(energies=energies,
                         voltage=self['voltage'],
                         neutral_level=self['neutral_level'],
                         voltage_broadening=self['voltage_broadening'],
                         temperature=self['temperature'],
                         )
        weights = w_e*wk*ws

        return weights

    def range(self):
        safety = 6*(self['voltage_broadening'] + kB*self['temperature'])
        V = self.voltage()
        N = self.neutralLevel()
        if isinstance(N, EnergyLevel):
            N = N.level()

        if isinstance(V, EnergyLevel):
            V = V.level()
            if isinstance(V, API.CalcTrollException):
                return np.array([N, N])
        else:
            V += N

        ran = sorted([N, V])

        ran[0] -= safety
        ran[1] += safety

        return np.array(ran)
