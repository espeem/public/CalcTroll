import dask
import tempfile

import numpy as np
from os.path import join
import tempfile
import multiprocessing as mp
from scipy.io import netcdf


from .AbstractManager import AbstractManager
from ..Utilities import gridSpacingCell, xyz_from_dh

class GridManager(AbstractManager):
    def __init__(
            self,
            block_height=80,
            *args,
            **kwargs
            ):
        
        self.__block_height = block_height
        self.__block_number = 0

        super().__init__( 
            *args, 
            **kwargs
            )

    def makeNetCDF(self):
        r_t = self.rInput()
        dh_cell = gridSpacingCell(r_t)
        tmp_array = np.zeros(r_t.shape[1:])
        null, r = self.expandData(tmp_array, r_t)
        
        filename = join(tempfile.gettempdir(), 'grids.nc')

        f = netcdf.netcdf_file(filename, 'w')
        f.createDimension('z', None)
        f.createDimension('x', r.shape[1])
        f.createDimension('y', r.shape[2])
        n_eigs = max([len(self.filenames()[path]) for path in self.paths()])
        f.createDimension('eigdim', n_eigs)

        f.createDimension('complex', 2)
        f.createDimension('vector3d', 3)

        dh = f.createVariable('dh', dimensions=('vector3d', 'vector3d'), type=np.dtype('f4'))
        dh[:,:] = dh_cell

        origin = f.createVariable('origin', dimensions=('vector3d', ), type=np.dtype('f4'))
        origin[:] = r[:, 0, 0, 0]

        for i, grid in enumerate(self):
            assert grid._np_type in (np.complex64, np.float64)
            dimensions = grid.makeNCDimensions(f)

            if grid._np_type is np.complex64:
                dimensions.append('complex')

            f.createVariable(str(i), dimensions=dimensions, type=np.dtype('f4'))

        return f

    def blockStart(self):
        return self.__block_track[-2]

    def blockHeight(self):
        return self.__block_height

    def z(self, grid):
        sh = self.file().variables['0'].shape[0]
        origin = self.file().variables['origin'][2]
        dh = self.file().variables['dh'][2, 2]

        z = np.arange(sh) * dh + origin

        return z

    def r(self):
        sh = np.zeros(3, dtype=int)
        f = self.file()
        sh[0] = int(f.dimensions['x'])
        sh[1] = int(f.dimensions['y'])
        sh[2] = int(f.variables['0'].shape[0])
        origin = np.array(f.variables['origin'][:])
        dh = np.zeros(3)
        dh[0] = f.variables['dh'][0, 0]
        dh[1] = f.variables['dh'][1, 1]
        dh[2] = f.variables['dh'][2, 2]
        assert f.variables['dh'][0, 1] < 1e-4
        assert f.variables['dh'][0, 2] < 1e-4
        assert f.variables['dh'][1, 2] < 1e-4

        return xyz_from_dh(shape=sh, dh=dh, origin=origin)

    def allocateBlocks(self, add=2):
        bn = self.__block_number
        bh = self.__block_height 
        blocks = [slice(i*bh, (i+1)*bh) for i in range(bn, bn + add)]

        self.__block_number += add
        sl = slice(blocks[0].start, blocks[-1].stop)

        for i in range(len(self)):
            var = self.file().variables[str(i)]
            sh = list(var.shape)
            sh[0] = sl.stop - sl.start
            var[sl] = np.zeros(sh, float) 

        return blocks

    def storeInNetCDF(self, blocks, results):
        for i, block in enumerate(blocks):
            for j, grid in enumerate(self):
                var = self.file().variables[str(j)]
                cont = results[i, j]
                if len(cont.shape) == 3:
                    cont = np.transpose(cont, (2, 0, 1))
                    if grid._np_type is np.float64:
                        var[block, :, :] += cont
                    elif grid._np_type is np.complex64:
                        var[block, :, :, 0] += cont.real
                        var[block, :, :, 1] += cont.imag
                elif len(cont.shape) == 2:
                    cont = np.transpose(cont, (1, 0))
                    if grid._np_type is np.float64:
                        var[block, :] += cont
                    elif grid._np_type is np.complex64:
                        var[block, :, 0] += cont.real
                        var[block, :, 1] += cont.imag
                else:
                    raise NotImplementedError

