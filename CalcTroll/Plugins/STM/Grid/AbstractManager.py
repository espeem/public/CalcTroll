import os
import gc
import logging
import fnmatch
import dask
import numpy as np
from os.path import join
import multiprocessing as mp
from scipy.io import netcdf
import psutil
import pickle


from CalcTroll.Core.Utilities import unique
from CalcTroll.Core.Flags import TooSmallBoundingBox

from CalcTroll.Plugins.ReferenceEnergy import TRIVIAL_ENERGY
from ..Utilities import findBardeenVariables, getVacuumDecayRate, expandData


class AbstractManager:
    def __len__(self):
        return len(self.__grids)

    def __getitem__(self, i):
        return self.__grids[i]

    def __init__(
            self,
            items,
            label,
            periodic=(False, False),
            paths=None,
            padding=7,
            ):
        if paths == None:
            paths = [os.path.dirname(label)]
        self.__paths = paths
        basename = os.path.basename(label)

        all_filenames = dict()
        eig_result = dict()
        for path in paths:
            filenames = fnmatch.filter(os.listdir(path), basename + '*.nc')
            sorters = [tuple(map(int, tuple(fn[:-3].split('_')[2:]))) for fn in filenames]
            sorters.sort()
            filenames = []
            for sorter in sorters:
                filename = '_'.join([basename, str(sorter[0]), str(sorter[1]), str(sorter[2])]) + '.nc'
                filenames.append(join(path, filename))
            all_filenames[path] = filenames
            eigfile = join(path, 'eigs_for_bardeen.pkl')
            with open(eigfile, 'rb') as f:
                eig_result[path] = pickle.load(f)

        r = findBardeenVariables(filenames[0])
        self.__filenames = all_filenames
        self.__eigresult = eig_result
        self.__items = items
        self.__periodic = tuple(periodic)
        self.__padding = padding
        self.__r_input = r

        grids = []
        for item in items:
            grids.extend(item.grids())
        grids = sorted(unique(grids))
        self.__grids = grids

        regimes = []
        for item in items:
            regimes.append(item.regime())
        regimes = unique(regimes)
        self.__regimes = regimes 

        self._netcdf = self.makeNetCDF()

    def regimes(self):
        return self.__regimes
    
    def grids(self):
        return self.__grids
    
    def paths(self):
        return self.__paths
    
    def filenames(self):
        return self.__filenames
    
    def items(self):
        return self.__items

    def makeNetCDF(self):
        raise NotImplementedError

    def rInput(self):
        return self.__r_input

    def padding(self):
        return self.__paddding
    
    def periodic(self):
        return self.__periodic
    
    def dhCell(self):
        return self.__dh_cell

    def filenames(self):
        return self.__filenames

    def probes(self):
        probes = unique([grid.probe() for grid in self])

        return probes

    def padding(self):
        return self.__padding

    def index(self, grid):
        return self.__grids.index(grid)

    def file(self):
        return self._netcdf

    def xy(self):
        return self.__xy
    
    def z(self, grid):
        raise NotImplementedError
    
    def allocateBlocks(self):
        raise NotImplementedError

    def results(self, grids):
        indices = [self.index(grid) for grid in grids]
        results = []
        for index in indices:
            var = self.file().variables[str(index)]
            if len(var.shape) == 2:
                result = var[:, :]
            elif len(var.shape) == 3:
                result = var[:, :, :]
            elif len(var.shape) == 4:
                result = var[:, :, :, 0] + var[:, :, :, 1] * 1j
            else:
                raise Exception(str(var.shape))

            if tuple(self[index].dimensions()) == ('z', 'x', 'y'):
                result = np.transpose(result, (1, 2, 0))

            results.append(result)

        return results

    def save(self, label):
        items = list(self.__items)
        results = list(self.__results)

        clses = unique([item.__class__ for item in items])
        for cls in clses:
            items_cs = [items[i] for i in range(len(items)) \
                         if isinstance(items[i], cls)]
            results_cs = [results[i] for i in range(len(items)) \
                         if isinstance(items[i], cls)]
            cls.save_nc(items_cs, results_cs, label)

    def range(self):
        mini, maxi = np.inf, -np.inf
        for grid in self.__grids:
            ran = grid.range()
            mini = min(ran[0], mini)
            maxi = max(ran[1], maxi)

        return np.array([mini, maxi])

    def neutralLevel(self):
        level = None
        for grid in self:
            nl = grid.neutralLevel()
            if level == None:
                level = nl
            else:
                assert abs(level - nl) < TRIVIAL_ENERGY

        return level

    def expandData(self, var, r):
        return expandData(var, r, padding=self.__padding, periodic=self.__periodic)
        
    def totalWeights(self, path, filename):
        index = self.__filenames[path].index(filename)
        return self.__weights[path][:, index]
        
    def calculateAndSave(self, label):
        """
        """
        self.calculate()
        self.save(label)

    def energyForFilename(self, path, filename):
        EF, kpts, wk, eigs, bandnumbers = self.__eigresult[path]
        filename = os.path.basename(filename)
        sh = eigs.shape
        if sh[2] == 2:
            ws = 1
        elif sh[1]:
            ws = 2

        tmp = filename.split('.')[0].split('_')[2:]
        kpt, band, spin = map(int, tmp)
        band_index = list(bandnumbers).index(band)
        return eigs[kpt, band_index, spin], kpts[kpt], ws*wk[kpt]
    

    def _setWeights(self):
        """
        Update calculation grids based on one file containing
        wavefunctions.
        """
        eigs = dict()
        weights = dict()
        for path in self.paths():
            energies = []
            filenames = self.__filenames[path]
            for filename in filenames:
                energy, kpt, wk = self.energyForFilename(path, filename)
                energies.append(energy)

            eigs[path] = np.array(energies)

            all_w = np.zeros((len(self.__grids), len(energies)))
            for i, grid in enumerate(self.__grids):
                e = energies
                w = grid.initiateWeights(
                    energies=e,
                )
            
                all_w[i, :] = w
            
            weights[path] = all_w

        self.__eigs = eigs
        self.__weights = weights

    def eigs(self):
        return self.__eigs

    def calculate(self):
        self._setWeights()
        logging.basicConfig(filename='calculate_images.log', level=logging.INFO, 
                    format='%(asctime)s - %(levelname)s - %(message)s')

        logging.info("Start of log")

        while True:
            self.calculateFromPaths()

            try:
                results = []
                for item in self.__items:
                    result = item.generate(grids=self)
                    results.append(result)
                break

            except TooSmallBoundingBox as error:
                print('{0}'.format(error))
                print('Expanding z-direction.')
                logging.info("Expanding z")
                pass
        
        logging.info("End of log")

        self.__results = results

    def calculateFromPaths(self):
        paths = self.paths()
        blocks = self.allocateBlocks()  
        results = np.array([[None] * len(self.grids())] * len(blocks))
        path_results = [self.calculateFromFilesDask(path, blocks) for path in paths]    
        for i in range(len(blocks)):
            for j, grid in enumerate(self.grids()):
                results[i, j] = self.pathWeights([tmp[i, j] for tmp in path_results], regime=grid.regime())
        self.storeInNetCDF(blocks, results)

    def pathWeights(self, Is, regime):
        """ Default behaviour is to reject more than one path
            contribution.
        """
        n = len(Is)
        assert n == 1
        return Is[0]

    def calculateFromFilesDask(self, path, blocks):
        mem_limit = 40.0 # Protect memory from being overloaded.

        results = np.array([[None] * len(self.grids())] * len(blocks))
        batch = np.array([[None] * len(self.grids())] * len(blocks))
        filenames = self.__filenames[path]

        for i in range(len(filenames)):
            filename = filenames[i]
            cont = self.calculateGridsFromFileDask(path, filename, blocks)

            for j in range(len(blocks)):
                for k in range(len(self)):
                    if batch[j, k] is None:
                        batch[j, k] = cont[j, k]
                    elif cont[j, k] is None:
                        pass
                    else:
                        batch[j, k].extend(cont[j, k])

            if psutil.virtual_memory().percent > mem_limit:
                batch = computeAndSaveBatch(batch, results)
                gc.collect()

        computeAndSaveBatch(batch, results)
        gc.collect()

        return results
    
    def storeInNetCDF(self, blocks, results):
        raise NotImplementedError

    def calculateGridsFromFileDask(self, path, filename, blocks):
        """
        Update calculation grids based on one file containing
        wavefunctions.
        """
        nl = self.neutralLevel()
        probes = self.probes()
        r = findBardeenVariables(filename)

        fconts = np.array([[None] * len(self)] * len(blocks))

        with netcdf.netcdf_file(filename, 'r', mmap=False) as f:
            energy, kpt, wk = self.energyForFilename(path, filename)
            we = self.totalWeights(path, filename)
            wg = we*wk

            thick_surface = np.array(f.variables['thick_surface'][:], bool)
            c = np.zeros(r.shape, float)

            for i in range(3):
                c[i, thick_surface] = f.variables['c'][i]

            fw = ' '.join(['%2.4f'%w for w in wg])

            print((energy, fw))
            if  abs(wg).sum() < 1e-4:
                return fconts

            #kappa = getVacuumDecayRate(energy)
            kappa = getVacuumDecayRate(nl)

            u = np.zeros(thick_surface.shape, complex)
            u[thick_surface] += f.variables['u'][:, 0]
            u[thick_surface] += 1j*f.variables['u'][:, 1]
            A = np.zeros(thick_surface.shape, complex)
            A[thick_surface] += f.variables['A'][:, 0]
            A[thick_surface] += 1j*f.variables['A'][:, 1]

            A, rr = self.expandData(var=A, r=r)

            B = np.zeros((3, A.shape[0], A.shape[1], A.shape[2]), A.dtype)
            for isp in range(3):
                Btmp, rr = self.expandData(var=u, r=r)
                ctmp, rr = self.expandData(var=c[isp], r=r)
                B[isp] = ctmp*Btmp

            for probe in probes:
                probe_grids = [grid for grid in self if probe==grid.probe()]
                n = probe.numberOfWavefunctions()

                for j, block in enumerate(blocks):
                    sl = block
                    block_start = sl.start
                    block_height = sl.stop - sl.start

                    for k in range(n):
                        nu, nvar = probe.propagateWaveFunction(
                            number=k,
                            A=A,
                            B=B,
                            kpt=kpt,
                            r=rr,
                            kappa=kappa,
                            block_height=block_height,
                            block_start=block_start,
                            use_dask=True,
                        )

                        for grid in probe_grids:
                            l = self.index(grid)
                            cont = grid.calculateContribution(u=nu, r=nvar, energy=energy, w=wg[l], use_dask=True)
                            if fconts[j, l] is None:
                                fconts[j, l] = [cont]
                            else:
                                fconts[j, l].append(cont)

                        del nu
                        del nvar
            del(u, A, B, rr)

        return fconts

def computeAndSaveBatch(batch, results):
    sh = batch.shape
    comp = []
    for i in range(sh[0]):
        for j in range(sh[1]):
            b = batch[i, j]
            if b is None:
                item = None
            else:
                item = dask.delayed(sum)(b)
            comp.append(item)

    print("Computing Batch")
    computed = list(dask.compute(*comp))

    for i in range(sh[0]):
        for j in range(sh[1]):
            item = computed.pop(0)
            if item is None:
                continue
            item = np.array(item)

            if results[i, j] is None:
                results[i, j] = item
            else:
                results[i, j] += item

    return np.array([[None] * sh[1]] * sh[0])