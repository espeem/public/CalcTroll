# Written by Mads Engelund, 2017, http://espeem.com
import os
import unittest

from CalcTroll.ASEUtils.Atoms import Atoms
from CalcTroll.API import DEFAULT

from CalcTroll.Plugins.Systems import Molecule
from CalcTroll.Plugins.ReferenceEnergy import HOMO, LUMO

from CalcTroll.Plugins.STM.Grid.Grid import WFGrid

from CalcTroll.Core.Test.Case import CalcTrollTestCase


molecule = Molecule(atoms=Atoms('H2', positions=[[0, 0, 0], [0, 1, 0]]))

class GridTest(CalcTrollTestCase):
    def testInitialization(self):
        WFGrid(level=HOMO(), index=0)



if __name__=='__main__':
    unittest.main()

