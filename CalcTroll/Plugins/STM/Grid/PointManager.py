import tempfile
import numpy as np
from os.path import join
import tempfile
import multiprocessing as mp
from scipy.io import netcdf
import psutil

from ..Utilities import  gridSpacingCell
from .Utilities import setNetCDFToZero
from .AbstractManager import AbstractManager


class PointManager(AbstractManager):
    def makeNetCDF(self):
        tmp_filename = join(tempfile.gettempdir(), 'grids.nc')
        r_t = self.rInput()[:, :, :, :2]
        tmp_array = np.zeros(r_t.shape[1:])
        null, r = self.expandData(tmp_array, r_t)
        dh_cell = gridSpacingCell(r)
        self.__dh_cell = dh_cell
        self.__xy = r[:2, :, :, 0]
        origin = r[:, 0, 0, 0]
        dh_z = dh_cell[2, 2]
        nsteps = []        
        origins = []
        for point in self.items():
            position = point.position()
            o = origin.copy()
            diff_z = position[2] - o[2]
            nstep = int(np.floor(diff_z/dh_z))
            o[2] = nstep*dh_z + origin[2]
            origins.append(o)
            nsteps.append(nstep)

        self.__nsteps = nsteps
        self.__origins = np.array(origins)         

        all_eigs = []
        for path in self.paths():
            filenames = self.filenames()[path]
            for filename in filenames:
                energy, kpt, wk = self.energyForFilename(path, filename)
                all_eigs.append(energy)
        all_eigs = np.array(all_eigs)


        f = netcdf.netcdf_file(tmp_filename, 'w')
        f.createDimension('z', 2)
        f.createDimension('x', r.shape[1])
        f.createDimension('y', r.shape[2])
        f.createDimension('eigdim', len(all_eigs))

        f.createDimension('complex', 2)
        f.createDimension('vector3d', 3)

        eigs_var = f.createVariable('eigs', dimensions=('eigdim', ), type=np.dtype('f4'))
        eigs_var[:] = all_eigs

        dh = f.createVariable('dh', dimensions=('vector3d', 'vector3d'), type=np.dtype('f4'))
        dh[:,:] = dh_cell

        for i, grid in enumerate(self):
            assert grid._np_type in (np.complex64, np.float64)
            dimensions = grid.makeNCDimensions(f)

            if grid._np_type is np.complex64:
                dimensions.append('complex')

            var = f.createVariable(str(i), dimensions=dimensions, type=np.dtype('f4'))
            setNetCDFToZero(f, var)

        return f
    

    def z(self, grid):
        i = self.index(grid)
        sh = self._netcdf.variables[str(i)].shape[0]
        origin = self.__origins[i, 2]
        dh = self._netcdf.variables['dh'][2, 2]

        z = np.arange(sh) * dh + origin

        return z

    def allocateBlocks(self):
        return [slice(nstep, nstep + 2) for nstep in self.__nsteps]

    def storeInNetCDF(self, blocks, results):
        for i, block in enumerate(blocks):
            for j, grid in enumerate(self):
                var = self._netcdf.variables[str(j)]
                cont = results[i, j]
                if len(cont.shape) == 3:
                    cont = np.transpose(cont, (2, 0, 1))
                    if grid._np_type is np.float64:
                        var[:, :, :] += cont
                    elif grid._np_type is np.complex64:
                        var[:, :, :, 0] += cont.real
                        var[:, :, :, 1] += cont.imag
                elif len(cont.shape) == 2:
                    cont = np.transpose(cont, (1, 0))
                    if grid._np_type is np.float64:
                        var[:, :] += cont
                    elif grid._np_type is np.complex64:
                        var[:, :, 0] += cont.real
                        var[:, :, 1] += cont.imag
                else:
                    raise NotImplementedError


        return