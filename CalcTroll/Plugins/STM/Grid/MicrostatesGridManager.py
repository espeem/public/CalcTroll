import os
import gc
import fnmatch
import numpy as np
from os.path import join
import tempfile
from scipy.io import netcdf
import dask
import psutil

from CalcTroll import API
from CalcTroll.Core.Utilities import unique
from CalcTroll.Core.Flags import TooSmallBoundingBox

from CalcTroll.Plugins.ReferenceEnergy import findUniqueEnergies, TRIVIAL_ENERGY
from CalcTroll.Plugins.STM.Utilities import findBardeenVariables, getVacuumDecayRate, expandData, gridSpacingCell
from CalcTroll.Plugins.STM.Grid.GridManager import GridManager

class MicrostatesGridManager(GridManager):
    def pathWeights(self, Is, regime):
        if regime == API.DYNAMIC:
            n = len(Is)
            N = np.zeros(Is[0].shape)
            for I in Is:
                N += 1/I

            result = n/N
        
        elif regime == API.THERMAL:
            n = len(Is)
            N = np.zeros(Is[0].shape)
            for I in Is:
                N += I

            result = N/n

        return result