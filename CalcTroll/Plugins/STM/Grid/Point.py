import numpy as np
from ase.units import kB
import dask

from CalcTroll import API
from CalcTroll.Plugins.ReferenceEnergy import MidGapLevel
from CalcTroll.Plugins.STM.Utilities import grid2Line
from CalcTroll.Plugins.STM.Probe import TersoffHamann

class Point(API.Parameters):
    _np_type = np.float64
    def __init__(
        self,
        position=(0, 0),
        energy_range=tuple([-2, -2]),
        spatial_broadening=1.0,
        probe=TersoffHamann(),
        neutral_level=MidGapLevel(),
        regime=API.DEFAULT,
        ):
        self.__position = position
        self.__probe = probe
        self.__energy_range = energy_range
        self.__spatial_broadening = spatial_broadening
        self.__neutral_level = neutral_level
        self.__regime = regime
        self.__eigs = None

    def regime(self):
        return self.__regime

    def neutralLevel(self):
        return self.__neutral_level

    def position(self):
        return self.__position

    def probe(self):
        return self.__probe

    def spatialBroadening(self):
        return self.__spatial_broadening

    def range(self):
        return self.__energy_range

    def xy(self):
        return np.array(self.position())[:2]

    def dimensions(self):
        return ['z', 'eigdim']

    def makeNCDimensions(self, f):
        return self.dimensions()

    def eigs(self):
        return self.__eigs

    def initiateWeights(self,
            energies,
            ):
        self.__eigs = energies
        w_e = np.ones(len(energies))
        e_min, e_max = self.range()
        w_e[energies < e_min] = 0
        w_e[energies > e_max] = 0

        return np.transpose(w_e)

    def calculateContribution(self, u, r, energy, w, use_dask=False):
        if use_dask:
            getContribution = dask.delayed(calculateContribution)
        else:
            getContribution = calculateContribution

        return getContribution(self, u=u, r=r, energy=energy, w=w)


def calculateContribution(grid, u, r, energy, w):
    sigma = grid.spatialBroadening()
    I = abs(u)**2 / grid.probe().densityPrCurrent()

    I_z, r = grid2Line(
        position=grid.xy(),
        values=I,
        r=r,
        sigma=sigma,
        )
    eigs = grid.eigs()
    index = abs(eigs - energy).argmin()
    result = np.zeros((len(eigs), len(I_z)))
    result[index] = w * I_z

    return result

