import numpy as np

def setNetCDFToZero(f, var):
    dims = var.dimensions
    dim_sizes = [f.dimensions[dim] for dim in dims]
    a = np.zeros(dim_sizes)
    if len(dim_sizes) == 1:
        var[:] = a
    elif len(dim_sizes) == 2:
        var[:, :] = a
    elif len(dim_sizes) == 3:
        var[:, :, :] = a
    elif len(dim_sizes) == 4:    
        var[:, :, :, :] = a
    else:
        raise Exception