import numpy as np


def repeatTraceData(data, cells=((-1, 1), (-1, 1))):
    X, Y, values = data
    r = np.array([X, Y])

    values, r = repeatData(values=values, r=r, cells=cells)
    X, Y = r
    Z = values

    return np.array([X, Y, Z])


def openTopographyData(filename, factor=1):
    if filename[-4:] == '.asc':
        result = openAscTopographyData(filename, factor)
    elif filename[-4:] == '.txt':
        assert factor == 1
        result = openTxtTopographyData(filename, factor)

    return result


def openAscTopographyData(filename, factor):
    with open(filename, 'r') as f:
        data = []
        for line in f:
            line = line.strip()
            if '# x-pixels =' in line:
                x_pixels = int(line.split( )[-1])
            elif '# y-pixels =' in line:
                y_pixels = int(line.split( )[-1])
            elif '# x-length =' in line:
                x_length = float(line.split( )[-1])
            elif '# y-length =' in line:
                y_length = float(line.split( )[-1])
            elif ' Voltage =' in line:
                line = line.split( )
                voltage = float(line[-2])
                assert line[-1] == 'Volt'
            elif '# Start of Data' in line:
                break

        for line in f:
            line = line.strip()
            line = line.split('\t')
            try:
                line = list(map(float, line))
            except ValueError:
                pass
            else:
                if len(line) > 1 and len(data) < y_pixels:
                    data.append(line)

        data = np.array(data)

    x_length = x_length*10
    y_length = y_length*10

    x_length *= factor
    y_length *= factor

    Z = data.transpose()
    Z *= 10

    x_pixels, y_pixels

    X, Y = np.mgrid[0:x_length:x_pixels*1j,0:y_length:y_pixels*1j]
    Y = - Y

    variables = np.array([X, Y])

    return Z, variables


def cropTopographyData(data, center, size):
    Z, XY = data
    X, Y = XY
    mask = np.logical_and(X[:,0] > center[0]-size/2.0, X[:,0] < center[0] + size/2.0)
    X, Y, Z = X[mask], Y[mask], Z[mask]
    mask = np.logical_and(Y[0] > center[1]-size/2.0, Y[0] < center[1] + size/2.0)
    X, Y, Z = X[:, mask], Y[:, mask], Z[:, mask]

    return Z, np.array([X, Y])


def rotateTraceData(data, center=(0,0), theta=0):
    Z, XY = data
    X, Y = XY
    level = Z.min()
    data -= level

    theta = theta*(np.pi/180)
    X -= center[0]
    Y -= center[1]
    X, Y = np.cos(theta)*X + np.sin(theta)*Y , np.cos(theta)*Y - np.sin(theta)*X

    return Z, np.array([X, Y])


def openTxtTopographyData(filename, factor=1):
    xyzs = []
    with open(filename) as f:
        f.readline()
        f.readline()
        f.readline()
        f.readline()
        for line in f:
            xyzs.append(list(map(float, line.strip().split('\t'))))
    X, Y, Z = np.array(xyzs).transpose()
    cut_indices = np.arange(len(xyzs))[:-1][(X[1:] -X[:-1]) < 0]
    x_size = cut_indices[0] + 1
    y_size, r = divmod(len(X), x_size)
    x_size, y_size = y_size, x_size
    assert r == 0
    X = np.reshape(X, (x_size, y_size))*10
    Y = np.reshape(Y, (x_size, y_size))*10
    X *= factor
    Y *= factor

    Z = np.reshape(Z, (x_size, y_size))*10

    return Z, np.array([X, Y])

