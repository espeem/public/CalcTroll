import numpy as np
from CalcTroll import API
from CalcTroll.API import DEFAULT

from CalcTroll.Plugins.Analysis.EnergySpectrum import EnergySpectrum

class ElectronicImage(API.Image):
    """ Scripting interface item representing a Scanning Tunneling Microscopy(STM) image.
    """
    def __init__(self,
                 system=None,
                 method=DEFAULT,
                 parameters=DEFAULT,
                 inputs=tuple(),
                 ):
        inputs = list(inputs)

        if system != None:
            es = EnergySpectrum(
                system=system,
                method=method,
                parameters=parameters,
                )
            inputs.append(es)


        API.Image.__init__(self,
            system=system,
            method=method,
            parameters=parameters,
            inputs=inputs,
            )

        self.__grids = self._defineGrids()

    def energySpectrum(self):
        return self.inputs()[-1]

    def _defineGrids(self):
        raise NotImplementedError

    def grids(self):
        return self.__grids

    def range(self):
        mini, maxi = np.inf, -np.inf
        for grid in self.grids():
            ran = grid.range()
            if isinstance(ran, API.InconsistentApproximation):
                continue
            mini = min(ran[0], mini)
            maxi = max(ran[1], maxi)

        return np.array([mini, maxi])

    def generate(self, grids):
        raise NotImplementedError

    def _locateOwnGrids(self, grids):
        own_grids = self.grids()
        indices = [grids.index(own_grid) for own_grid in own_grids]

        return indices

    def binaryFile(self):
        return 'stm_images.nc'
