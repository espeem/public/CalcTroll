import numpy as np

from CalcTroll import API

from CalcTroll.Core.Utilities import apply2DGaussian
from CalcTroll.Core.Visualizations import padVariables, addMeasuringStick, addInfo
from CalcTroll.Plugins.ReferenceEnergy import HOMO, EnergyLevel
from CalcTroll.Plugins.ReferenceEnergy import MO, MidGapLevel
from .Utilities import evaluateOnIsoSurface, write_gsf
from .CurrentImage import CurrentImage
from .Grid.Grid import DifferentialCurrentGrid, IntegratedCurrentGrid

class dIdVMap(CurrentImage):
    def __init__(
            self,
            system=None,
            method=API.DEFAULT,
            probe=API.DEFAULT,
            bias_voltage=API.DEFAULT,
            temperature=10,
            voltage_broadening=0.1,
            spatial_broadening=0.5,
            neutral_level=API.DEFAULT,
            setpoint_current=50,
            dc_signal_spatial_broadening=2.0,
            ):
        self.__p_ds = dc_signal_spatial_broadening
        self.__I = setpoint_current

        if bias_voltage is API.DEFAULT:
            bias_voltage = HOMO()
        if neutral_level is API.DEFAULT:
            neutral_level = MidGapLevel()

        if isinstance(bias_voltage, MO) and \
           bias_voltage.include() is API.DEFAULT:
            bias_voltage = bias_voltage.copy(include=False)

        CurrentImage.__init__(
                self,
                system=system,
                method=method,
                probe=probe,
                bias_voltage=bias_voltage,
                neutral_level=neutral_level,
                temperature=temperature,
                voltage_broadening=voltage_broadening,
                spatial_broadening=spatial_broadening,
                )

    def _defineGrids(self):
        igrid = IntegratedCurrentGrid(
            voltage=self.biasVoltage(),
            temperature=self.temperature(),
            voltage_broadening=self.voltageBroadening(),
            neutral_level=self.neutralLevel(),
            probe=self.probe(),
            )
        dgrid = DifferentialCurrentGrid(
            voltage=self.biasVoltage(),
            temperature=self.temperature(),
            voltage_broadening=self.voltageBroadening(),
            neutral_level=self.neutralLevel(),
            probe=self.probe(),
            )

        return [igrid, dgrid]

    def explain(self):
        return API.Explanation(
            "The simulated image was generated to emulate dIdV mapping using a scanning tunneling microscope. ")

    def title(self):
        return "dIdV"

    def treatDataForPlotting(self, data, **kwargs):
        image, variables = data
        image[np.isinf(image)] = 0
        vmax = image.max()
        vmin = 0

        return data, {'vmax':vmax, 'vmin':vmin, 'cmap':'hot'}

    def generate(self, grids):
        results = grids.results(self.grids())
        assert len(results) == 2
        r = grids.r()
        data1 = results[0], r
        data2 = results[1], r

        current, variables = apply2DGaussian(
                data1,
                sigma=self.__p_ds,
                )
        current_diff, variables = apply2DGaussian(
                data2,
                sigma=self.spatialBroadening(),
                )
        values, variables = evaluateOnIsoSurface(
            iso_values=current,
            values=current_diff,
            variables=variables,
            contour_value=self.__I,
        )
        data = values, variables

        return data

    def gsf_save(self, filename, data=None, **kwargs):
        if data is None:
            data = self.read()

        if isinstance(data, API.CalcTrollException):
            return None

        image, variables = data
        image[np.isinf(image)] = 0

        image *= 1e-9   # nA to A
        variables *= 1e-10 # Ang to m
        X, Y = variables
        xres, yres = image.shape

        xreal = X.max() - X.min()
        yreal = Y.max() - Y.min()

        image = image[:, ::-1]
        image = np.transpose(image)
        imagedata = image.flatten()

        zunits = 'A/V'

        write_gsf(
            filename=filename,
            imagedata=imagedata,
            xres=xres,
            yres=yres,
            xreal=xreal,
            yreal=yreal,
            xyunits='m',
            zunits=zunits,
        )
