import os
from os.path import join
import pickle
import numpy as np
from ase.units import kB
from scipy.io import netcdf

from CalcTroll import API
from CalcTroll.Core.Flags import TooSmallBoundingBox

from CalcTroll.Core.Utilities import vectorLength
from CalcTroll.Plugins.LengthScales import LengthScales
from CalcTroll.Plugins.ReferenceEnergy import MidGapLevel, EnergyLevel
from CalcTroll.Core.Plugin.Types import AnalysisProvider
from CalcTroll.Plugins.Analysis.EnergySpectrum import EnergySpectrum
from .Utilities import eigContributionToIVCurve
from .Utilities import eigContributionToDIdVCurve
from .Probe import TersoffHamann
from .Grid.Point import Point

class PositionBasedOnImage(API.NoTasksAnalysis):
    def __init__(
        self,
        image,
        ):
        inputs = [image]

        API.NoTasksAnalysis.__init__(self, system=image.system(), method=image.method(), inputs=inputs)

    def image(self):
        return self.inputs()[0]

class ImageMaximum(PositionBasedOnImage):
    def read(self):
        self.image().updateStatus()
        if not self.isDone():
            return None

        Z, XY = self.image().read()
        X, Y = XY
        X, Y, Z = X.flatten(), Y.flatten(), Z.flatten()
        Zmax = Z.max()
        max_indices = np.arange(len(Z))[Z == Zmax]
        l = np.array([vectorLength((X[i], Y[i])) for i in max_indices])
        index = max_indices[l.argmin()]
        pos = (X[index], Y[index], Z[index])

        return pos

class ImageMinimum(PositionBasedOnImage):
    def read(self):
        self.image().updateStatus()
        if not self.isDone():
            return None
        Z, XY = self.image().read()
        X, Y = XY
        X, Y, Z = X.flatten(), Y.flatten(), Z.flatten()
        Zmax = Z.min()
        max_indices = np.arange(len(Z))[Z == Zmax]
        l = np.array([vectorLength((X[i], Y[i])) for i in max_indices])
        index = max_indices[l.argmin()]
        pos = (X[index], Y[index], Z[index])

        return pos


class FixedXY(PositionBasedOnImage):
    def __init__(
        self,
        image,
        position=(0.0, 0.0),
        ):
        self.__position = np.array(position)
        PositionBasedOnImage.__init__(self, image)

    def read(self):
        self.image().updateStatus()
        Z, XY = self.image().read()
        self.image().updateStatus()
        x, y = self.__position
        X, Y = XY
        X, Y, Z = X.flatten(), Y.flatten(), Z.flatten()
        diff = np.sqrt((X - x)**2 + (Y - y)**2)
        index = diff.argmin()
        pos = (X[index], Y[index], Z[index])

        return pos

def interpolateCurrentToHeight(I, z, height):
    Ilog = np.zeros(I.shape)
    take = (I > 0).all(axis=0)
    Ilog[:, take] = np.log(I[:, take])
    index = (z < height).sum() - 1
    i1, i2 = index, index + 1
    w1, w2 = (z[i2] - height)/(z[i2] - z[i1]) , (height - z[i1])/(z[i2] - z[i1])

    Iav = w1 * Ilog[i1, take] + w2 * Ilog[i2, take]
    Ires = np.zeros(len(take))
    Ires[take] = np.exp(Iav)

    return Ires

class AbstractIV(API.Spectrum):
    def __init__(
            self,
            position=API.DEFAULT,
            bias_range=tuple([-2, 2]),
            temperature=10,
            voltage_broadening=API.DEFAULT,
            spatial_broadening=API.DEFAULT,
            neutral_level=API.DEFAULT,
            probe=API.DEFAULT,
            regime=API.DEFAULT,
            ):

        inputs = []
        self.__bias_range = np.array(bias_range, float)
        self.__temperature = temperature

        if isinstance(position, PositionBasedOnImage):
            image = position.image()
            system = image.system()
            method = image.method()
            parameters = image.parameters()
            if voltage_broadening is API.DEFAULT:
                voltage_broadening = image.voltageBroadening()
            if spatial_broadening is API.DEFAULT:
                spatial_broadening = image.spatialBroadening()
            if probe is API.DEFAULT:
                probe = image.probe()
            if neutral_level is API.DEFAULT:
                neutral_level = image.neutralLevel()
            if regime is API.DEFAULT:
                regime = image.regime()

            position = position.copy(image=image)
            es = EnergySpectrum(
                system=system,
                method=method,
                parameters=parameters,
                )
            inputs.append(es)
            inputs.append(position)
        else:
            system=None
            method=None
            parameters=API.DEFAULT
            if voltage_broadening is API.DEFAULT:
                voltage_broadening = 0.1
            if spatial_broadening is API.DEFAULT:
                spatial_broadening = 1.0
            if probe is API.DEFAULT:
                probe = TersoffHamann()

        self.__position = position
        self.__grids = None
        self.__neutral_level= neutral_level
        self.__spatial_broadening = spatial_broadening
        self.__voltage_broadening = voltage_broadening
        self.__probe = probe
        self.__regime = regime

        API.Spectrum.__init__(
            self,
            system=system,
            method=method,
            parameters=parameters,
            inputs=inputs,
            )

    def regime(self):
        return self.__regime

    def energySpectrum(self):
        es = [inp for inp in self.inputs() if isinstance(inp, EnergySpectrum)][0]

        return es

    def temperature(self):
        return self.__temperature

    def biasRange(self):
        return self.__bias_range

    def range(self):
        ran = self.biasRange()
        safety = (6*self.voltageBroadening() + kB*self.temperature())
        N = self.neutralLevel()
        ran = [ran[0] - safety + N, ran[1] + safety + N]

        return ran

    def probe(self):
        return self.__probe

    def spatialBroadening(self):
        return self.__spatial_broadening

    def voltageBroadening(self):
        return self.__voltage_broadening

    def neutralLevel(self):
        if isinstance(self.__neutral_level, EnergyLevel):
            return self.__neutral_level.level()

        return self.__neutral_level

    def _defineGrids(self):
        displacements = self.probe().displacements()
        grids = []
        for disp in displacements:
            pos = self.position()[:2] + disp[:2]
            grid = Point(
                    position=pos,
                    spatial_broadening=self.spatialBroadening(),
                    probe=self.probe(),
                    energy_range=self.range(),
                    neutral_level=self.neutralLevel(),
                    regime=self.regime(),
                    )
            grids.append(grid)

        return grids

    def grids(self):
        if self.__grids is None:
            self.__grids = self._defineGrids()

        return self.__grids

    def image(self):
        if isinstance(self.__position, PositionBasedOnImage):
            return self.__position.image()

        return None

    def position(self):
        if isinstance(self.__position, PositionBasedOnImage):
            position = self.__position.read()
            return position
        else:
            return self.__position

    def height(self):
        return self.position()[2]

    def selfContained(self):
        point = self.position()
        assert point[2] != -np.inf
        N = self.neutralLevel()
        N = np.around(N, 10)

        return [dIdV(position=point,
                    voltage_broadening=self.voltageBroadening(),
                    spatial_broadening=self.spatialBroadening(),
                    neutral_level=N,
                    bias_range=self.biasRange(),
                    probe=self.probe(),
                    )]


    def generate(self, grids):
        assert len(grids.paths()) == 1
        path = grids.paths()[0]
        results = grids.results(self.grids())
        displacements = self.probe().displacements()
        assert len(results) == len(displacements)
        eigs = grids.eigs()[path]
        z = grids.z(self.grids()[0])
        height = self.height()
        zmax = 0
        for I, disp in zip(results, displacements):
            zt = height + disp[2]
            zmax = max(zmax, zt)


        if zt > z[-1]:
            convergence = 'Z-Boundary Value Exceeded (max/target=%2.2f/%2.2f)' % (z[-1], zt)
            raise TooSmallBoundingBox(convergence)

        Itot = np.zeros(len(eigs))
        for I, disp in zip(results, displacements):
            zt = height + disp[2]
            Itot += interpolateCurrentToHeight(I=I, z=z, height=zt)

        s = Itot > 0.01
        Itot = Itot[s]
        eigs = eigs[s]

        return Itot, eigs

    @classmethod
    def save_nc(
            cls,
            items,
            results,
            label,
            ):
        assert all([isinstance(item, cls) for item in items])
        items = list(items)
        filename = "%s_%s.pickle" % (label, 'spec')
        old_scripts, old_results = cls.read_nc(filename)
        new_results = list(results)
        new_scripts = [item.script(detail_level=API.ALL) \
                   for item in items]
        scripts = list(old_scripts)
        results = list(old_results)
        for i in range(len(new_scripts)):
            if new_scripts[i]:
                scripts.append(new_scripts[i])
                results.append(new_results[i])

        # Write all images again.
        s_length = 2000
        didv, eigs = results[0]
        store = []
        for i in range(len(results)):
            data = scripts[i], results[i]
            store.append(data)

        with open(filename, 'wb') as f:
            pickle.dump(store, f)

    def binaryFile(self):
        return 'didv_spec.pickle'

    def plot(self, data=None, ax=None, frame=API.DEFAULT, color=None, label=None, neutral_level=None):
        from matplotlib import pylab as plt
        if data is None:
            data = self.read()

        if isinstance(data, API.CalcTrollException):
            pmin, pmax = np.array([[0, 0], [1, 1]])
        else:
            pmin, pmax= np.array(self.frame(data=data))

        if frame is not API.DEFAULT:
            amin, amax = np.array(frame)
            pmin = [pmin[i]  if amin[i] is None else amin[i] for i in range(2)]
            pmax = [pmax[i]  if amax[i] is None else amax[i] for i in range(2)]

        data = self.treatDataForPlotting(data, neutral_level)

        I, v = data

        ax.set_xlim((pmin[0], pmax[0]))
        ax.set_ylim((pmin[1], pmax[1]))

        ax.set_xlabel("U/V")
        ax.set_ylabel(self._ylabel())
        ax.plot(v, I, color=color, label=label)

        return ax

    def _ylabel(self):
        raise NotImplementedError

    def eigContributionToCurve(self, I, eigs, bias_range, bias_broadening, temperature):
        raise NotImplementedError

    def treatDataForPlotting(self, data, neutral_level=None):
        I, eigs  = data
        N = self.neutralLevel()
        br = self.biasRange()
        T = self.temperature()
        dv = self.voltageBroadening()
        if neutral_level is None:
            eigs = eigs - N
        else:
            eigs = eigs - neutral_level

        return self.eigContributionToCurve(
                   I=I,
                   eigs=eigs,
                   bias_range=br,
                   bias_broadening=dv,
                   temperature=T,
                   )

    def png_save(self, filename, data=None, **kwargs):
        return self.jpg_save(filename, data=data, **kwargs)

    def jpg_save(self, filename, data=None, frame=API.DEFAULT, border=API.DEFAULT, **kwargs):
        from matplotlib import pylab as plt
        if data is None:
            data = self.read()

        fig = plt.figure()

        ax = fig.gca()

        self.plot(data=data, ax=ax, frame=frame, **kwargs)

        plt.savefig(filename)
        plt.close('all')

        return frame

    def csv_save(self, filename, data=None, **kwargs):
        if data is None:
            data = self.read()

        data = self.treatDataForPlotting(data)
        Is, Vs = data
        with open(filename, 'w') as f:
            f.write('U/V,[dI/dV]/[nA/V]\n')
            for V, I in zip(Vs, Is):
                f.write('%.4f, %.4f\n' %(V, I))

        return None

    def frame(self, data=None, border=API.DEFAULT):
        raise NotImplementedError


    @classmethod
    def read_nc(
            cls,
            filename,
            ):
        if not os.path.exists(filename):
            return [], []


        with open(filename, 'rb') as f:
            store = pickle.load(f)

        scripts = []
        results = []
        for script, result in store:
            scripts.append(script)
            results.append(result)

        return scripts, results

    def inNCFile(self, filename):
        s_script = self.script(detail_level=API.ALL)
        scripts, results = self.read_nc(filename)

        try:
            index = scripts.index(s_script)
        except ValueError:
            return False
        else:
            return index

    def readData(self, path):
        items = self.selfContained()
        assert len(items) == 1
        item = items[0]

        if isinstance(item, API.InconsistentApproximation):
            return item
        filename = join(path, item.binaryFile())
        index = item.inNCFile(filename)
        if index is False:
            return False
        scripts, results = self.read_nc(filename)

        return results[index]

class IV(AbstractIV):
    def _ylabel(self):
        return "I/nA"

    def eigContributionToCurve(self, I, eigs, bias_range, bias_broadening, temperature):
        return eigContributionToIVCurve(
                   I=I,
                   eigs=eigs,
                   bias_range=bias_range,
                   bias_broadening=bias_broadening,
                   temperature=temperature,
                   )

    def frame(self, data=None):
        if data is None:
            data = self.read()

        data = self.treatDataForPlotting(data)

        I, v = data
        Imax = abs(I).max() * 1.3
        Imax = round(Imax, -int(np.floor(np.log10(abs(Imax)))))
        Imin = -Imax
        vmin, vmax = v.min(), v.max()

        return [[vmin, Imin], [vmax, Imax]]

class dIdV(AbstractIV):
    def _ylabel(self):
        return "dIdV/[nA/V]"

    def eigContributionToCurve(self, I, eigs, bias_range, bias_broadening, temperature):
        return eigContributionToDIdVCurve(
                   I=I,
                   eigs=eigs,
                   bias_range=bias_range,
                   bias_broadening=bias_broadening,
                   temperature=temperature,
                   )

    def frame(self, data=None):
        if data is None:
            data = self.read()

        data = self.treatDataForPlotting(data)

        I, v = data
        Imax = I.max() * 1.3
        Imax = round(Imax, -int(np.floor(np.log10(abs(Imax)))))
        Imin = 0
        vmin, vmax = v.min(), v.max()

        return [[vmin, Imin], [vmax, Imax]]

    def convergenceValue(self, other):
        if other is None:
            return np.inf
        assert self.__class__ == other.__class__
        if not self.isDone() or not other.isDone():
            API.calculate(self)
            API.calculate(other)
            return None

        own_data = self.read()
        other_data = other.read()
        ownI, energies = self.treatDataForPlotting(data=own_data)
        otherI, energies = other.treatDataForPlotting(data=other_data)

        diff = abs(ownI - otherI)
        index = np.argmax(diff)
        diff_av = abs(ownI - otherI).sum()
        own_norm = ownI.sum()
        other_norm = otherI.sum()

        diff_av = np.sqrt((abs(ownI - otherI)**2).sum())
        own_norm = np.sqrt((ownI**2).sum())
        other_norm = np.sqrt((ownI**2).sum())

        convergence_value = (diff_av/own_norm + diff_av/other_norm)/2

        return convergence_value
