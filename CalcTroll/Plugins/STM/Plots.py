import numpy as np
from CalcTroll import API
from CalcTroll.Plugins.ReferenceEnergy import HOMO, LUMO
from CalcTroll.Core.Flags import CalcTrollException

from .OrbitalImage import OrbitalImage
from .ConstantCurrent import ConstantCurrent

class EnergyVsWF(API.NoTasksAnalysis):
    def __init__(
        self,
        system=None,
        method=API.DEFAULT,
        higher=HOMO(),
        lower=LUMO(),
        setpoint_current=50,
        ):
        assert isinstance(lower, (HOMO, LUMO))
        assert isinstance(higher, (HOMO, LUMO))
        assert (higher > lower)

        self.__lower = lower
        self.__higher = higher
        self.__setpoint_current = setpoint_current
        inputs = self._makeInputs(system, method)
        system = API.Relaxed(system, method=method)

        API.NoTasksAnalysis.__init__(
                self,
                system=system,
                method=method,
                inputs=inputs,
            )

    def levels(self):
        return self.__levels

    def explain(self):
        return API.Explanation("")

    def _makeInputs(self, system, method):
        lower = self.__lower
        higher = self.__higher
        levels = [lower]
        while levels[-1].higher().script(detail_level=API.ALL) != higher.script(detail_level=API.ALL):
            levels.append(levels[-1].higher())
        levels.append(higher)

        setpoint_current = self.__setpoint_current
        o_images, cc_images = [], []
        new_levels = []
        for level in levels:
            image = ConstantCurrent(
                    system=system,
                    method=method,
                    bias_voltage=level,
                    setpoint_current=setpoint_current,
                    spatial_broadening=0.01,
                    )
            cc_images.append(image)

            image = OrbitalImage(
                    system=system,
                    method=method,
                    level=level,
                    degeneracy_index=API.ALL,
                    setpoint_current=setpoint_current,
                    )
            o_images.append(image)
            new_levels.append(image.level())

        self.__levels = new_levels
        self.__cc_images = cc_images
        self.__o_images = o_images

        return cc_images + o_images


    def levels(self):
        lower = self.__lower
        higher = self.__higher
        levels = [lower]
        while levels[-1].higher().script(detail_level=API.ALL) != higher.script(detail_level=API.ALL):
            levels.append(levels[-1].higher())
        levels.append(higher)

        return levels

    def plot(self, data=None, ax=None, change_phase=None, **kwargs):
        if not self.isDone():
            return None

        if change_phase is None:
            change_phase = {}

        from matplotlib import pylab as plt
        cc_data, o_data = data
        image, variables = cc_data[0]

        Xi, Yi = variables
        Xi -= Xi.min()
        Yi -= Yi.min()
        Xsp = Xi.max()
        Ysp = Yi.max()

        spin_deg = []
        for image in self.__cc_images:
            l = image.biasVoltage()
            d = l.degeneracy(spin_resolved=True)
            if not isinstance(d, API.InconsistentApproximation):
                spin_deg.append(np.array(d))
        size = np.array(spin_deg).max(axis=0)
        cols = size.sum()

        for i, o_image in enumerate(self.__o_images):
            o_image = self.__o_images[i]
            od = o_data[i]
            level = o_image.level()
            deg = level.degeneracy(spin_resolved=True)

            if isinstance(deg, CalcTrollException):
                continue

            for j in range(sum(deg)):
                image = o_image.copy(degeneracy_index=j)
                js = (size[0] - deg[0] + j)
                Xshift = Xsp*js
                Yshift = Ysp*i
                atoms = self.system().atoms()
                xmin, ymin = self.system().frame()[0]
                plot = image.plot
                image, variables = od[j]
                X = Xi + Xshift
                Y = Yi + Yshift
                data = image, (X, Y)
                phase = (i, js) in change_phase
                plot(data=data,
                     ax=ax,
                     phase=phase,
                     info=False,
                     bar=False)
                atoms.translate([Xshift + 0.5*Xsp, Yshift + 0.5*Ysp, 0])
                self.system().plot(
                     ax=ax,
                     data=atoms,
                     bar=False,
                     facecolor='white',
                     size=.8,
                     )

        x = Xsp*size[0]
        ax.axvline(x=x, color='black')
        y = np.array([isinstance(level, HOMO) for level in self.levels()]).sum()*Ysp
        ax.axhline(y=y, color='black', linestyle='--')

        for i, cc_image in enumerate(self.__cc_images):
            data = cc_data[i]
            Xshift = Xsp*cols
            Yshift = Ysp*i
            X = Xi + Xshift
            Y = Yi + Yshift
            if isinstance(data, API.CalcTrollException):
                ax.text(
                    X.mean(),
                    Y.mean(),
                    "Error",
                    color='red',
                    verticalalignment='center',
                    horizontalalignment='center',
                )
                continue

            image, variables = data
            data = (image, (X, Y))
            cc_image.plot(
                    ax=ax,
                    data=data,
                    bar=False,
                    info=False,
                    repeat=False,
                    )


    def readData(self, **kwargs):
        cc_data = []
        o_data = []
        for cc_image, o_image in \
                zip(self.__cc_images, self.__o_images):
            cc_data.append(cc_image.read())
            o_data.append(o_image.read())

        return (cc_data, o_data)

    def frame(self, data=None, border=API.DEFAULT):
        if data is None:
            data = self.read()

        cc_data, o_data = data
        image, variables = cc_data[0]
        X, Y = variables

        deg = []
        for image in self.__cc_images:
            level = image.biasVoltage()
            deg.append(level.degeneracy(spin_resolved=True))

        spin_deg = np.array([item for item in deg if not isinstance(item, CalcTrollException)])
        size = spin_deg.max(axis=0)
        cols = size.sum() + 1
        rows = len(self.__cc_images)

        Xsp = X.max() - X.min()
        Ysp = Y.max() - Y.min()

        frame = [[0, 0], [Xsp*cols, Ysp*rows]]

        return frame

    def jpg_save(self, filename, data=None, frame=None, border=API.DEFAULT, **kwargs):
        from matplotlib import pylab as plt
        if data is None:
            data = self.read()

        if frame is None:
            frame = self.frame(data=data, border=border)
        frame = np.array(frame)

        pmin, pmax = frame
        d = pmax - pmin
        dx = d[0]
        dy = d[1]
        ratio = dx/dy
        fig = plt.figure(frameon=False, figsize=(4*ratio, 4))
        fig.patch.set_facecolor('white')

        ax = plt.Axes(fig, [0., 0., 1., 1.])
        fig.add_axes(ax)

        self.plot(data=data, ax=ax, frame=frame, **kwargs)

        ax.set_xlim((pmin[0], pmax[0]))
        ax.set_ylim((pmin[1], pmax[1]))

        plt.savefig(filename)
        plt.close('all')

        return frame
