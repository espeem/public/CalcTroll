import numpy as np
from scipy.signal import convolve as scipy_convolve
from CalcTroll import API
from CalcTroll.Core.Utilities import vectorLength, convolve

from .Utilities import gridSpacingCell, phaseFactor
from dask import delayed
del_convolve = delayed(convolve)


def compHeavyPazSoler(A, B, f1, f2, center, dV, block_height, block_start, use_dask=False):
    if use_dask:
        convolver = del_convolve
    else:
        convolver = convolve

    sh = np.array(A.shape)
    sh[2] = block_height
    u = np.zeros(sh, dtype=A.dtype)
    # Term 1, A @ (phase*GFc).

    u = convolver(A, f1[::-1, ::-1, ::-1], center=center) * dV

    # Term 2. -B @ (phase*grad_GF - ikGFc) for each spatial direction.
    for i in range(3):
        u -= convolver(B[i], f2[i, ::-1, ::-1, ::-1], center=center) * dV

    u = u[:, :, -block_height:]

    return u

class Probe(API.Parameters):
    def propagateWaveFunction(
            self,
            number,
            A,
            B,
            kpt,
            r,
            kappa=1,
            block_height=20,
            block_start=0,
            use_dask=False,
        ):
        dh_cell = gridSpacingCell(r)
        dh = np.array(list(map(vectorLength, dh_cell)))
        new_var = self.xyz(
                r,
                block_height=block_height,
                block_start=block_start,
                )
        shape = new_var.shape[1:]
        tip_shape = list(shape)
        tip_shape[2] = block_height + A.shape[2]

        dV = dh[0]*dh[1]*dh[2]
        tip_offset = block_start - A.shape[2]

        GF, grad_GF, center, o_variables = \
            self.vacuumGreensFunctionTip(
                    number=number,
                    kappa=kappa,
                    dh=dh,
                    shape=tip_shape,
                    tip_offset=tip_offset,
                    )

        # Apply kpt phase-factor to the tip-kernel in stead of the substrate.
        # This is more convenient since we can work with periodic funtions for the substrate
        # rather than always worry about the boundary conditions. Tip functions are
        # non-periodic and should therefore vanish at boundary anyway.
        factor = phaseFactor(kpt, o_variables)

        GFc = np.conjugate(GF)
        grad_GFc = np.conjugate(grad_GF)

        f1 = factor*GFc
        ikGFc = 1j*np.reshape(np.outer(kpt, GFc), grad_GFc.shape)
        f2 = factor*(grad_GF - ikGFc)
        center = reverseCenter(np.array(GFc.shape), center)

        u = compHeavyPazSoler(
                A=A,
                B=B,
                f1=f1,
                f2=f2,
                dV=dV,
                center=center,
                block_height=block_height,
                block_start=block_start,
                use_dask=use_dask,
                )

        return u, new_var

    def displacements(self):
        return [(0.0, 0.0, 0.0)]

    def baseProbe(self):
        return self

    def numberOfWavefunctions(self):
        return 1

    def xyz(self, r, block_height, block_start):
        dh_cell = gridSpacingCell(r)
        dh = np.array(list(map(vectorLength, dh_cell)))
        shape = np.array(r.shape[1:])
        shape[2] = block_height
        #if block_start == 0:
        #    shape[2] += block_height
        #else:
        #    shape[2] = block_height

        o_shape = (shape[0], shape[1], shape[2])
        origin = [entry.min() for entry in r]
        origin[2] += block_start*dh[2]
        new_var = xyz(o_shape, dh, origin=origin)

        return new_var

    def propagateWFToPlane(
            self,
            number,
            A,
            B,
            kpt,
            r,
            kappa=1,
            height=5.0,
        ):
        dh_cell = gridSpacingCell(r)
        dh = np.array(list(map(vectorLength, dh_cell)))
        dV = dh[0]*dh[1]*dh[2]
        shape = A.shape
        offset_height = height - r[2, 0, 0, -1]
        GF, grad_GF, center, o_variables = self.vacuumGreensFunctionHeight(
                number=number,
                kappa=kappa,
                dh=dh,
                shape=shape,
                offset_height=offset_height,
                )
        GFc = np.conjugate(GF)
        grad_GFc = np.conjugate(grad_GF)

        # Apply kpt phase-factor to the tip-kernel in stead of the substrate.
        # This is more convenient since we can work with periodic funtions for the substrate
        # rather than always worry about the boundary conditions. Tip functions are
        # non-periodic and should therefore vanish at boundary anyway.
        factor = phaseFactor(kpt, o_variables)
        f1 = factor*GFc
        ikGFc = 1j*np.reshape(np.outer(kpt, GFc), grad_GFc.shape)
        f2 = factor*(grad_GF - ikGFc)

        # Pick only the top plane.
        sl = (slice(None, None), slice(None, None), -1)

        # Paz/Soler (9)
        center = reverseCenter(np.array(GFc.shape), center)
        # Term 1, A @ (phase*GFc).
        new_u = convolve(A, f1[::-1, ::-1, ::-1], center=center)[sl] * dV

        # Term 2. -B @ (phase*grad_GF - ikGFc) for each spatial direction.
        for i in range(3):
            new_u -= convolve(B[i], f2[i, ::-1, ::-1, ::-1], center=center)[sl] * dV

        new_var = r[:2, :, :, -1]

        return new_u, new_var

    def vacuumGreensFunctionHeight(self, number, kappa, dh, shape, offset_height):
        size = np.array(shape) * dh
        s = [slice(0, s - shift, sh * 1j) for s, shift, sh in zip(size, dh, shape)]
        x, y, z = np.mgrid[s[0], s[1], s[2]]
        x -= x[shape[0]//2,0,0]
        y -= y[0, shape[1]//2, 0]
        z -= z[0, 0, -1] + offset_height
        diff_r = np.array([x, y, z])

        func, grad_func = self.vacuumGreensFunction(number, kappa, diff_r)

        center = np.array(func.shape)//2
        center[2] = func.shape[2] - 1

        return func, grad_func, center, diff_r

    def vacuumGreensFunctionTip(self, number, kappa, dh, shape, tip_offset):
        size = np.array(shape) * dh
        s = [slice(0, s - shift, sh * 1j) for s, shift, sh in zip(size, dh, shape)]
        offset_height = dh[2] * tip_offset
        x, y, z = np.mgrid[s[0], s[1], s[2]]
        x -= x[shape[0]//2,0,0]
        y -= y[0, shape[1]//2, 0]
        z -= z[0, 0, -1] + offset_height
        diff_r = np.array([x, y, z])

        func, grad_func = self.vacuumGreensFunction(number, kappa, diff_r)

        center = np.array(func.shape)//2
        center[2] = func.shape[2] - 1

        return func, grad_func, center, diff_r

    def vacuumGreensFunction(self, number, kappa, diff_r):
        raise NotImplementedError

    def generate(self, data):
        return data


class TersoffHamann(Probe):
    def shorthand(self):
        return 'S'

    def __init__(self, density_pr_current=1e-9):
        self.__density_pr_current = float(density_pr_current)
        Probe.__init__(self)

    def explain(self):
        dc = self.densityPrCurrent()/1e-9
        return API.Explanation("We used the Tersoff-Hamann approximation [*] assuming a proportionality factor of %.2f nA*Ang^(-3) for the ratio between the local density of states and the current." % dc,
                ["J. Tersoff and D. R. Hamann, Phys. Rev. Lett., 1983, 50, 1998-2001."])

    def vacuumGreensFunction(self, number, kappa, diff_r):
        assert number == 0
        x, y, z = diff_r
        ra = np.sqrt(x**2 + y**2  + z**2)
        take = ra==0
        ra[take] = 0.1
        prefactor = 1/(4*np.pi)
        func = prefactor*(1/ra*np.exp(-kappa*ra))
        grad_func = -prefactor*(ra**(-3) + kappa * ra**(-2)) * np.exp(-kappa*ra)
        grad_func = np.array([entry*grad_func for entry in diff_r])

        return func, grad_func

    def densityPrCurrent(self):
        return self.__density_pr_current

    def wavefunctionContourValue(self, current):
        """
        Conveniece function to find the absolute value of the
        wavefunction that correspond to a given current.
        :param current:
        :return:
        """
        rho_value = current * self.densityPrCurrent()
        wf_value = np.sqrt(rho_value)
        precision = -int(np.floor(np.log10(wf_value))) + 6

        return np.around(wf_value, precision)


class GeometricTip(TersoffHamann):
    def name(self):
        name = self.__system.name()
        if self.__rotation is not None:
            name += '_%d' % self.__rotation

        return name

    def displacements(self):
        pos = - self.atoms().get_positions()
        pos = pos[pos[:, 2] < 5]

        return pos


    def __init__(
            self,
            system,
            rotation=None,
            density_pr_current=1e-9,
            ):
        self.__system = system
        self.__atoms = None
        self.__rotation = rotation

        TersoffHamann.__init__(
                self,
                density_pr_current=density_pr_current,
                )

    def system(self):
        return self.__system

    def atoms(self):
        if self.__atoms is None:
            atoms = self.system().atoms()
            r0 = atoms.get_positions()[0]
            assert vectorLength(r0) < 1e-5
            self.__atoms = atoms

        return self.__atoms


    def generate(self, data):
        I, variables = data
        atoms = self.atoms()

        Itot = np.zeros(I.shape)
        dh_cell = gridSpacingCell(variables)
        for r in atoms.positions:
            Icont = I.copy()
            for i in range(3):
                n = int(np.around(np.dot(r, dh_cell[i])/vectorLength(dh_cell[i])**2))
                Icont = np.roll(Icont, n, axis=i)

            if n < 0:
                Icont[:, :, n:] = 0

            Itot += Icont

        return Itot, variables


class PWaveTip(Probe):
    """ Model probe that emulates a real tip which has a
    dominant frontier orbital with p-type symmetry """

    def shorthand(self):
        return 'P'

    def densityPrCurrent(self):
        return 1e-9

    def vacuumGreensFunction(self, number, kappa, diff_r):
        assert number == 0
        x, y, z = diff_r
        r = np.sqrt(x**2 + y**2  + z**2)
        take = r==0
        r[take] = 0.1
        harmonic = z/r
        harm_pre = (np.sqrt(3/(4*np.pi)))
        prefactor = 1/np.sqrt(4*np.pi)*harm_pre
        func = prefactor*(1/r*np.exp(-kappa*r))*harmonic

        # Analytical gradient of above expression.
        grad_term1 = -z/r**3*(2/r + kappa)
        grad_term1 = np.array([entry*grad_term1 for entry in diff_r])

        grad_term2 = np.zeros([3] + list(r.shape))
        grad_term2[2] = 1/r**2
        grad_func = prefactor*(grad_term1 + grad_term2)*np.exp(-kappa*r)

        return func, grad_func

class DWaveTip(Probe):
    """ Model probe that emulates a real tip which has a
    dominant frontier orbital with p-type symmetry """

    def shorthand(self):
        return 'D'

    def densityPrCurrent(self):
        return 1e-9

    def vacuumGreensFunction(self, number, kappa, diff_r):
        assert number == 0
        x, y, z = diff_r
        r = np.sqrt(x**2 + y**2  + z**2)
        take = r==0
        r[take] = 0.1
        harm = 3*(z/r)**2 - 1
        harm_pre = np.sqrt(5/(16*np.pi))
        radial = (1/r*np.exp(-kappa*r))
        radial_pre = 1/np.sqrt(4*np.pi)
        prefactor = radial_pre*harm_pre
        func = prefactor*radial*harm

        # Analytical gradient of harmonic.
        hd_term1 =  -6*(z**2/r**4)
        hd_term2 = 6*(z/r**2)
        hd = np.array([entry*hd_term1 for entry in diff_r])
        hd[2] +=  hd_term2

        # Analytical gradient of 1/r*exp(-kappa*r)
        rd = -(kappa + 2/r)*(1/r**2)*np.exp(-kappa*r)
        rd = np.array([entry*rd for entry in diff_r])

        fd = rd*harm + radial*hd

        return func, fd

def reverseCenter(shape, center):
    return shape - center - 1


def xyz(shape, dh, origin=None):
    abc = np.mgrid[0:shape[0],
          0:shape[1],
          0:shape[2],
          ]

    xyz = np.array([d * a for d, a in zip(dh, abc)])

    if origin is not None:
        xyz[0] += origin[0]
        xyz[1] += origin[1]
        xyz[2] += origin[2]

    return xyz
