import os
import sys
import itertools
import fnmatch
import shutil
import array
import math
from collections import OrderedDict
from os.path import join
from scipy.io import netcdf
from scipy.interpolate import RectBivariateSpline
from scipy.interpolate import interpn
from ase.units import kB
import numpy as np

from CalcTroll.Plugins.Constants import me, hbar
from CalcTroll.Core.Utilities import vectorLength, xy_z, fermi, gaussian, convolve, unique
from CalcTroll.Core.Flags import ALL
from CalcTroll.Core.Flags import TooSmallBoundingBox
from CalcTroll.Core.Utilities import fermi, convolve, diff_fermi, gaussian
from CalcTroll.API import DEFAULT

from CalcTroll.Core.Utilities import aperiodic_convolve1D


def testPlot(u, r, cp):
    from matplotlib import pyplot as plt
    from CalcTroll.Core.Visualizations import padVariables
    ax = plt.gca()
    X, Y = r[:2, :,:, 0]
    ax.set_xlim((X.min(), X.max()))
    ax.set_ylim((Y.min(), Y.max()))
    vmax = abs(u[:,:, cp].real).max()
    vmin = -vmax
    ax.pcolormesh(r[0, :,:, cp], r[1, :,:, cp], u[:,:, cp].real, vmin=vmin, vmax=vmax, cmap='seismic')
    ax.set_aspect('equal')
    plt.show()
    dddddddddddddddd


def propagateWaveFunctionToPlane(
        A,
        B,
        kpt,
        r,
        kappa,
        height,
):
    dh_cell = gridSpacingCell(r)
    dh = np.array(list(map(vectorLength, dh_cell)))

    dV = dh[0]*dh[1]*dh[2]

    GF, grad_GF, center, o_variables = vacuumGreensFunctionHeight(
        kappa=kappa,
        r=r,
        height=height,
    )

    GFc = np.conjugate(GF)
    grad_GFc = np.conjugate(grad_GF)

    factor = phaseFactor(kpt, o_variables)
    f1 = factor*GFc
    ikGFc = 1j*np.reshape(np.outer(kpt, GFc), grad_GFc.shape)
    f2 = factor*(grad_GF - ikGFc)

    # Paz/Soler (9)
    center = reverseCenter(np.array(GF.shape)[:2], center)
    f1 = f1[::-1, ::-1]
    f2 = f2[:,::-1, ::-1]
    new_u = np.zeros(r.shape[1:3], complex)
    for i in range(r.shape[3]):
        term1 = convolve(A[:,:,i], f1[:, :, i], center=center)
        term2a = np.array([convolve(B[j,:,:,i], f2[j, :, :, i], center=center) for j in range(3)])
        term2 = term2a.sum(axis=0)
        new_u += (term1 - term2)*dV

    r = r[:2, :, :, 0]

    return new_u, r

def eigContributionToIVCurve(I, eigs, bias_range, bias_broadening, temperature):
    include = 6
    br = bias_range
    Npoints = int(np.ceil(((br[1] - br[0])/bias_broadening  + 2* include) * 100))
    T=temperature
    kBT = kB * T
    Vs = np.linspace(br[0] - 6 * bias_broadening, br[1] + 6 * bias_broadening, Npoints)
    dv = Vs[1] - Vs[0]
    X = np.zeros(Vs.shape)

    for i in range(len(eigs)):
        X += - I[i]*(fermi((Vs - eigs[i])/kBT) - fermi((0 - eigs[i])/kBT))

    b_voltages = np.arange(-6*bias_broadening, 6*bias_broadening + dv, dv)
    G = gaussian(b_voltages, 0, bias_broadening)
    I = aperiodic_convolve1D(X, G).real * dv

    take = np.logical_and(Vs > br[0], Vs < br[1])

    return I[take], Vs[take]


def eigContributionToDIdVCurve(I, eigs, bias_range, bias_broadening, temperature):
    I, Vs = eigContributionToIVCurve(I, eigs, bias_range, bias_broadening, temperature)
    dv = Vs[1] - Vs[0]
    dIdV = np.gradient(I, dv)

    return dIdV, Vs

def interpolateToHeight(data, height):
    values, r = data
    X, Y, Z = r
    z = Z[0, 0, :]
    h_index = (z < height).sum() - 1
    z1, z2 = z[h_index], z[h_index + 1]
    v1, v2 = values[:, :, h_index], values[:, :, h_index + 1]
    d = z2 - z1
    w1, w2 = (z2-height)/d, (height - z1)/d
    take = np.logical_and(v1>0, v2>0)
    v = np.zeros(v1.shape)
    v[take] = w1 * np.log(v1[take]) + w2 * np.log(v2[take])
    v[take] = np.exp(v[take])

    r = np.array([X[:,:,0], Y[:,:,0]])

    return v, r

def constructCell(r):
    dh_cell = gridSpacingCell(r)
    x, y, z = r[0, :, 0 , 0], r[1, 0, : , 0], r[2, 0, 0 , :]

    cell = np.array((
            (x.max() - x.min(), 0, 0),
            (0, y.max() - y.min(), 0),
            (0, 0, z.max() - z.min()),
            ))
    cell += dh_cell

    return cell

def grid2Line(position, values, r, sigma):
    cell = constructCell(r)[:2,:2]
    xy = r[:2, :, :, 0]
    rs = r[2, 0, 0, :]
    diff = np.array([xy[i] - position[i] for i in range(2)])
    M = np.matrix(cell).T

    fr = np.tensordot(M**(-1), diff, (1, 0))
    fr = (fr + 0.5) % 1 - 0.5
    r = np.tensordot(M, fr, (1, 0))
    gauss = gaussian(r, r0=(0, 0), sigma=sigma)
    gauss /= gauss.sum()

    result = np.tensordot(gauss, values, axes=([0, 1], [0, 1]))

    return result, rs

def grid2Line2(position, values, r):
    rs = np.array([list(position) + [0]] * r.shape[3])
    rs[:, 2] = r[2, 0, 0, :]
    x, y, z = r[0, :, 0 , 0], r[1, 0, : , 0], r[2, 0, 0 , :],
    x1 = (x < position[0]).sum() - 1
    y1 = (y < position[1]).sum() - 1
    values = values[x1:x1+2, y1:y1+2]
    x = np.array([r[0, x1, 0, 0], r[0, x1 + 1, 0, 0]])
    y = np.array([r[1, 0, y1, 0], r[1, 0, y1 + 1, 0]])
    result = interpn((x, y, z), values, xi=rs)

    return result, rs

def integrated_weights_array(eigenvalues, voltages, neutral_level, voltage_broadening, temperature):
    N = neutral_level
    T = temperature
    kBT = kB * T
    sign = np.sign(eigenvalues - N)

    e_diff = - sign * np.subtract.outer(voltages, eigenvalues)
    de = voltage_broadening/100

    w_e = sign * fermi(e_diff/kBT)
    b_energies = np.arange(-5*voltage_broadening, 5*voltage_broadening + de, de)
    w_b = gaussian(b_energies, 0, voltage_broadening)

    w = aperiodic_convolve1D(w_e, w_b).real*de

    return w


def integrated_weights(energies, voltage, neutral_level, voltage_broadening, temperature):
    """ Return the contribution from different wavefunctions in the
        bias interval.
    """
    N = neutral_level
    T = temperature
    V = voltage
    voltage_broadening = float(voltage_broadening)
    e_min = np.min([N, N + V])
    e_max = np.max([N, N + V])
    de = voltage_broadening/100
    kBT = kB * T
    safety = 6*(voltage_broadening + kBT)
    e_win = np.arange(e_min - safety, e_max + safety, de)
    w_e = fermi((e_win - e_max)/kBT) - fermi((e_win - e_min)/kBT)
    b_energies = np.arange(-5*voltage_broadening, 5*voltage_broadening, de)
    w_b = gaussian(b_energies, 0, voltage_broadening)
    w = convolve(w_e, w_b).real*de
    w_e = np.interp(energies, e_win, w, left=0, right=0)

    return w_e

def differential_weights(energies, voltage, neutral_level, voltage_broadening, temperature):
    """ Return the contribution from different wavefunctions in the
        bias interval - differetiated with respect to voltage.
    """
    V = voltage
    N = neutral_level
    T = temperature
    voltage_broadening = float(voltage_broadening)
    E = V + N
    de = voltage_broadening/100
    kBT = kB * T
    safety = 6*(voltage_broadening + kBT)
    e_win = np.arange(E - safety, E + safety, de)
    w_e = diff_fermi((e_win - E)/kBT)*(-1/kBT)
    b_energies = np.arange(-5*voltage_broadening, 5*voltage_broadening, de)
    w_b = gaussian(b_energies, 0, voltage_broadening)
    w = convolve(w_e, w_b).real*de

    w_e = np.interp(energies, e_win, w, left=0, right=0)

    return w_e

def add_background(z, z0, voltage, neutral_level, voltage_broadening, temperature):
    """ Add an xy-uniform background density which decays in the
        z-direction.
    """
    V = voltage
    N = neutral_level
    T = temperature
    de = voltage_broadening/100.
    kBT = kB * T
    safety = 6*(voltage_broadening + kBT)
    E = V + N
    Emin = min(N, E) - safety
    Emax = max(N, E) + safety
    if Emax > -de:
        Emax = -de
    e = np.arange(Emin, Emax, de)
    Z, E = np.meshgrid(z, e, indexing='ij')
    kappa = getVacuumDecayRate(E)
    rho_e = (np.exp(-kappa*(Z - z0)))**2
    i_w = integrated_weights(
            energies=e,
            voltage=V,
            neutral_level=N,
            voltage_broadening=voltage_broadening,
            temperature=T)

    d_w = differential_weights(
            energies=e,
            voltage=V,
            neutral_level=N,
            voltage_broadening=voltage_broadening,
            temperature=T)

    rho = np.tensordot(i_w, rho_e, (0, 1))*de
    rho_diff = np.tensordot(d_w, rho_e, (0, 1))*de

    result = np.array([rho, rho_diff])

    return result


def gridSpacingCell(variables):
    """ Takes an array representing coordinates and return the
    distance between grid point.
    """
    variables = np.array(variables)
    if len(variables) == 2:
        dh_cell = [
                   variables[:, 1, 0] - variables[:, 0, 0],
                   variables[:, 0, 1] - variables[:, 0, 0],
                  ]
    elif len(variables) == 3:
        dh_cell = [
                   variables[:, 1, 0, 0] - variables[:, 0, 0, 0],
                   variables[:, 0, 1, 0] - variables[:, 0, 0, 0],
                   variables[:, 0, 0, 1] - variables[:, 0, 0, 0],
                  ]
    else:
        raise ValueError

    return np.array(dh_cell)

def isGloballySpinPolarized(eigs):
    """
    Takes an array of energy eigen-values and evaluate if the
    system is spin-polarized. Note that the system could still
    be locally polarized.
    """
    eigs = np.array(eigs)
    if (eigs[:, 3] == 0).all():
        return False

    spin_1 = eigs[eigs[:, 3] == 0, 0]
    spin_2 = eigs[eigs[:, 3] == 1, 0]

    diff = spin_1 - spin_2

    return abs(diff).max() > 0.01

def isLocallySpinPolarized(rho):
    """
    Takes a spin-resolved density of  and evaluate if the
    system is spin-polarized. Note that the system could still
    be locally polarized.
    """
    assert len(rho.shape) == 4

    rho_sum = rho[0] + rho[1]
    rho_max = rho_sum.max()
    rho_diff = rho[0] - rho[1]
    rho_diff_max = rho_diff.max()
    polarized = rho_diff_max > 0.01*rho_max

    return polarized

def isSpinPolarized(eigs, rho):
    """
    Evaluates if spin-dependent energy eigenvalues and
    spin-resolved electron density belongs to a
    locally or glablaay spin-polarized system.
    """

    if len(rho.shape) == 3:
        return False

    if isGloballySpinPolarized(eigs):
        return True

    return isLocallySpinPolarized(rho)

def handleSpinPolarization(eigs, rho):
    """
    If the system is not spin-polarized then
    collapse the density and handle the spin with
    a spin degeneracy factor.
    """
    if isSpinPolarized(eigs, rho):
        w_s = 1
    else:
        if len(rho.shape) == 4:
            rho = rho.sum(axis=0)
        w_s = 2

    return eigs, rho, w_s

def getVacuumDecayRate(energy):
    """
    Returns the vacuum decay rate for a given energy.
    """
    kappa = np.sqrt(-2*me/hbar**2*energy)

    return kappa

def findMinimumHeight(filename):
    """
    """
    file_exists = os.path.exists(filename)
    with netcdf.netcdf_file(filename, 'r', mmap=False) as f:
        nz_points = f.variables['thick_surface'][:].shape[2]
        origin_z = f.variables['origin'][2]
        dz = f.variables['grid_cell'][2, 2]

    min_z = origin_z + (nz_points - 1) * dz

    return min_z


def findBardeenVariables(filename):
    """
    Returns the xyz-variables consistent with the wavefunctions
    saved an netcdf-file.
    """
    with netcdf.netcdf_file(filename, 'r', mmap=False) as f:
        thick_surface = np.array(f.variables['thick_surface'][:], bool)
        sh = thick_surface.shape
        grid_cell = f.variables['grid_cell'][:]
        origin = f.variables['origin'][:]
        abc = np.mgrid[
              0:sh[0],
              0:sh[1],
              0:sh[2],
              ]
        r = np.tensordot(grid_cell, abc, (0, 0))
        r[0] += origin[0]
        r[1] += origin[1]
        r[2] += origin[2]

    return r


def simpleTopographyFrom3D(
        data,
        contour_value=None,
        ):
    """
    Calculate a topography 2D-image, based on 3D current data.
    """
    iso_values, r = data
    x, y, z = r

    sh = np.array(z.shape)
    top = np.zeros(sh[:2])
    try:
        for i, j in itertools.product(list(range(sh[0])), list(range(sh[1]))):
            top[i, j] = findTopographyHeight(
                iso_values=iso_values[i, j],
                z=z[i, j],
                contour_value=contour_value,
            )
    except TooSmallBoundingBox:
        zspan = '%2.1f,%2.1f' % (z[0, 0, 0], z[0, 0, -1])
        Imax = np.max(iso_values[:,:,-1])
        convergence = 'Z-Boundary Value Exceeded (z=%s)(max/limit=%2.f/%2.f)' % (zspan, Imax , contour_value)
        raise TooSmallBoundingBox(convergence)

    variables = np.array([x[:,:, 0], y[:,:, 0]])

    return top, variables


def wavefunctionTopography(
        grids,
        index,
        contour_value=None,
):
    """
    Calculate a topography 2D-image, based on 3D current data.
    This topography image is a complex number with the argument
    being carried over from the 3D data.
    """
    var = grids.file().variables[str(index)]
    # Assert the wavefunction is real (gamma-point calculation).
    assert abs(var[:, :, :, 0]).sum() * 1e-10 > abs(var[:, :, :, 1]).sum()
    z = grids.z()

    sh = np.array(var.shape)
    top = np.zeros(sh[1:3])
    for i, j in itertools.product(range(sh[1]), range(sh[2])):
        pos_values =  np.array(var[:, i, j, 0])
        neg_values = -pos_values.copy()

        pos_values[pos_values < 0] = contour_value/2.
        neg_values[neg_values < 0] = contour_value/2.

        top_pos = findTopographyHeight(
            iso_values=pos_values,
            z=z,
            contour_value=contour_value,
        )
        top_neg = findTopographyHeight(
            iso_values=neg_values,
            z=z,
            contour_value=contour_value,
        )
        if top_pos > top_neg:
            top[i, j] = top_pos
        elif top_pos < top_neg:
            top[i, j] = -top_neg
        else:
            top[i, j] = -np.inf

    xy = grids.xy()

    return top, xy


def findTopographyHeight(
        iso_values,
        z,
        contour_value,
        values=None,
    ):
    """
    Calculate the height corresponding a specific iso_value.
    This function operates on one 1D-array at a time.
    """
    if values is None:
        values = z
    assert values.shape == iso_values.shape

    indices = np.arange(len(iso_values))

    if iso_values[-1] > contour_value:
        raise TooSmallBoundingBox('The height of the calculation bounding is insufficient.')

    smaller = iso_values < contour_value
    take = np.logical_xor(smaller[1:], smaller[:-1])
    cv = np.log(contour_value)

    if take.sum() > 0:
        index = indices[:-1][take][-1]
        v1 = np.log(iso_values[index])
        v2 = np.log(iso_values[index+1])
        w2 = (cv - v1)/(v2 - v1)
        w1 = 1 - w2

        result = w1*values[index] + w2*values[index+1]
    else:
        result = -np.inf

    return result


def evaluateOnIsoSurface(
        values,
        iso_values,
        variables,
        contour_value=None,
):
    """
    Evaluate a the value of a 3D on the iso-surface of another
    3D array.
    """
    x, y, z = variables
    sh = np.array(z.shape)
    res = np.zeros(sh[:2])

    for i, j in itertools.product(list(range(sh[0])), list(range(sh[1]))):
        res[i, j] = findTopographyHeight(
            iso_values=iso_values[i, j],
            z=z[i, j],
            contour_value=contour_value,
            values=values[i, j],
        )

    variables = np.array([x[:,:, 0], y[:,:, 0]])

    return res, variables


def domainSurface(domain, variables):
    dh_cell = gridSpacingCell(variables)
    dh = np.array(vectorLength(dh_cell))
    dV = dh[0]*dh[1]*dh[2]

    k = np.array([-1, 1])
    c = np.zeros([3] + list(domain.shape), dtype=float)
    for i, shape in enumerate([(2,1,1), (1,2,1), (1,1,2)]):
        kernel = np.reshape(k, shape)

        result = convolve(domain, kernel).real
        result = np.array(np.round(result, 10), dtype=int)
        take_pos = np.array(result == 1, int)
        take_neg = np.array(result == -1, int)
        take_neg = np.roll(take_neg, 1, axis=i)
        c[i] = (take_pos - take_neg)*(dV/dh[i])

    take = c[0] != 0
    take = np.logical_and(take, c[1] != 0)
    take = np.logical_and(take, c[2] != 0)
    c[:, take] /= np.sqrt(2)

    return c, variables, domain


def surfaceIntegratorTest(rho, variables, rho0=1e-4, DS=1):
    domain = rho > rho0

    return domainSurface(domain, variables)

def correctOrigin(r, origin):
    r = r.copy()
    r[0] -= origin[0]
    r[1] -= origin[1]
    r[2] -= origin[2]

    return r

def surfaceIntegrator(rho, variables, rho0=1e-4, DS=1):
    dh_cell = gridSpacingCell(variables)
    dh = np.array(list(map(vectorLength, dh_cell)))
    S = np.ones(rho.shape)*(-1e99)
    dS = np.zeros(rho.shape)
    c = np.zeros([3] + list(rho.shape))

    take = rho > 0
    S[take] = np.log(rho[take]/rho0)

    thick_surface = np.logical_and(S < DS, S > -DS)
    dS[thick_surface] = 15./(16.*DS)*(1 - (S[thick_surface]/DS)**2)

    grad_rho = np.gradient(rho, *dh)
    for i in range(3):
        c[i][thick_surface] = dS[thick_surface]*grad_rho[i][thick_surface]/rho[thick_surface]

    return c, variables, thick_surface


def phaseFactor(kpt, r):
    r = np.array(r)
    dot = np.tensordot(kpt, r, (0, 0))
    phase_factor = np.exp(1j*dot)

    return phase_factor


def repeatData(values, r, cells=((-1, 1), (-1, 1))):
    r = np.array(r)
    dh_cell = gridSpacingCell(r)
    shape = values.shape
    cells = np.array(cells)
    ran = np.array(cells)
    ran[:, -1] = cells[:, -1] + 1
    repeats = ran[:, -1] - ran[:, 0]
    if len(r) == 3:
        cell = np.array([r[:, -1,  0, 0],  r[:,  0, -1, 0]]) - r[:, 0, 0, 0]
        cell += dh_cell[:2]
        r = np.tile(r, (1, repeats[0], repeats[1], 1))
        values = np.tile(values, (repeats[0], repeats[1], 1))
    elif len(r) == 2:
        cell = np.array([r[:, -1,  0],  r[:,  0, -1]]) - r[:, 0, 0]
        cell += dh_cell[:2]
        r = np.tile(r, (1, repeats[0], repeats[1]))
        values = np.tile(values, (repeats[0], repeats[1]))
    else:
        raise ValueError

    for i in range(*ran[0]):
        i_s = i - ran[0, 0]
        for j in range(*ran[1]):
            j_s = j - ran[1, 0]
            sl_x = slice(i_s*shape[0], (i_s+1)*shape[0])
            sl_y = slice(j_s*shape[1], (j_s+1)*shape[1])
            shift = np.dot([i, j], cell)
            r[0, sl_x, sl_y] += shift[0]
            r[1, sl_x, sl_y] += shift[1]

    return values, r


def periodic2DExpand(values, r, padding):
    size = [(r[i].max() - r[i].min()) for i in range(2)]
    extra_cells = np.array([np.around(padding/size[i]) for i in range(2)])
    cells = tuple([(0, int(extra_cells[i])) for i in range(2)])

    return repeatData(values=values, r=r, cells=cells)


def mixedPeriodicExpand(values, r, padding):
    size = (r[0].max() - r[0].min())
    extra_cells = np.around(padding/size)
    each_direction = np.ceil(extra_cells/2)
    cells = (-int(each_direction), int(each_direction))
    cells = (cells, (0, 0))
    values, r = repeatData(values=values, r=r, cells=cells)

    dh_cell = gridSpacingCell(r)
    y_points = int(np.ceil(padding/dh_cell[1, 1]))
    y_pad = y_points * dh_cell[1, 1]
    x_range = (r[0].min(), r[0].max())
    y_range = (r[1].min() - y_pad, r[1].max() + y_pad)
    z_range = (r[2].min(), r[2].max())

    shape = list(r.shape[1:])
    shape[1] += 2 * y_points
    span = [x_range, y_range, z_range]

    rr = xyz(span=span, shape=shape)

    new_values = np.zeros(rr.shape[1:], values.dtype)
    new_values[:, y_points:-y_points, :] = values

    return new_values, rr


def nonPeriodicExpand(values, r, padding):
    """
    Put a buffer of 0-value around an array.
    """
    dh_cell = gridSpacingCell(r)
    x_points = int(np.ceil(padding/dh_cell[0, 0]))
    y_points = int(np.ceil(padding/dh_cell[1, 1]))
    x_pad =  x_points * dh_cell[0, 0]
    y_pad = y_points * dh_cell[1, 1]
    x_range = (r[0].min() - x_pad, r[0].max() + x_pad)
    y_range = (r[1].min() - y_pad, r[1].max() + y_pad)
    z_range = (r[2].min(), r[2].max())

    shape = list(r.shape[1:])
    shape[0] += 2 * x_points
    shape[1] += 2 * y_points
    span = [x_range, y_range, z_range]

    rr = xyz(span=span, shape=shape)

    new_values = np.zeros(rr.shape[1:], values.dtype)
    new_values[x_points:-x_points, y_points:-y_points, :] = values

    return new_values, rr


def expandData(values, r, padding=5, periodic=(False, False)):
    """
    Put a buffer of 0-value around an array.
    """
    if periodic == (False, False):
        return nonPeriodicExpand(values=values, r=r, padding=padding)
    elif periodic == (True, True):
        return periodic2DExpand(values=values, r=r, padding=padding)
    elif periodic == (True, False):
        return mixedPeriodicExpand(values=values, r=r, padding=padding)

    raise NotImplementedError


def vacuumGreensFunction(kappa, diff_r):
    x, y, z = diff_r
    ra = np.sqrt(x**2 + y**2  + z**2)
    take = ra==0
    ra[take] = 0.1
    prefactor = 1/(4*np.pi)
    func = prefactor*(1/ra*np.exp(-kappa*ra))
    grad_func = -prefactor*(ra**(-3) + kappa * ra**(-2)) * np.exp(-kappa*ra)
    grad_func = np.array([entry*grad_func for entry in diff_r])

    return func, grad_func


def vacuumGreensFunctionHeight(kappa, r, height):
    center = np.array(r.shape)[1:3]/2
    x0 = r[0, center[0], center[1], 0]
    y0 = r[1, center[0], center[1], 0]
    z0 = height

    diff_r = np.array([r[0] - x0, r[1] - y0, r[2] - z0])

    func, grad_func = vacuumGreensFunction(kappa, diff_r)

    return func, grad_func, center, diff_r

def propagateWaveFunctionToPlane(
        A,
        B,
        kpt,
        r,
        kappa,
        height,
):
    dh_cell = gridSpacingCell(r)
    dh = np.array(list(map(vectorLength, dh_cell)))

    dV = dh[0]*dh[1]*dh[2]

    GF, grad_GF, center, o_variables = vacuumGreensFunctionHeight(
        kappa=kappa,
        r=r,
        height=height,
    )

    GFc = np.conjugate(GF)
    grad_GFc = np.conjugate(grad_GF)

    factor = phaseFactor(kpt, o_variables)
    f1 = factor*GFc
    ikGFc = 1j*np.reshape(np.outer(kpt, GFc), grad_GFc.shape)
    f2 = factor*(grad_GF - ikGFc)

    # Paz/Soler (9)
    center = reverseCenter(np.array(GF.shape)[:2], center)
    f1 = f1[::-1, ::-1]
    f2 = f2[:,::-1, ::-1]
    new_u = np.zeros(r.shape[1:3], complex)
    for i in range(r.shape[3]):
        term1 = convolve(A[:,:,i], f1[:, :, i], center=center)
        term2a = np.array([convolve(B[j,:,:,i], f2[j, :, :, i], center=center) for j in range(3)])
        term2 = term2a.sum(axis=0)
        new_u += (term1 - term2)*dV

    r = r[:2, :, :, 0]

    return new_u, r


def findSpan(r, rho, density_range, dh_cell, point_density, axis):
    min_value = 10**(density_range[0])
    max_value = 10**(density_range[1])

    assert max_value > min_value
    if axis == 0:
        values = r[axis][:, 0, 0]
        rho = np.transpose(rho, (1, 2, 0))
    elif axis == 1:
        values = r[axis][0, :, 0]
        rho = np.transpose(rho, (0, 2, 1))
    elif axis == 2:
        values = r[axis][0, 0, :]

    sh = rho.shape
    t_density = np.reshape(rho, (sh[0]*sh[1], sh[2]))
    d_min = t_density.min(axis=0)
    d_max = t_density.max(axis=0)

    sh = len(values)
    take = np.ones(len(d_min), bool)
    take = np.logical_and(take, d_min < max_value)
    take = np.logical_and(take, d_max > min_value)
    last = np.arange(len(take))[take][-1]
    take = take[:last]
    if all(take):
        first = 0
    else:
        first = np.arange(last)[np.logical_not(take)][-1] + 1

    padding = dh_cell[axis, axis]
    span = (values[first] - padding, values[last] + padding)
    n_points = np.ceil((span[1] - span[0]) * point_density + 1)

    return (span, n_points)

def xyz(shape, span):
    abc = np.mgrid[
            0:1:shape[0]*1j,
            0:1:shape[1]*1j,
            0:1:shape[2]*1j,
                   ]
    mult = np.zeros((3,3))
    for i in range(3):
        mult[i,i] = span[i][1] - span[i][0]
    result = np.tensordot(mult, abc, (0, 0))

    for i in range(3):
        result[i] += span[i][0]

    return result



def xyz_from_dh(shape, dh, origin=None):
    abc = np.mgrid[0:shape[0],
          0:shape[1],
          0:shape[2],
          ]

    xyz = np.array([d * a for d, a in zip(dh, abc)])

    if origin is not None:
        xyz[0] += origin[0]
        xyz[1] += origin[1]
        xyz[2] += origin[2]

    return xyz

def determineWaveFunctionRange(
        rho,
        r,
        point_density=10,
        density_range=(-3.5, -2.5),
        periodic=(True, True),
        limits=((None, None), (None, None), (None, None)),
        origin=(0.0, 0.0, 0.0),
        ):
    takes = []
    for axis, lim in enumerate(limits):
        if lim == (None, None):
            take = np.ones(rho.shape[axis], bool)
        else:
            if axis == 0:
                values = r[axis][:, 0, 0]
            elif axis == 1:
                values = r[axis][0, :, 0]
            elif axis == 2:
                values = r[axis][0, 0, :]

            if lim[0] == None:
                take1 = np.ones(len(values), bool)
            else:
                take1 = values > lim[0] + origin[axis]

            if lim[1] == None:
                take2 = np.ones(len(values), bool)
            else:
                take2 = values < lim[1] + origin[axis]
            take = np.logical_and(take1, take2)

        takes.append(take)

    rho = rho[takes[0]]
    rho = rho[:, takes[1]]
    rho = rho[:, :, takes[2]]

    r = r[:, takes[0]]
    r = r[:, :, takes[1]]
    r = r[:, :, :, takes[2]]

    min_value = 10**(density_range[0])
    max_value = 10**(density_range[1])
    assert max_value > min_value
    periodic = list(periodic) + [False]

    r = np.array(r)
    dh_cell = gridSpacingCell(r)
    sh = rho.shape
    cell = np.array([sh[i]*dh_cell[i] for i in range(3)])

    shape = np.zeros(3, int)
    spans = []
    for i in range(3):
        if not periodic[i]:
            span, n_points = findSpan(r, rho, density_range, dh_cell, point_density, i)
        else:
            n_points = int(np.ceil(cell[i, i] * point_density))
            span = (0, cell[i, i]/n_points * (n_points - 1))

        spans.append(span)
        shape[i] = n_points

    r = xyz(shape=shape, span=spans)

    return r


def write_gsf(filename, imagedata, xres, yres,
              xreal=None, yreal=None, xyunits=None, zunits=None, title=None):
    """Write a Gwyddion GSF file.

    filename -- Name of the output file.
    imagedata -- Image data.
    xres -- Horizontal image resolution.
    yres -- Vertical image resolution.
    xreal -- Horizontal physical dimension (optional).
    yreal -- Vertical physical dimension (optional).
    xyunits -- Unit of physical dimensions (optional).
    zunits -- Unit of values (optional).
    title -- Image title (optional).

    Image data may be passed as any listable object that can be used to form
    a floating point array.array().  This includes tuples, lists, arrays,
    numpy arrays and other stuff.
    """
    data = array.array('f', imagedata)
    if len(data) != xres*yres:
        raise ValueError("imagedata does not have xres*yres items")
    isinf = math.isinf
    isnan = math.isnan
    for z in data:
        if isinf(z) or isnan(z):
            raise ValueError("GSF files may not contain NaNs and infinities")
    if sys.byteorder == 'big':
        data.byteswap()
    header = ['Gwyddion Simple Field 1.0']
    header.append('XRes = %u' % xres)
    header.append('YRes = %u' % yres)
    if xreal is not None:
        header.append('XReal = %.12g' % xreal)
    if yreal is not None:
        header.append('YReal = %.12g' % yreal)
    if xyunits is not None:
        header.append('XYUnits = %s' % xyunits)
    if zunits is not None:
        header.append('ZUnits = %s' % zunits)
    if title is not None:
        header.append('Title = %s' % title)

    header = ''.join(x + '\n' for x in header)
    header = header.encode()

    l = len(header)
    sentinel = bytearray()
    for j in range(4 - l % 4):
        sentinel.append(0)

    with open(filename, 'wb') as f:
        f.write(header)
        f.write(sentinel)
        f.write(data)

def read_gsf(file_name):
    '''Read a Gwyddion Simple Field 1.0 file format
    http://gwyddion.net/documentation/user-guide-en/gsf.html
    
    Args:
        file_name (string): the name of the output (any extension will be replaced)
    Returns:
        metadata (dict): additional metadata to be included in the file
        data (2darray): an arbitrary sized 2D array of arbitrary numeric type
    '''
    if file_name.rpartition('.')[1] == '.':
        file_name = file_name[0:file_name.rfind('.')]
    
    gsfFile = open(file_name + '.gsf', 'rb')
    
    metadata = {}
    
    # check if header is OK
    if not(gsfFile.readline().decode('UTF-8') == 'Gwyddion Simple Field 1.0\n'):
        gsfFile.close()
        raise ValueError('File has wrong header')
        
    term = b'00'
    # read metadata header
    while term != b'\x00':
        line_string = gsfFile.readline().decode('UTF-8')
        metadata[line_string.rpartition(' = ')[0]] = line_string.rpartition('=')[2]
        term = gsfFile.read(1)
        gsfFile.seek(-1, 1)
    
    gsfFile.read(4 - gsfFile.tell() % 4)
    
    #fix known metadata types from .gsf file specs
    #first the mandatory ones...
    metadata['XRes'] = int(metadata['XRes'])
    metadata['YRes'] = int(metadata['YRes'])
    
    #now check for the optional ones
    if 'XReal' in metadata:
        metadata['XReal'] = float(metadata['XReal'])
    
    if 'YReal' in metadata:
        metadata['YReal'] = float(metadata['YReal'])
                
    if 'XOffset' in metadata:
        metadata['XOffset'] = float(metadata['XOffset'])
    
    if 'YOffset' in metadata:
        metadata['YOffset'] = float(metadata['YOffset'])
    
    data = np.frombuffer(gsfFile.read(),dtype='float32').reshape(metadata['YRes'],metadata['XRes'])
    
    gsfFile.close()
    
    return metadata, data

def errorPlot(
    ax,
    error,
    ):
    ax.text(
        0.5,
        0.5,
        s=str(error),
        color='red',
        verticalalignment='center',
        horizontalalignment='center',
        wrap=True,
        )
    ax.set_xlim((0, 1))
    ax.set_ylim((0, 1))


def willItFloat(entry):
    try:
        float(entry)
    except ValueError:
        return False
    else:
        return True
