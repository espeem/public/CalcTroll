import numpy as np
from CalcTroll import API
from CalcTroll.Core.Flags import TooSmallBoundingBox

from CalcTroll.Plugins.ReferenceEnergy import HOMO, MidGapLevel, EnergyLevel, MO
from CalcTroll.Core.Utilities import apply2DGaussian

from .CurrentImage import CurrentImage
from .Grid.Grid import IntegratedCurrentGrid, DifferentialCurrentGrid
from .Utilities import interpolateToHeight
from .Utilities import write_gsf

class ConstantHeight(CurrentImage):
    def __init__(
            self,
            system=None,
            method=API.DEFAULT,
            probe=API.DEFAULT,
            regime=API.DYNAMIC,
            bias_voltage=API.DEFAULT,
            height=5,
            temperature=10,
            voltage_broadening=0.1,
            spatial_broadening=1.0,
            neutral_level=API.DEFAULT,
            image_type=API.INTEGRATED,
    ):
        self.__h = height
        self.__image_type = image_type

        if bias_voltage is API.DEFAULT:
            bias_voltage = HOMO()
        if neutral_level is API.DEFAULT:
            neutral_level = MidGapLevel()

        if isinstance(bias_voltage, MO) and \
           bias_voltage.include()==API.DEFAULT:
               include = image_type == API.INTEGRATED
               bias_voltage = bias_voltage.copy(include=include)

        CurrentImage.__init__(
                self,
                system=system,
                method=method,
                probe=probe,
                regime=regime,
                bias_voltage=bias_voltage,
                spatial_broadening=spatial_broadening,
                voltage_broadening=voltage_broadening,
                temperature=temperature,
                neutral_level=neutral_level,
                )

    def _defineGrids(self):
        if self.__image_type is API.INTEGRATED:
            grid = IntegratedCurrentGrid(
                voltage=self.biasVoltage(),
                temperature=self.temperature(),
                voltage_broadening=self.voltageBroadening(),
                neutral_level=self.neutralLevel(),
                probe=self.probe(),
                regime=self.regime(),
                )
        elif self.__image_type is API.DIFFERENTIAL:
            grid = DifferentialCurrentGrid(
                voltage=self.biasVoltage(),
                temperature=self.temperature(),
                voltage_broadening=self.voltageBroadening(),
                neutral_level=self.neutralLevel(),
                probe=self.probe(),
                regime=self.regime(),
                )

        grids = [grid]

        return grids

    def explain(self):
        return API.Explanation(
            "The simulated image was generated to emulate the constant-height operation of a scanning tunneling microscope.  ")

    def height(self):
        return self.__h

    def title(self):
        return {
            API.INTEGRATED:"CH",
            API.DIFFERENTIAL:"DCH",
        }[self['image_type']]

    def generate(self, grids):
        results = grids.results(self.grids())
        assert len(results) == 1
        result = results[0]
        r = grids.r()
        data = result, r

        h = self.height()
        ds = self.spatialBroadening()

        if r[2].max() < self.height():
            raise TooSmallBoundingBox

        data = apply2DGaussian(data, sigma=ds)
        data = interpolateToHeight(data, height=h)

        return data

    def treatDataForPlotting(self, data, **kwargs):
        image, variables = data
        vmax = image.max()
        vmin = image.min()

        return data, {'vmax':vmax, 'vmin':vmin, 'cmap':'hot'}

    def gsf_save(self, filename, data=None, **kwargs):
        if data is None:
            data = self.read()

        if isinstance(data, API.CalcTrollException):
            return None

        image, variables = data
        image *= 1e-9   # nA to A
        variables *= 1e-10 # Ang to m
        X, Y = variables
        xres, yres = image.shape

        xreal = X.max() - X.min()
        yreal = Y.max() - Y.min()

        image = image[:, ::-1]
        image = np.transpose(image)
        imagedata = image.flatten()

        zunits = {
            API.INTEGRATED: 'A',
            API.DIFFERENTIAL:'A/V',
        }[self['image_type']]


        write_gsf(
            filename=filename,
            imagedata=imagedata,
            xres=xres,
            yres=yres,
            xreal=xreal,
            yreal=yreal,
            xyunits='m',
            zunits=zunits,
        )

    def selfContained(self):
        item = CurrentImage.selfContained(self)[0]

        if isinstance(self, API.CalcTrollException):
            return [item]

        min_z = self.implementation().findMinimumHeight()
        h = self.height()

        if h < min_z:
            mess = "A scanning height of %.2f Angstrom was requested, but technical restriction of the calculation method limits the height to above %.2f Angstrom." % (h, min_z)
            error = API.InconsistentApproximation(mess)

            return [error]

        return [item]
