from CalcTroll import API
from CalcTroll.Plugins.STM.dIdV import dIdV
from CalcTroll.Plugins.STM.dIdVCleanSurfaceReference import dIdVCleanSurfaceReference
from CalcTroll.Plugins.STM.ConstantCurrent import ConstantCurrent
from CalcTroll.Plugins.STM.dIdV import dIdV, ImageMaximum

class PositionedDIdV(dIdV):
    def __init__(
        self,
        system,
        method,
        parameters=API.DEFAULT,
        bias_range=(-2, 2),
        **kwargs
        ):
        if system is not None:
            if not isinstance(system, API.AbstractRelaxed):
                parameters = system.defaultParameters(parameters)
                system = API.Relaxed(system, method, parameters)
        imaging_stub = ConstantCurrent(**kwargs)
        if kwargs.get('bias_voltage') is None:
            kwargs['bias_voltage'] = bias_range[0]

        neutral_level = dIdVCleanSurfaceReference(
                system=system,
                method=method,
                parameters=parameters,
                imaging_stub=imaging_stub,
                bias_range=(-2, 2),
                )

        image = ConstantCurrent(
                system=system,
                method=method,
                parameters=parameters,
                neutral_level=neutral_level,
                **kwargs
                )
        position = ImageMaximum(image)

        dIdV.__init__(self,
                position=position,
                bias_range=bias_range,
                neutral_level=neutral_level,
                )

    def extractFingerprint(self):
        if not self.isDone():
            return None

        data = self.read()
        data = self.treatDataForPlotting(data)
        I, v = data
        i = I.argmax()
        v1 = v[i]
        take = v > 0
        I, v = I[take], v[take]
        i = I.argmax()
        v2 = v[i]

        return (v1, v2 - v1)




