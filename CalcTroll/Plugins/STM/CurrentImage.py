import os
from os.path import join
import numpy as np
from scipy.io import netcdf

from CalcTroll import API
from CalcTroll.Plugins.LengthScales import LengthScales
from CalcTroll.Core.Visualizations import padVariables, addMeasuringStick, addInfo
from CalcTroll.Plugins.ReferenceEnergy import HOMO, MidGapLevel, EnergyLevel, HIGHEST_ALLOWED_ENERGY_LEVEL

from .ElectronicImage import ElectronicImage
from .Probe import Probe, TersoffHamann
from .Utilities import repeatData, errorPlot

class CurrentImage(ElectronicImage):
    def __init__(
            self,
            system=None,
            method=API.DEFAULT,
            parameters=API.DEFAULT,
            bias_voltage=API.DEFAULT,
            setpoint_current=50,
            voltage_broadening=0.1,
            spatial_broadening=1.0,
            temperature=10,
            neutral_level=API.DEFAULT,
            probe=API.DEFAULT,
            regime=API.DYNAMIC,
            ):
        if system != None:
            parameters = system.defaultParameters(parameters=parameters)

            if not isinstance(system, API.AbstractRelaxed):
                r_parameters = parameters.ignoreLongRangeCorrelation()
                system = API.Relaxed(system, method, r_parameters)

        if bias_voltage is API.DEFAULT:
            bias_voltage = HOMO()

        if neutral_level is API.DEFAULT:
            neutral_level = MidGapLevel()

        if probe is API.DEFAULT:
            probe = TersoffHamann()

        inputs = []
        if isinstance(bias_voltage, EnergyLevel):
            bias_voltage = bias_voltage.copy(
                system=system,
                method=method,
                parameters=parameters,
                )
            inputs.append(bias_voltage)

        if isinstance(neutral_level, EnergyLevel):
            neutral_level = neutral_level.copy(
                system=system,
                method=method,
                parameters=parameters,
                )
            inputs.append(neutral_level)

        self.__I = setpoint_current
        self.__V = bias_voltage
        self.__ds = spatial_broadening
        self.__dV = voltage_broadening
        self.__T = temperature
        self.__nl = neutral_level
        self.__probe = probe
        self.__regime = regime

        ElectronicImage.__init__(
            self,
            system=system,
            method=method,
            parameters=parameters,
            inputs=inputs,
            )

    def regime(self):
        return self.__regime
        
    def setpointCurrent(self):
        return self.__I

    def biasVoltage(self):
        return self.__V

    def spatialBroadening(self):
        return self.__ds

    def voltageBroadening(self):
        return self.__dV

    def temperature(self):
        return self.__T

    def neutralLevel(self):
        return self.__nl

    def probe(self):
        return self.__probe

    def plot(self,
             data,
             ax=None,
             frame=None,
             color='white',
             repeat=True,
             bar=True,
             info=True,
             **kwargs):
        ax.set_aspect('equal')
        ax.set_xticks([])
        ax.set_yticks([])

        if isinstance(data, API.CalcTrollException):
            errorPlot(ax, error=data)

            return None

        data, kwargs = self.treatDataForPlotting(data, **kwargs)
        image, variables = data

        if repeat:
            image, variables = repeatData(image, variables)

        X, Y = padVariables(variables)
        if frame is None:
            frame = self.frame(data=data)

        ax.pcolormesh(X, Y, image, **kwargs)

        ax.set_xlim((frame[0, 0], frame[1, 0]))
        ax.set_ylim((frame[0, 1], frame[1, 1]))

        if bar:
            addMeasuringStick(ax, color=color, size=5)

        if any(self.system().pbc()):
            atoms = self.system().atoms()
            self.system().plotUnitCell(data=atoms, ax=ax, direction='-z')

        voltage = self.biasVoltage()
        if isinstance(voltage, EnergyLevel):
            text = voltage.title()
        else:
            text = '%.1f' % voltage

        text = self.title() + ' ' + text

        if info:
            addInfo(ax, text=text, color=color)

        return frame


    def selfContained(self):
        """
        Return a mode where the placeholder arguments for energies are replaced
        with numerical ones.
        """
        bias_voltage = self.biasVoltage()
        neutral_level = self.neutralLevel()
        dV = self.voltageBroadening()
        N = neutral_level.level()
        N = np.around(N, 10)

        if isinstance(bias_voltage, EnergyLevel):
            level = bias_voltage.level()
            if isinstance(level, API.CalcTrollException):
                return [level]
            if abs(level - N) < dV:
                level = bias_voltage._determine_include()

            bias_voltage = level - N
            bias_voltage = np.around(bias_voltage, 10)

        if bias_voltage + N < HIGHEST_ALLOWED_ENERGY_LEVEL:
            new_mode = self.copy(bias_voltage=bias_voltage, neutral_level=N)
        else:
            mess = "The requested voltage('%s') is higher than %.2f V above the vacuum level." % (
                    bias_voltage,
                    HIGHEST_ALLOWED_ENERGY_LEVEL,
                    )

            new_mode = API.InconsistentApproximation(mess)

        return [new_mode]
