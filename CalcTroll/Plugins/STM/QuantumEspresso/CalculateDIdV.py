import os
from os.path import join
import sys
import numpy as np
from CalcTroll import API

from CalcTroll.Core.Utilities import unique

class CalculateDIdV(API.Task):
    """ Task set up by the Paz/Soler method.
        and propagate them into the vacuum assuming a flat potential.
    """
    def __init__(
            self,
            items,
            image_task,
            inputs=tuple(),
            ):
        self.__items = items

        inputs = list(inputs) + [image_task]
        API.Task.__init__(
            self,
            path=image_task.path(),
            method=image_task.method(),
            identifier=image_task.parameters().identifier(),
            inputs=inputs,
            )

    def compatible(self, other):
        if self.__class__ != other.__class__:
            return False

        accept = self.acceptItem(other)

        return accept

    def acceptItem(self, item):
        if self.system() != item.system():
            return False
        if self.method() != item.method():
            return False
        if self.parameters() != item.parameters():
            return False

        return True


    @staticmethod
    def hasOpenBoundaries():
        return False

    def imageTask(self):
        return self.inputs()[0]

    def numberOfValenceElectrons(self):
        return self.imageTask().numberOfValenceElectrons()

    def directoryBaseName(self):
        return self.bardeenTask().directoryBaseName()

    def bardeenTask(self):
        return self.imageTask().bardeenTask()

    def energySpectrum(self):
        return self.bardeenTask().energySpectrum()

    def estimate(self):
        NE = self.system().inputAtoms().number_of_valence_electrons()

        return float(NE**3)

    def items(self):
        return self.__items

    def runFileBasename(self):
        return self.className() + '.' + self.method().runFileEnding()

    def executable(self):
        return 'python'

    def selfContainedItems(self):
        new_items = []
        for item in self.items():
            new_items.extend(item.selfContained())

        items = unique(new_items)

        return items

    def _getLocalStatus(self):
        items = self.selfContainedItems()
        items = [item for item in items if not isinstance(item, API.CalcTrollException)]
        if len(items) == 0:
            return API.DONE

        if not os.path.exists(self.outFile(path=API.LOCAL)):
            return API.NOT_STARTED

        line = ''
        with open(self.outFile(path=API.LOCAL), 'r') as f:
            for line in f:
                pass

        if not "CALCULATION FINISHED" in line:
            return API.FAILED

        for f in self.binaryFiles():
            if not os.path.exists(f):
                return API.NOT_STARTED

        items = self.selfContainedItems()
        items = [item for item in items if not isinstance(item, API.CalcTrollException)]

        missing = []
        for item in items:
            f = join(self.runDirectory(path=API.LOCAL), item.binaryFile())
            in_file = item.inNCFile(f)
            missing.append(in_file is False)

        if any(missing):
            return API.NOT_STARTED
        else:
            return API.DONE

    def neededFiles(self):
        return self.method().neededFiles() + ['wf*.nc']

    def resultFiles(self):
        return ['*.out', '*stm_images.nc']

    def makeCommands(self, form):
        return [
                form % (self.runFile(API.TAIL), 'TMP.out'),
                'mv %s %s' % ('TMP.out', self.outFile(API.TAIL))
                ]

    def binaryFiles(self, path=API.LOCAL):
        files = np.unique([item.binaryFile() for item in self.items()])
        files = [join(self.runDirectory(path=path), f) for f in files]

        return files

    def parameters(self):
        return self.imageTask().parameters()

    def inputAtoms(self):
        substrate = self.system()
        atoms = substrate.atoms(parameters=self.parameters(), constrained=False)

        return atoms

    def write(self):
        atoms = self.inputAtoms()
        periodic = list(atoms.pbc[:2])
        script =  'from CalcTroll.Plugins.STM.Grid.GridManager import GridManager\n'
        script +=  'from CalcTroll.Interface import *\n'

        items = self.selfContainedItems()
        items = [item for item in items if not isinstance(item, API.CalcTrollException)]

        if len(items) > 0:
            script += """items = (
"""

            for item in items:
                script += '    ' + item.script() + ',\n'
            script += ')\n'

            script += """
gm = GridManager(
        items=items,
        label='./wf_plane',
        periodic=%s,
        )
gm.calculateAndSave(label='didv')
""" % periodic
        script += """
print("CALCULATION FINISHED")
"""
        with open(self.runFile(path=API.LOCAL), 'w') as f:
            f.write(script)

    def outFileBasename(self):
        return self.className() + '.' + self.method().outFileEnding()

    def readData(self, item=None, path=API.LOCAL):
        if item is None:
            item = self.items()[-1]

        path = self.runDirectory(path=path)

        return item.readData(path)

    def system(self):
        return self.imageTask().system()
