from CalcTroll import API

from CalcTroll.Core.Utilities import unique
from CalcTroll.Plugins.STM.ElectronicImage import ElectronicImage
from CalcTroll.Plugins.STM.dIdV import AbstractIV
from CalcTroll.Plugins.Programs.QuantumEspresso import QuantumEspresso
from CalcTroll.Plugins.Programs.QEPlusYamboGW import QEPlusYamboGW
from CalcTroll.Plugins.STM.Utilities import findMinimumHeight

from .BardeenPlane import BardeenPlane
from .CalculateImages import CalculateImages
from .CalculateDIdV import CalculateDIdV

class STMCollatorQuantumEspresso(API.AnalysisImplementation):
    @classmethod
    def canHandle(cls, item):
        if not isinstance(item, (ElectronicImage, AbstractIV,)): return False
        if not isinstance(item.method(), (QuantumEspresso, QEPlusYamboGW)): return False
        if isinstance(item.system(), API.Collection): return False

        return True        

    def itemsToInputTasks(self, items):
        all_items = list(items)
        i = 0
        while i < len(all_items):
            all_items.extend(all_items[i].inputs())
            i += 1

        all_items = unique(all_items)
        new_items = all_items[len(items):]

        all_tasks = []
        for item in new_items:
            all_tasks.extend(item.tasks())

        all_tasks = list(all_tasks)
        i = 0
        while i < len(all_tasks):
            all_tasks.extend(all_tasks[i].inputs())
            i += 1        
        
        all_tasks = unique(all_tasks)

        for task in all_tasks:
            assert isinstance(task, API.Task)

        return all_tasks

    def makeTasks(
            self,
            items,
            ):
        inputs = self.itemsToInputTasks(items)

        task = BardeenPlane(
                items=items,
                inputs=inputs,
                )
        tasks =[task]

        ivs = [item for item in items if isinstance(item, AbstractIV)]
        images = [item for item in items if isinstance(item, ElectronicImage)]
        iv_images = [item.image() for item in ivs]
        images.extend(iv_images)
        images = unique(images)

        image_task = CalculateImages(
                        images,
                        bardeen_task=task,
                        )
        tasks.append(image_task)

        if len(ivs) > 0:
            iv_task = CalculateDIdV(
                    items=ivs,
                    image_task=image_task,
                    )
            tasks.append(iv_task)


        return tasks

    def read(self, item, **kwargs):
        task = self.tasks()[-1]

        return task.readData(item)

    def findMinimumHeight(self):
        bardeen_task = self.tasks()[0]
        filename = bardeen_task.firstBinaryOutputFile()

        return findMinimumHeight(filename)
