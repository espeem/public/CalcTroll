import os
import time
import datetime
from ase.units import Bohr
import subprocess
import multiprocessing as mp
import pickle
import numpy as np
from scipy.io import netcdf

from CalcTroll.Core.Utilities import vectorLength
from CalcTroll.Core.Utilities import readCube
from CalcTroll.Plugins.STM.Utilities import surfaceIntegrator, correctOrigin


def runCommand(command, timeout_duration=600):
    # Run the command
    print("Running command: '%s'" % command)
    
    try:
        process = subprocess.run(command, capture_output=True, text=True, shell=True, timeout=timeout_duration)

        # Check if the command was successful
        if process.returncode != 0:
            print("An error occurred while running the command.")
            print("Error Output:", process.stderr)
            return False
        else:
            print("Command ran successfully.")
            return True

    except subprocess.TimeoutExpired:
        print(f"Command timed out after {timeout_duration} seconds!")
        return False
    except Exception as e:
        print(f"An exception occurred: {e}")
        return False


def calculateChargeDensityAndWFs(
                band_range,
                density_range=(-3.5, -2.5), 
                periodic=[True, True], 
                origin=(0.0, 0.0, 0.0), 
                limits=((None, None), (None, None), (None, None)),
                ):
    calculateChargeDensity()

    charge_file = 'charge_density.cube'
    rho, r, dh_cell = readCube(charge_file)

    rho, r, slices = determineWaveFunctionRange(rho, r, density_range=density_range, limits=limits, origin=origin, periodic=periodic)

    rho0 = 10**(np.array(density_range).mean())
    DS = (density_range[1] - density_range[0])/2.0
    c, rc, domain = surfaceIntegrator(rho=rho, variables=r, rho0=rho0, DS=DS)

    with open("rho.pkl", "wb") as f:
        pickle.dump((c, slices), f)

    band_range = list(range(band_range[0], band_range[1] + 1))

    with open('eigs_for_bardeen.pkl', 'rb') as f:
        EF, kpts, wk, eigs, band_numbers = pickle.load(f)
 
    for band_index in band_range:
        for k_index in range(len(kpts)):
            for spin_index in range(eigs.shape[2]):
                args = (band_index, k_index, spin_index, origin)

                script = """from CalcTroll.Plugins.STM.QuantumEspresso.Utilities import calculateSingleWF
calculateSingleWF(
    band=%d,
    n_kpt=%s,
    spin=%d,
    origin=%s,
)
""" % args
                filename = 'band_%d_%d_%d.py' % (k_index, band_index, spin_index)
                with open(filename, 'w') as f:
                    f.write(script)
        

    return None
    

def calculateChargeDensity():
    filename = 'calculate_density.ppi'
    tmp_file = 'tmp_charge.pp'
    outfile = 'charge_density_pp.out'
    fileout = 'charge_density.cube'
    in_file = """&inputpp
    prefix  = 'pwscf',
    outdir  = './SAVE',
    filplot = '%s'
    plot_num= 0,
/
&plot
   iflag = 3,
   output_format = 6,
   fileout = '%s',
/
""" % (tmp_file, fileout)
    
    if os.path.exists(fileout):
        return None

    with open(filename, 'w') as f:
        f.write(in_file)

    command = "$ESPRESSO_PP_COMMAND < %s > %s" % (filename, outfile)

    runCommand(command)
    os.remove(tmp_file)

def calculateSingleWF(
        band,
        n_kpt,
        spin,
        origin,
        ):
    cubefile = 'wf_%d_%d_%d.cube' % (n_kpt, band, spin)
    tmp_file = 'tmp_%d_%d_%d.pp' % (n_kpt, band, spin)
    filename = 'wf_plane_%d_%d_%d.nc' % (n_kpt, band, spin)
    if os.path.exists(filename):
        return None

    script = """
&inputpp
    prefix  = 'pwscf',
    outdir  = './SAVE',
    filplot = '%s'
    plot_num= 7,
    lsign   = .True.
    kpoint(1) = %d
    kband(1) = %d
    spin_component(1) = %d
/
&plot
   iflag = 3,
   output_format = 6,
   fileout = '%s',
/
""" % (tmp_file, n_kpt +1, band + 1, spin + 1, cubefile)
    
    ppi_file = "wf_%d_%d_%d.ppi" % (n_kpt, band, spin)
    ppi_out = "wf_%d_%d_%d.out" % (n_kpt, band, spin)
    with open(ppi_file, 'w') as f:
        f.write(script)
    with open("rho.pkl", "rb") as f:
        c, slices = pickle.load(f)

    command = "$ESPRESSO_PP_COMMAND < %s > %s" % (ppi_file, ppi_out)
    if not os.path.exists(cubefile):
        runCommand(command)
    
    if not os.path.exists(cubefile):
        return False

    u_sqr, r, dh_cell = readCube(cubefile)
    dh = np.array([dh_cell[0,0], dh_cell[1,1], dh_cell[2,2]])

    u_sqr = u_sqr[slices[0], slices[1], slices[2]]
    u_sqr *= Bohr
    u = np.sign(u_sqr) * np.sqrt(np.abs(u_sqr))
    r = r[:, slices[0], slices[1], slices[2]]

    thick_surface = vectorLength(c, axis=0) > 1e-9
    take_z = thick_surface.sum(axis=0)
    take_z = take_z.sum(axis=0) > 0
    thick_surface = thick_surface[:,:,take_z]

    with netcdf.netcdf_file(filename, 'w') as f:
        r = correctOrigin(r, origin)

        grad_u = np.array(np.gradient(u, *dh))
        A = (c*grad_u).sum(axis=0)
        r = r[:,:,:, take_z]
        u = u[:,:,take_z]
        A = A[:,:,take_z]
        ct = c[:, :, :, take_z]

        f.createDimension('x', u.shape[0])
        f.createDimension('y', u.shape[1])
        f.createDimension('z', u.shape[2])
        f.createDimension('range', 2)
        f.createDimension('points', thick_surface.sum())
        f.createDimension('vector3d', 3)
        f.createDimension('complex', 2)

        d_var = f.createVariable('thick_surface', dimensions=('x', 'y', 'z'), type=np.dtype('b'))
        ran_var = f.createVariable('grid_cell', dimensions=('vector3d', 'vector3d'), type=np.dtype('f'))
        ran_var[:] = dh_cell
        origin_var = f.createVariable('origin', dimensions=('vector3d', ), type=np.dtype('f'))
        origin_var[:] = np.array([r[0].min(), r[1].min(), r[2].min()])

        d_var[:] = thick_surface
        c_var = f.createVariable('c', dimensions=('vector3d', 'points'), type=np.dtype('f4'))
        for ci in range(3):
            c_var[ci,:] = ct[ci, thick_surface]
        u_var = f.createVariable('u', type=np.dtype('f4'), dimensions=('points', 'complex'))
        A_var = f.createVariable('A', type=np.dtype('f4'), dimensions=('points', 'complex'))

        u_var[:, 0] = u[thick_surface].real
        u_var[:, 1] = u[thick_surface].imag
        A_var[:, 0] = A[thick_surface].real
        A_var[:, 1] = A[thick_surface].imag

        del u_var, A_var

    os.remove(ppi_file)
    os.remove(ppi_out)
    os.remove(cubefile)
    os.remove(tmp_file)

    return True

def reciprocalLattice(cell):
    a1, a2, a3 = np.transpose(cell)
    V = np.dot(a1, np.cross(a2, a3))
    b1 = 2*np.pi/V*np.cross(a2, a3)
    b2 = 2*np.pi/V*np.cross(a3, a1)
    b3 = 2*np.pi/V*np.cross(a1, a2)

    return np.array([b1, b2, b3])


def limitsToSlices(r, limits, origin):
    takes = []
    for axis, lim in enumerate(limits):
        if lim == (None, None):
            take = np.ones(r.shape[axis+1], bool)
        else:
            if axis == 0:
                values = r[axis][:, 0, 0]
            elif axis == 1:
                values = r[axis][0, :, 0]
            elif axis == 2:
                values = r[axis][0, 0, :]

            if lim[0] == None:
                take1 = np.ones(len(values), bool)
            else:
                take1 = values > lim[0] + origin[axis]

            if lim[1] == None:
                take2 = np.ones(len(values), bool)
            else:
                take2 = values < lim[1] + origin[axis]
            take = np.logical_and(take1, take2)
        
        indices = np.arange(len(take))[take]
        
        takes.append((indices[0], indices[-1] + 1))

    return takes

def determineWaveFunctionRange(
        rho,
        r,
        density_range=(-3.5, -2.5),
        periodic=(True, True),
        limits=((None, None), (None, None), (None, None)),
        origin=(0.0, 0.0, 0.0),
        ):
    takes = limitsToSlices(r, limits=limits, origin=origin)

    min_value = 10**(density_range[0])
    periodic = list(periodic) + [False]

    slices = []
    for i in range(3):
        if periodic[i]:
            slices.append(slice(None, None))
            continue

        take1 = takes[i]
        axes = [0, 1, 2]
        axes.pop(i)
        tmp = rho.max(axes[0])
        tmp = tmp.max(axes[1]-1)
        take = tmp > min_value
        indices = np.arange(len(take))[take]

        take2 = (indices[0], indices[-1] + 1)
        sl = slice(max(take1[0], take2[0]), min(take1[1], take2[1]))
        slices.append(sl)

    r = np.array(r)

    rho = rho[slices[0], slices[1], slices[2]]
    r = r[:, slices[0], slices[1], slices[2]]

    return rho, r, slices

def inspectNetcdf(filename):
    with netcdf.netcdf_file(filename, 'r') as f:
        print(f.variables)


# Try at reading the density from h5df - something still off. 
def readH5dfWavefunctions(file_path, atoms, xs):
    import h5py
    r_lattice = reciprocalLattice(atoms.get_cell())
    print(r_lattice)
    with h5py.File(file_path, 'r') as file:
        for key in file.keys():
            print(key)
        
        dataset = file['evc']
        wfs = dataset[()]

        dataset = file['MillerIndices']
        ind = dataset[()]
 
    print(wfs.shape)
    print(ind.shape)
    eeeeeeeeeeeeee
    rho_r = rho[::2]
    rho_i = rho[1::2]
    rho = rho_r + 1.j*rho_i
    
    wave_vectors = np.tensordot(ind, r_lattice, (1, 0))
    kx = np.tensordot(wave_vectors, xs, (1, 1))
    phase_factors = np.exp(1.j*kx)
    
    # Because gamma only.
    rho0 = rho[0].real
    rho = 2*np.tensordot(rho[1:], phase_factors[1:], (0, 0)).real
    rho += rho0

    return rho*1/Bohr**3

# Try at reading the density from h5df - something still off. 
def readH5dfDensity(file_path, atoms, xs):
    import h5py
    r_lattice = reciprocalLattice(atoms.get_cell())
    with h5py.File(file_path, 'r') as file:
        for key in file.keys():
            print(key)
        
        dataset = file['rhotot_g']
        rho = dataset[()]

        dataset = file['MillerIndices']
        ind = dataset[()]
 
    rho_r = rho[::2]
    rho_i = rho[1::2]
    rho = rho_r + 1.j*rho_i
    
    wave_vectors = np.tensordot(ind, r_lattice, (1, 0))
    kx = np.tensordot(wave_vectors, xs, (1, 1))
    phase_factors = np.exp(1.j*kx)
    
    # Because gamma only.
    rho0 = rho[0].real
    rho = 2*np.tensordot(rho[1:], phase_factors[1:], (0, 0)).real
    rho += rho0

    return rho*1/Bohr**3