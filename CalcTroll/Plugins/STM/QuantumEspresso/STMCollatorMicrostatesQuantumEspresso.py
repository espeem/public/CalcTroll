from CalcTroll import API

from CalcTroll.Core.Utilities import unique
from CalcTroll.Plugins.STM.ElectronicImage import ElectronicImage
from CalcTroll.Plugins.Programs.QuantumEspresso import QuantumEspresso
from CalcTroll.Plugins.Systems.Microstates import Microstates
from CalcTroll.Plugins.STM.Utilities import findMinimumHeight

from .BardeenPlane import BardeenPlane
from .CalculateMicrostatesImages import CalculateMicrostatesImages

class STMCollatorMicrostatesQuantumEspresso(API.AnalysisImplementation):
    @classmethod
    def canHandle(cls, item):
        if not isinstance(item, (ElectronicImage,)): return False
        if not isinstance(item.method(), QuantumEspresso): return False
        if not isinstance(item.system(), Microstates): return False

        return True        

    def acceptItem(self, item):
        if self.system() != item.system():
            return False
        if self.method() != item.method():
            return False
        if self.parameters() != item.parameters():
            return False

        return True

    def itemsToInputTasks(self, items):
        all_items = list(items)
        i = 0
        while i < len(all_items):
            all_items.extend(all_items[i].inputs())
            i += 1

        all_items = unique(all_items)
        new_items = all_items[len(items):]

        all_tasks = []
        for item in new_items:
            all_tasks.extend(item.tasks())

        all_tasks = list(all_tasks)
        i = 0
        while i < len(all_tasks):
            all_tasks.extend(all_tasks[i].inputs())
            i += 1        
        
        all_tasks = unique(all_tasks)

        for task in all_tasks:
            assert isinstance(task, API.Task)

        return all_tasks

    def makeTasks(
            self,
            items,
            ):
        systems = items[0]['system']

        tasks = []
        bardeen_tasks = []
        for system in systems:
            s_items = [item.copy(system=system) for item in items]
            inputs = self.itemsToInputTasks(s_items)
            task = BardeenPlane(
                items=s_items,
                inputs=inputs,
                )
            bardeen_tasks.append(task)
            tasks.append(task)

        images = [item for item in items if isinstance(item, ElectronicImage)]
        images = unique(images)
        

        image_task = CalculateMicrostatesImages(
                        images,
                        bardeen_tasks=bardeen_tasks,
                        )
        tasks.append(image_task)

        return tasks

    def read(self, item, **kwargs):
        task = self.tasks()[-1]

        return task.readData(item)

    def findMinimumHeight(self):
        bardeen_task = self.tasks()[0]
        filename = bardeen_task.firstBinaryOutputFile()

        return findMinimumHeight(filename)
