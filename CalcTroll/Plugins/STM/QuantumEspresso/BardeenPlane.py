import os
from os.path import join
import sys
import pickle
import numpy as np
import fnmatch

from CalcTroll.Plugins.ReferenceEnergy import HIGHEST_ALLOWED_ENERGY_LEVEL
from CalcTroll.Core.Utilities import unique
from CalcTroll.Plugins.Programs.QuantumEspresso.EnergySpectrumTask import EnergySpectrumTask 
from CalcTroll.Plugins.Programs.QEPlusYamboGW.SaveWavefunctions import SaveWavefunctions
from CalcTroll import API

class BardeenPlane(API.Task):
    """ Task needed for the Paz/Soler method. The task consists in evaluating wave-functions on
        an iso-density surface above a sample.
    """
    def __init__(
            self,
            items,
            inputs,
            ):
        item0 = items[0]
        energy_spectrum = item0.energySpectrum()
        method = item0.method()

        self.__items = items
        self.__energy_spectrum = energy_spectrum
        
        
        es = [task for task in inputs if isinstance(task, (EnergySpectrumTask, SaveWavefunctions))]
        assert len(es) == 1
        es = es[0]
        self.__use_reference = es

        API.Task.__init__(
            self,
            path=es.path(),
            method=method,
            identifier=es.parameters().identifier(),
            inputs=inputs,
            )

    def compatible(self, other):
        if self.__class__ != other.__class__:
            return False

        accept = self.acceptItem(other)

        return accept

    def numberOfValenceElectrons(self):
        return self.__use_reference.numberOfValenceElectrons()

    def acceptItem(self, item):
        if self.system() != item.system():
            return False
        if self.method() != item.method():
            return False
        if self.parameters() != item.parameters():
            return False

        return True

    def directoryBaseName(self):
        return self.__use_reference.directoryBaseName()

    def explain(self):
        ex = API.Explanation("To compute the STM images we followed the surface integration technique of Paz and Soler[*].",
                [ "O . Paz and J. M. Soler, Phys. Status Solidi B, 2006, 243, 1080-1094."])

        probes = []
        for item in self.__items:
            if hasattr(item, 'probe'):
                probes.append(item.probe())

        probes = unique(probes)

        if len(probes) == 0:
            pass
        elif len(probes) == 1:
            ex += probes[0].explain()
        elif len(probes) > 1:
            raise Exception

        return ex

    def energySpectrum(self):
        return self.__energy_spectrum

    def estimate(self):
        NE = self.system().inputAtoms().number_of_valence_electrons()

        return float(NE**3)

    def system(self):
        return self.energySpectrum().system()

    def items(self):
        return self.__items

    def executable(self):
        return self.system().mainTask().executable()

    @staticmethod
    def hasOpenBoundaries():
        return False

    def allFilesThere(self):
        ran, size = self.findBandRange()
        if ran == None:
            return True

        fs = self.findBinaryOutputFiles()
        if len(fs) == 0:
            return False

        first, last = fs[0], fs[-1]
        first = int(first.split('_')[3].split('.')[0])
        last = int(last.split('_')[3].split('.')[0])

        if ran[0] < first:
            return False
        if ran[1] > last:
            return False

        return True

    def _getLocalStatus(self):
        outfile = self.outFile(path=API.LOCAL)
        if not os.path.exists(outfile):
            return API.NOT_STARTED

        with open(outfile, 'r') as f:
            for line in f:
                pass

        if not "CALCULATION FINISHED" in line:
            return API.FAILED

        if not self.allFilesThere():
            print("Missing Files")
            return API.NOT_STARTED

        return API.DONE

    @staticmethod
    def binaryBaseName():
        return 'wf_plane'

    def firstBinaryOutputFile(self):
        filenames = self.findBinaryOutputFiles()

        return join(self.runDirectory(), filenames[0])

    def findBinaryOutputFiles(self):
        filenames = fnmatch.filter(os.listdir(self.runDirectory()), self.binaryBaseName() + '*.nc')
        indices = [int(fn.split('_')[-2].split('.')[0]) for fn in filenames]
        argsort = np.argsort(indices)
        filenames = np.array(filenames)[argsort]

        return list(filenames)

    def neededFiles(self):
        return self.method().neededFiles() + self.binaryInputFiles()

    def resultFiles(self):
        return ['wf*.nc', '*.out', '*.fdf']

    def executable(self):
        return 'pp.x'

    def makeCommands(self, form):
        commands = [
                form % (self.runFile(API.TAIL), 'TMP.out'),
                ]
        band_range, block_size = self.findBandRange()
        EF, kpts, wk, eigs = self.energySpectrum().read()
        commands.append("START_TIME=$(date +%s)")
        band_range = list(range(band_range[0], band_range[-1] + 1))
        k_range = range(len(kpts))
        spin_range = range(eigs.shape[2])
        band_commands = []
        for i in k_range:
            for j in band_range:
                for k in spin_range:
                    band_commands.append("python band_%d_%d_%d.py &" % (i, j, k))

        for i, band_command in enumerate(band_commands):    
            commands.append(band_command)
            if i % 4 == 3:
                commands.append('wait')
        if commands[-1] != 'wait':
            commands.append('wait')
        commands.append('rm band_*.py')
        commands.append('mv %s %s' % ('TMP.out', self.outFile(API.TAIL)))
        commands.extend(
            [
            "END_TIME=$(date +%s)",
            "ELAPSED_TIME=$(( $END_TIME - $START_TIME ))",
            """echo "Elapsed time: $ELAPSED_TIME seconds""",
            ])

        return commands

    def parameters(self):
        return self.__energy_spectrum.parameters()

    def energyRange(self):
        mini, maxi = 1e99, -1e99
        for item in self.items():
            ran = item.range()
            mini = min(mini, ran[0])
            maxi = max(maxi, ran[1])

        return mini, maxi

    def findBandRange(self):
        es = self.energySpectrum()
        EF, kpts, wk, eigenvalues, band_numbers = es.read(read_bandnumbers=True)

        e_range = self.energyRange()

        # Remove data when eigenvalue of any spin or kpt is higher than 0.
        tmp = np.transpose(eigenvalues, (1, 0, 2))
        sh = tmp.shape
        tmp = np.reshape(tmp, (sh[0], sh[1]*sh[2]))
        take = tmp.min(axis=1) < HIGHEST_ALLOWED_ENERGY_LEVEL
        eigenvalues = eigenvalues[:, take]
        band_numbers = band_numbers[take]

        block_size = 1

        e_bandmin = eigenvalues.min(axis=(0, 2))
        e_bandmax = eigenvalues.max(axis=(0, 2))
        take1 = e_bandmax > e_range[0]
        take2 = e_bandmin < e_range[1]
        take = np.logical_and(take1, take2)
        band_numbers = band_numbers[take]

        if len(band_numbers) == 0:
            band_range = None
        else:
            band_range = int(band_numbers[0]), int(band_numbers[-1])

        return band_range, block_size

    def inputAtoms(self):
        substrate = self.system()
        atoms = substrate.atoms(parameters=self.parameters(), constrained=False)

        return atoms

    def write(self):
        atoms = self.inputAtoms()
        passivation_tag = atoms.tag('Passivation')
        if passivation_tag == None:
            limits = [(None, None)]*3
        else:
            pas_pos = atoms[passivation_tag.mask()].positions
            mini = pas_pos.min(axis=0)
            maxi = pas_pos.max(axis=0)
            limits = [(mini[0], maxi[0]), (mini[1], maxi[1]), (maxi[2], None)]

        origin = -atoms.get_celldisp()
        eig_result = self.__energy_spectrum.read(read_bandnumbers=True)
        runfile = self.runFile()
        with open(join(self.runDirectory(), 'eigs_for_bardeen.pkl'), 'wb') as f:
            pickle.dump(eig_result, f)

        periodic = tuple(atoms.get_pbc()[:2])
        band_range, block_size = self.findBandRange()
        origin_str = '(%.5f, %.5f, %.5f)' % tuple(origin)

        script = """from CalcTroll.Plugins.STM.QuantumEspresso.Utilities import calculateChargeDensityAndWFs

calculateChargeDensityAndWFs(
    periodic=%s, 
    band_range=%s, 
    limits=%s,
    origin=%s,
    )

print('CALCULATION FINISHED')
""" % (periodic, band_range, limits, origin_str)
        with open(runfile, 'w') as f:
            f.write(script)

