from CalcTroll import API
from CalcTroll.Plugins.STM.PositionedDIdV import PositionedDIdV
from CalcTroll.Plugins.Analysis.HomoLumoGap import HomoLumoGap

class dIdVGap(API.NoTasksAnalysis):
    def __init__(
        self,
        didv,
        **kwargs
        ):
        self.__didv = didv

        API.NoTasksAnalysis.__init__(
                self,
                system=didv.system(),
                method=didv.method(),
                parameters=didv.parameters(),
                inputs=[self.__didv],
                )

    def homoLumoGap(self):
        if not self.isDone():
            return None
    
        h_gap = HomoLumoGap(
            system=self.system(),
            method=self.method(),
            parameters=self.parameters(),
        )

        return h_gap.read()

    def didv(self):
        return self.__didv

    def read(self, **kwargs):
        if not self.isDone():
            return None

        didv = self.didv()
        data = didv.read()
        data = didv.treatDataForPlotting(data)

        I, v = data
        take = v < 0
        In, vn = I[take], v[take]
        i_n = In.argmax()
        v1 = vn[i_n]
        take = v > 0
        Ip, vp = I[take], v[take]
        i_p = Ip.argmax()
        v2 = vp[i_p]

        Iav = (Ip[i_p] + In[i_n])/2
        It = Iav/10

        try: 
            v1 = vn[In > It][-1]
            v2 = vp[Ip > It][0]
        except IndexError:
            return None
        else:        
            return v2 - v1     



class _dIdVGap(API.Analysis):
    def __init__(
        self,
        system,
        method,
        parameters=API.DEFAULT,
        **kwargs
        ):
        if not isinstance(system, API.AbstractRelaxed):
            parameters = system.defaultParameters(parameters)
            system = API.Relaxed(system, method, parameters)

        self.__didv = PositionedDIdV(
                system=system,
                method=method,
                parameters=parameters,
                **kwargs)

        API.Analysis.__init__(
                self,
                system=system,
                method=method,
                parameters=parameters,
                inputs=[self.__didv],
                )

    def didv(self):
        return self.__didv
