import os
import numpy as np
from CalcTroll import API
from CalcTroll.API import DEFAULT


class AverageImage(API.Analysis):
    def __init__(self, images):
        assert len(images) > 1
        self.__images = images

        assert len(set([image.method() for image in images])) == 1
        method = images[0].method()
        assert len(set([image.system() for image in images])) == 1
        system = images[0].system()
        assert len(set([image.mode() for image in images])) > 1

        API.Analysis.__init__(
            self,
            system=system,
            method=method,
            inputs=images,
        )

    def read(self):
        values, variables = self.__images[0].read()
        for image in self.__images[1:]:
            n_values, variables = image.read()
            values += n_values

        values /= len(self.__images)

        return values, variables

    def frame(self, **kwargs):
        return self.__images[0].frame(**kwargs)

    def plot(self, data=None, **kwargs):
        if data is None:
            data = self.read()
        return self.__images[0].plot(data, **kwargs)
