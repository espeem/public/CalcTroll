# Written by Mads Engelund, 2017, http://espeem.com
# Contains functions for retrieving information from SIESTA output and writing input files with the purpose of
# calculating STM properties.
import os
from os.path import join
import glob
import shutil
import numpy as np
import subprocess
import itertools
import time
from scipy.io import netcdf_file
import multiprocessing
from CalcTroll.Plugins.Programs.Siesta.SiestaIO import readKpoints, readEigenvalues, readNCDensity
from CalcTroll.Plugins.Programs.Siesta.SiestaIO import makeDencharInput, readCubeDensity
from CalcTroll.Plugins.Programs.Siesta.SiestaIO import readCubeWavefunction, hasCubeDensity
from CalcTroll.Plugins.ReferenceEnergy import HIGHEST_ALLOWED_ENERGY_LEVEL
from CalcTroll.Plugins.STM.Utilities import handleSpinPolarization, surfaceIntegrator
from CalcTroll.Plugins.STM.Utilities import determineWaveFunctionRange
from CalcTroll.Plugins.STM.Utilities import phaseFactor
from CalcTroll.Core.Utilities import vectorLength


def makeBlocks(band_range, block_size):
    n_blocks = int(np.ceil((band_range[1] + 1)/block_size))
    blocks = [[i*block_size, (i+1)*block_size - 1] for i in range(n_blocks)]

    i = 0
    while i < len(blocks):
        filename  = 'wf_plane_0_%d_0.nc' % blocks[i][0]
        if os.path.exists(filename):
            blocks.pop(i)
        else:
            i += 1

    return blocks

def generateWFSXFiles(blocks, atoms):
    template_file = 'denchar_wf_template.fdf'
    with open(template_file, 'r') as f:
        template = f.read()

    for block in blocks:
        calculator = atoms.get_calculator()
        label = 'block_%d_%d' % (block[0], block[1])
        if os.path.exists(label + '.WFSX'):
            continue

        shutil.copy2('siesta.DM', label + '.DM')

        values = {'WFS.Band.Min': block[0] + 1, 'WFS.Band.Max': block[1] + 1}
        calculator.parameters['fdf_arguments'].update(values)
        calculator.prefix=label
        calculator.reset()
        atoms.set_calculator(calculator)
        atoms.get_potential_energy()

        shutil.move(label + '.fullBZ.WFSX', label + '.WFSX')

        files = glob.iglob(label + '*')
        for f in files:
            if not f.split('.')[1] in ('WFSX', 'fdf', 'DIM', 'PLD'):
                os.remove(f)

        runfile = label + '.fdf'
        with open(runfile, 'a') as f:
            f.write(template)


def readInfoForSTMSimulation(label='./siesta'):
    kpts, wk = readKpoints(label=label)
    EF, eigs = readEigenvalues(label=label)

    n_eigs = []
    sh = eigs.shape
    for kn, band, spin in itertools.product(list(range(sh[0])), list(range(sh[1])), list(range(sh[2]))):
        n_eigs.append((eigs[kn, band, spin], kn, band, spin))

    n_eigs.sort()
    eigs = n_eigs

    return EF, kpts, wk, eigs

def prepareDenchar(density_range=(-3.5, -2.5), periodic=[True, True], origin=(0.0, 0.0, 0.0), limits=((None, None), (None, None), (None, None))):
    """ Create input files for the SIESTA utility Denchar.
        This is a step in the Paz-Soler method.

        @param density_range : The range of the logarithm of the density that will define the 'thick' bardeen plane
                               in the Paz/Soler method.
    """
    base_label = './siesta'
    folder = os.path.dirname(base_label)
    tail = os.path.basename(base_label)
    os.chdir(folder)
    nc_filename = join('Rho.grid.nc')
    rho, r, dh_cell = readNCDensity(nc_filename)
    r = np.array(r)
    rho = rho.sum(axis=0)

    new_limits = []
    takes = []
    for axis, lim in enumerate(limits):
        if lim == (None, None):
            take = np.ones(rho.shape[axis], bool)
        else:
            if axis == 0:
                values = r[axis][:, 0, 0]
            elif axis == 1:
                values = r[axis][0, :, 0]
            elif axis == 2:
                values = r[axis][0, 0, :]

            if lim[0] == None:
                take1 = np.ones(len(values), bool)
            else:
                take1 = values > lim[0] + origin[axis]

            if lim[1] == None:
                take2 = np.ones(len(values), bool)
            else:
                take2 = values < lim[1] + origin[axis]
            take = np.logical_and(take1, take2)

        takes.append(take)

    rho = rho[takes[0]]
    rho = rho[:, takes[1]]
    rho = rho[:, :, takes[2]]

    r = r[:, takes[0]]
    r = r[:, :, takes[1]]
    r = r[:, :, :, takes[2]]

    X, Y, Z = determineWaveFunctionRange(
            rho=rho,
            r=r,
            point_density=10,
            density_range=density_range,
            periodic=periodic,
            )
    span = np.array([
         [X.min(), X.max()],
         [Y.min(), Y.max()],
         [Z.min(), Z.max()],
        ])

    d_input = makeDencharInput(
            span=span,
            shape=X.shape,
            main_file=tail + '.fdf',
            plot_wavefunctions=False,
            plot_charge=True,
            )
    with open('denchar_charge.fdf', 'w') as f:
         f.write(d_input)

    d_input = makeDencharInput(
                span=span,
                shape=X.shape,
                main_file=None,
                plot_wavefunctions=True,
                plot_charge=False,
                )
    filename = 'denchar_wf_template.fdf'
    with open(filename, 'w') as f:
        f.write(d_input)

def runOneDencharProcess(block, density_range, origin, condition, sema):
    sema.acquire()
    label = 'block_%d_%d' % tuple(block)
    cmd = 'denchar < %s.fdf > %s.out' % (label, label)
    print(cmd)
    p = subprocess.check_output(cmd, shell=True)
    print('done calculating ' + label)
    condition.wait()
    print('writing ' + label)
    saveWavefunctionsToPlane(
            block_label=label,
            density_range=density_range,
            origin=origin)
    print('writing done ' + label)

    files = glob.iglob(label+ "*")
    for f in files:
        os.remove(f)

    sema.release()

def runChargeDencharProcess(sema):
    if hasCubeDensity('siesta'):
        return

    sema.acquire()
    cmd = 'denchar < denchar_charge.fdf > denchar_charge.out'
    print(cmd)
    p = subprocess.check_output(cmd, shell=True)
    print('density done')
    sema.release()

def runProcesses(den, deps, event):
    den.start()
    for p in deps:
        p.start()

    den.join()
    event.set()
    for p in deps:
        p.join()

def runDencharAndSave(blocks, density_range, origin):
    count = multiprocessing.cpu_count()
    sema = multiprocessing.Semaphore(count)
    event = multiprocessing.Event()
    den = multiprocessing.Process(
            target=runChargeDencharProcess,
            name='density',
            args=(sema,),
            )

    deps = []
    for block in blocks:
        name = 'block_%d_%d' % tuple(block)
        args = (tuple(block), tuple(density_range), tuple(origin), event, sema)
        p = multiprocessing.Process(
                target=runOneDencharProcess,
                name=name,
                args=args,
                )
        deps.append(p)

    try:
        runProcesses(den, deps, event)
    except KeyboardInterrupt:
        den.terminate()
        for p in deps:
            p.terminate()

        raise KeyboardInterrupt


def correctOrigin(r, origin):
    r = r.copy()
    r[0] -= origin[0]
    r[1] -= origin[1]
    r[2] -= origin[2]

    return r

def saveWavefunctionsToPlane(
        block_label='siesta',
        density_range=(-3.5, -2.5),
        origin=(0.0, 0.0, 0.0),
    ):
    label = './siesta'
    rho, r,  dh_cell = readCubeDensity(label=label)
    r = correctOrigin(r, origin)

    dh = np.array([dh_cell[0,0], dh_cell[1,1], dh_cell[2,2]])
    EF, kpts, wk, eigs = readInfoForSTMSimulation(label=label)
    eigs, rho, w_s = handleSpinPolarization(eigs, rho)
    is_spin_polarized = w_s == 1

    if is_spin_polarized:
        rho = rho.sum(axis=0)

    energies = np.array(eigs)[:,0]
    i_o = (energies < EF).sum() - 1
    rho0 = 10**(np.array(density_range).mean())
    DS = (density_range[1] - density_range[0])/2.0
    c, c_r, domain = surfaceIntegrator(rho=rho, variables=r, rho0=rho0, DS=DS)
    thick_surface = vectorLength(c, axis=0) > 1e-9
    take_z = thick_surface.sum(axis=0)
    take_z = take_z.sum(axis=0) > 0
    thick_surface = thick_surface[:,:,take_z]

    wfs = []
    EF, energies = readEigenvalues(label=label)
    folder = os.path.dirname(label)
    for wf_filename in os.listdir(folder):
        wf_filename = wf_filename.split('.')
        if wf_filename[0] == block_label and wf_filename[-2] == 'REAL' and wf_filename[-1] == 'cube':
            kn = int(wf_filename[1][1:])
            band = int(wf_filename[3])
            spin = wf_filename[4]
            if is_spin_polarized:
                spin = {'UP':0, 'DOWN':1}[spin]
            elif spin=='DOWN':
                continue
            else:
                spin = 0
            wfs.append((energies[kn-1, band-1, spin], kn-1, band-1, spin))
    wfs.sort()
    assert len(wfs) > 0

    for i, info in enumerate(wfs):
        energy, kn, band, spin = info
        kpt, w_k = kpts[kn], wk[kn]
        filename = 'wf_plane_%d_%d_%d.nc' % (kn, band, spin)
        with netcdf_file(filename, 'w') as f:
            if energy > HIGHEST_ALLOWED_ENERGY_LEVEL:
                continue
            wf, r, dh_cell = readCubeWavefunction(band + 1, kn=kn+1, spin=spin+1, label=block_label, folder=folder)
            r = correctOrigin(r, origin)

            eikr = phaseFactor(kpt, r)
            u = wf/eikr

            grad_u = np.array(np.gradient(u, *dh))
            A = (c*grad_u).sum(axis=0)
            r = r[:,:,:, take_z]
            u = u[:,:,take_z]
            A = A[:,:,take_z]
            ct = c[:, :, :, take_z]

            f.createDimension('x', u.shape[0])
            f.createDimension('y', u.shape[1])
            f.createDimension('z', u.shape[2])
            f.createDimension('range', 2)
            f.createDimension('points', thick_surface.sum())
            f.createDimension('vector3d', 3)
            f.createDimension('complex', 2)

            d_var = f.createVariable('thick_surface', dimensions=('x', 'y', 'z'), type=np.dtype('b'))
            ran_var = f.createVariable('grid_cell', dimensions=('vector3d', 'vector3d'), type=np.dtype('f'))
            ran_var[:] = dh_cell
            origin_var = f.createVariable('origin', dimensions=('vector3d', ), type=np.dtype('f'))
            origin_var[:] = np.array([r[0].min(), r[1].min(), r[2].min()])

            d_var[:] = thick_surface
            c_var = f.createVariable('c', dimensions=('vector3d', 'points'), type=np.dtype('f4'))
            for ci in range(3):
                c_var[ci,:] = ct[ci, thick_surface]
            u_var = f.createVariable('u', type=np.dtype('f4'), dimensions=('points', 'complex'))
            A_var = f.createVariable('A', type=np.dtype('f4'), dimensions=('points', 'complex'))

            u_var[:, 0] = u[thick_surface].real
            u_var[:, 1] = u[thick_surface].imag
            A_var[:, 0] = A[thick_surface].real
            A_var[:, 1] = A[thick_surface].imag



    return True
