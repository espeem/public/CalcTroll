# Written by Mads Engelund, 2017, http://espeem.com
import unittest
from CalcTroll.Core.Test.Case import CalcTrollTestCase

from CalcTroll.Plugins.STM.Siesta.IO import *

class SiestaIOTest(CalcTrollTestCase):
    def testPrepareDenchar(self):
        testdir = self.prepareTestDirectory(__file__, 'H2')

        prepareDenchar(density_range=(-3.5, -2.5))
        listdir = os.listdir(testdir)
        self.assertTrue('denchar_wf_template.fdf' in listdir)
        self.assertTrue('denchar_charge.fdf' in listdir)

    def testSaveWavefunctions(self):
        testdir = self.prepareTestDirectory(__file__, 'O2')
        filename = 'wf_plane_3_4.nc'
        saveWavefunctionsToPlane(filename, density_range=[-3.5, -2.5], origin=[6.0, 6.0, 6.0])
        self.assertTrue(filename in os.listdir(testdir))


if __name__=='__main__':
    unittest.main()
