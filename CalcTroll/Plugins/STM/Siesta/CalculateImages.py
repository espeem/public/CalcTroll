import os
from os.path import join
import sys
import numpy as np
from CalcTroll import API

from CalcTroll.Core.Utilities import unique

class CalculateImages(API.Task):
    """ Task set up by the Paz/Soler method. This task will take the wavefunctions defined on the Bardeen plane
        and propagate them into the vacuum assuming a flat potential.
    """
    def __init__(
            self,
            items,
            bardeen_task,
            inputs=tuple(),
            ):
        self.__items = items
        self.__bardeen_task = bardeen_task
        inputs = list(inputs) + [bardeen_task]
        API.Task.__init__(
            self,
            path=bardeen_task.path(),
            identifier=bardeen_task.identifier(),
            method=bardeen_task.method(),
            inputs=[bardeen_task],
            )

    def compatible(self, other):
        if self.__class__ != other.__class__:
            return False

        accept = self.acceptItem(other)

        return accept

    def acceptItem(self, item):
        if self.system() != item.system():
            return False
        if self.method() != item.method():
            return False
        if self.parameters() != item.parameters():
            return False

        return True

    @staticmethod
    def hasOpenBoundaries():
        return False

    def directoryBaseName(self):
        return self.bardeenTask().directoryBaseName()

    def explain(self):
        items = self.bardeenTask().items()
        clses = unique([item.__class__ for item in items])

        t = []
        for item in items:
            if hasattr(item, 'spatialBroadening') and hasattr(item, 'voltageBroadening'):
                t.append((item.spatialBroadening(), item.voltageBroadening()))
        t = unique(t)

        t = np.array(t).transpose()
        values = []
        if len(t) == 0:
            return API.Explanation()

        for i in range(len(t)):
            spec = unique(['%.2f' % s for s in t[i]])

            if len(spec) == 1:
                spec = spec[0]
            else:
                spec = ', '.join(spec[:-1]) + ' or ' + spec[-1]

            values.append(spec)

        return API.Explanation("In order to mimic the effect of spatial uncertainty in the measurement which reduces the resolution of the STM images, we have convoluted our currents with a Gaussian kernel K(r,r0) = (pi*sigma)^(3/2)*exp(|r-r0|^2/(2s^2)) with sigma = %s Ang. Similarly, we have introduced a spectral broadening of %s V." % tuple(values))

    def bardeenTask(self):
        return self.__bardeen_task

    def energySpectrum(self):
        return self.bardeenTask().energySpectrum()

    def estimate(self):
        NE = self.system().inputAtoms().number_of_valence_electrons()

        return float(NE**3)

    def items(self):
        return self.__items

    def runFileBasename(self):
        return self.className() + '.' + self.method().runFileEnding()

    def executable(self):
        return self.__bardeen_task.executable()

    def selfContainedItems(self):
        new_items = []
        for item in self.items():
            new_items.extend(item.selfContained())

        return new_items

    def _getLocalStatus(self):
        items = self.selfContainedItems()
        items = [item for item in items if not isinstance(item, API.CalcTrollException)]
        if len(items) == 0:
            return API.DONE

        if not os.path.exists(self.outFile(path=API.LOCAL)):
            return API.NOT_STARTED

        line = ''
        with open(self.outFile(path=API.LOCAL), 'r') as f:
            for line in f:
                pass

        if not "CALCULATION FINISHED" in line:
            return API.FAILED

        for f in self.binaryFiles():
            if not os.path.exists(f):
                return API.NOT_STARTED

        missing = []
        for item in items:
            f = join(self.runDirectory(path=API.LOCAL), item.binaryFile())
            in_file = item.inNCFile(f)
            missing.append(in_file is False)

        if any(missing):
            return API.NOT_STARTED
        else:
            return API.DONE

    def neededFiles(self):
        return self.method().neededFiles() + ['wf*.nc']

    def resultFiles(self):
        return ['*.out', '*stm_images.nc']

    def makeCommands(self, form):
        define_prog_c = "export PROGRAM=%s" % sys.executable
        return [
                define_prog_c,
                form % (self.runFile(API.TAIL), 'TMP.out'),
                'mv %s %s' % ('TMP.out', self.outFile(API.TAIL))
                ]

    def binaryFiles(self, path=API.LOCAL):
        files = np.unique([item.binaryFile() for item in self.items()])
        files = [join(self.runDirectory(path=path), f) for f in files]

        return files

    def inputAtoms(self):
        substrate = self.system()
        atoms = substrate.atoms(parameters=self.parameters(), constrained=False)

        return atoms

    def parameters(self):
        return self.__bardeen_task.parameters()

    def write(self):
        atoms = self.inputAtoms()
        periodic = list(atoms.pbc[:2])
        script =  'from CalcTroll.Plugins.STM.Grid.GridManager import GridManager\n'
        script +=  'from CalcTroll.Interface import *\n'

        items = self.selfContainedItems()
        items = [item for item in items if not isinstance(item, API.CalcTrollException)]

        if len(items) > 0:
            script += """items = (
"""

            for item in items:
                script += '    ' + item.script(exclude=['system', 'method']) + ',\n'
            script += ')\n'

            script += """
gm = GridManager(
        items=items,
        label='./wf_plane',
        periodic=%s,
        )
gm.calculateAndSave(label='stm')
""" % periodic
        script += """
print("CALCULATION FINISHED")
"""
        with open(self.runFile(path=API.LOCAL), 'w') as f:
            f.write(script)

    def outFileBasename(self):
        return self.className() + '.' + self.method().outFileEnding()

    def readData(self, item=None, path=API.LOCAL):
        if item is None:
            item = self.items()[-1]

        path = self.runDirectory(path=path)

        return item.readData(path)

    def system(self):
        return self.__bardeen_task.system()
