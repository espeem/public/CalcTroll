import os
from os.path import join
import pickle
import sys
import numpy as np
import fnmatch

from CalcTroll.Core.Utilities import unique
from CalcTroll.Plugins.ReferenceEnergy import HIGHEST_ALLOWED_ENERGY_LEVEL
from CalcTroll import API

from CalcTroll.Plugins.Programs.Siesta.EnergySpectrumTask import EnergySpectrumTask

class BardeenPlane(API.Task):
    """ Task needed for the Paz/Soler method. The task consists in evaluating wave-functions on
        an iso-density surface above a sample.
    """
    def __init__(
            self,
            items,
            inputs,
            ):
        item0 = items[0]
        energy_spectrum = item0.energySpectrum()
        method = item0.method()

        self.__items = items
        self.__energy_spectrum = energy_spectrum
        task = energy_spectrum.tasks()[0]
        es = [task for task in inputs if isinstance(task, EnergySpectrumTask)]
        assert len(es) == 1
        self.__use_reference = es[0]

        API.Task.__init__(
            self,
            path=task.path(),
            method=method,
            identifier=task.parameters().identifier(),
            inputs=inputs,
            )

    def compatible(self, other):
        if self.__class__ != other.__class__:
            return False

        accept = self.acceptItem(other)

        return accept

    def acceptItem(self, item):
        if self.system() != item.system():
            return False
        if self.method() != item.method():
            return False
        if self.parameters() != item.parameters():
            return False

        return True

    def directoryBaseName(self):
        return self.__use_reference.directoryBaseName()

    def explain(self):
        ex = API.Explanation("To compute the STM images we followed the surface integration technique of Paz and Soler[*].",
                [ "O . Paz and J. M. Soler, Phys. Status Solidi B, 2006, 243, 1080-1094."])

        probes = []
        for item in self.__items:
            if hasattr(item, 'probe'):
                probes.append(item.probe())

        probes = unique(probes)

        if len(probes) == 0:
            pass
        elif len(probes) == 1:
            ex += probes[0].explain()
        elif len(probes) > 1:
            raise Exception

        return ex

    def energySpectrum(self):
        return self.__energy_spectrum

    def estimate(self):
        NE = self.system().inputAtoms().number_of_valence_electrons()

        return float(NE**3)

    def energySpectrum(self):
        return self.__energy_spectrum

    def system(self):
        return self.energySpectrum().system()

    def items(self):
        return self.__items

    def executable(self):
        return self.system().mainTask().executable()

    @staticmethod
    def hasOpenBoundaries():
        return False

    def allFilesThere(self):
        ran, size = self.findBandRange()
        if ran == None:
            return True

        fs = self.findBinaryOutputFiles()
        if len(fs) == 0:
            return False

        first, last = fs[0], fs[-1]
        first = int(first.split('_')[3].split('.')[0])
        last = int(last.split('_')[3].split('.')[0])

        if ran[0] < first:
            return False
        if ran[1] > last:
            return False

        return True

    def binaryInputFiles(self):
        files = ['Rho.grid.nc']
        return files

    def _getLocalStatus(self):
        outfile = self.outFile(path=API.LOCAL)
        if not os.path.exists(outfile):
            return API.NOT_STARTED

        line=''
        with open(self.outFile(path=API.LOCAL), 'r') as f:
            for line in f:
                pass

        if not "CALCULATION FINISHED" in line:
            return API.FAILED
        
        binary_files = self.binaryInputFiles()
        for f in binary_files:
            f = join(self.runDirectory(path=API.LOCAL), f)
            if not os.path.exists(f):
                return API.FAILED

        if not self.allFilesThere():
            return API.NOT_STARTED

        return API.DONE

    @staticmethod
    def binaryBaseName():
        return 'wf_plane'

    def firstBinaryOutputFile(self):
        filenames = self.findBinaryOutputFiles()

        return join(self.runDirectory(), filenames[0])

    def findBinaryOutputFiles(self):
        filenames = fnmatch.filter(os.listdir(self.runDirectory()), self.binaryBaseName() + '*.nc')
        indices = [int(fn.split('_')[3].split('.')[0]) for fn in filenames]
        argsort = np.argsort(indices)
        filenames = np.array(filenames)[argsort]

        return list(filenames)
    
    def neededFiles(self):
        return self.method().neededFiles() + self.binaryInputFiles()

    def resultFiles(self):
        return ['wf*.nc', '*.out', '*.fdf']

    def numberOfValenceElectrons(self):
        return self.system().fullyUnrelaxed().atoms().number_of_valence_electrons()

    def environmentCommands(self):
        cmds = []

        method = self.method()
        program = method.program()

        python_executable = sys.executable
        pseudo_path = program.pseudoPath()

        cmd = "export SIESTA_PP_PATH=%s" % pseudo_path
        cmds.append(cmd)
        if self.numberOfValenceElectrons() < 50:
            cmd = """export ASE_SIESTA_COMMAND='siesta < PREFIX.fdf > PREFIX.out'"""
            cmds.append(cmd)
        cmd = "export PROGRAM=%s" % python_executable
        cmds.append(cmd)

        return cmds

    def makeCommands(self, form):
        commands = self.environmentCommands()
        define_prog_c = "export PROGRAM=%s" % sys.executable
        commands.extend([
                define_prog_c,
                form % (self.runFile(API.TAIL), 'TMP.out'),
                'mv %s %s' % ('TMP.out', self.outFile(API.TAIL))
                ])

        return commands


    def parameters(self):
        return self.__energy_spectrum.parameters()

    def energyRange(self):
        mini, maxi = 1e99, -1e99
        for item in self.items():
            ran = item.range()
            mini = min(mini, ran[0])
            maxi = max(maxi, ran[1])

        return mini, maxi

    def findBandRange(self):
        es = self.energySpectrum()
        EF, kpts, wk, eigs = es.read()
        e_range = self.energyRange()

        # Remove data when eigenvalue of any spin or kpt is higher than 0.
        tmp = np.transpose(eigs, (1, 0, 2))
        sh = tmp.shape
        tmp = np.reshape(tmp, (sh[0], sh[1]*sh[2]))
        take = tmp.min(axis=1) < HIGHEST_ALLOWED_ENERGY_LEVEL
        eigs = eigs[:, take]

        nkpts = eigs.shape[0]
        n_bands = eigs.shape[1]

        block_size = 1

        e_bandmin = eigs.min(axis=(0, 2))
        e_bandmax = eigs.max(axis=(0, 2))
        take1 = e_bandmax > e_range[0]
        take2 = e_bandmin < e_range[1]
        take = np.logical_and(take1, take2)
        indices = np.array(range(n_bands))[take]

        if len(indices) == 0:
            band_range = None
        else:
            band_range = indices[0], indices[-1]

        return band_range, block_size

    def inputAtoms(self):
        substrate = self.system()
        atoms = substrate.atoms(parameters=self.parameters(), constrained=False)

        return atoms

    def write(self):
        atoms = self.inputAtoms()
        passivation_tag = atoms.tag('Passivation')
        if passivation_tag == None:
            limits = [(None, None)]*3
        else:
            pas_pos = atoms[passivation_tag.mask()].positions
            mini = pas_pos.min(axis=0)
            maxi = pas_pos.max(axis=0)
            limits = [(mini[0], maxi[0]), (mini[1], maxi[1]), (maxi[2], None)]

        eig_result = self.__energy_spectrum.read(read_bandnumbers=True)
        runfile = self.runFile()
        with open(join(self.runDirectory(), 'eigs_for_bardeen.pkl'), 'wb') as f:
            pickle.dump(eig_result, f)


        parameters = self.parameters()
        method = self.method()
        runfile = self.runFile()

        band_range, block_size = self.findBandRange()

        method = method.writeDensity()
        method = method.writeWaveFunctions(band_range=(band_range[0], band_range[0]))

        method.writeOneShot(
            runfile=runfile,
            atoms=atoms,
            kpts=parameters.kpts(),
        )

        # Protection from relaxation with FIRE
        with open(runfile, 'r') as f:
            for line in f:
                assert 'FIRE' not in line

        periodic = list(atoms.pbc[:2])
        for i, p in enumerate(periodic):
            if p:
                limits[i] = (None, None)
        limits = tuple(limits)
        origin = list(-atoms.get_celldisp())
        density_range = [-3.5, -2.5]
        script = """
import os
from CalcTroll.Plugins.STM.Siesta.IO import makeBlocks, prepareDenchar, generateWFSXFiles
from CalcTroll.Plugins.STM.Siesta.IO import runDencharAndSave
"""
        if band_range != None:
            script += """blocks = makeBlocks(band_range=%s, block_size=%d)""" % (band_range, block_size)
        else:
            script += """blocks = []"""

        script += """
prepareDenchar(density_range=%s, periodic=%s, origin=%s, limits=%s)
generateWFSXFiles(blocks, atoms)
""" % (density_range, periodic, origin, limits)
        script += """
runDencharAndSave(blocks, density_range=%s, origin=%s)
""" % (density_range, origin)

        script += """
print("CALCULATION FINISHED")
"""

        with open(runfile, 'a') as f:
            f.write(script)
