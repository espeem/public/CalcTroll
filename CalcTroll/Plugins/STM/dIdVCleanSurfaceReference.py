import numpy as np
from CalcTroll import API
from CalcTroll.Plugins.ReferenceEnergy import EnergyLevel
from .dIdV import dIdV, ImageMinimum, ImageMaximum


class dIdVCleanSurfaceReference(EnergyLevel):
    def __init__(self,
                 system=None,
                 method=None,
                 parameters=API.DEFAULT,
                 imaging_stub=API.DEFAULT,
                 bias_range=(-2, 2),
                 ):
        if system == None or method == None:
            inputs=[]

        if system is not None:
            if not isinstance(system, API.AbstractRelaxed):
                parameters = system.defaultParameters(parameters)
                system = API.Relaxed(system, method, parameters)
            ref_system = system.referenceSystem()
            ref_parameters = ref_system.defaultParameters()
            ref_system = API.Relaxed(ref_system, method, ref_parameters)
            image = imaging_stub.copy(system=ref_system, method=method, parameters=ref_parameters)

            position=ImageMaximum(image)
            didv = dIdV(position=position, bias_range=bias_range)
            inputs = [didv]

        EnergyLevel.__init__(
                self,
                system=system,
                method=method,
                parameters=parameters,
                inputs=inputs,
                )

    def didv(self):
        return self.inputs()[0]

    def system(self):
        return self.didv().system()

    def image(self):
        return self.didv().image()

    def _determine(self, **kwargs):
        didv = self.inputs()[0]

        data = didv.read()

        dIdV, V = didv.treatDataForPlotting(data)

        d2IdV = np.gradient(dIdV, V)
        Vm = (V[1:] + V[:-1])/2
        dIdVm = (dIdV[1:] + dIdV[:-1])/2
        cutoff = dIdVm > 5
        maximum = np.arange(len(Vm))[(np.sign(d2IdV[:-1]) == 1) * (np.sign(d2IdV[1:]) == -1) * cutoff]
        ref_peak = maximum[Vm[maximum] < 0].max()

        hm = dIdVm[ref_peak]/4
        next_high_peaks = maximum[(dIdVm[maximum] > hm) & (Vm[maximum] > 0)]
        if len(next_high_peaks) > 0:
            ref_peak_2 = next_high_peaks.min()
            sl = slice(ref_peak + 1, ref_peak_2 - 1)
        else:
            sl = slice(ref_peak + 1, None)

        V = V[sl]
        Vm = (V[1:] + V[:-1])/2
        dIdV = dIdV[sl]
        if (dIdV > hm).all():
            raise API.CalcTrollException('Reference energy undefined')
        down_crossing = np.arange(len(Vm))[(dIdV[:-1] > hm) & (dIdV[1:] < hm)].max()
        up_take = (dIdV[:-1] < hm) & (dIdV[1:] > hm)
        if up_take.sum() == 0:
            raise Exception()
        up_crossing = np.arange(len(Vm))[up_take].min()

        reference_value = (Vm[down_crossing] + Vm[up_crossing])/2

        neutral_level = self.didv().neutralLevel()
        reference_value += neutral_level

        return reference_value
