import os
import unittest
from scipy.io import netcdf
from CalcTroll.Core.Test.Case import CalcTrollTestCase
from CalcTroll.Core.Flags import TooSmallBoundingBox
from CalcTroll.Plugins.Programs.Siesta.SiestaIO import readNCDensity

from .Siesta.IO import readInfoForSTMSimulation
from .Utilities import *

def makeTestSimulationGrid(n, box_size):
    x = np.linspace(-box_size, box_size, n)
    y = np.linspace(-box_size*1.1, box_size*1.1, n + 1)
    z = np.linspace(-box_size*1.2, box_size*1.2, n + 2)
    r = np.array(np.meshgrid(x, y, z, indexing='ij'))

    return r

def makeTestWavefunctionsAndDensity(r):
    dh_cell = gridSpacingCell(r)
    dh = vectorLength(dh_cell, axis=0)
    dh_a = dh[0]*dh[1]*dh[2]
    us = np.zeros([3] + list(r[0].shape), float)
    for i, direction in enumerate(r):
        us[i] = direction*gaussian(r, (0,0,0), sigma=1)
        us[i] /= np.sqrt((us[i]**2).sum()*dh_a)

    rho = abs(us)**2
    rho = rho.sum(axis=0)

    return us, rho

class UtilitiesTest(CalcTrollTestCase):
    def testBackground(self):
        z = np.linspace(-5, 5, 11)
        result = add_background(z=z, z0=0, voltage=3, neutral_level=-5.6, voltage_broadening=0.5, temperature=10)
        self.assertEqual(result.shape, (2, len(z)))

    def testPhaseFactor(self):
        r = np.mgrid[0:4.5:1.2,3:5:0.8]
        kpt = np.zeros(2)
        result = phaseFactor(kpt, r)
        self.assertTrue((result == 1).all())
        kpt = [.8, 0.2]
        result = phaseFactor(kpt, r)
        expected = [[ 0.82533561+0.56464247j,
                      0.72483601+0.68892145j,
                      0.60582016+0.79560162j],
                    [ 0.01079612+0.99994172j,
                     -0.14865070+0.98888977j,
                     -0.30430017+0.95257619j],
                    [-0.81295204+0.58233065j,
                     -0.89534431+0.44537464j,
                     -0.95486462+0.29704135j],
                    [-0.94328460-0.33198519j,
                     -0.87834501-0.47802725j,
                     -0.79096771-0.61185789j]]
        self.assertArraysEqual(result, expected)

    def notestIsSpinPolarized(self):
        testdir = self.prepareTestDirectory(__file__, 'unpolarized')
        eigs, kpts, EF = readInfoForSTMSimulation()
        nc_filename = join(testdir, 'Rho.grid.nc')
        rho, r, dh_cell = readNCDensity(nc_filename)
        self.assertFalse(isSpinPolarized(eigs, rho))

    def testDomainSurfaceSphere(self):
        """ Test that a spherical volume can be handled my domainSurface"""
        # Set up sphere defined on a grid.
        n = 81
        box_size = 10
        radius = 6
        sphere_surface = 4*np.pi*radius**2
        x = np.linspace(-box_size, box_size, n)
        y = np.linspace(-box_size, box_size, n)
        z = np.linspace(-box_size, box_size, n)
        Y, X, Z = np.meshgrid(x, y, z)
        variables = np.array([X, Y, Z])
        r = np.sqrt(X**2 + Y**2 + Z**2)

        # Test whether the approximated area is less than the bounding sphere.
        domain = r < radius
        c1, variables, non_zero = domainSurface(domain, variables)
        self.assertEqual(c1.shape, (3, n,n,n))
        A = np.sqrt((c1**2).sum(axis=0))
        area1 = A.sum()
        self.assertLess(area1, sphere_surface)

        # Test that the surface is oriented away from the center.
        take = np.abs(c1) > 0
        self.assertArraysEqual(np.sign(c1)[take], np.sign(variables)[take])

        # Test that the surface outside the sphere is larger than the area of the
        # sphere.
        domain = r > radius
        c2, variables, non_zero = domainSurface(domain, variables)
        area2 = np.sqrt((c2**2).sum(axis=0)).sum()
        self.assertGreater(area2, sphere_surface)

        # Test that the surface is oriented towards the center.
        take = np.abs(c2) > 0
        self.assertArraysEqual(np.sign(c2)[take], -np.sign(variables)[take])

    def testFindTopographyHeight(self):
        z = np.linspace(1, 30, 30)
        values = z**-1
        result = findTopographyHeight(iso_values=values,
                                      z=z,
                                      contour_value=1e-1,
                                      )
        self.assertEqual(result, 10.0)

        result = findTopographyHeight(
            iso_values=values,
            z=z,
            contour_value=0.5e-1,
        )
        self.assertEqual(result, 20.0)

        result = findTopographyHeight(iso_values=values, z=z, contour_value=10**(1))
        self.assertTrue(result == -np.inf)

    def testSimpleTopographyFrom3D(self):
        x = np.linspace(-2, 2, 5)
        y = np.linspace(-2, 2, 5)
        z = np.linspace(0, 10, 30)
        variables = np.meshgrid(x, y, z, indexing='ij')
        X, Y, Z = variables
        values = np.exp(-(X**2+Y**2) - Z)
        data = values, variables
        image, variables = simpleTopographyFrom3D(data, contour_value=1e-3)
        test = [[-np.inf, 1.90776, 2.90776, 1.90776, -np.inf],
                [1.90776, 4.90776, 5.90776, 4.90776, 1.90776],
                [2.90776, 5.90776, 6.90776, 5.90776, 2.90776],
                [1.90776, 4.90776, 5.90776, 4.90776, 1.90776],
                [-np.inf, 1.90776, 2.90776, 1.90776, -np.inf]]

        self.assertArraysEqual(test, image, 5)

    def testSimpleTopographyFrom3DRaises(self):
        x = np.linspace(-2, 2, 5)
        y = np.linspace(-2, 2, 5)
        z = np.linspace(0, 5, 15)
        variables = np.meshgrid(x, y, z, indexing='ij')
        X, Y, Z = variables
        values = np.exp(-(X**2+Y**2) - Z)
        data = values, variables
        self.assertRaises(TooSmallBoundingBox, lambda:simpleTopographyFrom3D(data, 1e-3))

    def testEvaluateOnIsoSurface(self):
        x = np.linspace(-2, 2, 5)
        y = np.linspace(-2, 2, 5)
        z = np.linspace(0, 10, 30)
        variables = np.meshgrid(x, y, z, indexing='ij')
        X, Y, Z = variables
        iso_values = np.exp(-(X**2+Y**2) - Z)
        values = np.sin(X + Y)

        image, variables = evaluateOnIsoSurface(
            iso_values=iso_values,
            variables=variables,
            contour_value=1e-3,
            values=values,
            )

        test = [[-np.inf , -0.14112, -0.9093 , -0.84147, -np.inf ],
                [-0.14112, -0.9093 , -0.84147, 0.0     , 0.84147 ],
                [-0.9093 , -0.84147,  0.0    , 0.84147 , 0.9093  ],
                [-0.84147, 0.0     ,  0.84147, 0.9093  , 0.14112 ],
                [-np.inf , 0.84147 ,  0.9093 , 0.14112 , -np.inf ]]

        self.assertArraysEqual(test, image, 5)

    def testDetermineWavefunctionRange(self):
        """ Test """
        # Set up three wavefunctions on a grid.
        density_range = (-4, -2)
        r = makeTestSimulationGrid(n=80, box_size=10)
        us, rho = makeTestWavefunctionsAndDensity(r)

        rho_max = rho.max(axis=0).max(axis=0)
        take = rho_max > 10**density_range[0]
        zs = r[2, 0, 0, take]
        r = determineWaveFunctionRange(rho, r, point_density=2, density_range=density_range)
        self.assertGreater(r[2].max(), zs.max())
        self.assertLess(r[2].min(), zs.min())


if __name__=='__main__':
    unittest.main()

