import numpy as np
from CalcTroll import API
from CalcTroll.API import DEFAULT, DYNAMIC
from CalcTroll.Plugins.ReferenceEnergy import HOMO, MidGapLevel, EnergyLevel, MO
from CalcTroll.Core.Utilities import apply2DGaussian

from .Probe import TersoffHamann
from .CurrentImage import CurrentImage
from .Grid.Grid import IntegratedCurrentGrid
from .Utilities import simpleTopographyFrom3D, write_gsf

class ConstantCurrent(CurrentImage):
    def __init__(
            self,
            system=None,
            method=DEFAULT,
            parameters=DEFAULT,
            regime=DYNAMIC,
            probe=DEFAULT,
            bias_voltage=DEFAULT,
            neutral_level=DEFAULT,
            setpoint_current=1,
            temperature=10,
            voltage_broadening=0.1,
            spatial_broadening=1.0,
            ):
        self.__I = setpoint_current
        self.__ds = spatial_broadening

        if bias_voltage is DEFAULT:
            bias_voltage = HOMO()
        if neutral_level is DEFAULT:
            neutral_level = MidGapLevel()

        if isinstance(bias_voltage, MO) and \
           bias_voltage.include() is DEFAULT:
            bias_voltage = bias_voltage.copy(include=True)


        CurrentImage.__init__(
                self,
                system=system,
                method=method,
                parameters=parameters,
                bias_voltage=bias_voltage,
                spatial_broadening=spatial_broadening,
                voltage_broadening=voltage_broadening,
                temperature=temperature,
                neutral_level=neutral_level,
                probe=probe,
                regime=regime,
                )

    def _defineGrids(self):
        grids = [IntegratedCurrentGrid(
                voltage=self.biasVoltage(),
                temperature=self.temperature(),
                voltage_broadening=self.voltageBroadening(),
                neutral_level=self.neutralLevel(),
                probe=self.probe(),
                regime=self.regime(),
                ) ]

        return grids

    def explain(self):
        return API.Explanation(
            "The simulated image was generated to emulate the constant-current operation of a scanning tunneling microscope.  ")

    def setpointCurrent(self):
        return self.__I

    def title(self):
        return "CC"

    def treatDataForPlotting(self, data, periodic=False):
        image, variables = data
        take = image > -np.inf
        if len(image[take]) == 0:
            return data, {'vmax':1, 'vmin':0, 'cmap':'hot'}

        vmax = image[take].max()
        vmin = image[take].min()
        image[np.logical_not(take)] = vmin
        if not periodic:
            vmin = vmax*0.5 + vmin*0.5

        return data, {'vmax':vmax, 'vmin':vmin, 'cmap':'hot'}

    def generate(self, grids):
        results = grids.results(self.grids())
        assert len(results) == 1
        result = results[0]
        r = grids.r()
        data = result, r

        data = self.probe().generate(data)
        data = apply2DGaussian(data, sigma=self.__ds)
        data = simpleTopographyFrom3D(data, contour_value=self.setpointCurrent())

        return data


    def gsf_save(self, filename, data=None, zero_height=0, **kwargs):
        if data is None:
            data = self.read()

        if isinstance(data, API.CalcTrollException):
            return None

        image, variables = data
        image -= zero_height

        min_z = image[image != -np.inf].min()
        image[image == -np.inf] = min_z

        image *= 1e-10   # Ang to m
        variables *= 1e-10 # Ang to m
        X, Y = variables
        xres, yres = image.shape

        xreal = X.max() - X.min()
        yreal = Y.max() - Y.min()

        image = image[:, ::-1]
        image = np.transpose(image)
        imagedata = image.flatten()

        zunits = 'm'

        write_gsf(
            filename=filename,
            imagedata=imagedata,
            xres=xres,
            yres=yres,
            xreal=xreal,
            yreal=yreal,
            xyunits='m',
            zunits=zunits,
        )
