from CalcTroll.Interface import *

setpoint_current = 50
bias_voltage = HOMO(minus=1)
height = 5
spatial_broadening = 1.0
voltage_broadening = 0.1
image_type = API.DIFFERENTIAL

system = Smiles('C1=CC(=C2C=C1)C=CC=C2')

method = Siesta(xc="PBE",
                     mesh_cutoff=200 * Ry,
                     spin='collinear',
                     basis_set="DZP",
                     )

image = ConstantHeight(
        system=system,
        method=method,
        bias_voltage=bias_voltage,
        height=height,
        voltage_broadening=voltage_broadening,
        spatial_broadening=spatial_broadening,
        image_type=image_type,
)

frame = save(image.system(), 'molecule_topview.jpg', direction='-z')

save(image, 'constant_height.jpg')
save(image, 'constant_height.gsf')

molecule = image.system()

save(molecule, '%s_relaxed.xyz' % molecule.name())
