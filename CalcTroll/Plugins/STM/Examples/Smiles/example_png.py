from CalcTroll.Interface import *

setpoint_current = 50
bias_voltage = HOMO(minus=0)
spatial_broadening = 1.0
voltage_broadening = 0.1

molecule = Smiles('CCC')

method = Siesta(xc="PBE",
                     mesh_cutoff=200 * Ry,
                     spin='collinear',
                     basis_set="DZP",
                     )

image = ConstantCurrent(
    system=molecule,
    method=method,
    bias_voltage=bias_voltage,
    setpoint_current=setpoint_current,
    voltage_broadening=voltage_broadening,
    spatial_broadening=spatial_broadening,
)

frame = save(image.system(), 'molecule_topview.png', direction='-z')

save(image, 'topography.png')

molecule = image.system()

save(molecule, '%s_relaxed.png' % molecule.name())
