from ase.units import Ry
from CalcTroll.Interface import *

height = 1
level = HOMO(minus=0)
spatial_broadening = 0.1

system = Dimer("H")

method = Siesta(xc="PBE",
                     mesh_cutoff=200 * Ry,
                     spin='non-polarized',
                     basis_set="DZP",
                     )

image = ConstantHeight(
    system=system,
    method=method,
    height=height,
    voltage_broadening=0.01,
    spatial_broadening=spatial_broadening,
    bias_voltage=level,
)

frame = save(image, '%s_LOW.jpg' % system.name())

system = image.system()
save(system,
     '%s-z.jpg' % system.name(),
     direction='-z',
     frame=frame,
     )

save(system, '%s.xyz'%system.name())
