from ase.units import Ry
from CalcTroll.Interface import *

setpoint_current = 50
level = LUMO()
spatial_broadening = 0.1

system = Dimer("H")

method = Siesta(xc="PBE",
                     mesh_cutoff=200 * Ry,
                     spin='non-polarized',
                     basis_set="DZP",
                     )

image = ConstantCurrent(
    system=system,
    method=method,
    setpoint_current=setpoint_current,
    voltage_broadening=0.01,
    spatial_broadening=spatial_broadening,
    bias_voltage=level,
)

frame = save(image, '%s_RIGHT.jpg' % system.name())

system = image.system()
save(system,
     '%s-z.jpg' % system.name(),
     direction='-z',
     frame=frame,
     )

save(system, '%s.xyz'%system.name())
