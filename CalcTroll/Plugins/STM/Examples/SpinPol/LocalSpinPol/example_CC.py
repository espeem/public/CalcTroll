from CalcTroll.Interface import *

levels = [
          HOMO(minus=1),
          HOMO(minus=0),
         ]
system = SMALL_POLARIZED()
spatial_broadenings = [0.1, 0.5, 1.0]
setpoint_current = 50
frame = None
method = Siesta(spin='collinear',
                basis_set='SZ',
                xc='LDA',
                )
for spatial_broadening in spatial_broadenings:
    for level in levels:
        image = ConstantCurrent(
                system=system,
                method=method,
                setpoint_current=setpoint_current,
                bias_voltage=level,
                spatial_broadening=spatial_broadening,
                )
        frame = save(image, '%s_%s_%.1f.jpg' % (system.name(), level.title(), spatial_broadening), frame=frame)

save(image.system(), '%s.jpg' % system.name(), frame=frame, direction='-z')
