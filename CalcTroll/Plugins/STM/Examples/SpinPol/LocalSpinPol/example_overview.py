from CalcTroll.Interface import *

higher = LUMO(plus=0)
lower = HOMO(minus=0)
setpoint_current = 50
molecule = SMALL_POLARIZED()
method = Siesta(spin='collinear',
                basis_set='SZ',
                xc='LDA',
                )

ewf = EnergyVsWF(system=molecule,
                 method=method,
                 higher=higher,
                 lower=lower,
                 setpoint_current=setpoint_current,
                 )
save(ewf, '%s.jpg' % molecule.name(), change_phase=[(2, 0)])
