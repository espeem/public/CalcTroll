from CalcTroll.Interface import *


bias_range = (-4, 2)
molecule = SMALL_POLARIZED()
filename = molecule.name()
method = Siesta(spin='collinear',
                basis_set='SZ',
                xc='LDA',
                )
image = ConstantCurrent(
               system=molecule,
               method=method,
               bias_voltage=bias_range[0],
               voltage_broadening=0.2,
               setpoint_current=50,
               spatial_broadening=2.0,
               )
save(image, filename + '_image.png')
position=ImageMaximum(image)
didv = dIdV(position=position, bias_range=bias_range)
save(didv, filename + '_didv.png', frame=[[bias_range[0], 0], [bias_range[1], 100]])
save(didv, filename + '_didv.csv')
