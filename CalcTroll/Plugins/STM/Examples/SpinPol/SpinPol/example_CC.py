from CalcTroll.Interface import *

levels = [
          #HOMO(minus=2),
          HOMO(minus=1),
          HOMO(minus=0),
          #LUMO(plus=0),
          #LUMO(plus=1),
          #LUMO(plus=2),
         ]
molecule = Dimer('O')
setpoint_current = 50
method = Siesta(spin='collinear',
                basis_set='SZ',
                xc='LDA',
                )
for level in levels:
    image = ConstantCurrent(
            system=molecule,
            method=method,
            setpoint_current=setpoint_current,
            bias_voltage=level,
            spatial_broadening=0.01,
            )
    save(image, '%s_%s.jpg' % (molecule.name(), level.title()))
