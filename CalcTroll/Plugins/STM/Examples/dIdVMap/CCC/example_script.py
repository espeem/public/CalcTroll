from CalcTroll.Interface import *

setpoint_current = 50
bias_voltage = HOMO(minus=0)
spatial_broadening = 1.0
voltage_broadening = 0.1
dc_signal_spatial_broadening = 2.0 # Must be larger than 'spatial_broadening'

system = Smiles('CCC')

method = Siesta(xc="PBE",
                     mesh_cutoff=200 * Ry,
                     spin='collinear',
                     basis_set="DZP",
                     )

image = dIdVMap(
        system=system,
        method=method,
        bias_voltage=bias_voltage,
        setpoint_current=setpoint_current,
        voltage_broadening=voltage_broadening,
        spatial_broadening=spatial_broadening,
        dc_signal_spatial_broadening=dc_signal_spatial_broadening,
)

frame = save(image.system(), 'molecule_topview.jpg', direction='-z')

save(image, 'didv_map.jpg')
save(image, 'didv_map.gsf')

molecule = image.system()

save(molecule, '%s_relaxed.xyz' % molecule.name())
