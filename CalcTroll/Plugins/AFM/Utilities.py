import numpy as np
from ase import io
import sys

from CalcTroll.Core.Utilities import unique


def translateForCalculation(pf):
    import pyProbeParticle as PPU
    PPU.loadParams(pf)
    translate = np.array(PPU.params['gridA']) + np.array(PPU.params['gridB'])

    return translate/2.0

def forceFieldFromXYZ(filename):
    import pyProbeParticle as PPU
    from   pyProbeParticle            import basUtils
    import pyProbeParticle.HighLevel as PPH
    import pyProbeParticle.GridUtils      as GU
    import pyProbeParticle.core as PPC
    from pyProbeParticle.cpp_utils import PACKAGE_PATH
    data_format = 'npy'
    PPU.loadParams('params.ini')
    atoms, lvec = PPH.importGeometries(filename)
    FFparams = PPU.loadSpecies(PACKAGE_PATH + '/defaults/atomtypes.ini')

    iZs, Rs, Qs = PPH.parseAtoms(atoms, autogeom=False, PBC=False, FFparams=FFparams)
    FFLJ, VLJ = PPH.computeLJ(Rs, iZs, FFLJ=None, FFparams=FFparams, Vpot=False)
    GU.limit_vec_field( FFLJ, Fmax=10.0 ) # remove too large valuesl
    xsfHead=basUtils.bas2xsf(iZs, Rs, [0,0,0], PPU.params['gridA'], PPU.params['gridB'], PPU.params['gridC'] )
    GU.save_vec_field( 'FFLJ', FFLJ, lvec, data_format=data_format, xsfHead=xsfHead)

    FFel, Vel = PPH.computeCoulomb( Rs, Qs, FFel=None, Vpot=False)
    GU.save_vec_field('FFel', FFel, lvec, data_format=data_format, xsfHead=xsfHead)

def interpolateToHeight(data, height):
    values, r = data
    X, Y, Z = r
    z = Z[0, 0, :]
    h_index = (z < height).sum() - 1
    z1, z2 = z[h_index], z[h_index + 1]
    d = z2 - z1
    w1, w2 = (z2-height)/d, (height - z1)/d
    v1, v2 = values[:, :, h_index], values[:, :, h_index + 1]
    v = w1*v1 + w2*v2

    r = np.array([X[:,:,0], Y[:,:,0]])

    return v, r

def calculateAndSaveImages(images):
    import pyProbeParticle as PPU
    from   pyProbeParticle            import basUtils
    import pyProbeParticle.HighLevel as PPH
    import pyProbeParticle.GridUtils      as GU
    import pyProbeParticle.core as PPC
    from pyProbeParticle.cpp_utils import PACKAGE_PATH
    PPU.loadParams('params.ini')
    data_format = 'npy'
    A = PPU.params['Amplitude']
    dz  = PPU.params['scanStep'][2]

    translate = translateForCalculation('params.ini')

    min_height = min([i.height() for i in images])
    max_height = max([i.height() for i in images])
    max_height += A + 0.1

    PPU.params['scanMin'] = np.array((0, 0, min_height))
    PPU.params['scanMax'] = np.array((
        PPU.params['gridA'][0],
        PPU.params['gridB'][1],
        max_height,
        ))

    print(" load Electrostatic Force-field ")
    FFel, lvec, nDim = GU.load_vec_field( "FFel" ,data_format=data_format)

    print(" load Lennard-Jones Force-field ")
    FFLJ, lvec, nDim = GU.load_vec_field( "FFLJ" , data_format=data_format)
    PPU.lvec2params( lvec )
    PPC.setFF( FFLJ )

    xTips,yTips,zTips,lvecScan = PPU.prepareScanGrids( )

    probes = [image.probe() for image in images]
    probes = unique(probes)

    for probe in probes:
        p_images = [image for image in images if image.probe() == probe]
        Q = probe.charge()
        K = probe.stiffness()

        FF = FFLJ + FFel * Q
        PPC.setFF_Fpointer( FF )
        kSpring = -K
        kSpring[2] = 0.0  # Why this?
        PPC.setTip(kSpring=kSpring)
        Fs, rPPs, rTips = PPH.relaxedScan3D( xTips, yTips, zTips )

        fzs = Fs[:, :, :, 2]
        extent = (xTips[0], xTips[-1], yTips[0], yTips[-1])
        dfs = PPU.Fz2df(fzs, dz=dz, k0=PPU.params['kCantilever'], f0=PPU.params['f0Cantilever'], n= int(A/dz) )

        dfs = np.transpose(dfs, (2, 1, 0))
        r = np.transpose(rTips, (3, 2, 1, 0))
        r[0] -= translate[0]
        r[1] -= translate[1]
        data = (dfs, r)

        results = []
        for image in p_images:
            image_data = image.gridToImage(data)
            results.append(image_data)

        image.save_nc(items=p_images, results=results, label='afm')
