import numpy as np
from CalcTroll.Core.Analysis.Probe import Probe

class ParticleProbe(Probe):
    def __init__(self,
            charge=-0.05,
            stiffness=(0.5, 0.5, 0.0),
            probe_type=8,
            ):
        self.__q = charge
        self.__k = np.array(stiffness)
        self.__probe_type = probe_type

        Probe.__init__(self)

    def probeType(self):
        return self.__probe_type

    def charge(self):
        return self.__q

    def stiffness(self):
        return self.__k

class COProbe(ParticleProbe):
    def __init__(self):
        ParticleProbe.__init__(
                self,
                charge=-0.05,
                stiffness=(0.03, 0.03, 1.5),
                probe_type=8)
