from CalcTroll.Interface import *

systems = [Acene(2)]
base_method = Siesta(xc="LDA",
                     mesh_cutoff=100 * Ry,
                     spin='non-polarized',
                     basis_set="SZ",
                     )

for system in systems:
    system = Relaxed(
                 system=system,
                 method=base_method,
                 )

    method = AtomicChargeMethod(base_method=base_method)
    save(system, '%s.jpg' % system.name(), direction='-z')
    s = 0.1

    probe = COProbe()
    for h in [6.0, 6.5, 7.0, 7.5, 8.0]:
        image = QAFM(
                height=h,
                sigma=s,
                probe=probe,
                system=system,
                method=method,
                )

        save(image, '%s_%.1f_%.1f_br.jpg' % (system.name(), h, s), colorscheme='blue-red')
        save(image, '%s_%.1f_%.1f_g.jpg' % (system.name(), h, s), colorscheme='greys')
