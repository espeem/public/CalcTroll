from CalcTroll import API

class ProbeParticleModel(API.Method):
    def __init__(self, base_method=None):
        self.setBaseMethod(base_method)
        super(ProbeParticleModel, self).__init__()

    def setBaseMethod(self, method):
        self.__base_method = method

    def baseMethod(self):
        return self.__base_method

    def makeTasks(
            self,
            items,
            ):

        return []

    def _assertBaseMethod(self):
        if self.baseMethod() is None:
            raise ValueError('PazSoler instance received query that cannot be performed with None as base_method.')

    def runFileEnding(self):
        self._assertBaseMethod()
        return self.baseMethod().runFileEnding()

    def outFileEnding(self):
        self._assertBaseMethod()
        return self.baseMethod().outFileEnding()

    def checkIfFinished(self, filename, runfile=None):
        self._assertBaseMethod()
        return self.baseMethod().checkIfFinished(filename, runfile=runfile)
