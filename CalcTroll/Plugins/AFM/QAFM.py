# Written by Mads Engelund, 2020, http://espeem.com
# Defines the scripting interface to creating Scanning Tunneling Microscopy(STM) images.
import os
import numpy as np
from os.path import join
from scipy.io import netcdf
from CalcTroll import API
from CalcTroll.API import DEFAULT

from CalcTroll.Core.Utilities import apply2DGaussian
from CalcTroll.Core.Utilities import xy
from CalcTroll.Core.Analysis.Image import Image
from CalcTroll.Core.Visualizations import padVariables, addMeasuringStick, addInfo
from CalcTroll.Plugins.LengthScales import LengthScales
from CalcTroll.Plugins.Analysis.ElectrostaticPotential import ElectrostaticPotential
from .ParticleProbe import ParticleProbe
from .Utilities import interpolateToHeight

class QAFM(Image):
    def __init__(
            self,
            system=None,
            method=API.DEFAULT,
            probe=ParticleProbe(),
            height=6.0,
            sigma=0.1,
            ):
        self.__probe = probe
        self.__height = height
        self.__sigma = sigma

        if isinstance(system, API.System):
            cl = LengthScales()['correlation_length']
            parameters = system.parameters().copy(padding=30)
        else:
            parameters = API.DEFAULT

        Image.__init__(
            self,
            system=system,
            method=method,
            parameters=parameters,
            inputs=[],
            )

    def probe(self):
        return self.__probe

    def height(self):
        return self.__height

    def sigma(self):
        return self.__sigma

    def explain(self):
        return API.Explanation("")

    def treatDataForPlotting(self, data, colorscheme, **kwargs):
        if colorscheme == 'blue-red':
            image, variables = data
            take = image > -np.inf
            vmax = image[take].max()
            vmin = image[take].min()
            vmax = max([vmax, -vmin])
            vmin = -vmax

            data = image, variables

            return data, {'vmax':vmax, 'vmin':vmin, 'cmap':'seismic'}

        if colorscheme == 'greys':
            image, variables = data
            take = image > -np.inf
            vmax = image[take].max()
            vmin = image[take].min()

            return data, {'vmax':vmax, 'vmin':vmin, 'cmap':'Greys_r'}

    def gridToImage(self, grid_data):
        height = self.height()
        image_data = interpolateToHeight(grid_data, height)
        image_data = apply2DGaussian(image_data, sigma=self.sigma())

        return image_data

    def plot(
            self,
            data,
            ax=None,
            frame=None,
            info=True,
            bar=True,
            color='black',
            colorscheme='blue-red',
            **kwargs):

        ax.set_aspect('equal')
        ax.set_xticks([])
        ax.set_yticks([])

        image, r = data
        X, Y = padVariables(r)

        if frame is None:
            frame = self.frame(data=data)

        data, kwargs = self.treatDataForPlotting(
                data=data,
                colorscheme=colorscheme,
                **kwargs)
        ax.pcolormesh(X, Y, image, **kwargs)

        ax.set_xlim((frame[0, 0], frame[1, 0]))
        ax.set_ylim((frame[0, 1], frame[1, 1]))

        if bar:
            addMeasuringStick(ax, color=color, size=5)

        if info:
            text = 'h = %.1f $\AA$' % self.height()
            addInfo(ax, text=text, color=color)

        return frame

    def frame(self, data=None, border=API.DEFAULT):
        if data is None:
            data = self.read()

        if isinstance(data, API.CalcTrollException):
            return None

        frame = self.system().frame(border=border)

        return frame

    def selfContained(self):
        return [self.copy()]

    def binaryFile(self):
        return 'afm_images.nc'
