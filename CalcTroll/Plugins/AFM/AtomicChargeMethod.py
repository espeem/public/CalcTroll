import numpy as np
import os
from ase import io
import sys
from os.path import join
from CalcTroll import API
from CalcTroll.Plugins.Analysis import Charges
from CalcTroll.Plugins.Programs.Siesta.Siesta import Siesta
from .QAFM import QAFM
from .Utilities import translateForCalculation


class AtomicChargeMethod(API.Method):
    def __init__(self, base_method=None):
        self.setBaseMethod(base_method)
        super(AtomicChargeMethod, self).__init__()

    def program(self):
        return self.__program

    def setBaseMethod(self, method):
        self.__base_method = method

    def baseMethod(self):
        return self.__base_method

    def findImplementation(self, item):
        if isinstance(item, QAFM):
            return AtomicChargeQAFM([item])

        return None

    def _assertBaseMethod(self):
        if self.baseMethod() is None:
            raise ValueError('PazSoler instance received query that cannot be performed with None as base_method.')

    def runFileEnding(self):
        self._assertBaseMethod()
        return self.baseMethod().runFileEnding()

    def outFileEnding(self):
        self._assertBaseMethod()
        return self.baseMethod().outFileEnding()

    def checkIfFinished(self, filename, runfile=None):
        self._assertBaseMethod()
        return self.baseMethod().checkIfFinished(filename, runfile=runfile)


      


class AtomicChargeQAFM(API.AnalysisImplementation):
    @classmethod
    def canHandle(cls, item):
        if not isinstance(item, QAFM): return False
        if not isinstance(item.method(), AtomicChargeMethod): return False

        return True  

    def makeTasks(self, items):
        task1 = CalculateEFF(items)
        task2 = RelaxScan(task1)

        return [task1, task2]

    def xyzFilename(self):
        return 'AFM.xyz'

    def read(self, item, **kwargs):
        task = self.tasks()[-1]

        return task.readData(item)


class RelaxScan(API.Task):
    def __init__(
            self,
            task,
            ):
        self.__task = task

        API.Task.__init__(
            self,
            path=task.path(),
            method=task.method(),
            identifier=task.parameters().identifier(),
            inputs=[task],
            )

    def explain(self):
        return API.Explanation("")

    def items(self):
        return self.__task.items()

    def write(self):
        script =  'from CalcTroll.Plugins.AFM.Utilities import calculateAndSaveImages\n'
        script +=  'from CalcTroll.Interface import *\n'
        items = self.items()
        if len(items) > 0:
            script += """items = (
"""

            for item in items:
                script += '    ' + item.script(exclude=['system', 'method']) + ',\n'
            script += ')\n'


        script += """\
calculateAndSaveImages(items)
print("JOB COMPLETED")
"""
        with open(self.runFile(), 'w') as f:
            f.write(script)

    def _getLocalStatus(self):
        if os.path.exists(self.outFile()):
            with open(self.outFile(), 'r') as f:
                lines = f.readlines()
            if len(lines) > 0 and 'JOB COMPLETED\n' in lines:
                pass
            else:
                return API.FAILED
        else:
            return API.NOT_STARTED

        items = self.items()
        items = [item for item in items if not isinstance(item, API.CalcTrollException)]

        missing = []
        for item in items:
            f = join(self.runDirectory(path=API.LOCAL), item.binaryFile())
            in_file = item.inNCFile(f)
            missing.append(in_file is False)

        if any(missing):
            return API.NOT_STARTED
        else:
            return API.DONE

    def makeCommands(self):
        define_prog = "export PYTHON=%s" % sys.executable
        runfile = self.runFile(API.TAIL)

        gen = "$PYTHON %s &> %s" % (runfile, self.outFile(API.TAIL))

        return [
                define_prog,
                gen,
                ]

    def readData(self, item=None, path=API.LOCAL):
        if item is None:
            item = self.items()[-1]

        path = self.runDirectory(path=path)

        return item.readData(path)

class CalculateEFF(API.Task):
    def __init__(self, items):
        self.__items = items
        item0 = items[0]
        method = item0.method()
        base_method = method.baseMethod()
        system = item0.system()
        parameters=item0.parameters()

        charges = Charges(
                    system=system,
                    method=base_method,
                    parameters=parameters,
                    )
        self.__q = charges
        task = charges.tasks()[0]

        API.Task.__init__(
            self,
            path=task.path(),
            method=method,
            identifier=task.parameters().identifier(),
            inputs=[task],
            )
    def items(self):
        return self.__items

    def parameters(self):
        return self.__q.parameters()

    def method(self):
        return self.system().method()

    def explain(self):
        ex = API.Explanation("QAFM images were calculated according to the ProbeParticleModel.[*]",
                [ "Prokop Hapala et al. etc."])

        return ex

    def system(self):
        return self.__q.system()

    def write(self):
        q = self.__q.read()
        rundir = self.runDirectory()
        xyz_file = join(rundir, self.xyzFilename())
        pf = join(rundir, self.paramsFilename())
        atoms = self.system().atoms(parameters=self.__q.parameters())
        writeParams(atoms, pf)
        atoms.set_initial_charges(q)
        translate = translateForCalculation(pf)
        atoms.translate(translate)
        writeProkopTypeXYZ(xyz_file, atoms)

        script = """\
from CalcTroll.Plugins.AFM.Utilities import forceFieldFromXYZ

forceFieldFromXYZ('%s')
print('JOB COMPLETED')
""" % self.xyzFilename()

        runfile = self.runFile()
        with open(runfile, 'w') as pf:
            pf.write(script)

    def paramsFilename(self):
        return 'params.ini'

    def xyzFilename(self):
        return 'AFM.xyz'

    def makeCommands(self):
        define_prog = "export PYTHON=%s" % sys.executable
        runfile = self.runFile(API.TAIL)

        gen = "$PYTHON %s &> %s" % (runfile, self.outFile(API.TAIL))

        return [
                define_prog,
                gen,
                ]

    def _getLocalStatus(self):
        if os.path.exists(self.outFile()):
            with open(self.outFile(), 'r') as f:
                lines = f.readlines()
            if len(lines) > 0 and 'JOB COMPLETED\n' in lines:
                return API.DONE
            else:
                return API.FAILED
        else:
            return API.NOT_STARTED


def writeProkopTypeXYZ(filename, atoms):
    init_line = str(len(atoms))
    r = atoms.get_positions()
    ch = atoms.get_chemical_symbols()
    q = atoms.get_initial_charges()
    lines = [str(len(atoms)) + '\n']
    for i in range(len(atoms)):
        line = '%s\t%4.5f\t%4.5f\t%4.5f\t%4.5f\n' % (ch[i], r[i, 0], r[i, 1], r[i, 2], q[i])
        lines.append(line)

    with open(filename, 'w') as f:
        for line in lines:
            f.write(line)

def writeParams(atoms, filename):
    r = atoms.get_positions()
    cell = atoms.get_cell()
    mn = r.min(axis=0)
    mx = r.max(axis=0)

    scan_min = np.array((0, 0, 0))
    scan_max = cell.sum(axis=0)

    size = scan_max - scan_min

    script = """\
# main parameters for the calculations  #
probeType       8    #
charge          0.05 #
stiffness       0.5  0.5  20.00  #
r0Probe         0.0  0.0   4.00  #
PBC             False"""

    script += """
gridA           %.0f    0.00      0.00    #
gridB           0.00    %.0f      0.00    #
gridC           0.00    0.00      %.0f    #""" \
% tuple(size)

    with open(filename, 'w') as f:
        f.write(script)
