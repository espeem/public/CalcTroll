# Written by Mads Engelund, 2017, http://espeem.com
import os
import sys
import unittest
import importlib
import inspect

from CalcTroll.Core.Test.Case import CalcTrollTestCase

TEST_SUITE = unittest.TestSuite()

def importWalk(test_path, method=None):
    if method is None:
        def method(imp):
            print(("Importing %s..."% imp))
            importlib.import_module(imp)


    cwd = os.getcwd()
    for path, dirs, files in os.walk(test_path):
        if 'Test' in path:
            continue

        if 'Setup' in path:
            continue

        for f in files:
            if f[-3:] != '.py':
                continue
            elif f == 'Tests.py':
                continue

            spath = path.split(os.path.sep)
            imp = '.'.join(spath)
            imp = '.'.join([imp, f[:-3]])
            if isExample(imp):
                os.chdir(path)

            method(imp)
            os.chdir(cwd)


def isTest(module_name):
    name = module_name.split('.')[-1]

    return name[-4:] == 'Test'

def isExample(module_name):
    name = module_name.split('.')[-1]

    return name[:8] == 'example_'

def isPluginModule(module_name):
    return not (isTest(module_name) or isExample(module_name))

def findTestCases(imp):
    if not isTest(imp):
        return

    #print "Importing %s..."%imp
    mod = importlib.import_module(imp)
    for key in list(mod.__dict__.keys()):
        if '__' in key:
            continue
        code = 'from %s import %s as tmp'%(imp, key)
        d = locals()
        exec(code, d)
        tmp = d['tmp']

        if inspect.isclass(tmp) and issubclass(tmp, CalcTrollTestCase):
            TEST_SUITE.addTest(unittest.makeSuite(tmp))

class ImportTests(CalcTrollTestCase):
    def testImportAll(self):
        cwd = os.getcwd()
        before = os.listdir(cwd)
        for test_path in self.test_paths:
            importWalk(test_path=test_path)
        os.chdir(cwd)
        after = os.listdir(cwd)

        self.markForRemoval(list(set(after) - set(before)))

