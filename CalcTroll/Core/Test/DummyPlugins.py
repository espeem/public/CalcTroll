# Written by Mads Engelund, 2017, http://espeem.com
TEST_ATOMS = Atoms('CC', [[0, 0, -1], [0, 0, 1]])

class TestRelaxationTask(API.Task):
    def __init__(self,
                 system,
                 method,
                 path=DEFAULT,
                 host=DEFAULT,
                 submit_type=DEFAULT,
                 ):
        API.Task.__init__(
                self,
                method=method,
                path=path,
                host=host,
                submit_type=submit_type,
                )
        self.__system = system

    def _getStatus(self):
        return DONE

    def atoms(self):
        atoms = self.__system.atoms()
        atoms.positions *= 1.1

        return atoms


class TestHost(API.Host):
    def dataPath(self):
        return self.localDataPath()

    def localDataPath(self):
        return CalcTrollTestCase.pathForTesting(__file__)


class TestMethod(API.Method):
    def makeTasks(self, items):
        tasks = []
        for item in items:
            if isinstance(item, Relaxed):
                path = item.systemClass().className()
                tasks.append(
                        TestRelaxationTask(
                             system=item.system(),
                             method=self,
                             path=path,
                             host=TestHost(),
                             submit_type='Test'),
                        )

        return tasks

    def outFileEnding(self):
        return 'test'

    def runFileEnding(self):
        return 'test'

    def checkIfFinished(self, outfile, runfile):
        return True

class TestSubSystem(API.System):
    @classmethod
    def className(cls):
        return 'TestSub'

    def defaultParameters(self, parameters=DEFAULT):
        return parameters

    def makeAtomsRelaxed(self, atoms, relaxation):
        atoms = atoms.copy()
        atoms.positions = relaxation.positions

        return atoms

    def atoms(self, parameters=DEFAULT):
        return TEST_ATOMS.copy()

    def subSystems(self):
        return []


class TestSystem(API.System):
    @classmethod
    def className(cls):
        return 'Test'

    def __init__(self):
        API.System.__init__(self, sub_systems=[TestSubSystem()])

    def defaultParameters(self, parameters=DEFAULT):
        return parameters

    def subParameters(self, parameters=DEFAULT):
        return (DEFAULT, )

    def makeAtomsRelaxed(self, atoms, relaxation):
        atoms = atoms.copy()
        atoms.positions = relaxation.positions

        return atoms

    def atoms(self, parameters=DEFAULT):
        atoms = self.subSystems()[0].atoms()

        return atoms
