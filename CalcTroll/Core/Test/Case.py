# Written by Mads Engelund, 2017, http://espeem.com
# Defines the internal testsuite class which as a few additional features. It has convenient assertions for ASE Atoms
# object and for setting up a test directory with specific files.

import os
from os.path import join
import unittest
import numpy as np
import tempfile
import shutil

def formatArray(array, max_size=None, level=0, prestring=''):
    if max_size is None:
        max_size = np.array(list(map(len, list(map(str, array.flatten()))))).max()

    if isinstance(array, np.ndarray):
        line = [formatArray(entry, max_size=max_size, level=level+1) for entry in array]
        if isinstance(array[-1], np.ndarray):
            joiner =  ',\n' + ' '*(level+1 + len(prestring))
        else:
            joiner = ', '
        string = prestring  + '['+ joiner.join(line) + ']'
    else:
        string = str(array) + (max_size-len(str(array)))*' '

    return string

class CalcTrollTestCase(unittest.TestCase):
    def setUp(self):
        self.__previous_directory = os.getcwd()
        self.__remove_paths = []
        self.__remove_files = []

    def tearDown(self):
        os.chdir(self.__previous_directory)
        for path in self.__remove_paths:
            if os.path.exists(path):
                shutil.rmtree(path)

        for f in self.__remove_files:
            os.remove(f)

    def markForRemoval(self, files):
        self.__remove_files.extend(files)

    @staticmethod
    def formatArray(array, max_size=None, level=0, prestring=''):
        return formatArray(array=array, max_size=max_size, level=level,
                prestring=prestring)

    def prepareTestDirectory(self, testcase_file, tail=''):
        abspath = os.path.abspath(testcase_file)
        tempdir = tempfile.mkdtemp()
        self.__remove_paths.append(tempdir)
        tempdir = join(tempdir, 'TestFiles')

        # If the directory TestFiles exists, then copy it to temp folder.
        test_file_dir = join(os.path.dirname(abspath), 'TestFiles', tail)
        if os.path.exists(test_file_dir):
            shutil.copytree(test_file_dir, tempdir)

        if not os.path.exists(tempdir):
            os.makedirs(tempdir)

        os.chdir(tempdir)

        return tempdir


    def assertArraysEqual(self, array1, array2, tolerance=5):
        array1 = np.array(array1)
        array2 = np.array(array2)

        if array1.shape != array2.shape:
            message = "Arrays of different shape:"
            if array2.shape[0] == 0:
                raise AssertionError("Empty array: Shape %s expected" % list(array1.shape))
            received = np.around(array2, tolerance)
            received = formatArray(received, prestring='        array = ')
            shape_string = 'expected shape: %s\treceived shape:%s'%(array1.shape, array2.shape)
            string = "\n%s\n%s\n%s\n%s"%(message, received, message, shape_string)

            raise AssertionError(string)

        finite_match = (np.isfinite(array1) == np.isfinite(array2)).all()
        undef_match = (np.isnan(array1) == np.isnan(array2)).all()
        pattern_match = finite_match and undef_match
        is_complex = np.iscomplexobj(array1)

        if is_complex:
            if not pattern_match:
                fin_string = 'finite match = %s'%finite_match
                nan_string = 'undef match = %s'%undef_match
                string = "\nThe two arrays have different patterns of finite/infinite/undefined values:\n%s\n%s"%(
                    fin_string,
                    nan_string,
                )
                raise AssertionError(string)


        else:
            pinf_match = (np.isposinf(array1) == np.isposinf(array2)).all()
            ninf_match = (np.isneginf(array1) == np.isneginf(array2)).all()
            pattern = pattern_match and pinf_match and ninf_match
            if not pattern_match:
                fin_string = 'finite match = %s'%finite_match
                pin_string = '+inf match = %s'%pinf_match
                nin_string = '-inf match = %s'%ninf_match
                nan_string = 'undef match = %s'%undef_match
                string = "\nThe two arrays have different patterns of finite/infinite/undefined values:\n%s\n%s\n%s\n%s"%(
                    fin_string,
                    pin_string,
                    nin_string,
                    nan_string,
                )
                raise AssertionError(string)

        finite_pattern = np.isfinite(array1)
        diff = np.zeros(array1.shape, dtype=array1.dtype)
        diff[finite_pattern] = array1[finite_pattern] - array2[finite_pattern]
        pattern = abs(diff) > 10**(-tolerance)
        if any(pattern.flatten()):
            array1 = np.around(array1, tolerance)
            array2 = np.around(array2, tolerance)
            diff = np.around(diff, tolerance)

            expected = formatArray(array1, prestring='expected = ')
            received = formatArray(array2, prestring='received = ')
            error_pattern = formatArray(pattern, prestring='pattern = ')
            difference = formatArray(diff, prestring='expected - received = ')
            error_pattern = error_pattern.replace('True ', 'Fail').replace('False', 'Pass')

            string = "\nThe two array-like quantities are not equal:\n%s\n%s\n%s\n%s"%(
                    expected,
                    received,
                    difference,
                    error_pattern,
                    )


            string += '\n'
            error_indices = [str(i) for i, p in enumerate(pattern) if any(p)]
            string += 'Error at indices: ' + ','.join(error_indices)

            raise AssertionError(string)

    def assertAtomsEqual(self, atoms1, atoms2, tolerance=5):
        self.assertEqual(atoms1.get_chemical_symbols(), atoms2.get_chemical_symbols())
        self.assertArraysEqual(atoms1.positions, atoms2.positions, tolerance=tolerance)
