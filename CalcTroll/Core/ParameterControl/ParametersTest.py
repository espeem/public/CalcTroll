# Written by Mads Engelund, 2017, http://espeem.com
import unittest
from ase.visualize import view
from ase.units import *
from collections import OrderedDict
from inspect import Signature

from CalcTroll.Core.Test.Case import CalcTrollTestCase
from CalcTroll.ASEUtils.AtomUtilities import fixBelow
from CalcTroll.Core.ParameterControl import ParameterControl
from CalcTroll.Core.Flags import ALL, MINIMAL

class ParametersTest(CalcTrollTestCase):
    def classConstruction(self, cls):
        self.assertEqual(cls.argumentNames(), ['x', 'fix'])
        self.assertEqual(cls.defaults(),
                OrderedDict([('x', Signature.empty), ('fix', True)]))

    def testClassConstruction(self):
        class Test(ParameterControl):
            def __init__(self, x, fix=True): pass
        self.classConstruction(Test)
        self.assertEqual(Test.className(), 'Test')

        class Test2(Test):pass
        self.classConstruction(Test2)
        self.assertEqual(Test2.className(), 'Test2')

        class Test3(Test):
            def __init__(self, x, fix=True): pass
        self.classConstruction(Test3)
        self.assertEqual(Test3.className(), 'Test3')

    def testCallToDictionary(self):
        class Test(ParameterControl):
            def __init__(self, x, y, fix=True, nonsence=9): pass
        call = ([4], {'fix':False})
        self.assertEqual(
                Test.callToDictionary(call, detail_level=MINIMAL),
                OrderedDict([('x', 4), ('fix', False)]),
                )
        self.assertEqual(
                Test.callToDictionary(call, detail_level=ALL),
                OrderedDict([
                    ('x', 4),
                    ('y', Signature.empty),
                    ('fix', False),
                    ('nonsence', 9),
                    ]
                    ),
                )
        call = ([4, 8], {'fix':True})
        self.assertEqual(
                Test.callToDictionary(call, detail_level=MINIMAL),
                OrderedDict([('x', 4), ('y', 8), ('fix', True)]),
                )

    def testMinimalConstruction(self):
        class Test(ParameterControl):
            def __init__(self, x=4): pass

        class Test2(Test):
            def __init__(self):
                Test.__init__(self, x=5)


        default = Test2()
        self.assertEqual(default.get('x', Test), 5)

    def testDefaultConstruction(self):
        class Test(ParameterControl):
            def __init__(self, x=1, fix=True): pass

        default = Test()
        self.assertEqual(default['x'], 1)
        self.assertEqual(default['fix'], True)
        self.assertEqual(default.className(), 'Test')
        self.assertEqual(default.script(MINIMAL), 'Test()')
        self.assertEqual(default.script(ALL), 'Test(x=1, fix=True)')

    def testConstruction(self):
        class Test(ParameterControl):
            def __init__(self, x=1, fix=True): pass

        instance = Test(fix=False)
        self.assertEqual(instance['x'], 1)
        self.assertEqual(instance['fix'], False)

    def testString(self):
        class Test(ParameterControl):
            def __init__(self, x=1, fix=True): pass

        default = Test()
        self.assertEqual(default.script(detail_level=MINIMAL), 'Test()')

    def testGetItem(self):
        class Test(ParameterControl):
            def __init__(self, grid_cutoff=89, what_is_this=4, free_layers=5): pass

            def freeLayers(self):
                return 10

        instance = Test(grid_cutoff=40)
        self.assertEqual(instance['grid_cutoff'], 40)
        self.assertEqual(instance['what_is_this'], 4)
        self.assertEqual(instance['free_layers'],  5)
        self.assertEqual(instance.freeLayers(),  10)

    def testMultilevel(self):
        class Level1(ParameterControl):
            def __init__(self,
                         cutoff=89,
                         extra=6,
                         ): pass
        level1 = Level1()
        class Level2(ParameterControl):
            def __init__(self,
                         group=level1,
                         what_is_this=4,
                         free_layers=5,
                         ):
                pass

        instance = Level2(free_layers=6, group=Level1(cutoff=8))
        self.assertEqual(instance['what_is_this'], 4)
        self.assertEqual(instance['free_layers'], 6)
        self.assertEqual(instance.script(detail_level=MINIMAL), 'Level2(group=Level1(cutoff=8), free_layers=6)')
        self.assertEqual(instance.script(detail_level=ALL), 'Level2(group=Level1(cutoff=8, extra=6), what_is_this=4, free_layers=6)')

    def testGet(self):
        class Level1(ParameterControl):
            def __init__(self,
                         cutoff=89,
                         extra=6,
                         group=3,
                         ): pass
        class Level2(Level1):
            def __init__(self,
                         group=Level1(),
                         what_is_this=4,
                         free_layers=5,
                         ):
                Level1.__init__(self)

        l2 = Level2(group=44)

        self.assertEqual(l2.get('group', cls=Level2), 44)
        self.assertEqual(l2.get('group', cls=Level1), 3)

    def testItems(self):
        class Test(ParameterControl):
            def __init__(self,
                         cutoff=89,
                         extra=6,
                         group=3,
                         ): pass

        self.assertEqual(
                [(key, value) for key,value in list(Test(extra=7).items())],
                [('cutoff', 89), ('extra', 7),('group',3)],
                )

    def testEqualOperator(self):
        class Level1(ParameterControl):
            def __init__(self,
                         cutoff=89,
                         extra=6,
                         group=3,
                         ): pass
        class Level2(Level1):
            def __init__(self,
                         group=Level1(),
                         what_is_this=4,
                         free_layers=5,
                         ):
                Level1.__init__(self)

        i1 = Level2(group=44)
        i2 = i1.copy()
        self.assertNotEqual(id(i1), id(i2))
        self.assertEqual(i1, i2)
        i2 = i1.copy(group=Level1())
        self.assertNotEqual(i1, i2)
        i3 = i2.copy(group=Level1())
        self.assertEqual(i2, i3)

    def testInheritance(self):
        class Inheritor1(ParameterControl):
            def __init__(self, argument=5):
                pass

        class Inheritor2(Inheritor1):pass

        run = Inheritor2(argument=9)
        self.assertEqual(run.script(), 'Inheritor2(argument=9)')

if __name__=='__main__':
    unittest.main()
