# Written by Mads Engelund, 2017, http://espeem.com
# This defines the base class of all plugins.
# The common functionality that this module defines is that the original call that sets up an instance is recorded.
# This means that the plugin can reproduce the script that created it and that it can be copied perfectly or while
# replacing one keyword.

import os
import numpy as np
import inspect
from collections import OrderedDict
import traceback
import functools

from CalcTroll.Core.Flags import ALL, MINIMAL, EXPANDED, DEFAULT, CalcTrollFlag
from CalcTroll.Core.Utilities import generateHash

def camelCase(string):
   words = string.split('_')
   first = words[0]
   return first + ''.join((w.capitalize() for w in words[1:]))


def copyInput(args, kwargs):
    nargs = []
    for arg in args:
        if isinstance(arg, ParameterControl):
            pass
        elif hasattr(arg, 'copy'):
            arg = arg.copy()

        nargs.append(arg)

    nkwargs = OrderedDict()
    for key, value in list(kwargs.items()):
        if isinstance(value, ParameterControl):
            pass
        elif hasattr(value, 'copy'):
            value = value.copy()

        nkwargs[key] = value

    return nargs, nkwargs


class InspectParameters(type):
    def __new__(cls, clsname, bases, dct):
        result = type.__new__(cls, clsname, bases, dct)

        oldinit = None
        signature = inspect.signature(object.__init__)
        if '__init__' in list(result.__dict__.keys()):
            signature = inspect.signature(result.__init__)
        else:
            for c in inspect.getmro(result)[1:]:
                if '__oldinit__' in list(c.__dict__.keys()):
                    try:
                        signature = inspect.signature(c.__oldinit__)
                    except TypeError:
                        pass

                    oldinit = c.__oldinit__
                    break

        if oldinit is None:
            oldinit = result.__init__

        def initStub(self, *args, **kwargs):
            args, kwargs = copyInput(args, kwargs)
            self.__calls[result] = args, kwargs

            try:
                tmp = result.__oldinit__(self, *args, **kwargs)
            except TypeError as error:
                text = traceback.format_exc()
                message = "%s: " % clsname +  str(error) + text
                print(message)

                raise TypeError({"message": message})

            return tmp

        result.__oldinit__ = oldinit
        result.__init__ = initStub
        result.__signature = signature

        return result

    def _resetCalls(self):
        self.__calls = OrderedDict()

    def _calls(self):
        return self.__calls

    def defaults(self):
        p = self.__signature.parameters
        defaults = OrderedDict()
        for name in p:
            defaults[name] = p[name].default
        del(defaults['self'])

        return defaults

    def argumentNames(cls):
        return list(cls.defaults().keys())

    def callToDictionary(cls, call, detail_level=MINIMAL):
        args, kwargs = call
        args, kwargs = copyInput(args, kwargs)
        argument_names = cls.argumentNames()

        # Convert all to keywords.
        for init_arg, arg in zip(argument_names, args):
            kwargs[init_arg] = arg

        defaults = cls.defaults()
        if detail_level is ALL:
            nkwargs = defaults.copy()
        elif detail_level is MINIMAL:
            nkwargs = OrderedDict()

        for arg in argument_names:
            if arg in list(kwargs.keys()):
                nkwargs[arg] = kwargs[arg]

        return nkwargs

class ParameterControl(metaclass=InspectParameters):
    def __hash__(self):
        return self.hash()
    
    def hash(self):
        if self.__hash == None:
            self.__hash = self.generateHash()
        
        return self.__hash

    def generateHash(self):
        return generateHash(self.script(detail_level=ALL))

    def __repr__(self):
        return self.script()

    def __eq__(self, other):
        if not hasattr(other, 'script'):
            return False

        s1 = self.script(detail_level=MINIMAL)
        s2 = other.script(detail_level=MINIMAL)

        result = s1 == s2

        return result

    def __ne__(self, other):
        return not self == other

    def __gt__(self, other):
        return repr(self) > repr(other)

    def __new__(self, *args, **kwargs):
        s = super(ParameterControl, self)
        if s.__new__ == object.__new__:
            result = object.__new__(self)
        else:
            result = super(ParameterControl, self).__new__(self, *args, **kwargs)

        InspectParameters._resetCalls(result)
        result.__hash = None

        return result

    def __getitem__(self, key):
        return self.get(key)

    def _modifyCall(self, **kwargs):
        assert set(kwargs.keys()).issubset(self.keys())
        cls = self.__class__
        call = InspectParameters._calls(self).get(cls)
        call[1].update(**kwargs)

    def __init__(self): pass

    def path(self):
        h = self.hash()
        path = '%s_%s' % (self.className(), h)

        return path

    def identifier(self):
        values = list(self.values())
        identifier = []
        for value in values:
            if isinstance(value, ParameterControl):
                identifier.append(value.identifier())
            else:
                identifier.append(value)

        return identifier

    def get(self, key, cls=DEFAULT):
        if cls is DEFAULT:
           cls = self.__class__

        call = InspectParameters._calls(self).get(cls)
        non_default_dict = cls.callToDictionary(call)

        if not key in list(non_default_dict.keys()):
            try:
                result = cls.defaults()[key]
            except KeyError:
                raise KeyError("%s does not take the argument %s"%(cls, key))
            return result
        else:
            return non_default_dict[key]

    def items(self):
        return iter(list(self.toDictionary(detail_level=ALL).items()))

    def toDictionary(self, detail_level=MINIMAL):
        call = InspectParameters._calls(self).get(self.__class__)
        return self.__class__.callToDictionary(call, detail_level=detail_level)

    def keys(self):
        return self.__class__.argumentNames()

    def values(self):
        return list(self.toDictionary(detail_level=ALL).values())

    def copy(self, *args, **kwargs):
        init_args = list(self.keys())
        assert len(args) < len(init_args)

        nkwargs = self.toDictionary(detail_level=ALL)
        minimal_arguments = set(\
                list(self.toDictionary(detail_level=MINIMAL).keys()) +\
                list(kwargs.keys()))

        # Convert all to keywords.
        for init_arg, arg in zip(init_args, args):
            nkwargs[init_arg] = arg

        nkwargs.update(kwargs)

        fkwargs = OrderedDict()
        for key, value in list(nkwargs.items()):
            if key in minimal_arguments:
                fkwargs[key] = value

        new = self.__class__(**fkwargs)

        return new

    def script(
            self,
            detail_level=MINIMAL,
            style=MINIMAL,
            exclude=tuple(),
            variable=None,
            ):
        name = self.className()
        dictionary = self.toDictionary(detail_level)
        for item in exclude:
            del dictionary[item]

        if style is MINIMAL:
            script = makeMinimalScript(
                         name=name,
                         dictionary=dictionary,
                         detail_level=detail_level,
                         variable=variable,
                         )
        if style is EXPANDED:
            script = makeExpandedScript(
                         name=name,
                         dictionary=dictionary,
                         detail_level=detail_level,
                         variable=variable,
                         )

        return script

    def save(self, filename, data=None, **kwargs):
        if data is None:
            data = self.read()

        basename, ext = os.path.splitext(filename)

        func = ext[1:] + '_save'
        save_function = getattr(self, func)
        frame = save_function(filename, data, **kwargs)

        return frame

    @classmethod
    def className(cls):
        string = str(cls)[:-2].split('.')[-1]

        return string

def makeMinimalScript(name, dictionary, detail_level, variable=None):
    arguments = []
    script = ''
    if variable is not None:
        script += variable + ' = '
    script = '%s('%name
    for key, value in list(dictionary.items()):
        string = scriptValue(value, detail_level)
        arguments.append('%s=%s'%(key, string))

    script += ', '.join(arguments)
    script += ')'

    return script

def makeExpandedScript(name, dictionary, detail_level, variable=None):
    arguments = []
    first_line = ''
    if variable is not None:
        first_line += variable + ' = '
    first_line += '%s('%name
    offset = len(first_line)
    p_scripts = []
    for key, value in list(dictionary.items()):
        if isinstance(value, ParameterControl):
            string = key
            v_script = value.script(detail_level=detail_level, variable=key, style=EXPANDED)
            p_scripts.append(v_script)
        else:
            string = scriptValue(value, detail_level)
        arguments.append('%s=%s'%(key, string))

    script = ''
    p_scripts = sorted([(len(s), s) for s in p_scripts])
    for lenght, p_script in p_scripts:
        script += p_script
        script += '\n\n'

    script += first_line
    script += (',\n' + ' ' * offset).join(arguments)
    if len(arguments) <=1:
        script += ')'
    else:
        script += ',\n' + ' '* offset + ')'

    return script

def scriptValue(value, detail_level):
    if hasattr(value, 'script'):
        string = value.script(detail_level=detail_level)
    elif isinstance(value, np.ndarray):
        string = str(tuple(value))
    elif isinstance(value, tuple):
        if len(value) == 0:
            string = 'tuple()'
        elif len(value) == 1:
            string = '(%s,)' % scriptValue(value[0], detail_level)
        else:
            string = '(' + ', '.join([scriptValue(entry, detail_level) for entry in value])
            string += ')'
    elif isinstance(value, list):
        string = '[' + ', '.join([scriptValue(entry, detail_level) for entry in value]) + ']'
    elif isinstance(value, str):
        if '\n' in value:
            string = '"""' + value + '"""'
        else:
            string = "'" + value + "'"
    elif inspect.isclass(value) and issubclass(value, CalcTrollFlag):
        string =  value.className()
    else:
        string = str(value)

    return string
