# Written by Mads Engelund, 2017, http://espeem.com
from CalcTroll.Core.ParameterControl import ParameterControl, Parameters

class InitialStateAtom(Parameters):
    def __init__(self, identifier, move_vector=(0,0,0), initial_moment=0): pass

class InitialState(ParameterControl):
    def __init__(
            self,
            name='InitialState',
            state_list=tuple(),
            default_magnetic_moment=0,
            ):
        self.__list = list(state_list)
        self.__name = name
        self.__default_magnetic_moment = default_magnetic_moment

    def defaultMagneticMoment(self):
        return self.__default_magnetic_moment

    def name(self):
        return self.__name

    def toList(self):
        return [item.copy() for item in self.__list]

    def __getitem__(self, sl):
        if isinstance(sl, int):
            return self.__list[sl]
        elif isinstance(sl, slice):
            return self.__class__(self.__list[sl])
        else:
            return ParameterGroup.__getitem__(self, sl)

