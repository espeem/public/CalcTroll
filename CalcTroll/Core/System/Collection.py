# Written by Mads Engelund, 2017, http://espeem.com
# Authored by Mads Engelund.

# All physical systems under consideration must use 'System' as a base class.
# In the CalcTroll code a system should be defined such that it can create
# different calculations that will approximate the true physical system,
# for instance a surface might be approximated by slabs of different width
# super-cells or it could be defined using open boundary conditions.

import numpy  as np
from math import sqrt,pi
from numpy import matrix
from ase.io import write
from ase.visualize import view

from CalcTroll.ASEUtils.Atoms import Atoms
from CalcTroll.ASEUtils.AtomUtilities import makeAtomsRelaxed
from CalcTroll.ASEUtils.AtomUtilities import findCopies
from CalcTroll.ASEUtils.AtomUtilities import findPeriodicallyEqualAtoms
from CalcTroll.Core.Flags import DEFAULT
from CalcTroll.Core.ParameterControl import Parameters
from CalcTroll.Core.Submission.Submitter import calculate

from .BasisChanger import BasisChanger
from .System import System


class Collection(System):
    def __init__(self, systems, name=DEFAULT):
        for system in systems:
            assert isinstance(system, System)

        if name == DEFAULT:
           name = systems[0].name() + "_Collection"

        System.__init__(
                self,
                sub_systems=systems,
                name=name)
    
    def __getitem__(self, index):
        return self.subSystems()[index]

    def baseSystem(self):
        return self.subSystems()[0]

    def defaultParameters(self, parameters=DEFAULT):
        return self.baseSystem().defaultParameters(parameters=parameters)

    def subParameters(self, parameters=DEFAULT):
        return [system.defaultParameters(parameters=parameters) for system in self.subSystems()]

    def atoms(self, parameters=DEFAULT, constrained=True, initialized=True, relaxed=True):
        result = []
        for system in self.subSystems():
            atoms = system.atoms(
                                 parameters=parameters,
                                 constrained=constrained,
                                 initialized=initialized,
                                 relaxed=relaxed,
                                 )
            result.append(atoms)

        return result

    def frame(self, data, **kwargs):
        return self.baseSystem().frame(**kwargs)

    def plotUnitCell(self, **kwargs):
        return self.baseSystem().plotUnitCell(**kwargs)

    def jpg_save(self, filename, data, **kwargs):
        return self.baseSystem().jpg_save(filename, data[0], **kwargs)

    def pbc(self):
        return self.baseSystem().pbc()
