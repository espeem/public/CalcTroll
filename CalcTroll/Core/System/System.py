# Written by Mads Engelund, 2017, http://espeem.com
# Authored by Mads Engelund.

# All physical systems under consideration must use 'System' as a base class.
# In the CalcTroll code a system should be defined such that it can create
# different calculations that will approximate the true physical system,
# for instance a surface might be approximated by slabs of different width
# super-cells or it could be defined using open boundary conditions.

import numpy  as np
from math import sqrt,pi
from numpy import matrix
from ase.io import write
from ase.visualize import view

from CalcTroll.ASEUtils.Atoms import Atoms
from CalcTroll.ASEUtils.AtomUtilities import makeAtomsRelaxed
from CalcTroll.ASEUtils.AtomUtilities import findCopies
from CalcTroll.ASEUtils.AtomUtilities import findPeriodicallyEqualAtoms
from CalcTroll.Core.Flags import DEFAULT, CalcTrollException, CalcTrollFlag
from CalcTroll.Core.Plugin.Types import SystemProvider
from CalcTroll.Core.ParameterControl import Parameters
from CalcTroll.Core.Submission.Submitter import calculate
from CalcTroll.Core.Visualizations import addMeasuringStick
from CalcTroll.Core.Visualizations import flatMatplotlibView
from CalcTroll.Core.Utilities import generateHash
from .BasisChanger import BasisChanger


class System(SystemProvider):
    COORDINATES=None
    ATOMS_STORE=dict()

    def __init__(
            self,
            sub_systems=tuple(),
            initial_state=DEFAULT,
            name=DEFAULT,
            parameters=DEFAULT,
            ):
        self.__sub_systems = sub_systems
        self.__basis_changer = None
        if initial_state is DEFAULT:
            initial_state = tuple()
        self.__initial_state = initial_state

        if name is DEFAULT:
            name = self._makeName()
        self.__name = name

        if isinstance(parameters, dict):
            parameters = self.parameterClass()(**parameters)

        self.__parameters = self.defaultParameters(parameters=parameters)

    def __call__(self, *args, **kwargs):
        return self.copy(*args, **kwargs)

    def generateHash(self):
        return generateHash(self.script())

    def _makeName(self):
        h =  self.generateHash()
        name = "%s_%s" % (self.className(), h)

        return name

    @classmethod
    def parameterClass(cls):
        message = "%s lacks an the method parameterClass" % cls.className()
        raise NotImplementedError(message)

    def atoms(self,
              parameters=DEFAULT,
              constrained=True,
              initialized=True,
              relaxed=True,
              ):
        if isinstance(parameters, dict):
            parameters = self.parameterClass()(**parameters)
        elif parameters == DEFAULT:
            if hasattr(self, 'parameters'):
                parameters = self.parameters()
        parameters = self.defaultParameters(parameters=parameters)

        key = (self.script(), parameters.script(), constrained, initialized, relaxed)
        if key in self.ATOMS_STORE.keys():
            return self.ATOMS_STORE[key].copy()


        # Only relax if it is possible.
        relaxed = relaxed and self.isRelaxed()

        # Only initialize if the system is not relaxed.
        #initialized = initialized and (not relaxed)

        system = self.fullyUnrelaxed()
        atoms = system.atomsTemplate(parameters=parameters)
        constraints = atoms.constraints
        del atoms.constraints

        atoms = self.applyInitializationConstraintsAndRelaxation(
                atoms=atoms,
                constraints=constraints,
                initialized=initialized,
                relaxed=relaxed,
                )

        if not constrained:
            del atoms.constraints

        self.ATOMS_STORE[key] = atoms.copy()

        return atoms

    def atomsTemplate(self, parameters=DEFAULT):
        raise NotImplementedError("%s" % self)

    def applyInitializationConstraintsAndRelaxation(
            self,
            atoms,
            constraints=tuple(),
            initialized=True,
            relaxed=True,
            ):
        if initialized:
            diff_vector, initial_moments = self.makeAtomsInitialized(atoms)

        atoms = self.makeSubsystemsRelaxed(atoms)

        if initialized:
            atoms.positions += diff_vector
            atoms.set_initial_magnetic_moments(initial_moments)

        if constraints is not None:
            for constraint in constraints:
                atoms.set_constraint(constraint)


        atoms = self.applySetupSystem(atoms)

        if relaxed:
            atoms = self.makeAtomsRelaxed(atoms)

        return atoms

    def applySetupSystem(self, atoms):
        setups_to_apply = []
        system = self
        while system.setupSystem() is not None:
            system = system.setupSystem()
            setups_to_apply.append(system.relaxation())

        setups_to_apply.reverse()
        for setup in setups_to_apply:
            atoms = makeAtomsRelaxed(atoms, setup)

        return atoms

    def makeSubsystemsInitialized(self, atoms):
        diff_vector = np.zeros(atoms.positions.shape)
        init_moments = np.zeros(len(atoms))

        for sub_system in self.subSystems():
            v, i = sub_system.makeAtomsInitialized(atoms)
            diff_vector += v
            init_moments += i

        return diff_vector, init_moments

    def initialStateIdentifiers(self, identifiers):
        return identifiers

    def makeAtomsInitialized(self, atoms):
        identifiers = [init['identifier'] for init in self.initialState()]
        identifiers = self.initialStateIdentifiers(identifiers)
        diff_vector, init_moments = self.makeSubsystemsInitialized(atoms)

        if len(identifiers) == 0:
            return diff_vector, init_moments

        system = self.fullyUnrelaxed()
        init_atoms, coordinates = system._findAtoms(identifiers)
        init_atoms = system.change(
                init_atoms,
                fro=coordinates,
                to=self.COORDINATES,
                )
        move = np.array([init['move_vector'] for init in self.initialState()])
        moment = np.array([init['initial_moment'] for init in self.initialState()])
        init_atoms.cell = atoms.cell
        init_atoms.pbc = atoms.pbc
        init_atoms.wrap()
        take = findCopies(init_atoms.positions, init_atoms.cell)
        init_atoms = init_atoms[take]
        move = move[take]
        moment = moment[take]

        group = findPeriodicallyEqualAtoms(init_atoms, atoms)
        change = (group !=-1)
        group = group[change]
        init_moments = np.zeros(len(change))
        diff_vector[change] += move[group]
        init_moments[change] = moment[group]

        return diff_vector, init_moments

    def _findAtoms(self, identifiers):
        raise NotImplementedError("%s" % self)

    def findAtoms(self,
            identifiers,
            initialized=True,
            relaxed=True,
            ):
        atoms, base_coordinates = self._findAtoms(identifiers)
        atoms = self.fullyUnrelaxed().change(
                atoms,
                to=self.COORDINATES,
                fro=base_coordinates,
                )

        atoms = self.applyInitializationConstraintsAndRelaxation(
                atoms=atoms,
                initialized=initialized,
                relaxed=relaxed,
                )

        return atoms

    def systemClass(self):
        return self.__class__

    def relaxation(self):
        return None

    def setupSystem(self):
        return None

    def name(self):
        return self.__name

    def isDone(self):
        raise CalcTrollException("Unrelaxed systems can only be analyzed by explicitly setting, 'system=Unrelaxed(system)' ")

    def initialState(self):
        return self.__initial_state

    def updateBasisChanger(self):
        basis_changer = self.setUpBasisChanger()
        self.__basis_changer = basis_changer

    def setUpBasisChanger(self):
        raise NotImplementedError

    def basisChanger(self):
        if self.__basis_changer is None:
            self.updateBasisChanger()
        return self.__basis_changer

    def subSystems(self):
        return self.__sub_systems

    def createSystemWithRelaxedSubSystems(self, sub_systems):
        assert len(sub_systems) == len(self.subSystems())
        for s1, s2 in zip(sub_systems, self.subSystems()):
            if not s1.systemClass() == s2.systemClass():
                raise ValueError("%s, %s"%(s1.__class__, s2.__class__))

        other = self.copy()
        other.__sub_systems = sub_systems
        other.__basis_changer = None

        return other

    def fullyUnrelaxed(self):
        return self

    def isRelaxed(self):
        return (not self.relaxation() == None)

    def change(self, thing, to, fro, difference=False):
        return self.basisChanger().transform(thing, to, fro, difference)

    def hasRelaxedSubsystems(self):
        if len(self.subSystems()) == 0:
            return False

        return all([sub_system.isRelaxed() or sub_system.hasRelaxedSubsystems() for sub_system in self.subSystems()])

    def defaultParameters(self, parameters=DEFAULT):
        raise NotImplementedError(str(self.className()))

    def subParameters(self, parameters=DEFAULT):
        return tuple()

    def pbc(self):
        raise NotImplementedError(str(self.className()))

    def relaxCell(self):
        return False

    def makeAtomsRelaxed(
            self,
            atoms,
            ):
        atoms = makeAtomsRelaxed(
                atoms,
                relaxation=self.relaxation(),
                )

        return atoms

    def makeSubsystemsRelaxed(self, atoms):
        for sub_system in self.subSystems():
            atoms = sub_system.makeSubsystemsRelaxed(
                    atoms=atoms,
                    )
            atoms = sub_system.makeAtomsRelaxed(
                    atoms,
                    )

        return atoms

    def read(self):
        is_done = self.isDone()
        calculate(self)

        if not is_done:
            return None

        return self.atoms()

    def frame(self, data=None, border=0.3, direction='-z'):
        raise NotImplementedError

    def customConstraints(self, atoms):
        return []

    def referenceEnergyMatchIndices(self, parameters):
        raise NotImplementedError

    def xyz_save(
            self,
            filename,
            data=None,
            ):
        if data is None:
            data = self.read()

        write(filename, data)

    def png_save(
            self,
            filename,
            data=None,
            **kwargs):
        return self.jpg_save(filename, data=data, **kwargs)

    def jpg_save(
            self,
            filename,
            data=None,
            frame=None,
            border=DEFAULT,
            direction='-z',
            **kwargs):
        from matplotlib import pylab as plt
        if data is None:
            data = self.read()
        if frame is None:
            frame = self.frame(
                data=data,
                border=border,
                direction=direction,
            )
        frame = np.array(frame)

        pmin, pmax = frame
        d = pmax - pmin
        dx = d[0]
        dy = d[1]
        ratio = dx/dy

        fig = plt.figure(frameon=False, figsize=(4*ratio, 4))
        fig.patch.set_facecolor('black')

        ax = plt.Axes(fig, [0., 0., 1., 1.])
        fig.add_axes(ax)

        self.plot(data=data,
                  ax=ax,
                  frame=frame,
                  direction=direction,
                  **kwargs)

        ax.set_xlim((pmin[0], pmax[0]))
        ax.set_ylim((pmin[1], pmax[1]))
        plt.savefig(filename)
        plt.close('all')

        return frame

    def plot(self,
             data=None,
             ax=None,
             frame=None,
             direction='-z',
             size=1.,
             bar=True,
             facecolor='black',
             ):
        if data is None:
            atoms = self.atoms()
        else:
            atoms = data

        size = size*0.4
        flatMatplotlibView(atoms, ax, direction=direction, size=size)
        if frame is not None:
            ax.set_xlim((frame[0, 0], frame[1, 0]))
            ax.set_ylim((frame[0, 1], frame[1, 1]))

            self.plotUnitCell(data=atoms, ax=ax, direction=direction)

            if bar:
                addMeasuringStick(ax, color='white', size=5)

        ax.patch.set_facecolor(facecolor)
        ax.set_xticks([])
        ax.set_yticks([])


