# Written by Mads Engelund, 2017, http://espeem.com
import numpy as np
from numpy.linalg import inv as inverse
import ase
from ase.visualize import view
from numpy import matrix
from ase.atoms import Atoms as AseAtoms
from ase.atoms import Atom

from CalcTroll.ASEUtils.Atoms import Atoms
from CalcTroll.Core.ParameterControl import ParameterControl

class Transformation(ParameterControl):
    def __init__(self, to=None, fro=None, trans_matrix=np.identity(3), rotation_point=[[0,0,0],[0,0,0]]): pass

class BasisChanger:
    def __init__(self, transformations=tuple()):
        self.__transformations = list(transformations)
        self.__makeAll()

    def copy(self):
        transformations = [t.copy() for t in self.__transformations]
        return BasisChanger(transformations=transformations)

    def keys(self):
        return list(self.__trans.keys())

    def transDict(self):
        return self.__trans

    def transformations(self):
        return tuple(self.__transformations)

    def update(self):
        self.__makeAll()

    def __getitem__(self, index):
        return self.__transformations[index].copy()

    def __add(self, transformation):
        self.__transformations.append(transformation)

    def addTransformation(self, transformation):
        self.__add(transformation)
        self.__makeAll()

    def __len__(self):
        return len(self.__trans)

    def include(self,changer,string=''):
        for to in list(changer.__trans.keys()):
            for fro in list(changer.__trans[to].keys()):
                new_to=to+string
                new_fro=fro+string
                t = Transformation(to=new_to,fro=new_fro,trans_matrix=changer.__trans[to][fro],rotation_point=changer.__rotation_point[to][fro])
                self.__add(t)

        self.__makeAll()

    def transform(self,thing,to,fro,difference=False,type=None):
        if to==fro:
            if type=='array of 3X3-matrices':
                return np.array(thing)

            if type=='array of 3NX3N-matrices':
                return np.array(thing)

            if isinstance(thing, np.ndarray):
                return np.array(thing)

            elif isinstance(thing, Atoms):
                thing = thing.copy()
                return thing

            elif isinstance(thing, Atom):
                position=np.array(thing.position)
                transformed=position-rotation_point[1]
                transformed=np.transpose(np.tensordot(a,transformed,(1,0)))
                transformed+=rotation_point[0]
                thing.position = transformed

            elif isinstance(thing, list):
                thing2=[self(item,to,fro) for item in thing]

                return thing2
            else:
                raise Exception("No transformation behaviour defined for %s"%thing.__class__)

        a = self.__trans[to][fro]

        if difference:
            rotation_point=np.array([0,0,0])
        else:
            rotation_point=self.__rotation_point[to][fro]

        inv_a = inverse(a)

        if type=='array of 3X3-matrices':
            #a*matrix*a^(-1) on each matrix
            temp=num.tensordot(thing,inv_a,(-1,0))
            temp=np.tensordot(temp,a,(-2,1))
            #print temp.shape
            transformed=np.swapaxes(temp,-1,-2)
            return transformed

        if type=='array of 3NX3N-matrices':
            dim1=thing.shape[0]
            N=thing.shape[-1]/3
            temp=np.reshape(thing,(dim1,N,3,N,3))
            temp=np.swapaxes(temp,-2,-3)

            temp=self.__call__(temp,to,fro,type='array of 3X3-matrices')
            temp=np.swapaxes(temp,-2,-3)

            transformed=np.reshape(temp,(dim1,N*3,N*3))
            return transformed

        if isinstance(thing, np.ndarray):
            thing=np.array(thing, float)
            if to == fro:
                return thing
            elif thing.shape == (0,):
                return np.array([])
            elif thing.shape==(3,):
                thing-=rotation_point[1]
                transformed=np.tensordot(a,thing,(1,0))
                transformed+=rotation_point[0]
            elif thing.shape[1]==3:
                thing-=rotation_point[1]
                transformed=np.transpose(np.tensordot(a,thing,(1,1)))
                transformed+=rotation_point[0]
            elif thing.shape==(2,):
                thing-=rotation_point[1][:2]
                transformed=np.tensordot(a[:2,:2],thing,(1,0))
                transformed+=rotation_point[0][:2]
            elif thing.shape[1]==2:
                thing-=rotation_point[1][:2]
                transformed=np.transpose(np.tensordot(a[:2,:2],thing,(1,1)))
                transformed+=rotation_point[0][:2]
            filter=np.greater(abs(transformed),1e-13)
            transformed=transformed*filter

            return transformed

        elif isinstance(thing, Atoms):
            atoms = thing.copy()
            atoms.positions = self.transform(atoms.positions,to,fro)
            cell = self.transform(np.array(atoms.get_cell()), to, fro, difference=True)
            atoms.set_cell(cell)

            for region in atoms.regions():
                cell = self.transform(region.cell(), to, fro, difference=True)
                region.setCell(cell)

            return atoms

        elif isinstance(thing, AseAtoms):
            raise Exception('Pure ASE Atoms object are not supported')

        elif isinstance(thing, Atom):
            position=np.array(thing.position)
            transformed=position-rotation_point[1]
            transformed=np.transpose(np.tensordot(a,transformed,(1,0)))
            transformed+=rotation_point[0]
            thing.position = transformed

        elif isinstance(thing, list):
            thing2=[self.transform(item,to,fro) for item in thing]
            return thing2
        else:
            raise Exception("No transformation behaviour defined for %s"%thing.__class__)

    def __makeAll(self):
        trans = {}
        rotation_point = {}
        for transformation in self.__transformations:
            trans_matrix = transformation['trans_matrix']
            point = transformation['rotation_point']
            to = transformation['to']
            fro = transformation['fro']
            try:
                trans[to][fro] = np.array(trans_matrix)
                rotation_point[to][fro] = np.array(point)
            except KeyError:
                trans[to]={}
                rotation_point[to]={}
                trans[to][fro] = np.array(trans_matrix)
                rotation_point[to][fro] = np.array(point)

        for to_key in list(trans.keys()):
            for from_key in list(trans[to_key]):
                try:
                    trans[from_key]
                except KeyError:
                    trans[from_key]={}
                    rotation_point[from_key]={}
                try:
                   trans[from_key][to_key]
                except KeyError:
                    trans[from_key][to_key] = inverse(trans[to_key][from_key])
                    array=rotation_point[to_key][from_key]
                    rotation_point[from_key][to_key]=np.array([array[1],array[0]])

        something_added=True
        while something_added:
            something_added=False
            for key3 in list(trans.keys()):
                  for key1 in list(trans.keys()):
                      if key1 != key3:
                          if trans[key3].get(key1) is None:
                              for key2 in list(trans.keys()):
                                  if key1!=key2 and key3!=key2:
                                      try:
                                          trans[key2][key1]
                                          trans[key3][key2]
                                      except KeyError:
                                          pass
                                      else:
                                          A21 = trans[key2][key1]
                                          A32 = trans[key3][key2]
                                          x_21_1 = \
                                              (rotation_point[key2][key1][1]).T
                                          x_21_2 = \
                                              (rotation_point[key2][key1][0]).T
                                          x_32_2 = \
                                              (rotation_point[key3][key2][1]).T
                                          x_32_3 = \
                                              (rotation_point[key3][key2][0]).T

                                          x_31_1 =  x_21_1
                                          x_31_3 =  A32 @ (x_21_2 - x_32_2) + x_32_3
                                          A31 = A32 @ A21


                                          x = np.array([x_31_3, x_31_1])
                                          trans[key3][key1] = A31
                                          rotation_point[key3][key1] = x
                                          something_added = True

        self.__trans = trans
        self.__rotation_point = rotation_point
