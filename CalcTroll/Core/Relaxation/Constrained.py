# Written by Mads Engelund, 2017, http://espeem.com
import os
import numpy as np
import string
import fnmatch
from os.path import join
from ase.visualize import view
from ase.data import chemical_symbols
import ase.data as data
from ase.atoms import Atoms
from ase.units import eV, Ang
from ase import io
from ase.constraints import FixedMode

#from CalcTroll.Programs.Siesta import NickTransiesta
#from CalcTroll.Programs.ASE.AtomUtilities import Rattle, RattleNearBy
#from CalcTroll import ParameterGroup
#from CalcTroll.Flags import DEFAULT, DONE, NOT_STARTED, INCOMPLETE
#from CalcTrollHosts import DEFAULT_HOST
#from CalcTroll.CoreSystems import System
#from CalcTroll.CoreSystems.Surface import convertShorthand
#from CalcTroll.Parameters.Doping import Doping
#from CalcTroll.Runs.Run import Run, CompositeRun, TAIL, REMOTE
#from CalcTroll.Runs.Runs import Relaxed
#from CalcTroll.Utilities import vectorLength
from CalcTroll.Core.ParameterControl.Parameters import Parameters
from CalcTroll.Core.Flags import DEFAULT

class Constraint(Parameters): pass

class Difference2(Constraint):
    @classmethod
    def constraintValue(self, atoms, to_indices, direction):
        diff = atoms.positions[to_indices].mean(axis=0)
        value = np.dot(diff, direction)

        return value

    def __init__(self, difference, direction=(0, 0, 1)):
        self.__difference = difference
        direction = np.array(direction, float)
        self.__direction = direction/vectorLength(direction)

    def direction(self):
        return self.__direction

    def difference(self):
        return self.__difference

    def modifyAtoms(self, atoms, indices):
        direction = self.direction()
        diff = atoms.positions[indices].mean(axis=0)
        diff = np.dot(diff, direction)
        move = (self.difference() - diff)*direction
        atoms.positions[indices] += move
        w_to = 1
        mode = np.zeros(3*len(atoms))
        for index1 in indices:
            mode[3*index1: 3*(index1+1)] = w_to*direction

        mode /= vectorLength(mode)
        constraint = FixedMode(mode)
        atoms.constraints.append(constraint)

        return atoms

    def identifier(self):
        identifier = 'D%.2f' % self.__difference
        identifier += '_%.2f_%.2f_%.2f' % tuple(self.direction())

        return identifier


class Difference(Constraint):
    @classmethod
    def constraintValue(cls, atoms, indices):
        index2 = indices[-1]
        other_indices = indices[:-1]
        direction = np.array([0, 0, 1.0])
        diff = atoms.positions[index2] - atoms.positions[other_indices].mean(axis=0)
        value = np.dot(diff, direction)

        return value

    def __init__(self, difference):
        self.__difference = difference

    def difference(self):
        return self.__difference

    def modifyAtoms(self, atoms, indices):

        index2 = indices[-1]
        other_indices = indices[:-1]

        direction = np.array([0, 0, 1.0])
        diff = atoms.positions[index2] - atoms.positions[other_indices].mean(axis=0)
        diff = np.dot(diff, direction)
        move = self.difference()*direction
        move -= diff*direction
        w_other = 1.0/len(indices)
        w2 = 1.0 - w_other
        atoms.positions[index2] += move*w2
        atoms.positions[other_indices] -= move*w_other

        mode = np.zeros(3*len(atoms))
        for index1 in other_indices:
            mode[3*index1: 3*(index1+1)] = w_other*direction
        mode[3*index2: 3*(index2+1)] = -w2*direction
        mode /= vectorLength(mode)
        constraint = FixedMode(mode)
        atoms.constraints.append(constraint)

        return atoms

    def identifier(self):
        return 'D%.2f' % self.__difference


class AdiabaticContinuation:
    def __init__(self, run, identifiers, limits, step):
        self.__identifiers = identifiers
        self.__limits = limits
        self.__step = step
        self.__relaxed_value = None
        CompositeRun.__init__(self, inputs=[run])

    def limits(self):
        return self.__limits

    def step(self):
        return self.__step

    def identifiers(self):
        return self.__identifiers

    def relaxedConstraintValue(self):
        if not self.__relaxed_value is None:
            return self.__relaxed_value

        atoms = self.inputs()[0].atoms()
        identifiers = convertShorthand(self.identifiers())
        atoms_indices = self.inputs()[0].relaxedSystem().findAtomIndices(identifiers, atoms)
        value = Difference.constraintValue(atoms, atoms_indices)

        return value

    def updateRuns(self):
        if not self.isReady():
            return None

        relaxed_run = self.inputs()[0]
        c_value = self.relaxedConstraintValue()
        c_min, c_max = c_value, c_value
        min_run, max_run = relaxed_run, relaxed_run
        min_is_cont = True
        max_is_cont = True

        done = False
        l_runs = 0
        while not done:
            l_runs = len(self.runs())
            for run in self.runs():
                value = run.constraint().difference()
                if value < c_min:
                    c_min = value
                    min_run = run
                    min_is_cont = run.isContinuation()

                if value > c_max:
                    c_max = value
                    max_run = run
                    max_is_cont = run.isContinuation()

            target_values = np.arange(self.limits()[0], self.limits()[1] + 1e-5, self.step())
            less_values = target_values[target_values < c_min]
            greater_values = target_values[target_values > c_max]
            take_middle = np.logical_and(target_values >= c_min, target_values <= c_max)
            middle_values = target_values[take_middle]

            if len(less_values) > 0 and min_is_cont:
                constraint = Difference(difference=less_values[-1])
                run = ConstrainedRelaxed(
                        baserun=min_run,
                        identifiers=self.identifiers(),
                        constraint=constraint,
                        path=relaxed_run.runDirectory(),
                        submit_type=relaxed_run.submitType()
                        )
                self.addRun(run)

            if len(greater_values) > 0 and max_is_cont:
                constraint = Difference(difference=greater_values[0])
                run = ConstrainedRelaxed(
                        baserun=max_run,
                        identifiers=self.identifiers(),
                        constraint=constraint,
                        path=relaxed_run.runDirectory(),
                        submit_type=run.submitType(),
                        )
                self.addRun(run)

            done = l_runs == len(self.runs())

class DesorptionPath:
    def __init__(self, run, step, end, method=None, direction=(0,0,1)):
        self.__step = step
        self.__end = end
        self.__relaxed_value = None
        self.__method = method
        direction = np.array(direction, float)
        self.__diretion = direction/vectorLength(direction)
        CompositeRun.__init__(self, inputs=[run])

    def direction(self):
        return self.__diretion

    def step(self):
        return self.__step

    def end(self):
        return self.__end

    def method(self):
        return self.__method

    def relaxedConstraintValue(self):
        if not self.__relaxed_value is None:
            return self.__relaxed_value

        atoms = self.inputs()[0].atoms()
        atoms_indices = self.inputs()[0].finalSystem().moleculeIndices(atoms)
        value = Difference2.constraintValue(atoms, atoms_indices, self.direction())

        return value

    def updateRuns(self):
        if not self.isReady():
            return None

        relaxedrun = self.inputs()[0]
        c_value = self.relaxedConstraintValue()
        c_value = np.around(c_value, 2)

        baserun = relaxedrun
        values = np.arange(0, self.end() + self.step(), self.step())
        for add_value in values:
            value = c_value + add_value
            constraint = Difference2(difference=value, direction=self.__diretion)
            run = ConstrainedRelaxed(
                    baserun=baserun,
                    identifiers=None,
                    constraint=constraint,
                    path=relaxedrun.runDirectory(),
                    method=self.method(),
                    submit_type=baserun.submitType(),
                    )
            self.addRun(run)
            baserun=run

class ConstrainedRelaxed:
    def shorthand(self):
        return 'CCG'

    def __init__(
            self,
            constraint,
            baserun,
            path,
            identifiers=None,
            method=None,
            submit_type=DEFAULT,
                ):
        self.__constraint = constraint
        self.__identifiers = identifiers

        if method is None:
            method = baserun.method()
        self.__method = method.copy(
                force_tolerance=0.01,
                )

        Run.__init__(self, inputs=[baserun], path=path, submit_type=submit_type)

    def hasOpenBoundaries(self):
        return self.inputAtoms().hasOpenBoundaries()

    def totalEnergy(self):
        return self.method().totalEnergy(self.runDirectory())

    def charges(self):
        return self.method().charges(self.runDirectory())

    def difference(self):
        return self.__constraint.difference()

    def identifiers(self):
        return self.__identifiers

    def system(self):
        return self.baseRun().system()

    def identifier(self):
        return self.constraint().identifier()

    def method(self):
        return self.__method

    def parameters(self):
        return self.baseRun().parameters()

    def constraint(self):
        return self.__constraint

    def baseRun(self):
        return self.inputs()[0]

    def saveBehaviour(self):
        return self.baseRun().saveBehaviour()

    def copyRun(self):
        return self.baseRun()

    def setupCommand(self, utilities):
        f_path = join(self.copyRun().runDirectory(), 'siesta.DM')
        command = 'cp %s %s/'%(f_path, self.runDirectory())
        os.system(command)

    def isContinuation(self):
        if not self.isDone():
            return False

        if not isinstance(self.baseRun(), ConstrainedRelaxed):
            return True

        diff = abs(self.charges() - self.baseRun().charges())
        c_change = abs(self.difference() - self.baseRun().difference())
        mark = diff.max()/c_change

        return mark < 2.0

    def write(self):
        atoms = self.inputAtoms()
        kpts = self.parameters().kpts()
        method = self.method()
        filename = self.runFile()
        method.writeRelaxation(
            runfile=filename,
            atoms=atoms,
            kpts=kpts,
            relax_cell=self.system().relaxCell(),
            save_behaviour=self.saveBehaviour(),
            )

        method.writeRelaxation(
                runfile=self.runFile(),
                atoms=atoms,
                kpts=kpts,
                relax_cell=self.system().relaxCell(),
                save_behaviour=self.saveBehaviour(),
                )

    def makeCommands(self, form):
        commands = [form%(self.runFile(path=REMOTE), 'TMP.out')]
        commands.append('mv TMP.out %s'%self.outFile(path=TAIL))

        return commands

    def isRelaxed(self):
        return self.isDone()

    def startingSystem(self):
        return self.baseRun().finalSystem()

    def finalSystem(self):
        if not self.isRelaxed():
            print(self.method().basisSet())
            print(self.outFile())
            print(self.path())
            print(self.status())
            raise Exception('%s is not done'%self.system().name())

        system = self.startingSystem()
        pbc = system.pbc()
        atoms = self.method().relaxedAtoms(self.runDirectory(), pbc)
        new_system = system.copy()
        new_system = new_system.setRelaxation(atoms)

        return new_system

    def inputAtoms(self):
        relaxed_system = self.baseRun().finalSystem()
        atoms = self.baseRun().atoms()
        atoms_indices = relaxed_system.findAtomIndices(self.identifiers(), atoms)
        atoms = self.constraint().modifyAtoms(atoms, atoms_indices)

        return atoms

    def atoms(self, constrained=True, parameters=DEFAULT):
        system = self.finalSystem()
        if parameters is DEFAULT:
            parameters = self.parameters()

        return system.atoms(constrained=constrained, parameters=parameters)

    def rattler(self):
        return None

    def usefulFiles(self):
        return ['*.EIG', '*.bands', '*.MDE', '*.slurm', '*nc']

    def neededFiles(self):
        return ['*.traj', '*.fdf', '*psf', '*.py', '*.DM']

    def resultFiles(self):
        return ['*.traj', '*.out', '*.XV', '*.DM']

    def coreFiles(self):
        return self.neededFiles() + self.resultFiles()

    def cleanUp(self, agressive=False):
        direc = self.runDirectory()
        files = [name for name in os.listdir(direc) if os.path.isfile(join(direc,name))]
        protect = []
        patterns = list(self.coreFiles())
        if not agressive:
            patterns.extend(self.usefulFiles())

        for filetype in patterns:
            protect.extend(fnmatch.filter(files, filetype))

        remove_files = set(files) - set(protect)
        remove_files = [join(direc, name) for name in remove_files]

        list(map(os.remove, remove_files))

