# Written by Mads Engelund, 2017, http://espeem.com
from CalcTroll.Core.Task import Task
from CalcTroll.Core.Relaxation.Relaxed import Relaxed

class RelaxationTask(Task):
    shorthand = 'Relax'
    def __init__(
            self,
            item,
            ):
        tasks = []
        for inp in item.inputs():
            tasks.extend(inp.tasks())

        Task.__init__(
                self,
                path=item.path(),
                inputs=tasks,
                )
        self.__system = item

    def system(self):
        return self.__system

    def parameters(self):
        return self.system().parameters()

    def method(self):
        return self.system().method()

    def updateInputs(self):
        pass

    def relaxation(self):
        raise NotImplementedError

    def totalEnergy(self):
        return self.method().totalEnergy(self.runDirectory(), filename='relaxation.out')

    def runFileBasename(self):
        return 'RELAX.' + self.method().runFileEnding()

    def outFileBasename(self):
        return 'RELAX.' + self.method().outFileEnding()
