# Written by Mads Engelund, 2017, http://espeem.com
from os.path import join

from CalcTroll.Core.System import System, Collection
from CalcTroll.Core.Task import AbstractHasTasks
from CalcTroll.Core.Flags import DEFAULT
from .AbstractRelaxed import AbstractRelaxed
from .Unrelaxed import Unrelaxed

class RelaxedSubSystems(System, AbstractRelaxed):
    @classmethod
    def className(cls):
        return 'RelaxedSubSystems'
    
    def path(self):
        return join(self.method().path(), self.system().path())

    def systemClass(self):
        return self.__system.__class__

    def __new__(cls, system, method=DEFAULT, parameters=DEFAULT, **kwargs):
        name = cls.className() + system.className()
        parameters = system.defaultParameters(parameters=parameters)

        if isinstance(system, AbstractRelaxed):
            relaxed_system = system
            cls = relaxed_system.__class__
            sub_systems = system.subSystems()
        else:
            sub_parameters = system.subParameters(parameters)

            if len(system.subSystems()) != len(sub_parameters):
                print(system.name())
            assert len(system.subSystems()) == len(sub_parameters)


            it = list(zip(system.subSystems(), sub_parameters))
            sub_systems = [sub if isinstance(sub, AbstractRelaxed)
                else Relaxed(sub, method=method, parameters=p)
                for sub, p in it]


            new_dict = dict(system.__dict__)
            assert '_System__basis_changer' in new_dict
            assert '_System__sub_systems' in new_dict
            new_dict['_System__basis_changer'] = None
            new_dict['_System__sub_systems'] = sub_systems


            cls = type(name,
                       (cls, system.__class__),
                        new_dict)

        instance = System.__new__(cls,)
        instance.__sub_systems = sub_systems
        instance.__parameters = parameters
        instance.__system = system

        return instance


    def copy(self):
        return RelaxedSubSystems(
                system=self.system(),
                parameters=self.parameters(),
                setup_system=self.setupSystem(),
                )

    def __init__(self,
                 system,
                 method=DEFAULT,
                 parameters=DEFAULT,
                 setup_system=None,
                 ):

        if method is DEFAULT:
            from CalcTroll.HOSTS import DEFAULT_METHOD
            method = DEFAULT_METHOD
        self.__method = method
        self.__setup_system = setup_system

        inputs = self.subSystems()
        if setup_system is not None:
            inputs.append(setup_system)
        AbstractRelaxed.__init__(self, inputs=inputs)

    def fullyUnrelaxed(self):
        unrelaxed = Unrelaxed(self.system().fullyUnrelaxed(), parameters=self.parameters())

        return unrelaxed

    def tasks(self):
        return []

    def setupSystem(self):
        return self.__setup_system
        
    def isDone(self):
        return AbstractHasTasks.isDone(self)

    def method(self):
        return self.__method

    def system(self):
        return self.__system

    def update(self):
        pass

    def parameters(self):
        return self.__parameters

class Relaxed(RelaxedSubSystems):
    def explain(self):
        ex = self.system().explain()
        return ex

    def name(self):
        return self.system().name()

    @classmethod
    def className(cls):
        return 'Relaxed'

    def path(self):
        path = join(self.system().path(), self.method().path())

        return path

    def __init__(self,
                 system,
                 method=DEFAULT,
                 parameters=DEFAULT,
                 setup_system=None,
                 ):
        if method is DEFAULT:
            from CalcTroll.HOSTS import DEFAULT_METHOD
            method = DEFAULT_METHOD

        self.__tasks = None

        RelaxedSubSystems.__init__(
                self,
                system=system,
                method=method,
                parameters=parameters,
                setup_system=setup_system,
                )


    def tasks(self):
        if self.__tasks == None:
            self.__tasks = self.implementation().makeTasks([self])

        return self.__tasks

    def copy(self,
            method=DEFAULT,
            parameters=DEFAULT,
            setup_system=DEFAULT,
            ):
        if method is DEFAULT:
            method = self.method()
        if parameters is DEFAULT:
            parameters = self.parameters()
        if setup_system is DEFAULT:
            setup_system = self.setupSystem()

        return Relaxed(
                system=self.system(),
                method=method,
                parameters=parameters,
                setup_system=setup_system,
                )


    def mainTask(self):
        return self.tasks()[0]

    def relaxation(self):
        return self.mainTask().relaxation()
