# Written by Mads Engelund, 2017, http://espeem.com
import unittest

from CalcTroll.Core.Test.Suite import CalcTrollTestCase
from CalcTroll.Core.Test.Dummies import TEST_ATOMS, TestSystemParameters
from CalcTroll.Core.Test.Dummies import TestMethod, TestSystem

from CalcTroll.Core.Relaxation import Relaxed


class RelaxedTest(CalcTrollTestCase):
    def setUp(self):
        self.__system = TestSystem()
        CalcTrollTestCase.setUp(self)

    def testConstruction(self):
        relaxed = Relaxed(self.__system, method=TestMethod())
        self.assertIsInstance(relaxed, Relaxed)
        self.assertIsInstance(relaxed, TestSystem)
        self.assertEqual(repr(relaxed), 'Relaxed(system=TestSystem(), method=TestMethod())')
        self.assertEqual(relaxed.defaultParameters(), TestSystemParameters())

if __name__=='__main__':
    unittest.main()
