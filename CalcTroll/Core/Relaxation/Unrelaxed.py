# Written by Mads Engelund, 2017, http://espeem.com
from CalcTroll.Core.System import System
from CalcTroll.Core.Task import HasInputs
from CalcTroll.Core.Flags import DEFAULT, MINIMAL
from CalcTroll.Core.ParameterControl import ParameterControl
from CalcTroll.Core.Explanation import Explanation

from .AbstractRelaxed import AbstractRelaxed

class Unrelaxed(System, AbstractRelaxed):
    @classmethod
    def className(cls):
        return 'Unrelaxed'

    def __new__(cls, system, parameters=DEFAULT, **kwargs):
        name = cls.className() + system.className()


        cls = type(name,
                   (cls, system.__class__),
                   system.__dict__)

        return ParameterControl.__new__(cls)

    def __init__(self, system, parameters=DEFAULT):
        self._system = system

        parameters = system.defaultParameters(parameters=parameters)
        self._parameters = parameters

    def path(self):
        return self.className() + self.system().path()

    def explain(self):
        ex = self.system().explain()

        ex += Explanation('The atomic forces were NOT relaxed.')

        return ex

    def systemClass(self):
        return self._system.__class__

    def system(self):
        return self._system

    def implementation(self):
        return None

    def isDone(self):
        return True

    def parameters(self):
        return self._parameters

    def tasks(self):
        return []

    def hasInputs(self):
        return False

    def updateStatus(self):
        pass
