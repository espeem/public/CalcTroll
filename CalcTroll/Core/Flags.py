# Written by Mads Engelund, 2017, http://espeem.com
# Contains flags and exception that are used in both core and modules.

class CalcTrollFlag:
    @classmethod
    def className(cls):
        string = str(cls)[:-2].split('.')[-1]

        return string

# Generic word flags
class ALL(CalcTrollFlag): pass
class MINIMAL(CalcTrollFlag): pass
class EXPANDED(CalcTrollFlag): pass
class DEFAULT(CalcTrollFlag): pass
class INTEGRATED(CalcTrollFlag): pass
class DIFFERENTIAL(CalcTrollFlag): pass
class POSITIVE(CalcTrollFlag): pass
class NEGATIVE(CalcTrollFlag): pass

# Approximation flags.
class FULL(CalcTrollFlag):pass
class COLLINEAR(CalcTrollFlag):pass

# Regime flags
class DYNAMIC(CalcTrollFlag):pass
class THERMAL(CalcTrollFlag):pass

class SPECTROSCOPY(CalcTrollFlag):pass
class IMAGE(CalcTrollFlag):pass

# Status flags.
class NOT_STARTED(CalcTrollFlag):pass
class NOT_CONVERGED(CalcTrollFlag):pass
class STARTED(CalcTrollFlag):pass
class FAILED(CalcTrollFlag):pass
class INCOMPLETE(CalcTrollFlag):pass
class DONE(CalcTrollFlag):pass

# File-based flags.
class NO_SUCH_FILE(CalcTrollFlag):pass

# Path-based flags.
class TAIL(CalcTrollFlag): pass
class LOCAL(CalcTrollFlag): pass
class REMOTE(CalcTrollFlag): pass

# Performance limit flags.
class SOFT_MEMORY_LIMIT(CalcTrollFlag): pass

# JSON response flags.
class NOT_UNICODE(CalcTrollFlag): pass
class NOT_DONE(CalcTrollFlag): pass


# Exceptions that will sometimes be automatically handled.
class CalcTrollException(CalcTrollFlag, Exception): pass
class TooHeavy(CalcTrollFlag, Exception): pass
class InconsistentApproximation(CalcTrollException): pass
class TooSmallBoundingBox(CalcTrollException): pass
