class Explanation:
    def __len__(self):
        return len(self.__text)

    def __init__(self, text='', citations=tuple()):
        assert text.count('[*]') == len(citations)
        self.__text = text
        self.__citations = list(citations)

    def __add__(self, other):
        if not isinstance(other, Explanation):
            raise ValueError(other)
        text = self.__text + '\n' + other.__text
        citations = self.__citations + other.__citations

        return Explanation(text, citations)

    def text(self):
        t = tuple(range(1, len(self.__citations) + 1))
        text = self.__text
        code = "djo934urohkfhwe3"
        text = text.replace('%', code)
        text = text.replace('[*]', '[%d]')
        text = text % t
        text += '\n\n'

        for i, citation in enumerate(self.__citations):
            text += '[%d] ' % (i + 1) + citation + '\n'

        text = text.replace(code, '%')

        return text

