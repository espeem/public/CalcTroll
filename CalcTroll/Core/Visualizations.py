# Written by Mads Engelund, 2017, http://espeem.com
# Contains functions for visualization that are not closely linked to specific plugins.

import numpy as np
from os.path import abspath
from ase.data.colors import jmol_colors
from ase.data.vdw import vdw_radii
from CalcTroll.ASEUtils.AtomUtilities import cellCenter

my_radii = vdw_radii.copy()
my_radii[22] = 3.00
my_radii[74] = 2.50
VALENCE_DICT = {'H': 1, 'Si': 4, 'C':4, 'B':3, 'Ge':4, 'Ag': 11, 'O':6}

DEFAULT_FONTSIZE = 10

def chargeDifferenceScheme(
        figure,
        atoms,
        charges,
        max_charge_difference=None,
        scale_factor=1,
        ):
    charges = -np.array(charges)
    max_charge = charges.max()
    colors = np.ones((len(atoms), 3))

    if max_charge_difference is None:
        maxi = abs(charges).max()
    else:
        maxi = max_charge_difference

    charges = charges.clip(-maxi, maxi)

    exponent = 1.
    take = charges > 0
    colors[take, 1] -= (charges[take]/maxi)**(exponent)
    colors[take, 2] -= (charges[take]/maxi)**(exponent)
    take = charges < 0
    colors[take, 0] -= (-charges[take]/maxi)**(exponent)
    colors[take, 1] -= (-charges[take]/maxi)**(exponent)

    numbers = atoms.get_atomic_numbers()
    u_numbers = np.unique(numbers)

    sizes = np.zeros(len(atoms))
    for number in u_numbers:
        take = numbers == number
        sizes[take] = my_radii[number]*scale_factor

    colorAndSizeEveryAtom(figure=figure,
         positions=atoms.positions,
         sizes=sizes,
         colors=colors,
         )

    return figure

def flatMatplotlibView(atoms, ax, direction='-z', size=0.4):
    from matplotlib.patches import Circle
    pos = atoms.get_positions()
    if direction == '-z':
        index = 2
        sign = 1
    elif direction == '-y':
        index = 1
        sign = 1
    elif direction == '-x':
        index = 0
        sign = 1
    else:
        raise ValueError('%s' % direction)
    take = np.array([i != index for i in range(3)])

    proj = sign*pos[:, index]
    argsort = np.argsort(proj)

    for i in argsort:
        atom = atoms[i]
        pos = tuple(atom.position[take])
        radius = my_radii[atom.number] * size
        color = jmol_colors[atom.number]
        circle = Circle(pos,
                        radius=radius,
                        facecolor=color,
                        lw=1,
                        ls='solid',
                        edgecolor='k',
                        )
        ax.add_artist(circle)

def chargeScheme(
        figure,
        atoms,
        charges,
        **kwargs
        ):
    symbols = atoms.get_chemical_symbols()
    valence = [VALENCE_DICT[symbol] for symbol in symbols]
    charges -= valence

    return chargeDifferenceScheme(figure=figure, atoms=atoms, charges=charges, **kwargs)


def dbColorScheme(
        figure,
        atoms,
        charges,
        max_difference=None,
        max_charging=None,
        exponent=1,
        ):
    max_charge = charges.max()
    colors = np.ones((len(atoms), 3))

    diff = charges[:, 0] - charges[:, 1]
    maxi_value = abs(diff).max()
    if max_difference is None:
        max_difference = maxi_value

    diff /= max_difference
    diff = np.clip(diff, -1, 1)

    take = diff > 0
    colors[take, 1] -= diff[take]**(exponent)
    colors[take, 2] -= diff[take]**(exponent)
    take = diff < 0
    colors[take, 0] += diff[take]**(exponent)
    colors[take, 1] += diff[take]**(exponent)

    total = charges.sum(axis=1)
    symbols = atoms.get_chemical_symbols()
    valence = [VALENCE_DICT[symbol] for symbol in symbols]
    charging = total - valence
    if max_charging is None:
        max_charging = abs(charging).max()
    charging = charging/max_charging
    charging = charging/2 + 0.50
    charging = np.clip(charging, 0, 1)

    colors = np.array([colors[:, i]*charging for i in range(3)]).transpose()

    numbers = atoms.get_atomic_numbers()
    u_numbers = np.unique(numbers)

    sizes = np.zeros(len(atoms))
    for number in u_numbers:
        take = numbers == number
        sizes[take] = my_radii[number]*2

    colorAndSizeEveryAtom(figure=figure,
         positions=atoms.positions,
         sizes=sizes,
         colors=colors,
         )

    return figure


def colorAndSizeEveryAtom(
        figure,
        positions,
        sizes,
        colors,
        colormap='hot',
        resolution=32,
        vmin=None,
        vmax=None,
        ):
    from mayavi import mlab
    for position, size, color in zip(positions, sizes, colors):
        x, y, z = position
        color = tuple(color)
        mlab.points3d(np.array([x]),
                      np.array([y]),
                      np.array([z]),
                      color=color,
                      scale_mode='none',
                      scale_factor=np.array([size]),
                      resolution=resolution,
                      reset_zoom=False,
                      figure=figure,
                )

    return figure

def addInfo(ax, text='', color='white'):
    x, y = ax.get_xlim()[1]-1, ax.get_ylim()[1]-1
    ax.text(x, y,
            text,
            color=color,
            verticalalignment='top',
            horizontalalignment='right',
            )

def addMeasuringStick(ax, color='white', size=5):
    from matplotlib.lines import Line2D
    text = {10:'1 nm', 5:'5 $\AA$'}[size]
    end_point = ax.get_xlim()[1]-1, ax.get_ylim()[0] + 1
    start_point = np.array(end_point)
    start_point[0] -= size
    x,y = np.array([start_point, end_point]).transpose()
    xm, ym = x.mean(), y.mean()
    l = Line2D([x[0], x[1]], [y[0], y[1]], color=color, linewidth=2)
    ax.add_line(l)
    ax.text(xm, ym,
            text,
            color=color,
            verticalalignment='bottom',
            horizontalalignment='center',
            )

    return  ax.transData.transform([start_point, end_point, (0, 0)])

def setLabel(ax, label, position=(0.05,0.95), color='black', fontsize=10, dpi=None):
    if label in ['', None]:
        return

    if dpi is None:
        factor = 1
    else:
        factor = dpi/200 * 3

    size = fontsize*{'A':1.6, 'B':1.3, 'C':1.6, 'D':1.6, 'E':1.3, 'F':1.3, 'G':1.6, 'H':1.4}[label]
    ax.text(position[0], position[1], label,
            fontsize=fontsize*1.5,
            color='white',
            transform=ax.transAxes,
            ha='left',
            va='top',
            bbox={'facecolor':color,
                  'alpha':1,
                  'edgecolor':color,
                  'width':size*factor,
                  })

def showImage(ax,
              filename,
              title='',
              label='',
              label_color='black',
              fontsize=DEFAULT_FONTSIZE,
              rotate=False,
              label_position=(0.05,0.95),
              dpi=None,
              ):
    from matplotlib import pyplot as plt
    import matplotlib.cbook as cbook
    setLabel(ax,
             label,
             color=label_color,
             fontsize=fontsize,
             position=label_position,
             dpi=dpi,
             )
    image_file = cbook.get_sample_data(abspath(filename))
    image = plt.imread(image_file)
    if rotate:
        image = np.transpose(image, (1,0,2))
    im = ax.imshow(image)
    ax.axis('off')
    ax.set_title(title, fontsize=fontsize)

    return im

def plot2DDencharWaveFunctionMod(
                        ax,
                        head,
                        number,
                        kpt=1,
                        spin=None,
                        title='',
                        ):

    if spin is None:
        real = head + '.CON.K%d.WF%d.MOD'%(kpt, number)
    else:
        spin_string = {0:'DOWN', 1:'UP'}[spin]
        real = head + '.CON.K%d.WF%d.%s.MOD'%(kpt, number, spin_string)

    xyz = []
    row = []
    last = None
    with open(real,'r') as f:
        for i, line in enumerate(f):
            xyc = list(map(float, line.strip().split('   ')))
            if i < 2 or np.sqrt((xyc[0]-last[0])**2 + (xyc[1]-last[1])**2) < 2*norm_dist:
                row.append(xyc)
            else:
                xyz.append(row)
                row = []
                row.append(xyc)

            if i == 1:
                norm_dist = np.sqrt((xyc[0]-last[0])**2 + (xyc[1]-last[1])**2)

            last = xyc

        xyz.append(row)

    X, Y, C = np.transpose(np.array(xyz), (2, 1, 0))
    ax.pcolormesh(X, Y, C[:-1,:-1], cmap='hot', vmin=0, vmax=np.max(C))
    ax.set_title(title)
    ax.axis('equal')
    ax.axis('off')

def plot2DDencharWaveFunctionPhase(
                        ax,
                        head,
                        number,
                        kpt=1,
                        spin=None,
                        title='',
                        ):

    if spin is None:
        real = head + '.CON.K%d.WF%d.PHASE'%(kpt, number)
    else:
        spin_string = {0:'DOWN', 1:'UP'}[spin]
        real = head + '.CON.K%d.WF%d.%s.PHASE'%(kpt, number, spin_string)

    xyz = []
    row = []
    with open(real,'r') as f:
        for i, line in enumerate(f):
            xyc = list(map(float, line.strip().split('   ')))

            if i < 2 or np.sqrt((xyc[0]-last[0])**2 + (xyc[1]-last[1])**2) < 2*norm_dist:
                row.append(xyc)
            else:
                xyz.append(row)
                row = []
                row.append(xyc)

            if i == 1:
                norm_dist = np.sqrt((xyc[0]-last[0])**2 + (xyc[1]-last[1])**2)
            last = xyc

        xyz.append(row)

    X, Y, C = np.transpose(np.array(xyz), (2, 1, 0))
    ax.pcolormesh(X, Y, C[:-1, :-1], cmap='hsv', vmin=-np.pi, vmax=np.pi)
    ax.set_title(title)
    ax.axis('equal')
    ax.axis('off')

def plot2DDencharWaveFunctionReal(
                        ax,
                        head,
                        number,
                        kpt=1,
                        spin=None,
                        title='',
                        ):

    if spin is None:
        real = head + '.CON.K%d.WF%d.REAL'%(kpt, number)
    else:
        spin_string = {0:'DOWN', 1:'UP'}[spin]
        real = head + '.CON.K%d.WF%d.%s.REAL'%(kpt, number, spin_string)

    xyz = []
    row = []
    with open(real,'r') as f:
        for line in f:
            try:
                line = list(map(float, line.strip().split('   ')))
            except:
                xyz.append(row)
                row = []
            else:
                row.append(line)
    X, Y, Z = np.transpose(np.array(xyz), (2, 1, 0))
    maxi = abs(Z).max()
    ax.pcolormesh(X, Y, Z, cmap='seismic', vmin=-maxi, vmax=maxi)
    ax.set_title(title)
    ax.axis('equal')
    ax.axis('off')

def mayaToMatplotlib(ax, figure):
    from mayavi import mlab
    screen = mlab.screenshot(figure=figure)
    ax.imshow(screen)
    ax.set_xticks([])
    ax.set_yticks([])

def mayaView(
             atoms,
             figure=None,
             colormap='hot',
             resolution=32,
             vmax=None,
             depth=3,
             ):
    from mayavi import mlab
    numbers = atoms.get_atomic_numbers()
    u_numbers = np.unique(numbers)
    if vmax is None:
        vmax = atoms.positions[:,2].max()
    vmin = vmax - depth
    my_colors = {1:(1,0,0), 6:(0.5,0.5,1)}

    for number in u_numbers:
        take = numbers == number
        element_atoms = atoms[take]
        points = element_atoms.positions
        points = points.transpose()
        radius = my_radii[number]*2
        mlab.points3d(points[0], points[1], points[2], points[2], scale_mode='none', scale_factor=radius, colormap=colormap, vmin=vmin, vmax=vmax,
                    resolution=resolution,
                    reset_zoom=False,
                    figure=figure,
                    )
    return figure

def colorRange(input_atoms, scale_factor=1):
    numbers = input_atoms.get_atomic_numbers()
    argmax = input_atoms.positions[:,2].argmax()
    argmin = input_atoms.positions[:,2].argmin()
    radius =  scale_factor*my_radii[numbers[argmax]]/2.0
    vmax = input_atoms.positions[argmax ,2] + radius
    radius =  scale_factor*my_radii[numbers[argmin]]/2.0
    vmin = input_atoms.positions[argmin ,2] + radius

    return vmin, vmax

def addDepthMap(figure,
                input_atoms,
                vmin,
                vmax,
                scale_factors=None,
                colormap='hot',
                resolution=32,
                ):
    from mayavi import mlab
    if scale_factors is None:
        scale_factors=np.ones(len(input_atoms))

    numbers = input_atoms.get_atomic_numbers()
    collections = set(zip(numbers, scale_factors))

    for number, scale_factor in collections:
        take1 = numbers == number
        take2 = scale_factors == scale_factor
        take = np.logical_and(take1, take2)
        atoms = input_atoms[take]
        points = atoms.positions
        radius = my_radii[number]/2.0
        radius *= scale_factor

        points = points[points[:, 2] > vmin - radius]

        for point  in points:
            x, y, z = sphere(point, radius, resolution=resolution)
            mlab.mesh(x,y,z,
                      scalars=z,
                      vmin=vmin,
                      vmax=vmax,
                      colormap=colormap,
                      figure=figure,
                      )


def sphere(center, radius, resolution=10):
    pi = np.pi
    cos = np.cos
    sin = np.sin
    dphi, dtheta = pi / resolution, pi / resolution
    [phi, theta] = np.mgrid[0:pi + dphi * 1.5:dphi,
                               0:2 * pi + dtheta * 1.5:dtheta]
    r = radius

    x = r * sin(phi) * cos(theta) + center[0]
    y = r * cos(phi) + center[1]
    z = r * sin(phi) * sin(theta) + center[2]

    return x, y, z

def standardView(figure, atoms, resolution=32, scale=1):
    from mayavi import mlab
    input_atoms = atoms.copy()
    del input_atoms.constraints
    cell_center = cellCenter(input_atoms)

    numbers = input_atoms.get_atomic_numbers()
    u_numbers = np.unique(numbers)
    vmin, vmax = input_atoms.positions[:,2].min(), input_atoms.positions[:,2].max()
    vmin += (vmax - vmin)/2

    for number in u_numbers:
        take = np.array(numbers == number, bool)
        atoms = input_atoms[take]
        points = atoms.positions
        points = points.transpose()
        color = tuple(jmol_colors[number])
        radius = my_radii[number]
        if np.isnan(radius):
            radius = 3.00
        radius *= 2*scale
        mlab.points3d(points[0], points[1], points[2], color=color,
                      scale_factor=radius,
                      resolution=resolution,
                      )

def mview(atoms, figure=None):
    if figure is None:
        from mayavi import mlab
        figure = mlab.figure(bgcolor=(0, 0, 0))
    cell_origin = atoms.get_celldisp()
    if cell_origin.shape == (3, 1):
        cell_origin = np.reshape(cell_origin, (3,))
    showCell(figure, atoms.cell, origin=cell_origin)
    for electrode in atoms.electrodes():
        cell_origin = electrode.celldisp()
        showCell(figure, electrode.cell(), origin=cell_origin, pbc=electrode.pbc())
    standardView(figure, atoms)

    return figure

def plotIsosurface(figure, values, variables, contour_value=0.0001, color=(0,0,1)):
    from mayavi import mlab
    x, y, z = variables
    if isinstance(color, str):
        color = {'blue': (0,0,1), 'red':(1,0,0)}[color]
    surf = mlab.contour3d(x,y,z,values, contours=[contour_value], color=color)

    return mlab

def colorIsosurface(
        figure,
        variables,
        iso_grid,
        color_grid,
        contour_value=0.01,
        colormap='jet',
        vmin=None,
        vmax=None,
        ):
    from mayavi import mlab
    x, y, z = variables
    src = mlab.pipeline.scalar_field(x, y, z, iso_grid)
    src.image_data.point_data.add_array(color_grid.T.ravel())

    # We need to give a name to our new dataset.
    src.image_data.point_data.get_array(1).name = 'potential'

    # Make sure that the dataset is up to date with the different arrays:
    src.image_data.point_data.update()

    contour = mlab.pipeline.contour(src)
    contour.filter.contours = [contour_value]


    contour2 = mlab.pipeline.set_active_attribute(contour,
                                        point_scalars='potential')

    ## And we display the surface. The colormap is the current attribute: the z-value.
    surface=mlab.pipeline.surface(contour, colormap=colormap, vmin=vmin, vmax=vmax)

def makeTracePlot(ax,
        data,
        size=20,
        c_range=2.0,
        detail_string='',
        set_limits=True,
        colormap='hot',
        ):
    X, Y, Z = data
    ax.set_aspect('equal')
    if set_limits:
        lim = size/2.
        ax.set_xlim(-lim, lim)
        ax.set_ylim(-lim, lim)
        take1 = np.logical_and(X > -lim, X < lim)
        take2 = np.logical_and(Y > -lim, Y < lim)
        take = np.logical_and(take1, take2)
        Zmax = Z[take].max()
        Zmin = Z[take].min()
    else:
        Zmax = Z.max()
        Zmin = Z.min()

    Z = Z.copy() - Zmax

    vmax = 0
    if c_range is None:
        vmin = Zmin - Zmax
    else:
        vmin = -c_range

    p = ax.pcolormesh(X, Y, Z, cmap=colormap, vmin=vmin, vmax=vmax)

    return p, c_range

def setupSurfacePlot(ax, data, c_range, mask_range):
    from pylab import cm
    x, y, z = data

    if z is None:
        z = np.zeros((len(x), len(y))) - c_range
        vmin, vmax = -c_range, 0
    else:
        take1 = z > mask_range[0]
        take2 = z < mask_range[1]
        take = np.logical_and(take1, take2)
        vmax = z[take].max()

        z = z - vmax
        vmax =  0
        if c_range is None:
            c_range = abs(z.min())
        vmin = -c_range

    ax.set_xlabel('x,y(Ang)')

    return ax.pcolormesh(x, y, z, vmin=vmin, vmax=vmax, cmap=cm.hot)

def padVariables(variables):
    variables = np.array(variables)
    if len(variables) == 2:
        dh_cell = [
                   variables[:, 1, 0] - variables[:, 0, 0],
                   variables[:, 0, 1] - variables[:, 0, 0],
                  ]
    elif len(variables) == 3:
        dh_cell = [
                   variables[:, 1, 0, 0] - variables[:, 0, 0, 0],
                   variables[:, 0, 1, 0] - variables[:, 0, 0, 0],
                   variables[:, 0, 0, 1] - variables[:, 0, 0, 0],
                  ]
    dh_cell = np.array(dh_cell)
    dh_vector = dh_cell.sum(axis=0)
    shape = np.array(variables.shape[1:])
    cell = np.array([shape[i]*dh_cell[i] for i in range(len(shape))])
    shape += 1

    if len(variables) == 2:
        abc = np.mgrid[0:shape[0]/float(shape[0]):shape[0]*1j,
                       0:shape[1]/float(shape[1]):shape[1]*1j,
                       ]
    elif len(variables) == 3:
        abc = np.mgrid[0:shape[0]/float(shape[0]):shape[0]*1j,
                       0:shape[1]/float(shape[1]):shape[1]*1j,
                       0:shape[2]/float(shape[2]):shape[2]*1j,
                       ]
    new_variables = np.tensordot(cell, abc, (0, 0))
    for i in range(len(shape)):
        new_variables[i] += variables[i].min() - dh_vector[i]/2.0

    return new_variables

def depthView(
        figure,
        atoms,
        colormap='hot',
        resolution=32,
        vmax=3.5,
        depth=4,
        ):
    import numpy as np
    from mayavi import mlab

    input_atoms = atoms.copy()
    vmin = vmax - depth

    cell_center = cellCenter(input_atoms)
    scale_factors = numpy.ones(len(input_atoms))*2

    addDepthMap(
            figure=figure,
            input_atoms=input_atoms,
            scale_factors=scale_factors,
            colormap=colormap,
            resolution=resolution,
            vmin=vmin,
            vmax=vmax,
            )

def showCell(
        figure,
        cell,
        origin=(0,0,0),
        color=(1,1,1),
        tube_radius=0.3,
        pbc=(True, True, True),
        ):
    from mayavi import mlab
    origin = np.array(origin)

    for i, p in enumerate(pbc):
        if isinstance(p, tuple) and len(p) == 2:
            p1, p2 = (origin, origin + cell[i])
            start, end = {(False, True):(p2, cell[i]), (True, False): (p1, -cell[i])}[p]
            mlab.quiver3d( start[0], start[1], start[2], end[0], end[1], end[2],
                    scale_factor=4*tube_radius,
                    scale_mode='vector',
                    )

    if origin.shape==(3, 1):
        origin = np.reshape(origin, (3,))
    if cell.shape == (2, 3):
        segments = np.array([
            [[0,0,0], cell[0]],
            [cell[0], cell[0] + cell[1]],
            [cell[0] + cell[1], cell[1]],
            [cell[1], [0,0,0]],
            ])
        points = np.array([[0,0,0], cell[0], cell[0]+cell[1], cell[1], [0,0,0]])
    elif cell.shape == (3, 3):
        segments = np.array([
            [[0,0,0], cell[0]],
            [cell[0], cell[0] + cell[1]],
            [cell[0] + cell[1], cell[1]],
            [cell[1], [0,0,0]],
            [cell[2]          , cell[2] + cell[0]],
            [cell[2] + cell[0]          , cell[2] + cell[0] + cell[1]],
            [cell[2] + cell[0] + cell[1], cell[2] + cell[1]],
            [cell[2] + cell[1],           cell[2] + [0,0,0]],
            [[0,0,0], cell[2]],
            [cell[0], cell[0] + cell[2]],
            [cell[0] + cell[1], cell[0] + cell[1] + cell[2]],
            [cell[1], cell[1] + cell[2]],
            ])
        points = np.array( [
            [0,0,0],
            cell[0],
            cell[0]+cell[1],
            cell[1],
            cell[2],
            cell[2] + cell[0],
            cell[2] + cell[0] + cell[1],
            cell[2] + cell[1],
            ])
    else:
        raise Exception("Cell argument is weird: %s" % cell)

    color = tuple(color)
    points += np.array(origin)
    segments += np.array([origin, origin])

    for segment in segments:
        xt, yt, zt = np.transpose(segment)
        mlab.plot3d(xt, yt, zt,
                tube_radius=tube_radius,
                figure=figure,
                color=color,
                tube_sides=32,
                )
    for point in points:
        x, y, z = point
        mlab.points3d(x, y, z,
                scale_factor=tube_radius*2,
                figure=figure,
                color=color,
                scale_mode='vector',
                resolution=32,
                )


def plotXYPhase(ax, item, variables, index):
    ax.pcolormesh(
            variables[0, :,:,0], variables[1, :, :, 0],
            np.angle(item[:,:, index]),
            cmap='hsv',
            vmin=-np.pi,
            vmax=np.pi,
            )

def plotXYAbs(ax, item, variables, index):
    ax.pcolormesh(
            variables[0, :,:,0], variables[1, :, :, 0],
            np.abs(item[:,:, index]),
            cmap='hot',
            )

def plotXZPhase(ax, item, variables, index):
    ax.pcolormesh(
            variables[0, :,0,:], variables[2, :, 0, :],
            np.angle(item[:, index, :]),
            cmap='hsv',
            vmin=-np.pi,
            vmax=np.pi,
            )

def plotXZAbs(ax, item, variables, index):
    ax.pcolormesh(
            variables[0, :,0,:], variables[2, :, 0, :],
            np.abs(item[:, index, :]),
            cmap='hot',
            )

def showRealWavefunction(figure, wf, r, contour_value=0.003, factor=1):
    wf = wf.real
    wf *= factor
    plotIsosurface(
            figure,
            values=wf,
            variables=r,
            contour_value=contour_value,
            color='red',
            )
    plotIsosurface(
            figure,
            values=wf,
            variables=r,
            contour_value=-0.003,
            color='blue',
            )
