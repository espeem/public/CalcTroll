# Written by Mads Engelund, 2017, http://espeem.com
import os
import sys
from importlib import reload
from CalcTroll.Core.Flags import DEFAULT

def makeSubmissionScript(filename, submit_type=DEFAULT):
    #module = loadScript(filename)
    #ALL_RUNS.script()
    with open(filename, 'r') as f:
        lines = f.readlines()

    name = '.'.join(filename.split('.')[:-1])

    s_filename = name + '__submit.py'
    with open(s_filename, 'w') as f:
        f.write(SUBMIT_TYPE_IDENTIFIER + ': %s\n' % submit_type)
        for line in lines:
            f.write(line)

    return s_filename

def removeLocalFile(filename):
    os.remove(progress_filename)


def loadScript(filename, should_reload=True):
    assert os.path.exists(filename)
    assert os.path.abspath(filename) == filename
    cwd = os.getcwd()
    dirname, basename = os.path.split(filename)
    os.chdir(dirname)
    sys.path.append(dirname)
    name = '.'.join(basename.split('.')[:-1])
    try:
        module = __import__(name)
    except Exception as error:
        print(("Warning: Reading the script: '%s' raised the following error:" % basename))
        print((error))
    else:
        if should_reload:
            reload(module)

    sys.path.pop(-1)
    os.chdir(cwd)


def loadAllScripts(scripts, should_reload=True):
    modules = []
    for script in scripts:
        with open(script, 'r') as f:
            first_line = f.readline().strip()
        if SUBMIT_TYPE_IDENTIFIER in first_line:
            submit_type = first_line.split()[-1]
        else:
            submit_type = DEFAULT
        ALL_RUNS.setSubmitType(submit_type)
        module = loadScript(script, should_reload=should_reload)
