# Written by Mads Engelund, 2018, http://espeem.com
from .Utilities import loadScript

def estimate(filename, host):
    """
    Provides an estimate of how much effort it would take to run all the tasks defined in
    a script.

    :param filename: The script(python file) that should be estimated
    :param host: The host machine that will be used for calculation.
    :return: A number representing the level of effort required.
    """
    from .Workflow import WORKFLOW
    print(filename)
    WORKFLOW.setHost(host)
    loadScript(filename, should_reload=False)
    WORKFLOW.collate()
    WORKFLOW.updateTasks()

    estimate = 0
    for task in WORKFLOW.tasks():
        if not task.isDone():
            estimate += task.estimate()

    return estimate
