# Written by Mads Engelund, 2017, http://espeem.com
from CalcTroll.Core.ParameterControl import ParameterControl
from CalcTroll.Core.Flags import DEFAULT

class SubmitType(ParameterControl):
    def __init__(self, string=DEFAULT): pass
