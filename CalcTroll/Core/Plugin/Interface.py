# Written by Mads Engelund, 2017, http://espeem.com
import importlib

from CalcTroll.Core.Plugin import Types
from CalcTroll.Core.Test.Suite import isPluginModule

from .Utilities import explorePackage

PACKAGES = ('CalcTroll.Plugins', )

class NotFoundPlugin(ValueError): pass

def load(name=None, packages=PACKAGES):
    if name is None:
        return loadAllPlugins(packages=packages)
    else:
        return loadPlugin(name, packages=packages)

def loadAllPlugins(packages):
    all_modules = []
    for package in packages:
        modules = loadPackageAllPlugins(package=package)
        all_modules.extend(modules)

    return getLoadedPlugins()

def loadPackageAllPlugins(package):
    module_names = explorePackage(package)
    for module_name in module_names:
        if isPluginModule(module_name):
            importlib.import_module(module_name)

    return getLoadedPlugins()

def getLoadedPlugins():
    plugin_dict = {}
    for key, value in list(Types.ALL.items()):
        plugin_dict[key] = value.plugins

    return plugin_dict

def loadPlugin(name, packages=PACKAGES):
    for package in packages:
        try:
            module = loadPackage(name=name, package=package)
        except NotFoundPlugin:
            pass
        else:
            return module

    for key, value in list(Types.ALL.items()):
        for cls in value.plugins:
            if name == cls.className():
                return cls

    raise NotFoundPlugin("The plugin '%s' was not found." % name)

def loadPackage(name, package):
    module_names = explorePackage(package)
    for module_name in module_names:
        if isPluginModule(module_name):
            importlib.import_module(module_name)

    for key, value in list(Types.ALL.items()):
        for cls in value.plugins:
            if name == cls.className():
                return cls

    raise NotFoundPlugin("The plugin '%s' was not found." % name)
