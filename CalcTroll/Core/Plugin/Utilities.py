# Written by Mads Engelund, 2017, http://espeem.com
# Adapted from Web Dev Zone Frederico Thomazetti
# In the public domain.

import sys
import os
import pkgutil

def explorePackage(
        module_name,
        modules=None,
        ):
    if 'Examples' in module_name:
        return modules

    if modules is None:
        modules = []
        failed_modules = []

    loader = pkgutil.get_loader(module_name)
    if loader is None:
        return modules

    filename = loader.get_filename()
    path = os.path.dirname(filename)

    for sub_module in pkgutil.walk_packages(path=[path]):
        module_path, sub_module_name, is_package = sub_module

        qname = module_name + "." + sub_module_name
        modules.append(qname)

        if is_package:
            explorePackage(
                    qname,
                    modules=modules,
                    )

    return modules
