# Written by Mads Engelund, 2017, http://espeem.com
from collections import OrderedDict
from CalcTroll.Core.ParameterControl import ParameterControl
from .Mount import Mount

class Type(ParameterControl): pass

class InternalType(ParameterControl): pass


class ProgramProvider(Type, metaclass=Mount):
    pass

class SystemProvider(Type, metaclass=Mount):
    pass

class AnalysisProvider(Type, metaclass=Mount):
    pass

class HostProvider(Type, metaclass=Mount):
    pass

class Parameters(Type, metaclass=Mount):
    pass

class ImplementationProvider(Type, metaclass=Mount):
    pass

loc = dict(locals())
ALL = OrderedDict([(key, value) for key, value in list(loc.items()) if \
        isinstance(value, type) and \
        issubclass(value, Type) and \
        value != Type
        ])
