# Written by Mads Engelund, 2017, http://espeem.com

from CalcTroll.Core.Flags import DEFAULT
from CalcTroll.Core.Relaxation import AbstractRelaxed, Relaxed
from .AbstractAnalysis import AbstractAnalysis


class Analysis(AbstractAnalysis):
    def __init__(self,
                 system=None,
                 method=DEFAULT,
                 parameters=DEFAULT,
                 inputs=tuple(),
                 ):

        inputs = list(inputs)
        if system == None:
            pass
        elif not isinstance(system, AbstractRelaxed):
            system = Relaxed(
                    system,
                    method=method,
                    parameters=parameters,
                    )

        if system != None:
            if method == DEFAULT:
                method = system.method()
            if parameters == DEFAULT:
                parameters = system.parameters()

        self.__system = system
        self.__method = method
        self.__parameters = parameters

        AbstractAnalysis.__init__(self, inputs=inputs)

    def method(self):
        return self.__method

    def system(self):
        return self.__system

    def parameters(self):
        return self.__parameters


class NoTasksAnalysis(Analysis):
    def tasks(self):
        return []

    def implementation(self):
        return None