from CalcTroll.Core.Plugin.Types import ImplementationProvider
from CalcTroll.Core.Task import AbstractHasTasks

class Implementation(AbstractHasTasks, ImplementationProvider):
    priority = 0
    @classmethod
    def canHandle(cls, item):
        # Returns a True/False value.
        return False

    def __eq__(self, other):
        if self.__class__ != other.__class__:
            return False

        return self.acceptItem(other)

    def __init__(
            self,
            items,
            ):
        self.__tasks = None
        self._setItems(items)

        super(Implementation, self).__init__([])

    def tasks(self):
        if self.__tasks is None:
            self.__tasks = self.makeTasks(self.items())

        return self.__tasks

    def items(self):
        return self.__items

    def acceptItem(self, item):
        if self.system() != item.system():
            return False
        if self.method() != item.method():
            return False
        if self.parameters() != item.parameters():
            return False

        return True

    def _setItems(self, items):
        self.__tasks = None
        self.__items = list(items)
        for item in items[1:]:
            accept = self.acceptItem(item)
            if not accept:
                raise ValueError


    def system(self):
        return self.__items[0].system()

    def method(self):
        return self.__items[0].method()

    def parameters(self):
        return self.__items[0].parameters()

    def read(self, item, **kwargs):
        raise NotImplementedError

    def makeTasks(self, items):
        raise NotImplementedError
