# Written by Mads Engelund, 2020, http://espeem.com
# Defines the scripting interface to creating images.
import os
import numpy as np
from scipy.io import netcdf
from os.path import join

from CalcTroll import API
from CalcTroll.Core.Analysis.Analysis import Analysis
from CalcTroll.Core.Flags import DEFAULT, ALL
from CalcTroll.Core.Relaxation import AbstractRelaxed
from CalcTroll.Core.Analysis.Probe import Probe
from CalcTroll.Core.Flags import CalcTrollException
from CalcTroll.Core.Utilities import xy

class Image(Analysis):
    """ Scripting interface item representing a Scanning Probe Microscopy
        (SPM) image.
    """
    @staticmethod
    def hasOpenBoundaries():
        return False

    def neededFiles(self):
        return self.method().neededFiles()

    def frame(self, data=None, border=DEFAULT):
        if data is None:
            data = self.read()

        if isinstance(data, CalcTrollException):
            return None

        atoms = self.system().atoms()
        frame = self.system().frame(data=atoms, border=border)

        return frame

    def png_save(self, filename, data=None, **kwargs):
        return self.jpg_save(filename=filename, data=data, **kwargs)

    def jpg_save(self, filename, data=None, frame=None, border=DEFAULT, **kwargs):
        from matplotlib import pylab as plt
        if data is None:
            data = self.read()

        if isinstance(data, CalcTrollException):
            pmin, pmax = np.array([[0, 0], [1, 1]])
        else:
            if frame is None:
                frame = self.frame(data=data, border=border)
                frame = np.array(frame)
            pmin, pmax = frame

        d = pmax - pmin
        dx = d[0]
        dy = d[1]
        ratio = dx/dy

        fig = plt.figure(frameon=False, figsize=(4*ratio, 4))
        fig.patch.set_facecolor('black')

        ax = plt.Axes(fig, [0., 0., 1., 1.])
        fig.add_axes(ax)

        self.plot(data=data, ax=ax, frame=frame, **kwargs)

        ax.set_xlim((pmin[0], pmax[0]))
        ax.set_ylim((pmin[1], pmax[1]))

        plt.savefig(filename)
        plt.close('all')

        return frame

    def txt_save(self, filename, data=None, **kwargs):
        if data is None:
            data = self.read()

        Z, variables = data

        X, Y = variables

        with open(filename, 'w') as f:
            for x, y, z in zip(X.flatten(), Y.flatten(), Z.flatten()):
                f.write('%10.5f\t%10.5f\t%10.5f\n' % (x, y, z))

    def gsf_save(
            self,
            filename,
            data=None,
            **kwargs):
        raise NotImplementedError

    @classmethod
    def save_nc(
            cls,
            items,
            results,
            label,
            ):
        assert all([isinstance(item, cls) for item in items])
        items = list(items)
        filename = "%s_%s.nc" % (label, 'images')
        old_scripts, old_results = cls.read_nc(filename)
        new_results = list(results)
        new_scripts = [item.script(detail_level=API.ALL, exclude=('system', 'method')) \
                   for item in items]
        scripts = list(old_scripts)
        results = list(old_results)
        for i in range(len(new_scripts)):
            if new_scripts[i]:
                scripts.append(new_scripts[i])
                results.append(new_results[i])

        # Write all images again.
        s_length = 2000
        with netcdf.netcdf_file(filename, 'w') as f:
            image, xy = results[0]
            sh = image.shape

            f.createDimension('n_items', len(results))
            f.createDimension('x', sh[0])
            f.createDimension('y', sh[1])
            f.createDimension('vector', 2)
            f.createDimension('s_length', s_length)
            origin = f.createVariable('origin', dimensions=('vector', ), type=np.dtype('f'))
            dxy = f.createVariable('dxy', dimensions=('vector', 'vector'), type=np.dtype('f'))

            var_images = f.createVariable('intensity', dimensions=('n_items', 'x', 'y'), type=np.dtype('f'))
            item_info = f.createVariable('item', dimensions=('n_items', 's_length'), type='c')
            origin[:] = np.array([xy[0, 0, 0], xy[1, 0, 0]])
            origin[:] = np.array([xy[0, 0, 0], xy[1, 0, 0]])
            dxy[:,:] = np.array([xy[:, 1, 0] - xy[:, 0, 0], xy[:, 0, 1] - xy[:, 0, 0]])

            for i in range(len(results)):
                image, xy = results[i]
                script = scripts[i]
                item_info[i, :] = script + ' ' * (s_length - len(script))
                var_images[i, :, :] = image


    @classmethod
    def read_nc(
            cls,
            filename,
            ):
        if not os.path.exists(filename):
            return [], []

        scripts = []
        results = []
        with netcdf.netcdf_file(filename, 'r', mmap=False) as f:
            vars = f.variables
            item = vars['item']
            for i in range(len(item[:])):
                intensity = vars['intensity'][i, :, :]
                sh = intensity.shape
                origin = vars['origin'][:]
                dxy = np.array(vars['dxy'][:,:])
                cell = np.array([dxy[i]*sh[i] for i in range(2)])
                XY = xy(shape=sh, cell=cell, origin=origin)
                script = item[i, :].tostring().strip().decode()
                scripts.append(script)
                results.append((intensity, XY))

        return scripts, results

    def inNCFile(self, filename):
        s_script = self.script(detail_level=API.ALL, exclude=('system', 'method'))
        with netcdf.netcdf_file(filename, 'r', mmap=False) as f:
            var = f.variables['item']
            scripts = [var[i, :].tostring().decode().strip() for i in range(var.shape[0])]

        try:
            index = scripts.index(s_script)
        except ValueError:
            return False
        else:
            return index

    def readData(self, path):
        items = self.selfContained()
        assert len(items) == 1
        item = items[0]

        if isinstance(item, API.InconsistentApproximation):
            return item
        filename = join(path, item.binaryFile())
        index = item.inNCFile(filename)
        if index is False:
            return False
        scripts, results = self.read_nc(filename)

        return results[index]

