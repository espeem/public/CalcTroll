# Written by Mads Engelund, 2017, http://espeem.com
from datetime import timedelta
from CalcTroll.Core.Task import AbstractHasTasks
from CalcTroll.Core.Plugin.Types import AnalysisProvider
from CalcTroll.Core.InterfaceFunctions import calculate, findImplementation
from CalcTroll.Core.Utilities import unique

class AbstractAnalysis(AnalysisProvider, AbstractHasTasks):
    def __init__(self, inputs=tuple()):
        self.__implementation = None
        self.__tasks = None
        AbstractHasTasks.__init__(self, inputs=inputs)

    def frame(self, **kwargs):
        raise NotImplementedError

    def plot(self, ax=None, **kwargs):
        print(self.__module__)
        raise NotImplementedError('"plot" on %s' % self.className())

    def explain(self):
        raise NotImplementedError(
            "The class %s has no 'explain' method."
            % self.className(),
            )

    def time(self):
        all_inputs = self.allInputs()
        all_tasks = self.tasks()

        for inp in all_inputs:
            all_tasks.extend(inp.tasks())

        all_tasks = unique(all_tasks)

        time = timedelta()
        for task in all_tasks:
            time += task.time()

        return time

    def implementation(self):
        if self.__implementation == None:
            self.__implementation = self.findImplementation()
        
        return self.__implementation

    def findImplementation(self):        
        return findImplementation(self)

    def tasks(self):
        if self.__tasks == None:
            self.__tasks = self.implementation().makeTasks([self])

        return self.__tasks

    def read(self, **kwargs):
        calculate(self)

        if not self.isDone():
            return None

        return self.implementation().read(self, **kwargs)
