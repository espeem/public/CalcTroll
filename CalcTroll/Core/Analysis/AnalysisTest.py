# Written by Mads Engelund, 2017, http://espeem.com
import unittest
from CalcTroll.Core.Test.Case import CalcTrollTestCase
from CalcTroll.Core.Test.Dummies import TestSystem, TestMethod, TestAnalysis

class AnalysisTest(CalcTrollTestCase):
    def testConstruction(self):
        system = TestSystem()
        method = TestMethod()
        run = TestAnalysis(system=system, method=method)
        self.assertRaises(NotImplementedError, run.tasks)
        self.assertEqual(run.script(), 'TestAnalysis(system=TestSystem(), method=TestMethod())')

    def testInheritance(self):
        system = TestSystem()
        method = TestMethod()
        class Inheritor(TestAnalysis): pass
        run = Inheritor(system=system, method=method)
        self.assertRaises(NotImplementedError, run.tasks)
        self.assertEqual(run.script(), 'Inheritor(system=TestSystem(), method=TestMethod())')

if __name__=='__main__':
    unittest.main()
