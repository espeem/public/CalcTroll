# Written by Mads Engelund, 2020, http://espeem.com
# Defines the scripting interface to creating images.
import os
import numpy as np
from scipy.io import netcdf
from os.path import join

from CalcTroll import API
from CalcTroll.Core.Analysis.Analysis import Analysis
from CalcTroll.Core.Flags import DEFAULT, ALL
from CalcTroll.Core.Relaxation import AbstractRelaxed
from CalcTroll.Core.Analysis.Probe import Probe
from CalcTroll.Core.Flags import CalcTrollException
from CalcTroll.Core.Utilities import xy

class Spectrum(Analysis):
    """ Scripting interface item representing a spectrum of one variable. """
    def neededFiles(self):
        return self.method().neededFiles()

    def frame(self, data=None, border=DEFAULT):
        raise NotImplementedError

    def jpg_save(self, filename, data=None, frame=None, border=DEFAULT, **kwargs):
        raise NotImplementedError

    def txt_save(self, filename, data=None, **kwargs):
        raise NotImplementedError

    def gsf_save(
            self,
            filename,
            data=None,
            **kwargs):
        raise NotImplementedError

    @classmethod
    def save_nc(
            cls,
            items,
            results,
            label,
            ):
        raise NotImplementedError

    @classmethod
    def read_nc(
            cls,
            filename,
            ):
        raise NotImplementedError

    def inNCFile(self, filename):
        raise NotImplementedError

    def readData(self, path):
        raise NotImplementedError

