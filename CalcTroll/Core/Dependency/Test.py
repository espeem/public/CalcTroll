# Written by Mads Engelund, 2021, http://espeem.com
# Test that all required dependecies are included
# and that they behave correctly.
import os
import unittest
import subprocess
from CalcTroll.Core.Test.Case import CalcTrollTestCase

class DependencyTest(CalcTrollTestCase):
    def testPymatgen(self):
        import pymatgen
    def testRDKIT(self):
        import rdkit

    def testASE(self):
        import ase
    def testMatplotLib(self):
        import matplotlib

    def testMatplotLib(self):
        from matplotlib import pylab as plt
        filename = 'test.jpg'
        self.markForRemoval([filename])
        fig = plt.figure()
        fig.savefig('test.jpg')

        self.assertTrue(os.path.exists(filename))

    def notestSiesta(self):
        self.prepareTestDirectory(__file__)
        cmd = 'siesta < siesta.fdf'

        out = subprocess.check_output(cmd, shell=True, stdin=subprocess.PIPE, universal_newlines=True)
        self.assertTrue('Job completed' in out)
        self.assertTrue(os.path.exists('Rho.grid.nc'))

if __name__=='__main__':
    unittest.main()
