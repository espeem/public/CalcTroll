# Written by Mads Engelund, 2017, http://espeem.com
import numpy as np
from ase.visualize import view as AseView
from ase.io import read as AseRead
from ase.atoms import Atoms as AseAtoms
from ase.data import chemical_symbols

from CalcTroll.Core.Plugin.Interface import load
from CalcTroll.Core.Analysis.Implementation import Implementation
from CalcTroll.Core.System import System
from CalcTroll.Core.Submission.Submitter import submit, calculate
from CalcTroll.Core.Submission.Estimation import estimate
from .Flags import CalcTrollException


def read_mol(filename):
    positions = []
    symbols = []
    with open(filename, 'r') as f:
        lines = f.readlines()
        del(lines[:4])
        for line in lines:
            print(line)
            line = line.split()
            if not str(line[3]) in chemical_symbols:
                break
            x, y, z, symbol = line[:4]
            symbols.append(symbol)
            positions.append([float(x), float(y), float(z)])
    return AseAtoms(symbols=symbols, positions=positions)

def read(calctroll_object):
    if not calctroll_object.isDone():
        calculate(calctroll_object)

        return None

    try:
        data = calctroll_object.read()
    except CalcTrollException as err:
        data = err

    return data

def time(calctroll_object):
    if not calctroll_object.isDone():
        calculate(calctroll_object)

        return None

    try:
        obj = calctroll_object
    except CalcTrollException as err:
        return err

    return obj.time()

def show(calctroll_object):
    data = read(calctroll_object)
    if data is None:
        string = 'NOT_CALCULATED'
    else:
        string = str(data)

    return string

def plot(calctroll_object, ax=None, show=False, **kwargs):
    data = calctroll_object.read()
    if data is None:
        return

    from matplotlib import pyplot as plt
    if ax is None:
        show = True
        figure = plt.figure()
        ax = figure.gca()

    if data is not None:
        calctroll_object.plot(data=data, ax=ax, **kwargs)

    if show:
        plt.show()


def clprint(calctroll_object, **kwargs):
    data = read(calctroll_object)
    print(data)


def save(calctroll_object, filename, **kwargs):
    data = read(calctroll_object)

    if data is None:
        return None

    return calctroll_object.save(filename, data, **kwargs)


def view(some_object):
    if isinstance(some_object, AseAtoms):
        AseView(some_object)
    elif isinstance(some_object, System):
        return AseView(some_object.atoms())

def findImplementation(item):

    plugin_dict = load()
    implementations = list(plugin_dict['ImplementationProvider'])
    implementations.remove(Implementation)

    available_implementations = []    
    for implementation in implementations:
        if implementation.canHandle(item):
            available_implementations.append(implementation)

    if len(available_implementations) == 0:
        raise NotImplementedError("No implementation exists for %s" % item)
    priorities = np.array([implementation.priority for implementation in available_implementations])
    max_priority = priorities.max()
    is_max = priorities == max_priority
    if is_max.sum() > 1:
        raise ValueError("Several implementations claim '%s' at priority '%s' - either make implementaions more selective, delete them, or raise/lower priorities" % (item, max_priority))

    implementation = np.array(available_implementations)[is_max][0]
    
    return implementation([item])




