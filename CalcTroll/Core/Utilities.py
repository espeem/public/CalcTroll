# Written by Mads Engelund, 2017, http://espeem.com
# Contains functions with functionality not tied to specific plugins, but generally useful.
import numpy as np
from numpy import fft
from scipy import signal
from numpy import linalg
from hashlib import sha256

from ase.units import Bohr

def generateHash(string):
    """ Define a default way to create a hex string hash in this project."""
    h = sha256(string.encode()).hexdigest()[:8]
    return h

def unique(my_list):
    """ Replacement for numpy.unique which sometimes is strange"""
    i = 0
    while i < len(my_list) - 1:
        indices = [j for j in range(i + 1, len(my_list)) if my_list[i] == my_list[j]]
        indices.reverse()
        for j in indices:
            my_list.pop(j)

        i += 1

    return my_list


def isSpinPolarized(eigs):
    eigs = np.array(eigs)
    if eigs.shape[2] == 1:
        return False

    spin_1 = eigs[:, :, 0]
    spin_2 = eigs[:, :, 1]

    diff = spin_1 - spin_2

    return abs(diff).max() > 0.01


def normalize(vector):
    vector = np.array(vector)
    length = vectorLength(vector)
    if length < 1e-13:
        return None
    else:
        return vector/length


def cellCenter(cell, directions=(True, True, True)):
    return cell[np.array(directions)].sum(0)/2


def makeCoordTransformationMatrix(angles):
    t1, t2, t3 = angles
    rotation_x=matrix([[1,0,0],[0,np.cos(t1),-np.sin(t1)],[0,np.sin(t1),np.cos(t1)]])
    rotation_y=matrix([[np.cos(t2),0,np.sin(t2)],[0,1,0],[-np.sin(t2),0,np.cos(t2)]])
    rotation_z=matrix([[np.cos(t3),-np.sin(t3),0],[np.sin(t3),np.cos(t3),0],[0,0,1]])
    trans_matrix=rotation_z*rotation_y*rotation_x

    return trans_matrix


def vectorLength(vec, axis=0):
    vec = np.array(vec)
    return np.sqrt(np.sum(vec**2, axis=axis))


def convertToIntegers(array, tolerance=1e-2):
    new_array = np.around(array)
    diff = array - new_array
    assert sum(sum(abs(diff)))/len(diff) < tolerance
    return np.array(new_array, dtype=int)


def convertToBasis(r, unit_cell):
    M = np.array(unit_cell).T
    Minv = moorePenroseInverse(M)
    r = np.array(r)
    if len(r.shape) == 1:
        r = np.reshape(r, (1, len(r)))

    f_positions = (Minv @ r.T).T

    tmp = convertFromBasis(f_positions, unit_cell)
    remainder = r - tmp

    return np.around(f_positions, 13), remainder

def convertFromBasis(positions, unit_cell):
    r = np.array(positions)
    if len(r.shape) == 1:
        r = np.reshape(r, (1, len(r)))
    M = np.array(unit_cell).T
    r = (M @ r.T).T

    return r

def wrapPositions(positions, unit_cell, center):
    f_pos, remainder = convertToBasis(positions, unit_cell)
    shift = np.array(center) + 1e-2
    f_pos = ((f_pos + shift)%1 - shift)
    pos = convertFromBasis(f_pos, unit_cell)
    pos = pos + remainder

    return pos


def periodicDistance(difference, unit_cell):
    difference = minimalDistance(difference, unit_cell)

    return vectorLength(difference, 1)


def minimalDistance(difference, unit_cell):
    if len(unit_cell) == 0:
        return difference

    f_pos, remainder = convertToBasis(difference, unit_cell)
    shift = 0.5
    f_pos = ((f_pos + shift)%1 - shift)
    pos = convertFromBasis(f_pos, unit_cell)
    pos = pos + remainder

    return pos


def findCopies(positions, unit_cell):
    f_pos, remainder = convertToBasis(positions, unit_cell)
    l_b = len(unit_cell)
    assert not np.isnan(unit_cell.sum())

    take = np.ones(len(positions), bool)
    for i in range(len(positions) - 1):
        diff = f_pos[i+1:] - f_pos[i]
        diff = (diff + 0.5)%1 - 0.5
        diff = convertFromBasis(diff, unit_cell)
        diff_length = vectorLength(diff, 1)
        condition = diff_length > 2e-1
        take[i+1:] = np.logical_and(take[i+1:], condition)

    return take


def findOrthogonalVector(v1, v2, v3):
    v3 = matrix(v3).transpose()
    A = matrix(np.array([v1, v2])).transpose()
    Ainv = moorePenroseInverse(A)
    x = Ainv*v3
    x = np.array(np.around(x), dtype=int)
    v3 = np.array(v3 - A*x)
    v3 = v3.transpose()[0]

    return v3


def moorePenroseInverse(A):
    A = np.array(A)
    Ah = (A.conjugate()).transpose()
    inverse = linalg.inv(Ah @ A) @ Ah

    return inverse


def findBoxAboutThisSize(cell, vectors):
    vectors, remainder = convertToBasis(np.array(vectors), cell)
    vectors = np.ceil(vectors)

    return vectors


def findLastVector(cell, vectors, radius):
    assert (vectorLength(vectors, 1) > 1e-5).all()
    v0, v1 = np.dot(cell.transpose(), vectors.transpose()).transpose()
    normal = np.array(np.cross(v0, v1), dtype=np.float)
    normal /= np.linalg.norm(normal)
    tmp_vector = radius*normal
    tmp_vector, remainder = convertToBasis(np.array([tmp_vector]), cell)
    tmp_vector = tmp_vector[0]

    test_vectors = []
    for f0 in [np.floor, np.ceil]:
        for f1 in [np.floor, np.ceil]:
            for f2 in [np.floor, np.ceil]:
                test_vectors.append([f0(tmp_vector[0]), f1(tmp_vector[1]), f2(tmp_vector[2])])
    test_vectors = np.array(test_vectors, dtype=np.int)
    test_vectors = convertFromBasis(test_vectors, cell)
    test_vectors = test_vectors[vectorLength(test_vectors, 1) > radius]
    pro = vectorLength(np.cross(test_vectors, normal), 1)
    test_vectors = test_vectors[pro < min(pro) + 1e-5]
    vector = test_vectors[np.argmin(vectorLength(test_vectors))]

    vector, remainder = convertToBasis(vector, cell)
    vector = convertToIntegers(vector)[0]

    return vector

def xy(shape, cell, origin=None):
    ab = np.mgrid[0:shape[0]/float(shape[0]+1):shape[0]*1j,
                      0:shape[1]/float(shape[1]+1):shape[1]*1j,
                      ]
    xy = np.tensordot(cell, ab, (0, 0))

    if origin is not None:
        xy[0] += origin[0]
        xy[1] += origin[1]

    return xy

def xyz(shape, cell, origin=None):
    abc = np.mgrid[0:shape[0]/float(shape[0]+1):shape[0]*1j,
                      0:shape[1]/float(shape[1]+1):shape[1]*1j,
                      0:shape[2]/float(shape[2]+1):shape[2]*1j,
                      ]
    xyz = np.tensordot(cell, abc, (0, 0))

    if origin is not None:
        xyz[0] += origin[0]
        xyz[1] += origin[1]
        xyz[2] += origin[2]

    return xyz


def xy_z(shape, cell, z_span):
    abc = np.mgrid[0:shape[0]/float(shape[0]+1):shape[0]*1j,
                   0:shape[1]/float(shape[1]+1):shape[1]*1j,
                   0:1:shape[2]*1j,
                   ]
    mult = np.zeros((3,3))
    mult[:2,:2] = cell[:2,:2]
    mult[2,2] = z_span[1] - z_span[0]
    result = np.tensordot(mult, abc, (0, 0))
    result[2] += z_span[0]

    return result


def gaussian(r, r0=0, sigma=1.0):
    diff = r.copy()
    if isinstance(r0, (int, float)):
        diff -= r0
        diff = diff**2
    else:
        r0 = np.array(r0)
        for i in range(len(r0)):
            diff[i] -= r0[i]

        diff = (diff**2).sum(axis=0)

    argument = diff/(2*sigma**2)
    argument = np.clip(argument,  0, 100)
    result = 1.0/(sigma*np.sqrt(2*np.pi))*np.exp(-argument)

    return result


def lorentzian(e, e0, gamma):
    return 1.0/(np.pi*gamma)*(gamma**2/((e-e0)**2 + gamma**2))


def fermi(e_norm):
    argument = np.clip(e_norm, -100, 100)
    result = 1/(np.exp(argument) + 1)

    return result


def diff_fermi(e_norm):
    argument = np.clip(e_norm, -100, 100)
    exp = np.exp(argument)
    result = -exp/(1 + exp)**2

    return result

def aperiodic_convolve1D(target, kernel, center=None):
    assert len(kernel) % 2 == 1
    assert len(kernel.shape) == 1
    sh = list(target.shape)
    sh[0] = sh[0] + (len(kernel)//2) * 2

    result = np.zeros(sh)
    result[len(kernel)//2:-(len(kernel)//2)] = target
    if len(sh) == 1:
        result[:len(kernel)//2] = target[0]
        result[-len(kernel)//2:] = target[-1]
    elif len(sh) == 2:
        result[:len(kernel)//2, :] = target[0, :]
        result[-len(kernel)//2:, :] = target[-1, :]
        kernel = np.reshape(kernel, (len(kernel), 1))

    result = signal.convolve(result, kernel, mode='valid')

    return result


def convolve(target, kernel, center=None):
    if center is None:
        center = np.array(kernel.shape, dtype=int)//2
    else:
        center = np.array(center)
    assert center.dtype == int

    # Protect the function from getting a too large calculation.
    n_kernel = len(kernel.flatten())

    if n_kernel < 2e6:
        return simple_convolve(target, kernel, center)

    s1 = kernel.shape[0]
    k1, k2 = kernel[:s1//2], kernel[s1//2:]

    c2 = np.array(center)
    c2[0] -= s1//2
    r1 = convolve(target, k1, center)
    r2 = convolve(target, k2, c2)
    result = r1 + r2

    return result


def simple_convolve(target, kernel, center):
    shape = [max(ts, ks) for ts, ks in zip(target.shape, kernel.shape)]
    f_kernel = fft.fftn(kernel, shape)
    f_input = fft.fftn(target, shape)
    f_result = f_kernel * f_input
    result = fft.ifftn(f_result)

    for i in range(len(center)):
        roll = -center[i]
        result = np.roll(result, roll, axis=i)

    return result

def apply2DGaussian(data, sigma):
    """
    Apply a Gaussian broadening to 3D data.
    """
    if sigma == 0.0:
        return data

    values, r = data
    sh = len(r.shape) - 1
    if sh == 3:
        X, Y, Z = r
        r_p = np.array([X[:,:,0], Y[:,:,0]])
    elif sh == 2:
        X, Y = r
        r_p = np.array([X, Y])
    center = np.array(r_p[0].shape)//2
    r0 = r_p[:, center[0], center[1]]

    kernel = gaussian(r_p, r0, sigma)
    kernel /= kernel.sum()
    if sh == 3:
        kernel = np.reshape(kernel, list(kernel.shape) + [1])

    values = convolve(values, kernel, center=center).real
    data = values, r

    return data

def readCube(filename):
    with open(filename, 'r') as f:
        # Ignore two comment lines
        f.readline().strip()
        f.readline().strip()

        natoms, x0, y0, z0 = splitLine(f.readline())
        natoms = int(natoms)
        origin = np.array([float(x0), float(y0), float(z0)])*Bohr

        shape = []
        dh_cell = []
        for i in range(3):
            s, x, y, z = splitLine(f.readline())
            shape.append(int(s))
            dh_cell.append([float(x), float(y), float(z)])
        shape = tuple(shape)
        dh_cell = np.array(dh_cell)*Bohr

        # Skip atoms
        for i in range(natoms):
            f.readline()

        volume_data = []
        for line in f:
            line = list(map(float, splitLine(line)))
            volume_data.extend(line)

        volume_data = np.reshape(volume_data, shape)

    in_coords = np.mgrid[0:shape[0], 0:shape[1], 0:shape[2]]

    x, y, z = np.tensordot(dh_cell, in_coords, (0,0))
    x += origin[0]
    y += origin[1]
    z += origin[2]

    return volume_data, np.array((x, y, z)), dh_cell

def splitLine(line):
    line = line.strip().split(' ')
    line = [entry for entry in line if len(entry) > 0]

    return line