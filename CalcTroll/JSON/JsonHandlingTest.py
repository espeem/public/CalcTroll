import os
import unittest
import json as jc
from ase.io import read
from ase.atoms import Atoms as ASEAtoms

from CalcTroll.Core.Flags import CalcTrollException
from CalcTroll.Core.Test.Case import CalcTrollTestCase
from CalcTroll.Core.Test.Dummies import MOL_FILE
from CalcTroll.Core.Submission.Workflow import Workflow
from CalcTroll.Core.ParameterControl import ParameterControl

from CalcTroll.JSON.JsonHandling import *

from CalcTroll import API


class JsonHandlingTest(CalcTrollTestCase):
    def testChemSpiderQuery(self):
        self.prepareTestDirectory(__file__)
        test_filename = '1234.mol'
        test_output = 'regularized.xyz'
        json = jc.dumps({
            'SourceFiles' : {
                test_filename : MOL_FILE,
                    },

            'Analysis'  :  {
                'PluginClass'  : 'Relaxed',
                'system' : {
                    'PluginClass' : 'ChemSpider',
                    'chemspider_id' : '1234',
                    },
                },
            'Output' : {
                'Save' : [
                    test_output,
                    ],
                }
            })
        response, script = queryJSON(json)
        response = jc.loads(response)
        self.assertEqual(response['Status'], 'NOT_DONE')

    def testValenceQuery(self):
        self.prepareTestDirectory(__file__)
        test_output = 'valence.txt'

        json = jc.dumps({
            'Analysis'  :  {
                'PluginClass'  : 'ValenceElectrons',
                'system' : {
                    'PluginClass' : 'Smiles',
                    'smiles_code' : "'NN'",
                    },
                },

            'Output' : {
                'Save' : [
                    test_output,
                    ],
                }
            })
        response, script = queryJSON(json)
        response = jc.loads(response)
        self.assertEqual(response['Status'], API.DONE.className())
        self.assertEqual(response[test_output], '14')

    def testChemSpider(self):
        self.prepareTestDirectory(__file__)
        test_filename = '1234.mol'
        test_output = 'regularized.xyz'
        json = jc.dumps({
            'SourceFiles' : {
                test_filename : MOL_FILE,
                    },

            'Analysis'  :  {
                'PluginClass'  : 'Unrelaxed',
                'system' : {
                    'PluginClass' : 'ChemSpider',
                    'chemspider_id' : '1234',
                    },
                },

            'Output' : {
                'Save' : [
                    test_output,
                    ],
                }
            })

        response, script = queryJSON(json)
        response = jc.loads(response)

        self.assertTrue(os.path.exists('script.py'))
        self.assertTrue(os.path.exists(test_filename))
        self.assertTrue(os.path.exists(test_output))

        with open(test_output, 'r') as f:
            content = f.read()
        self.assertEqual(response[test_output], content)

        atoms = read(test_output)

        expected = ASEAtoms('OCO', positions=[
                                              [-1.2, 0, 0],
                                              [ 0  , 0, 0],
                                              [ 1.2, 0, 0],
                                             ])
        self.assertAtomsEqual(expected, atoms)


    def testRegularization(self):
        self.prepareTestDirectory(__file__)
        json = jc.dumps({
            'Analysis'  :  {
                'PluginClass'        : 'Unrelaxed',
                'system' : {
                    'PluginClass' : 'Acene',
                    'number' : '0',
                    },
                },

            'Output' : {
                'Save' : [
                    'regularized.xyz',
                    'regularized.jpg',
                    ],
                },
            })
        response, script = queryJSON(json)
        response = jc.loads(response)

        self.assertEqual(response['Status'], API.DONE.className())
        self.assertEqual(response['regularized.jpg'], API.NOT_UNICODE.className())

        testfile = 'test.xyz'
        with open(testfile, 'w') as f:
            f.write(response['regularized.xyz'])
        atoms = read(testfile)

        expected = ASEAtoms('HHCCHH', positions=[
                                        [-0.86603, 1.2     , 0.0     ],
                                        [-0.86603, -1.2    , 0.0     ],
                                        [0.0     , 0.7     , 0.0     ],
                                        [0.0     , -0.7    , 0.0     ],
                                        [0.86603 , 1.2     , 0.0     ],
                                        [0.86603 , -1.2    , 0.0     ],
                                             ])

        self.assertAtomsEqual(expected, atoms)


    def testCalculation(self):
        self.prepareTestDirectory(__file__)
        top_jpg = 'topography.jpg'
        top_gsf = 'topography.gsf'
        topview = 'topview.jpg'
        xyz = 'relaxed.xyz'

        json = jc.dumps({
            'Analysis'  :  {
                'PluginClass'        : 'ConstantCurrent',

                'system' : {
                    'PluginClass' : 'Smiles',
                    'smiles_code' : "'CC'",
                    },

                'method' : {
                    'PluginClass' : 'Siesta',
                    'mesh_cutoff' : '2721.1386',
                    'basis_set'   : "'DZP'",
                    'spin'        : "'collinear'",
                    },

                'bias_voltage'       : {
                    'PluginClass' : 'LUMO',
                    'plus'        : '1',
                    },
                'setpoint_current'   : '50',
                'spatial_broadening' : '1.0',
                'voltage_broadening' : '0.1',
                    },

            'Output' : {
                'Save' : [
                    top_jpg,
                    top_gsf,
                    ],

                'SaveSystem' : [
                    topview,
                    xyz,
                    ],
                }
            })

        response, script = queryJSON(json)
        response = jc.loads(response)

        self.assertEqual(response['Status'],API.NOT_DONE.className())
        self.assertEqual(response[top_jpg], API.NOT_DONE.className())
        self.assertEqual(response[top_gsf], API.NOT_DONE.className())
        self.assertEqual(response[topview], API.NOT_DONE.className())
        self.assertEqual(response[xyz], API.NOT_DONE.className())


    def testMalicious(self):
        self.prepareTestDirectory(__file__)
        txt = 'keep it'
        with open('testfile.py', 'w') as f:
            f.write(txt)

        bad_function = 'exec'
        bad_script = "import os\nos.system('rm *')"

        json = jc.dumps({
                'Analysis' : {
                    'PluginClass' : bad_function,
                    'object'      : bad_script,
                    },
                'Output' : {
                    'Save' : ['dummy.txt'],
                    },
                })
        response, script = queryJSON(json)
        response = jc.loads(response)
        self.assertEqual(list(response.keys()), ['Status', 'Error'])

        # Test that nothing happened to the testfile.
        with open('testfile.py', 'r') as f:
            read = f.read()

        self.assertEqual(read, txt)

    def testMalformed(self):
        self.prepareTestDirectory(__file__)

        right = {
                'Analysis' : {
                    'PluginClass' : 'Unrelaxed',
                    'system'      : {
                        'PluginClass' : 'Acene',
                        'number' : '2',
                        },
                    },
                'Output' : {
                    'Save': [
                        'unrelaxed_system.xyz',
                        ],
                    },
                }

        jsonToScript(right)

        wrong = self.setupCorrect()
        wrong['SomethingNotBelonging'] = 'Test'
        self.assertRaises(BadJSONRequest, jsonToScript, wrong)
        wrong = self.setupCorrect()
        wrong.pop('Analysis')
        self.assertRaises(BadJSONRequest, jsonToScript, wrong)
        wrong = self.setupCorrect()
        wrong.pop('Output')
        self.assertRaises(BadJSONRequest, jsonToScript, wrong)
        wrong = self.setupCorrect()
        wrong['Output'].pop('Save')
        self.assertRaises(BadJSONRequest, jsonToScript, wrong)
        wrong = self.setupCorrect()
        wrong['Analysis'].pop('PluginClass')
        self.assertRaises(BadJSONRequest, jsonToScript, wrong)
        wrong = self.setupCorrect()
        wrong['Analysis']['PluginClass'] = 'NotAClassInInterface'
        self.assertRaises(BadJSONRequest, jsonToScript, wrong)

        wrong = self.setupCorrect()
        wrong['Output']['Save'] = set()
        self.assertRaises(BadJSONRequest, jsonToScript, wrong)

        wrong = self.setupCorrect()
        wrong['Output']['Save'] = set()
        wrong['Output']['Save'].add('%#)$*@)*')
        self.assertRaises(BadJSONRequest, jsonToScript, wrong)

        # If the objects are called with the wrong arguments.
        wrong = self.setupCorrect()
        wrong['Analysis'].pop('system')
        self.assertRaises(BadJSONRequest, jsonToScript, wrong)
        wrong = self.setupCorrect()
        wrong['Analysis']['should_not_be_there'] = "'Dummy'"
        self.assertRaises(BadJSONRequest, jsonToScript, wrong)

        # If they are called with the wrong arguments, just fail normally.
        wrong = self.setupCorrect()
        wrong['Analysis']['system'] = 'Something'
        self.assertRaises(ValueError, jsonToScript, wrong)

    def testUnexpectedError(self):
        self.prepareTestDirectory(__file__)
        class TestException(Exception): pass
        class BadClass(ParameterControl):
            def __init__(self):
                raise TestException("This was not expected")
        import CalcTroll.Interface as CI
        setattr(CI, 'BadClass', BadClass)


        test = jc.dumps({
                'Analysis' : {
                    'PluginClass' : 'BadClass',
                    },
                })

        response, script = queryJSON(test)
        response = jc.loads(response)
        self.assertEqual(response['Status'], 'Error')
        self.assertEqual(list(response['Error'].keys()), ['UnexpectedError'])


    def setupCorrect(self):
        right = {
                'Analysis' : {
                    'PluginClass' : 'Unrelaxed',
                    'system'      : {
                        'PluginClass' : 'Acene',
                        'number' : '2',
                        },
                    },
                'Output' : {
                    'Save': [
                        'unrelaxed.xyz' ,
                        ],
                    },
                }

        return right

if __name__=='__main__':
    unittest.main()

