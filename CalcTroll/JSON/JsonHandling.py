import os
import re
import json as jc
import hashlib
import inspect
from collections import OrderedDict
import CalcTroll.Interface as CI
from CalcTroll.Core.ParameterControl import ParameterControl
from CalcTroll import API

ALLOWED = set(["SourceFiles", "Analysis", "Output"])
class BadJSONRequest(API.CalcTrollFlag, Exception): pass
class UnexpectedError(API.CalcTrollFlag, Exception): pass
UNEXPECTED = UnexpectedError('An unforseen error occured during execution.')

def recordUnexpectedError(json, err):
    string = '%s\t%s' % (err.__class__, err)
    print
    print("The API received an unexpected error:")
    print(string)
    print("The error occurred due to following request:")
    print(json)
    print

def scriptToID(script):
    h = hashlib.blake2b()
    h.update(script.encode())

    return h.hexdigest()[:30]

def str2Argument(arg):
    assert isinstance(arg, str)
    if arg[0] == "'" and arg[-1] == "'":
        return arg[1:-1]

    try:
        result = int(arg)
    except ValueError:
        pass
    else:
        return result

    try:
        result = float(arg)
    except ValueError:
        pass
    else:
        return result

    if arg == 'None':
        return None

    # Is it a flag?
    if hasattr(CI, arg):
        return getattr(CI, arg)

    raise ValueError('Input %s not recognized.' % arg)


def jsonToObject(json):
    json = dict(json)
    clsname = json.get('PluginClass')
    if clsname is None:
        raise BadJSONRequest("The keyword 'PluginClass' is required to define CalcTroll objects" + \
                             " through JSON requests.")
    del json['PluginClass']
    if not hasattr(CI, clsname):
        raise BadJSONRequest("The '%s' class requested was not found in CalcTroll.Interface ." %
                             clsname)

    cls = getattr(CI, clsname)
    if not issubclass(cls, ParameterControl):
        raise BadJSONRequest("The '%s' class requested was not found in CalcTroll.Interface ." %
                             clsname)


    defaults = cls.defaults()

    requiered_keys = [key for key, value in defaults.items() if value is inspect._empty]
    all_requiered_keys_there = set(requiered_keys).issubset(json.keys())
    all_keys_allowed = (set(json.keys()).issubset(defaults.keys()))
    if not all_keys_allowed:
        raise BadJSONRequest("The '%s' class received arguments it does not take." %
                             clsname)

    if not all_requiered_keys_there:
        raise BadJSONRequest("The '%s' class must have arguments it didn't get." % clsname)

    kwargs = {}
    for key, item in json.items():
        if isinstance(item, dict):
            kwargs[key] = jsonToObject(item)
        else:
            kwargs[key] = str2Argument(item)

    obj = cls(**kwargs)

    return obj

def jsonToWrittenSourceFiles(json):
    source_files = json.get('SourceFiles')
    if source_files is not None:
        for filename, content in source_files.items():
            with open(filename, 'w') as f:
                f.write(content)

def jsonToFiles(json):
    jsonToWrittenSourceFiles(json)

    script = jsonToScript(json)
    with open('script.py', 'w') as f:
        f.write(script)

    return script

def createSaveScript(variable, json):
    script = ''
    if json is not None:
        if len(json) < 1:
            raise BadJSONRequest("The 'Save' key in 'Output' dictionary cannot be empty.")

        if not isinstance(json, list):
            raise BadJSONRequest("The 'Save' key in 'Output' dictionary must be a Python list of filenames.")
        for filename in json:
            s_filename = ''.join(re.findall(r'[A-Za-z0-9_\-\.]', filename))
            if s_filename != filename:
                raise BadJSONRequest("Only English letters, numbers, '_', '-' and '.' " +\
                                      "are allowed as filenames - got '%s'." % filename)

            split = filename.split('.')
            if len(re.findall(r'\.', filename)) != 1:
                raise BadJSONRequest("The 'Save' key in 'Output' can only contain one '.'.")

            script += "save(%s, '%s')\n" % (variable, filename)

    return script

def jsonToScript(json):
    script = 'from CalcTroll.Interface import *\n\n'
    variable = 'analysis'
    allowed_keys = {'SourceFiles', 'Analysis', 'Output'}
    keys = set(json.keys())

    extra_keys = keys.difference(allowed_keys)
    if len(extra_keys) > 0:
        raise BadJSONRequest("The JSON request cannot take the keys: " + str(extra_keys))

    a_json = json.get('Analysis')
    if a_json is None:
        raise BadJSONRequest("The JSON request must contain key 'Analysis'")

    analysis = jsonToObject(a_json)
    script += analysis.script(variable=variable, style=API.EXPANDED)
    script += '\n\n'
    o_json = json.get('Output')
    if o_json is None:
        raise BadJSONRequest("The JSON request must contain key 'Output'")

    keys = set(o_json.keys())
    if not ('Save' in keys or 'SaveSystem' in keys):
        raise BadJSONRequest("The 'Output' value in the JSON request must contain either" + \
                             "the key 'Save' or 'SaveSystem'")

    script += createSaveScript(variable=variable, json=o_json.get('Save'))
    system = variable + '.system()'
    script += createSaveScript(variable=system, json=o_json.get('SaveSystem'))

    return script

def formatErrorToJson(error):
    return {'Status' : 'Error',
            'Error' : {error.className(): str(error)}}

def queryJSON(json):
    json = jc.loads(json)
    original_json = dict(json)
    json = dict(json)
    err_json = None

    leftover = set(json.keys()) - ALLOWED
    if len(leftover) > 0:
        s = "Requests in JSON only takes %s keywords - got %s." % (ALLOWED, leftover)
        err = BadJSONRequest(s)
        err_json = formatErrorToJson(err)

    if err_json is not None:
        return jc.dumps(err_json), ""

    try:
        script = jsonToFiles(json)
    except (BadJSONRequest, API.CalcTrollException) as err:
        err_json = formatErrorToJson(err)
    except Exception as err:
        recordUnexpectedError(original_json, err)
        err_json = formatErrorToJson(UNEXPECTED)

    if err_json is not None:
        return jc.dumps(err_json), ""

    try:
        exec(script)
    except Exception as err:
        recordUnexpectedError(original_json, err)
        err_json = formatErrorToJson(UNEXPECTED)

    if err_json is not None:
        return jc.dumps(err_json), ""

    ID = scriptToID(script)
    response = {'ScriptID' : ID}

    o_json = json['Output']
    Done = True
    for key in ['Save', 'SaveSystem']:
        save = o_json.get(key)
        if save is not None:
            for filename in save:
                if os.path.exists(filename):
                    with open(filename, 'r') as f:
                        try:
                            value = f.read()
                        except UnicodeDecodeError as f:
                            value = API.NOT_UNICODE.className()
                else:
                    Done = False
                    value = API.NOT_DONE.className()

                response[filename] = value

    if Done:
        response['Status'] = API.DONE.className()
    else:
        response['Status'] = API.NOT_DONE.className()

    return jc.dumps(response), script
