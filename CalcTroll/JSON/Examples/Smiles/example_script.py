from CalcTroll.Interface import *

bias_voltage = LUMO(plus=1)

system = Smiles(smiles_code='CC')

method = Siesta(mesh_cutoff=2721.1386,
                     basis_set='DZP',
                     spin='collinear',
                     )


analysis = ConstantCurrent(system=system,
                           method=method,
                           bias_voltage=bias_voltage,
                           setpoint_current=50,
                           voltage_broadening=0.1,
                           spatial_broadening=1.0,
                           )

save(analysis, 'topography.jpg')
save(analysis, 'topography.gsf')
save(analysis.system(), 'topview.jpg')
save(analysis.system(), 'relaxed.xyz')
