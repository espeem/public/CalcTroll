# Written by Mads Engelund, 2017, http://espeem.com
import numpy as np
import math
from ase.atoms import Atoms
from ase.visualize import view
from ase.constraints import FixAtoms
from numpy.linalg import norm
from pymatgen.core import Molecule
from pymatgen.symmetry.analyzer import PointGroupAnalyzer

from CalcTroll.Core.Utilities import convertToBasis, convertFromBasis
from CalcTroll.Core.Utilities import wrapPositions
from CalcTroll.Core.Utilities import findCopies
from CalcTroll.Core.Utilities import vectorLength
from CalcTroll.Core.Utilities import minimalDistance
from CalcTroll.Core.Utilities import periodicDistance
from CalcTroll.Core.Flags import TooHeavy

from CalcTroll.ASEUtils.AtomUtilities import orderAtoms

SMALL = 0.01
SMALL_ANG = 0.1


def ase2pymatgen(atoms):
    coords = atoms.get_positions()
    symbols = atoms.get_chemical_symbols()

    return Molecule(symbols, coords)

def getSymmetry(atoms):
    mol = ase2pymatgen(atoms)
    ana = PointGroupAnalyzer(mol, tolerance=SMALL_ANG)

    return ana.sch_symbol

def findRotationAngle(R):
    # Diagonalize and extract angle.
    eigsum = np.linalg.eigvals(R).sum().real

    # Drop identity.
    if eigsum < 0:
        eigsum = 0

    angle = np.arccos((eigsum - 1)/2.)/np.pi*180

    return angle

def findRotationAndMirrorOperations(sym_ops):
    # Find the axis corresponding to the lowest angle
    # rotations.
    min_angle = 180
    rot_ops = []
    mir_ops = []
    for op in sym_ops:
        R = op.rotation_matrix
        det = np.linalg.det(R)

        # Only true rotations det(R) = 1 allowed.
        det = np.linalg.det(R)
        if abs(det - 1) < 0.01:

            rot_ops.append(op)

        # Find mirror invesions = -1
        elif abs(det + 1) < 0.01:
            mir_ops.append(op)

    return rot_ops, mir_ops

def findMinimalRotationAxes(rot_ops, disallow):
    if disallow is not None:
        disallow = np.array(disallow)
        disallow /= vectorLength(disallow)

    min_angle = 180
    rot = []
    for i, op in enumerate(rot_ops):
        R = op.rotation_matrix

        if abs(R.trace() - 3) < 0.01:
            continue
        # The disallow vector is not permitted to be
        # an eigenvector.
        # This will also exclude the identity.
        if disallow is not None:
            trans = np.tensordot(R, disallow, (1, 0))
            if abs(np.dot(disallow, trans) - 1) < SMALL:
                continue

        angle = findRotationAngle(R)

        if angle < min_angle:
            min_angle = angle

        rot.append((i, angle))

    low_rot_ops = [rot_ops[i] for i, angle in rot \
                  if abs(angle - min_angle) < 1]

    rot_axes = []
    for op in low_rot_ops:
        R = op.rotation_matrix
        eigs, vectors = np.linalg.eig(R)
        eigsum = eigs.sum().real

        # Just avoid problems when due to numerical
        # inaccuracy |eigsum| > 1
        if abs(eigsum) - 1 < SMALL:
            eigsum = np.clip(eigsum, -1, 1)

        angle = np.arccos((eigsum - 1)/2.).real/np.pi*180
        is_one = abs(eigs - 1) < 0.01
        assert is_one.sum() == 1
        index = list(is_one).index(True)
        axis = np.array(vectors[:, index].real)
        rot_axes.append(axis)

    rot_axes = np.array(rot_axes)
    rot_axes = removeVectorCopies(rot_axes)

    return rot_axes

def removeVectorCopies(vectors):
    i = 0
    while i < len(vectors) - 1:
        dot = np.tensordot(vectors[i], vectors[i+1:], (0, 1))
        keep = np.ones(len(vectors), bool)
        keep[i+1:] = abs(dot - 1) > SMALL
        vectors = vectors[keep]
        i += 1

    return vectors

def findMirrorAxes(mir_ops, disallow):
    if disallow is not None:
        disallow = np.array(disallow)
        disallow /= vectorLength(disallow)

    mir_axes = []
    for op in mir_ops:
        R = op.rotation_matrix
        # Using R = 1 + 2*N*NT where N is the mirror
        # plane normal vector.
        NNT = (np.identity(3) - R)/2.
        eigs, vectors = np.linalg.eig(NNT)
        argmax = eigs.argmax()
        assert abs(eigs[argmax] - 1) < 0.01
        axis = np.array(vectors).real[:, argmax]
        axis /= vectorLength(axis)

        if disallow is not None:
            test = abs(np.dot(disallow, axis))
            if abs(test - 1) < SMALL:
                continue

        mir_axes.append(axis)

    mir_axes = np.array(mir_axes)

    # If we have a disallow vector then return the
    # vector in the mirror plane rather than the
    # normal.
    if disallow is not None and len(mir_axes) > 0:
        mir_axes = np.cross(mir_axes, disallow)

    return mir_axes

def findHighestProjectionAxes(r, disallow):
    if disallow is not None:
        r = r - np.outer((r @ disallow), disallow)
    l = vectorLength(r, axis=1)
    works = l > l.max() - SMALL
    axes = r[works]

    return axes

def findPrincipalAxes(atoms, disallow=None):
    pymat_mol = ase2pymatgen(atoms)
    ana = PointGroupAnalyzer(pymat_mol, tolerance=SMALL_ANG)
    sym_ops = ana.get_symmetry_operations()

    rot_ops, mir_ops = \
        findRotationAndMirrorOperations(sym_ops)

    min_rot_axes = findMinimalRotationAxes(rot_ops, disallow)

    if len(min_rot_axes) > 0:
        return min_rot_axes

    mir_axes = findMirrorAxes(mir_ops, disallow)

    if len(mir_axes) > 0:
        return mir_axes

    r = atoms.get_positions()

    return findHighestProjectionAxes(r, disallow)


def regularize3DSym(atoms):
    axes = findPrincipalAxes(atoms)

    # Tie-breaker is axis with the highest projection of
    # an atomic position.
    r = atoms.get_positions()

    dot = np.tensordot(axes, r, (1, 1))
    argmax = abs(dot).max(axis=1).argmax()
    v1 = axes[argmax]
    dot = np.tensordot(v1, r, (0, 1))
    sign = np.sign(dot[abs(dot).argmax()])
    v1 *= sign

    v0 = np.array([0, 0, 1])
    atoms.rotate(a=v1, v=v0)
    atoms = regularize2DSym(atoms, subspace=[0, 1])

    return atoms

def flipZOrientation(atoms, subspace):
    """ Find a consistent choice of the direction of the z-axis.
        Especially the S4 group (Tetraphenylborate) is the problem.
    """
    last = list(set((0, 1, 2)).difference(subspace))[0]
    r = atoms.get_positions()

    # First criterion is the largest projection.
    mini, maxi = r[:, last].min(), r[:, last].max()
    diff = abs(mini) - maxi

    if abs(diff) > SMALL:
        return diff > 0

    # Second criterion takes the axes along the largest vectors.
    # Compares
    atoms2 = atoms.copy()
    v0 = np.zeros(3, float)
    v0[subspace[0]] = 1
    v1 = np.zeros(3, float)
    v1[last] = 1
    atoms2.rotate(a=180, v=v0)

    max_projections = []
    for a in [atoms, atoms2]:
        r = a.get_positions()

        l = vectorLength(r, axis=1)
        l_max = l.max()
        vps = r[l > l_max - SMALL]
        p_max = vps[:, last].max()
        vps = vps[vps[:, last] > p_max - SMALL]

        pro_max = 0
        for vp in vps:
            vp[last] = 0
            vp /= vectorLength(vp)
            tp = np.cross(vp, v1)
            dot1 = np.tensordot(tp, r, (0, 1))
            dot2 = np.tensordot(vp, r, (0, 1))
            dot3 = r[:, last]
            pro = dot1 + dot2 + dot3
            pro_max = max(pro.max(), pro_max)
        max_projections.append(pro_max)

    normal, reverse = max_projections
    if abs(normal - reverse) > SMALL:
        return (reverse - normal) > 0

    return False

def pickSecondAxis(r, axes, subspace):
    last = list(set((0, 1, 2)).difference(subspace))[0]
    vl = np.zeros(3, float)
    vl[last] = 1.0
    axes[:, last] = 0
    axes = np.array([ax/vectorLength(ax) for ax in axes])
    axes = removeVectorCopies(axes)

    # First tie-breaker - highest projection onto axis.
    dot = np.tensordot(r, axes, (1, 1)).max(axis=0)
    take = dot > dot.max() - SMALL
    axes = axes[take]
    if len(axes) == 1:
        return axes[0]

    # Second tie-breaker - highest possible projection onto (1, 1, 1).
    taxes = np.cross(axes, vl)
    c_system = np.zeros([3] + list(axes.shape))
    c_system[:2] = (taxes, axes)
    c_system[2] = vl
    c_system = np.transpose(c_system, (1, 0, 2))
    ra = np.transpose(np.tensordot(c_system, r, (2, 1)), (0, 2, 1))

    orientations = np.array([( 1, 1, 1), ( 1,-1,-1), (-1, 1,-1), (-1,-1, 1)])
    max_pro = np.zeros(len(axes))
    for orientation in orientations:
        a = orientation * ra
        sm = a.sum(axis=2).max(axis=1)
        max_pro = np.array([max_pro, sm]).max(axis=0)

    m = max_pro.max()
    take = max_pro > m - SMALL
    axes = axes[take]

    if len(axes) == 1:
        return axes[0]

    return axes[0]


def regularize2DSym(atoms, subspace):
    flip = flipZOrientation(atoms, subspace)
    if flip:
        v0 = np.zeros(3, float)
        v0[subspace[0]] = 1
        atoms.rotate(a=180, v=v0)

    last = list(set((0, 1, 2)).difference(subspace))[0]
    r = atoms.get_positions()

    vl = np.zeros(3, float)
    vl[last] = 1.0
    axes = findPrincipalAxes(atoms, disallow=vl)
    v1 = pickSecondAxis(r, axes, subspace)

    v0 = np.zeros(3, float)
    v0[subspace[1]] = 1.0
    atoms.rotate(a=v1, v=v0)

    return atoms


def setOrientationsUniqueDirections(atoms):
    r = atoms.get_positions()
    orientations = [
            ( 1, 1, 1),
            ( 1,-1,-1),
            (-1, 1,-1),
            (-1,-1, 1),
            ]
    orientations = np.array(orientations)

    dot = np.tensordot(r, orientations, (1, 1))
    max_dot = dot.max(axis=0)
    m = max_dot.max()
    orientations = orientations[max_dot > m - SMALL]

    orientation = orientations[0]

    dot_max = []
    for i in (2, 1, 0):
        j = (i - 1) % 3
        sub = np.array([j, i])
        dot = np.tensordot(r[:, sub], orientations[:, sub], (1, 1))
        dot_max.append(dot.max(axis=0))

    dot_max = np.array(dot_max)
    i = 0
    while len(orientations)  > 1 and i < 3:
        m = dot_max[i].max()
        take = dot_max[i] > m - SMALL
        dot_max = dot_max[:, take]
        orientations = orientations[take]
        i += 1

    orientation = orientations[0]
    r = r * orientation
    atoms.set_positions(r)


def regularizePart(atoms, mask):
    # Define mass-less coordinates.
    r = atoms.get_positions()
    sm = np.sqrt(atoms.get_masses())
    smr = (r.T*sm).T

    # Remove average displacement in m-coordinates.
    r -= smr[mask].sum(axis=0)/sm[mask].sum()
    smr = (r.T * sm).T

    # Solve X = sum(ri.T * ri) for eigenvectors.
    # This has the same eigenvectors as angular momentum.
    # Eigenvalues are, however, different.
    RS = np.tensordot(smr[mask], smr[mask], (0, 0))
    X, M = np.linalg.eigh(RS)

    X = X.real
    argsort = np.argsort(X)
    M = M.real
    X = X[argsort[::-1]]
    M = M[:, argsort[::-1]]
    M[:, 0] = np.linalg.det(M)*M[:, 0]
    smr = np.tensordot(M.T, smr, (1, 1)).T
    r = (smr.T/sm).T
    atoms.set_positions(r)
    setOrientationsUniqueDirections(atoms)

    return atoms


def regularizeMolecularDirections(atoms):
    # Define mass-less coordinates.
    r = atoms.get_positions()
    sm = np.sqrt(atoms.get_masses())
    smr = (r.T*sm).T

    # Remove average displacement in m-coordinates.
    r -= smr.sum(axis=0)/sm.sum()
    smr = (r.T * sm).T

    # Solve X = sum(ri.T * ri) for eigenvectors.
    # This has the same eigenvectors as angular momentum.
    # Eigenvalues are, however, different.
    RS = np.tensordot(smr, smr, (0, 0))
    X, M = np.linalg.eigh(RS)

    X = X.real
    argsort = np.argsort(X)
    M = M.real
    X = X[argsort[::-1]]
    M = M[:, argsort[::-1]]
    M[:, 0] = np.linalg.det(M)*M[:, 0]
    smr = np.tensordot(M.T, smr, (1, 1)).T
    r = (smr.T/sm).T
    atoms.set_positions(r)
    symbol = getSymmetry(atoms)

    # Create helper logical variables.
    one_atom = symbol == 'Kh'
    if symbol[-1] in ['h', 'd', 'v']:
        middle = symbol[1:-1]
    else:
        middle = symbol[-1]
    unique_directions = middle in ['1', '2', 's', 'i']
    linear = middle == '*'

    # Define the handling cases.
    exception = symbol == 'D2d'
    unique_directions = middle in ['1', '2', 's', 'i'] and not exception
    linear = middle == '*'
    do_nothing = one_atom
    choose2d = middle.isdigit() and int(middle) > 2 or exception
    choose3d = symbol in ['Td', 'Th', 'Oh', 'Ih']

    # Pick xyz-axis for molecule we have
    # two or three equal eigenvalues.
    if choose2d:
        r = atoms.get_positions()
        if abs(X[0] - X[1]) < abs(X[1] - X[2]):
            subspace = np.array([0, 1])
        else:
            subspace = np.array([1, 2])
        atoms = regularize2DSym(atoms, subspace)

    elif choose3d:
        atoms = regularize3DSym(atoms)

    elif unique_directions:
        pass

    elif linear:
        pass

    elif one_atom:
        pass
    else:
        raise Exception('Unknown Symmetry: %s' % sch_symbol)

    setOrientationsUniqueDirections(atoms)

    r = atoms.get_positions()
    r = np.around(r, decimals=5)
    atoms.set_positions(r)

    atoms.set_cell(r.max(axis=0) + abs(r.min(axis=0)) + 10)
    atoms = orderAtoms(atoms, tolerance=SMALL, order=(0, 1, 2))

    return atoms
