# Written by Mads Engelund, 2017, http://espeem.com
import numpy
from ase.atoms import Atoms
from CalcTroll.ASEUtils.RegularizeMolecule import *
from CalcTroll.Core.Test.Case import CalcTrollTestCase
from CalcTroll import API

import unittest

class RegularizeMoleculeTest(CalcTrollTestCase):
    def rotatedTests(self, expected, atoms, tolerance=4):
        for a1, a2 in [
                       (0, 0),
                       (21., 34.),
                       (66., 199.),
                       ]:
            tatoms = atoms.copy()
            tatoms.rotate(a=a1, v='y')
            tatoms.rotate(a=a2, v='z')
            tatoms = regularizeMolecularDirections(tatoms)
            self.assertAtomsEqual(expected, tatoms, tolerance=tolerance)

    def testNoSymmetry(self):
        """ Test C1 symmetry (no symmetry)."""
        atoms = Atoms("CHFClBr", positions=[
            [0,       0,        0     ],
            [ 1.0900, 0,        0     ],
            [-0.3633, 1.0277,   0     ],
            [-0.3633, -0.5138,  0.8900],
            [-0.3633, -0.5138, -0.8900],
            ])

        expected = Atoms("BrCHFCl", positions=[
            [-0.8567 , -0.15735, 0.06459 ],
            [0.15596 , 0.11216 , -0.23534],
            [0.20803 , 0.17632 , -1.3222 ],
            [0.41879 , 1.07694 , 0.19856 ],
            [0.85375 , -0.64717, 0.11762 ],
            ])
        self.rotatedTests(expected, atoms)

    def testOneAtom(self):
        """ Symmetry Kh - spherical one atom."""
        atoms = Atoms("H", positions=[[1.1, 1.2, 1.3]])
        atoms = regularizeMolecularDirections(atoms)

        expected = Atoms("H", positions=[[0, 0, 0]])
        self.rotatedTests(expected, atoms)

    def testMirrorPlane(self):
        """ Symmetry Cs - one mirror plane"""
        atoms = Atoms("HCCl", positions=[
            [0,       0,        0     ],
            [ 1.0900, 0,        0     ],
            [-0.3633, 1.0277,   0     ],
            ])

        atoms = regularizeMolecularDirections(atoms)

        expected = Atoms("ClHC", positions=[
            [-0.67885, 0.04335 , 0.0     ],
            [0.22688 , -0.56313, 0.0     ],
            [1.10053 , 0.08866 , 0.0     ],
            ])
        self.rotatedTests(expected, atoms)

    def testInversionCenter(self):
        """ Test molecule with only inversion center.
            Seems a little complicated."""
        atoms = Atoms("HCCl", positions=[
            [0,       0,        1     ],
            [ 1.0900, 0,        1     ],
            [-0.3633, 1.0277,   2     ],
            ])
        atoms2 = atoms.copy()
        atoms2.set_positions(-atoms.get_positions())
        atoms += atoms2

        expected = Atoms("ClHCCHCl", positions=[
            [-2.27282, -0.14973, 0.00337 ],
            [-0.89612, 0.23678 , -0.37538],
            [-0.78818, 1.25173 , 0.00715 ],
            [0.78818 , -1.25173, -0.00715],
            [0.89612 , -0.23678, 0.37538 ],
            [2.27282 , 0.14973 , -0.00337],
            ])

        self.assertEqual(getSymmetry(atoms), 'Ci')
        self.rotatedTests(expected, atoms)

    def testLinearNoInversion(self):
        """ Test linear molecule with no inversion center."""
        atoms = Atoms("HC", positions=[
            [1.1, 1.2, 1.3],
            [1.4, 1.5, 2.1],
            ])

        expected = Atoms("CH", positions=[
            [-0.2034, 0.0    , 0.0    ],
            [0.70213, 0.0    , 0.0    ],
            ])
        self.rotatedTests(expected, atoms)

    def testLinearInversion(self):
        """ Test linear molecule with inversion center."""
        atoms = Atoms("HH", positions=[
            [1.1, 1.2, 1.3],
            [1.4, 1.5, 2.1],
            ])

        expected = Atoms("HH", positions=[
            [-0.45277, 0.0     , 0.0     ],
            [0.45277 , 0.0     , 0.0     ],
            ])
        self.rotatedTests(expected, atoms)

    def testOpenBook(self):
        """ Test open book geometry."""
        atoms = Atoms("HCCH", positions=[
            [0.0, 1/np.sqrt(2), 1/np.sqrt(2)],
            [0.0, 0.0, 0.0],
            [1.0, 0.0, 0.0],
            [1.0, 1.0, 0.0],
            ])

        expected = Atoms("HCCH", positions=[
            [-0.52267, 0.71636 , 0.35109 ],
            [-0.49905, -0.20752, -0.03086],
            [0.49905 , -0.20752, 0.03086 ],
            [0.52267 , 0.71636 , -0.35109],
            ])
        self.rotatedTests(expected, atoms)

    def testPropeller(self):
        """ Test propeller-like symmetry."""
        atom_group = Atoms("C", positions=[[0, 1, -1]])
        atoms = atom_group.copy()
        for i in range(5):
            atoms.rotate(a=60., v=[1, 1, 1])
            atoms += atom_group.copy()

        atoms.translate([0, 0, 4])
        group = atoms.copy()
        atoms = group.copy()
        for i in range(2):
            group.rotate(a=120., v='y')
            atoms += group.copy()

        expected = Atoms("CCCCCCCCCCCCCCCCCC", positions=[
            [-4.41588, 2.54951 , 0.0     ],
            [-3.75575, 3.30066 , 1.0     ],
            [-3.66473, 1.88938 , -1.0    ],
            [-2.34447, 3.39168 , 1.0     ],
            [-2.25345, 1.9804  , -1.0    ],
            [-1.76505, -3.72621, 1.0     ],
            [-1.59332, 2.73155 , 0.0     ],
            [-1.56893, -2.74563, 0.0     ],
            [-0.98058, -4.9029 , 1.0     ],
            [-0.58835, -2.94174, -1.0    ],
            [0.0     , -5.09902, 0.0     ],
            [0.19612 , -4.11844, -1.0    ],
            [2.8418  , 0.96135 , -1.0    ],
            [3.16225 , 0.01408 , 0.0     ],
            [3.46861 , 2.22906 , -1.0    ],
            [4.10951 , 0.33453 , 1.0     ],
            [4.41588 , 2.54951 , 0.0     ],
            [4.73633 , 1.60224 , 1.0     ],
            ])

        self.rotatedTests(expected, atoms)


    def testPlanarInversion(self):
        """ Test planar molecule with inversion center."""
        atoms = Atoms("SNNS", positions=[
            [-1, -1, 0],
            [ 0,  0, 1],
            [ 0,  0, 2],
            [ 1,  1, 3.0],
            ])

        expected = Atoms("SNNS", positions=[
            [-2.0614, -0.0264, 0.0    ],
            [-0.3682, 0.3383 , 0.0    ],
            [0.3682 , -0.3383, 0.0    ],
            [2.0614 , 0.0264 , 0.0    ],
            ])
        self.rotatedTests(expected, atoms)


    def testBoricAcid(self):
        group = Atoms("OH", positions=[
            [ 0,  0  , 1 ],
            [ 0,  0.8, 1.1],
            ])
        atoms = Atoms('B', [[0, 0, 0]])
        atoms += group.copy()
        for i in range(2):
            group.rotate(a=120, v='x')
            atoms += group

        expected = Atoms("HOHBOOH", positions=[
            [-1.1779, 0.6801 , 0.0    ],
            [-0.9945, -0.105 , 0.0    ],
            [0.0    , -1.3601, 0.0    ],
            [0.0    , 0.0    , 0.0    ],
            [0.4063 , 0.9137 , 0.0    ],
            [0.5882 , -0.8087, 0.0    ],
            [1.1779 , 0.6801 , 0.0    ],
            ])
        self.rotatedTests(expected, atoms)

    def testWater(self):
        d = 0.7
        cos30, sin30 = np.cos(np.pi/3), np.sin(np.pi/3)
        atoms = Atoms("HOH", positions=[
            [ 0,  d * cos30 , -d * sin30 ],
            [ 0,  0         ,    0.0     ],
            [ 0,  d * cos30 ,  d * sin30 ],
            ])

        expected = Atoms("HOH", positions=[
            [-0.60622, 0.23302 , 0.0     ],
            [0.0     , -0.11698, 0.0     ],
            [0.60622 , 0.23302 , 0.0     ],
            ])
        self.rotatedTests(expected, atoms)

    def testAmmonia(self):
        atoms = Atoms("N", positions=[[0, 0,  0]])
        h1 = Atoms("H", positions=[[0, 0, 1.23]])
        h2 = h1.copy()
        a = np.arccos(-1./3)*(180/np.pi)
        h2.rotate(v='x', a=a)
        h3 = h2.copy()
        h3.rotate(v='z', a=120)
        atoms += h1 + h2 + h3

        expected = Atoms("HHNH", positions=[
            [-1.00429, 0.57983 , 0.22717 ],
            [0.0     , -1.15966, 0.22717 ],
            [0.0     , 0.0     , -0.18283],
            [1.00429 , 0.57983 , 0.22717 ],
            ])
        self.rotatedTests(expected, atoms)

    def testXenonOxytetraFluoride(self):
        """ Testing 4-fold rotation symmetric molecule."""
        atoms = Atoms("XeOHHHH", positions=[
            [ 0,       0,        0     ],
            [ 0,       0,        2     ],
            [ 1.5,     0,        0     ],
            [ 0,     1.5,        0     ],
            [-1.5,     0,        0     ],
            [ 0,    -1.5,        0     ],
            ])
        atoms.rotate(a=13, v='x')

        expected = Atoms("OHHXeHH", positions=[
            [-1.58921, 0.0     , 0.0     ],
            [0.41079 , -1.5    , 0.0     ],
            [0.41079 , 0.0     , -1.5    ],
            [0.41079 , 0.0     , 0.0     ],
            [0.41079 , 0.0     , 1.5     ],
            [0.41079 , 1.5     , 0.0     ],
            ])
        self.rotatedTests(expected, atoms)

    def testMilkingStool(self):
        atom_group = Atoms("CH", positions=[[0, 1.3, 0], [0, 2.2, 0]])
        atoms = atom_group.copy()
        for i in range(4):
            atoms.rotate(a=72., v='x')
            atoms += atom_group.copy()

        atoms += Atoms('NiNO', positions=[
                    [1,   0, 0],
                    [2.5, 0, 0],
                    [3.5, 0, 0],
                    ]
                    )

        expected = Atoms("ONNiHHCCHCCCHH", positions=[
            [-2.6784 , 0.0     , 0.0     ],
            [-1.6784 , 0.0     , 0.0     ],
            [-0.1784 , 0.0     , 0.0     ],
            [0.8216  , -2.09232, -0.67984],
            [0.8216  , -1.29313, 1.77984 ],
            [0.8216  , -1.23637, -0.40172],
            [0.8216  , -0.76412, 1.05172 ],
            [0.8216  , 0.0     , -2.2    ],
            [0.8216  , 0.0     , -1.3    ],
            [0.8216  , 0.76412 , 1.05172 ],
            [0.8216  , 1.23637 , -0.40172],
            [0.8216  , 1.29313 , 1.77984 ],
            [0.8216  , 2.09232 , -0.67984],
            ])
        self.rotatedTests(expected, atoms)

    def testBiphenyl(self):
        atom_group = Atoms("C", positions=[[0, 1.3, 0]])
        atoms = atom_group.copy()
        for i in range(5):
            atoms.rotate(a=60., v='x')
            atoms += atom_group.copy()

        atoms2 = atoms.copy()
        atoms2.translate([0, 4, 0])
        atoms2.rotate(a=34, v='y')
        atoms += atoms2
        atoms.rotate(40, v='z')
        atoms.rotate(31, v='x')

        expected = Atoms("CCCCCCCCCCCC", positions=[
            [-3.3    , 0.0     , 0.0     ],
            [-2.65   , -1.07664, 0.32916 ],
            [-2.65   , 1.07664 , -0.32916],
            [-1.35   , -1.07664, 0.32916 ],
            [-1.35   , 1.07664 , -0.32916],
            [-0.7    , 0.0     , 0.0     ],
            [0.7     , 0.0     , 0.0     ],
            [1.35    , -1.07664, -0.32916],
            [1.35    , 1.07664 , 0.32916 ],
            [2.65    , -1.07664, -0.32916],
            [2.65    , 1.07664 , 0.32916 ],
            [3.3     , 0.0     , 0.0     ],
            ])

        self.rotatedTests(expected, atoms)

    def testBoronTriFluoride(self):
        atoms = Atoms("BFFF", positions=[
            [0, 0.0, 0],
            [0, 0.0,-1],
            [0, np.sqrt(3)/2, 1/2],
            [0,-np.sqrt(3)/2, 1/2],
            ])
        atoms.rotate(a=50, v='x')

        expected = Atoms("FFBF", positions=[
            [-0.86603, 0.5     , 0.0     ],
            [0.0     , -1.0    , 0.0     ],
            [0.0     , 0.0     , 0.0     ],
            [0.86603 , 0.5     , 0.0     ],
            ])
        self.rotatedTests(expected, atoms)

    def notestXenonTetraFluoride(self):
        """ Test 4-fold rotation symmetry."""
        """ Disabled due to pymatgen warning."""

        atoms = Atoms("XeFFFF", positions=[
            [0, 0.0, 0],
            [0, 0.0,-1.1],
            [0, 0.0, 1.1],
            [0,  1.1, 0],
            [0, -1.1, 0],
            ])
        atoms.rotate(a=50, v='x')
        atoms = regularizeMolecularDirections(atoms)

        expected = Atoms("FFXeFF", positions=[
            [-1.1, 0.0 , 0.0 ],
            [0.0 , -1.1, 0.0 ],
            [0.0 , 0.0 , 0.0 ],
            [0.0 , 1.1 , 0.0 ],
            [1.1 , 0.0 , 0.0 ],
            ])
        self.assertAtomsEqual(expected, atoms)

    def testPentagon(self):
        atom_group = Atoms("CH", positions=[[0, 1.3, 0], [0, 2.2, 0]])
        atoms = atom_group.copy()
        for i in range(4):
            atoms.rotate(a=72., v='x')
            atoms += atom_group.copy()

        expected = Atoms("HHCCHCCCHH", positions=[
            [-2.0923, -0.6798, 0.0    ],
            [-1.2931, 1.7798 , 0.0    ],
            [-1.2364, -0.4017, 0.0    ],
            [-0.7641, 1.0517 , 0.0    ],
            [0.0    , -2.2   , 0.0    ],
            [0.0    , -1.3   , 0.0    ],
            [0.7641 , 1.0517 , 0.0    ],
            [1.2364 , -0.4017, 0.0    ],
            [1.2931 , 1.7798 , 0.0    ],
            [2.0923 , -0.6798, 0.0    ],
            ])
        self.rotatedTests(expected, atoms)

    def testBenzene(self):
        atom_group = Atoms("CH", positions=[[0, 2, 0.2], [0, 3, 0.3]])
        atoms = atom_group.copy()
        for i in range(5):
            atoms.rotate(a=60., v='x')
            atoms += atom_group.copy()

        expected = Atoms("HHCCHCCHCCHH", positions=[
            [-2.61103, -1.50748, 0.0     ],
            [-2.61103, 1.50748 , 0.0     ],
            [-1.74069, -1.00499, 0.0     ],
            [-1.74069, 1.00499 , 0.0     ],
            [0.0     , -3.01496, 0.0     ],
            [0.0     , -2.00998, 0.0     ],
            [0.0     , 2.00998 , 0.0     ],
            [0.0     , 3.01496 , 0.0     ],
            [1.74069 , -1.00499, 0.0     ],
            [1.74069 , 1.00499 , 0.0     ],
            [2.61103 , -1.50748, 0.0     ],
            [2.61103 , 1.50748 , 0.0     ],
            ])
        self.rotatedTests(expected, atoms)

    def testAllene(self):
        atoms = Atoms("HHCCCHH", positions=[
            [ 0,       1,        0     ],
            [ 0,      -1,        0     ],
            [ 0,       0,        0     ],
            [ 0,       0,        1     ],
            [ 0,       0,        2     ],
            [ 1,       0,        2     ],
            [-1,       0,        2     ],
            ])
        expected = Atoms("HCHCHCH", positions=[
            [-1.0   , -0.7071, 0.7071 ],
            [-1.0   , 0.0    , 0.0    ],
            [-1.0   , 0.7071 , -0.7071],
            [0.0    , 0.0    , 0.0    ],
            [1.0    , -0.7071, -0.7071],
            [1.0    , 0.0    , 0.0    ],
            [1.0    , 0.7071 , 0.7071 ],
            ])
        self.rotatedTests(expected, atoms)


    def testEthane(self):
        atoms = Atoms("CHHH", positions=[
            [0,       0,        0     ],
            [0,       0,        1     ],
            [0, np.sqrt(3)/2,  -1/2   ],
            [0,-np.sqrt(3)/2,  -1/2   ],
            ])
        atoms2 = atoms.copy()
        atoms2.translate([1, 0, 0])
        atoms2.rotate(a=60, v='x')
        atoms += atoms2

        expected = Atoms("HHCHHCHH", positions=[
            [-0.5  , -0.5  , -0.866],
            [-0.5  , -0.5  , 0.866 ],
            [-0.5  , 0.0   , 0.0   ],
            [-0.5  , 1.0   , 0.0   ],
            [0.5   , -1.0  , 0.0   ],
            [0.5   , 0.0   , 0.0   ],
            [0.5   , 0.5   , -0.866],
            [0.5   , 0.5   , 0.866 ],
            ])
        self.rotatedTests(expected, atoms)


    def testSulphurCrown(self):
        group = Atoms("S", positions=[
            [0, 0, 1.8],
            ])
        atoms = group.copy()
        for i in range(3):
            group.rotate(a=90, v='x')
            atoms += group
        atoms2 = atoms.copy()
        atoms2.translate([0.5, 0, 0])
        atoms2.rotate(a=45, v='x')
        atoms += atoms2

        expected = Atoms("S8", positions=[
            [-1.66298, -0.68883, 0.25    ],
            [-1.66298, 0.68883 , -0.25   ],
            [-0.68883, -1.66298, -0.25   ],
            [-0.68883, 1.66298 , 0.25    ],
            [0.68883 , -1.66298, 0.25    ],
            [0.68883 , 1.66298 , -0.25   ],
            [1.66298 , -0.68883, -0.25   ],
            [1.66298 , 0.68883 , 0.25    ],
            ])

        self.rotatedTests(expected, atoms)


    def testMethane(self):
        atoms = Atoms("C", positions=[[0, 0,  0]])

        h1 = Atoms("H", positions=[[0, 0, 1.23]])
        h2 = h1.copy()
        a = np.arccos(-1./3)*(180/np.pi)
        h2.rotate(v='x', a=a)
        h3 = h2.copy()
        h3.rotate(v='z', a=120)
        h4 = h3.copy()
        h4.rotate(v='z', a=120)
        atoms += h1 + h2 + h3 + h4

        expected = Atoms("HHHCH", positions=[
            [-1.00429, 0.57983 , 0.41    ],
            [0.0     , -1.15966, 0.41    ],
            [0.0     , 0.0     , -1.23   ],
            [0.0     , 0.0     , 0.0     ],
            [1.00429 , 0.57983 , 0.41    ],
            ])

        self.rotatedTests(expected, atoms)


    def testFerrocene(self):
        atom_group = Atoms("C", positions=[[0, 1.3, 0]])
        atoms = atom_group.copy()
        for i in range(4):
            atoms.rotate(a=72., v='x')
            atoms += atom_group.copy()

        atoms2 = atoms.copy()
        atoms2.translate([4, 0, 0])
        atoms2.rotate(a=36, v='x')
        atoms += atoms2
        atoms += Atoms("Fe", positions=[[2, 0, 0]])
        atoms.rotate(40, v='z')
        atoms.rotate(31, v='x')

        expected = Atoms("CCCCCFeCCCCC", positions=[
            [-2.0    , -1.05172, -0.76412],
            [-2.0    , -1.05172, 0.76412 ],
            [-2.0    , 0.40172 , -1.23637],
            [-2.0    , 0.40172 , 1.23637 ],
            [-2.0    , 1.3     , 0.0     ],
            [0.0     , 0.0     , 0.0     ],
            [2.0     , -1.3    , 0.0     ],
            [2.0     , -0.40172, -1.23637],
            [2.0     , -0.40172, 1.23637 ],
            [2.0     , 1.05172 , -0.76412],
            [2.0     , 1.05172 , 0.76412 ],
            ])

        self.rotatedTests(expected, atoms)

    def testTetraphenylBorate(self):
        atom_group = Atoms("CH", positions=[
                          [1.3, 0, 0],
                          [2.2, 0, 0],
                          ])
        atoms = atom_group.copy()
        for i in range(5):
            atom_group.rotate(a=60., v='z')
            atoms += atom_group.copy()
        atoms.pop(1)

        atoms.translate([-2.7, 0, 0])
        atoms.rotate(a=30, v='y')
        atoms.rotate(a=15, v='x')

        phenyl = atoms.copy()
        for i in range(3):
            phenyl.set_positions(-phenyl.get_positions())
            phenyl.rotate(a=90, v='z')
            atoms += phenyl.copy()

        atoms += Atoms('B', [[0, 0, 0]])

        self.assertEqual(getSymmetry(atoms), 'S4')

        expected = Atoms(
            "HHCCHCCHHHHHCCHCCCCHCCBCCHCCCCHCCHHHHHCCHCCHH",
            positions=[
                [-4.29064, 0.0     , -2.36652],
                [-3.59942, -1.82013, -1.34214],
                [-3.50256, 0.0     , -1.93185],
                [-3.09411, -1.07553, -1.32654],
                [-3.05545, 1.82013 , -2.32838],
                [-2.77268, 1.07553 , -1.90931],
                [-1.95578, -1.07553, -0.69869],
                [-1.82013, -3.05545, 2.32838 ],
                [-1.82013, -1.12905, 1.26586 ],
                [-1.82013, 1.67301 , 0.27962 ],
                [-1.82013, 3.59942 , 1.34214 ],
                [-1.67301, -1.82013, -0.27962],
                [-1.63435, 1.07553 , -1.28146],
                [-1.2259 , 0.0     , -0.67615],
                [-1.12905, 1.82013 , -1.26586],
                [-1.07553, -2.77268, 1.90931 ],
                [-1.07553, -1.63435, 1.28146 ],
                [-1.07553, 1.95578 , 0.69869 ],
                [-1.07553, 3.09411 , 1.32654 ],
                [0.0     , -4.29064, 2.36652 ],
                [0.0     , -3.50256, 1.93185 ],
                [0.0     , -1.2259 , 0.67615 ],
                [0.0     , 0.0     , 0.0     ],
                [0.0     , 1.2259  , 0.67615 ],
                [0.0     , 3.50256 , 1.93185 ],
                [0.0     , 4.29064 , 2.36652 ],
                [1.07553 , -3.09411, 1.32654 ],
                [1.07553 , -1.95578, 0.69869 ],
                [1.07553 , 1.63435 , 1.28146 ],
                [1.07553 , 2.77268 , 1.90931 ],
                [1.12905 , -1.82013, -1.26586],
                [1.2259  , 0.0     , -0.67615],
                [1.63435 , -1.07553, -1.28146],
                [1.67301 , 1.82013 , -0.27962],
                [1.82013 , -3.59942, 1.34214 ],
                [1.82013 , -1.67301, 0.27962 ],
                [1.82013 , 1.12905 , 1.26586 ],
                [1.82013 , 3.05545 , 2.32838 ],
                [1.95578 , 1.07553 , -0.69869],
                [2.77268 , -1.07553, -1.90931],
                [3.05545 , -1.82013, -2.32838],
                [3.09411 , 1.07553 , -1.32654],
                [3.50256 , 0.0     , -1.93185],
                [3.59942 , 1.82013 , -1.34214],
                [4.29064 , 0.0     , -2.36652],
                ])

        self.rotatedTests(expected, atoms)


    def testCubane(self):
        positions = np.array([
            [0,       0,        0     ],
            [1,       0,        0     ],
            [0,       1,        0     ],
            [1,       1,        0     ],
            [0,       0,        1     ],
            [1,       0,        1     ],
            [0,       1,        1     ],
            [1,       1,        1     ],
            ])
        atoms = Atoms("CCCCCCCC", positions=positions,)
        atoms.rotate(a=30, v='x')
        atoms.rotate(a=15, v='y')

        expected = Atoms("CCCCCCCC", positions=[
            [-0.5, -0.5, -0.5],
            [-0.5, -0.5, 0.5 ],
            [-0.5, 0.5 , -0.5],
            [-0.5, 0.5 , 0.5 ],
            [0.5 , -0.5, -0.5],
            [0.5 , -0.5, 0.5 ],
            [0.5 , 0.5 , -0.5],
            [0.5 , 0.5 , 0.5 ]
            ])
        self.rotatedTests(expected, atoms)

    def testBuckyBall(self):
        atoms = Atoms('C60', positions=[
            [17.1298 ,  -7.7252 ,  0.9383],
            [16.9210 ,  -8.0535 , -0.3336],
            [17.5841 ,  -9.0716 , -0.8748],
            [18.0004 ,  -8.4142 ,  1.6679],
            [17.0851 ,  -6.3992 ,  1.0322],
            [16.7474 ,  -6.9296 , -1.0258],
            [18.0727 ,  -8.9676 , -2.1087],
            [18.4547 ,  -9.7605 , -0.1451],
            [18.8276 ,  -7.7781 ,  2.4926],
            [18.6635 ,  -9.4322 ,  1.1268],
            [17.9124 ,  -5.7631 ,  1.8569],
            [16.8488 ,  -5.9081 , -0.1824],
            [17.2355 ,  -6.8252 , -2.2586],
            [19.2462 ,  -9.5929 , -2.1407],
            [17.8990 ,  -7.8445 , -2.7999],
            [19.4827 , -10.0832 , -0.9271],
            [18.7830 ,  -6.4520 ,  2.5866],
            [20.0023 ,  -8.4035 ,  2.4608],
            [19.9010 ,  -9.4249 ,  1.6174],
            [18.5027 ,  -4.6348 ,  1.4686],
            [17.4383 ,  -4.7809 , -0.5707],
            [17.8249 ,  -5.6979 , -2.6469],
            [20.2445 ,  -9.0960 , -2.8626],
            [18.8974 ,  -7.3475 , -3.5218],
            [20.7190 , -10.0758 , -0.4365],
            [19.9126 ,  -5.7505 ,  2.6504],
            [21.1312 ,  -7.7030 ,  2.5245],
            [20.9283 ,  -9.7473 ,  0.8365],
            [19.7389 ,  -4.6273 ,  1.9593],
            [18.2662 ,  -4.1446 ,  0.2551],
            [17.9263 ,  -4.6764 , -1.8036],
            [18.8522 ,  -6.0203 , -3.4278],
            [20.0709 ,  -7.9728 , -3.5537],
            [21.4808 ,  -9.0884 , -2.3719],
            [21.7173 ,  -9.5787 , -1.1584],
            [21.0860 ,  -6.3758 ,  2.6185],
            [22.1585 ,  -8.0254 ,  1.7437],
            [22.0572 ,  -9.0468 ,  0.9003],
            [20.7373 ,  -4.1303 ,  1.2374],
            [19.2645 ,  -3.6475 , -0.4668],
            [19.0551 ,  -3.9760 , -1.7398],
            [19.9811 ,  -5.3197 , -3.3641],
            [21.2005 ,  -7.2713 , -3.4899],
            [22.0711 ,  -7.9602 , -2.7602],
            [22.5451 ,  -8.9424 , -0.3326],
            [22.0844 ,  -5.8788 ,  1.8966],
            [22.7480 ,  -6.8981 ,  1.3554],
            [21.9108 ,  -4.7557 ,  1.2055],
            [20.5008 ,  -3.6401 ,  0.0238],
            [20.0824 ,  -4.2983 , -2.5206],
            [21.1559 ,  -5.9452 , -3.3959],
            [22.8984 ,  -7.3240 , -1.9355],
            [23.1347 ,  -7.8152 , -0.7209],
            [23.2361 ,  -6.7937 ,  0.1225],
            [22.3994 ,  -4.6517 , -0.0285],
            [21.5288 ,  -3.9628 , -0.7582],
            [21.3200 ,  -4.2911 , -2.0301],
            [21.9831 ,  -5.3091 , -2.5712],
            [22.8537 ,  -5.9980 , -1.8416],
            [23.0624 ,  -5.6698 , -0.5697],
            ])

        expected = Atoms('C60', positions=[
            [-3.2294, -0.3478, -0.5649],
            [-3.229 , 0.3495 , 0.566  ],
            [-2.8181, -1.6128, -0.5665],
            [-2.8174, -0.2172, 1.6968 ],
            [-2.8178, 0.2186 , -1.6957],
            [-2.8171, 1.6141 , 0.5674 ],
            [-2.4065, -2.1793, 0.5642 ],
            [-2.4062, -1.4821, 1.6951 ],
            [-2.406 , 1.4832 , -1.6943],
            [-2.4057, 2.1806 , -0.5635],
            [-2.1523, -1.8278, -1.699 ],
            [-2.1521, -0.6969, -2.3963],
            [-2.1509, 0.6981 , 2.3971 ],
            [-2.1507, 1.8289 , 1.6998 ],
            [-1.33  , -2.9626, 0.5635 ],
            [-1.3295, -1.8334, 2.3947 ],
            [-1.3294, 1.834  , -2.3943],
            [-1.3288, 2.9632 , -0.563 ],
            [-1.0766, -2.6103, -1.7003],
            [-1.0764, -0.347 , -3.096 ],
            [-1.0751, 0.3476 , 3.0963 ],
            [-1.0747, 2.6108 , 1.7007 ],
            [-0.6651, -3.1772, -0.5684],
            [-0.6646, -2.7485, 1.695  ],
            [-0.6639, -0.9187, 3.0944 ],
            [-0.6646, 0.919  , -3.0941],
            [-0.6639, 2.7488 , -1.6948],
            [-0.6635, 3.1775 , 0.5684 ],
            [-0.0009, -2.2603, -2.3999],
            [-0.0009, -1.1295, -3.0972],
            [0.0008 , 1.1295 , 3.0972 ],
            [0.0009 , 2.2603 , 2.3999 ],
            [0.6633 , -3.1775, -0.5685],
            [0.6639 , -2.7488, 1.6948 ],
            [0.6646 , -0.919 , 3.0941 ],
            [0.664  , 0.9187 , -3.0944],
            [0.6646 , 2.7484 , -1.6951],
            [0.6651 , 3.1772 , 0.5683 ],
            [1.0748 , -2.6108, -1.7007],
            [1.0751 , -0.3476, -3.0963],
            [1.0764 , 0.347  , 3.096  ],
            [1.0766 , 2.6102 , 1.7004 ],
            [1.3288 , -2.9632, 0.563  ],
            [1.3294 , -1.834 , 2.3943 ],
            [1.3294 , 1.8333 , -2.3947],
            [1.3301 , 2.9626 , -0.5635],
            [2.1508 , -1.8289, -1.6998],
            [2.1509 , -0.6981, -2.3971],
            [2.1521 , 0.6969 , 2.3964 ],
            [2.1523 , 1.8278 , 1.6991 ],
            [2.4057 , -2.1806, 0.5636 ],
            [2.406  , -1.4832, 1.6943 ],
            [2.4062 , 1.4821 , -1.6951],
            [2.4065 , 2.1794 , -0.5643],
            [2.8171 , -1.6141, -0.5674],
            [2.8178 , -0.2186, 1.6958 ],
            [2.8173 , 0.2171 , -1.6968],
            [2.8181 , 1.6128 , 0.5665 ],
            [3.229  , -0.3494, -0.5659],
            [3.2294 , 0.3478 , 0.5648 ],
            ])

        # Lower tolerance until a more precise input can be made.
        self.rotatedTests(expected, atoms, tolerance=2)

if __name__=='__main__':
    unittest.main()
