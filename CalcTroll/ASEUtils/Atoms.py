# Written by Mads Engelund, 2017, http://espeem.com
# This is a version of the ASE atoms object used internally.
# The main addition lies in supporting open-boundaries and tags.
import numpy as np
from ase.atoms import Atoms as AseAtoms
from ase.atoms import Atom as AseAtom
from ase.constraints import FixAtoms
from .AtomUtilities import areAtomsEqual

VALENCE_ELECTRONS = np.array([
    1, 2,
    1, 2, 3, 4, 5, 6, 7, 8,
    1, 2, 3, 4, 5, 6, 7, 8,
    1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18,
    1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18,
    1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18,
    1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18,
])

class Tag:
    def __repr__(self):
        return "'%s' tag"%self.name()

    def _arguments(self):
        return {'picker': self.picker()}

    def __init__(self,
            device,
            name,
            picker=slice(None, None),
            ):
        self.__name = str(name)
        self.__device = device
        self.setPicker(picker)

    def copy(self, new_device):
        return self.__class__(
                name=self.name(),
                device=new_device,
                picker=self.picker())

    def name(self):
        return self.__name

    def picker(self):
        return self.__picker.copy()

    def device(self):
        return self.__device

    def setPicker(self, picker):
        self.__picker = picker

    def moveTo(self, new_device, shift=0):
        new_tag = self.copy(new_device=new_device)

        new_tag.shift(shift)

        return new_tag

    def removedAtoms(self, new_device, sl):
        new_tag = self.copy(new_device)
        indices = np.arange(len(self.device()))[sl]
        new_tag.cut(indices)

        return new_tag

    def cut(self, sl):
        raise NotImplementedError

    def shift(self, shift):
        raise NotImplementedError

class IndexTag(Tag):
    def setPicker(self, picker):
        if picker is None:
            picker = np.arange(0, len(self.device()))
        elif isinstance(picker, (list,tuple)):
            picker = np.array(picker, int)
        elif isinstance(picker, np.ndarray):
            if picker.dtype == bool:
                picker = np.arange(0, len(self.device()))[picker]
            elif picker.dtype == int:
                pass
            else:
                raise Exception

        elif isinstance(picker, slice):
            picker = np.arange(0, len(self.device()))[picker]
        else:
            raise Exception

        assert isinstance(picker, np.ndarray) and len(picker.shape) == 1
        Tag.setPicker(self, picker)

    def addToTag(self, indices):
        picker = np.concatenate((self.picker(), indices))
        picker = np.unique(picker)
        self.setPicker(picker)

    def shift(self, shift):
        picker = self.picker() + shift
        self.setPicker(picker)

    def cut(self, cut_indices):
        if len(cut_indices)==0 or len(self.indices()) == 0:
            picker = np.zeros(0, int)
        else:
            max_index = max(max(cut_indices), max(self.indices()))
            mask = np.zeros(max_index + 1, bool)
            mask[self.indices()] = True
            mask = mask[cut_indices]
            picker = np.arange(len(mask))[mask]

        self.setPicker(picker)

    def indices(self):
        return self.picker().copy()

    def mask(self):
        mask = np.zeros(len(self.device()), dtype=bool)
        mask[self.indices()] = True

        return mask

class Buffer(IndexTag): pass

class DopingTag(IndexTag):
    def _arguments(self):
        return {
                'charge': self.charge(),
                'picker': self.picker(),
                }

    def copy(self, new_device):
        return self.__class__(
                name=self.name(),
                device=new_device,
                picker=self.picker(),
                charge=self.charge(),
                )

    def __init__(self, device, name, charge, picker):
        self.__charge = charge
        IndexTag.__init__(self, device=device, name=name, picker=picker)

    def charge(self):
        return self.__charge

class BlockTag(Tag):
    def __repr__(self):
        return "'%s' tag"%self.name()

    def __init__(self,
            device,
            name,
            picker,
            ):
        Tag.__init__(self, device, name, picker)

    def cut(self, sl):
        pass

    def copy(self, new_device):
        return self.__class__(
                       device=new_device,
                       name=self.name(),
                       picker=self.picker(),
                       )


    def setPicker(self, picker):
        Tag.setPicker(self, np.array(picker))

    def shift(self, shift):
        pass

class Region(IndexTag):
    def __repr__(self):
        return "'%s' region"%self.name()

    def _arguments(self):
        return {
                'cell':self.cell(),
                'pbc':self._fullPBC(),
                'picker': self.picker(),
                'voltage': self.voltage(),
                'celldisp': self.celldisp(),
                }

    def __init__(self,
            device,
            name,
            cell=tuple(),
            pbc=(False, False, False),
            picker=slice(None, None),
            voltage=0.0,
            celldisp=(0,0,0),
            ):
        self.__voltage = voltage
        IndexTag.__init__(self, name=name, device=device, picker=picker)
        self.setCell(cell)
        self.setPBC(pbc)
        self.setCellDisp(celldisp)

    def setCellDisp(self, celldisp):
        if celldisp is None:
            atoms = self.device()[self.picker()]
            p_max = atoms.positions.max(axis=0)
            p_min = atoms.positions.min(axis=0)
            center = (p_max + p_min)/2
            cell_center = self.cell().sum(axis=0)/2
            celldisp = center - cell_center

        self.__celldisp = np.array(celldisp)


    def celldisp(self):
        return np.array(self.__celldisp)


    def copy(self, device):
        new_tag = self.__class__(
                name=self.name(),
                device=device,
                picker=self.picker(),
                cell=self.cell(),
                pbc=self.pbc(),
                voltage=self.voltage(),
                celldisp=self.celldisp(),
                )

        return new_tag

    def voltage(self):
        return self.__voltage

    def isTheSame(self, other):
        atoms0 = self.atoms()
        atoms1 = other.atoms()

        if not areAtomsEqual(atoms0, atoms1):
            return False

        return True

    def cell(self):
        return np.array(self.__cell)

    def setCell(self, cell):
        self.__cell = np.array(cell)
        assert self.__cell.shape == (3, 3)

    def pbc(self):
        return tuple(self.__pbc)

    def _fullPBC(self):
        return np.array([entry if hasattr(entry, '__len__') else (entry, entry) for entry in self.__pbc])

    def setPBC(self, pbc):
        pbc = [tuple(entry) if hasattr(entry, '__len__') else entry for entry in pbc]
        pbc = [entry[0] if isinstance(entry, tuple) and entry[0]==entry[1] else entry for entry in pbc]
        assert len(pbc) == 3
        self.__pbc = tuple(pbc)

    def atoms(self):
        atoms = self.device()[self.picker()]
        atoms.set_cell(self.cell())
        atoms.set_pbc(self.pbc())
        atoms.purgeTags()
        atoms.set_celldisp(self.celldisp())

        assert len(atoms.electrodes()) == 0

        return atoms

    def start(self):
        start = self.picker()[0]
        return start

    def isElectrode(self):
        return (not self.semiInfiniteDirection() is None)

    def semiInfiniteDirection(self):
        result = np.array([
            isinstance(entry, tuple) and
            ((True, False)==entry or (False, True)==entry) for entry in self.pbc()])
        if result.sum() == 1:
            return list(result).index(True)
        else:
            return None

    def moveTo(self, new_device, shift=0):
        new = Region(
                name=self.name(),
                device=new_device,
                picker=self.picker(),
                cell=self.cell(),
                pbc=self.pbc(),
                celldisp=self.celldisp(),
                )
        new.shift(shift)

        return new

class DefaulRegion:
    def __init__(self, device):
        self.__device = device

    def name(self):
        return None

    def directAtoms(self):
        indices = self.__device.noRegionIndices()
        return Atoms(self.__device[indices])

    def cell(self):
        return self.__device.get_cell()[self.__device.get_pbc()]


class Atoms(AseAtoms):
    def __init__(self, *args, **kwargs):
        AseAtoms.__init__(self, *args, **kwargs)
        self.__tags = []

    def __add__(self, other):
        shift = len(self)
        atoms = AseAtoms.__add__(self, other)

        return atoms

    def __repr__(self):
        return "ASE_ATOMS"

    def __iadd__(self, other):
        shift = len(self)
        AseAtoms.__iadd__(self, other)

        if isinstance(other, Atoms):
            self.transfer(other, shift)

            for constraint in other.constraints:
                if isinstance(constraint, FixAtoms) and len(constraint.index) > 0:
                    indices = np.array(constraint.index) + shift
                    self.constraints[0].index = np.array(list(self.constraints[0].index) + list(indices))

        return self

    def number_of_valence_electrons(self):
        return VALENCE_ELECTRONS[self.get_atomic_numbers() - 1].sum()

    def _readTags(self, tag_info):
        if tag_info is None:
            return

        for name, classname, kwargs in tag_info:
            if classname == str(IndexTag).split('.')[-1]:
                self.setTag(name, **kwargs)
            elif classname == str(Buffer).split('.')[-1]:
                self.setTag(name, classtype=Buffer, **kwargs)
            elif classname == str(DopingTag).split('.')[-1]:
                self.setTag(name, classtype=DopingTag, **kwargs)
            elif classname == str(Region).split('.')[-1]:
                self.setRegion(name, **kwargs)
            else:
                raise Exception("Reading unknown tag type %s" % classname)

    def copy(self):
        atoms = AseAtoms.copy(self)
        atoms.transfer(self)
        atoms.set_celldisp(self.get_celldisp())

        return atoms

    def transfer(self, other, shift=0):
        for tag in other.tags():
            tag = tag.moveTo(self, shift)
            self.__tags.append(tag)

        self.__cleanupTags()

    def __cleanupTags(self):
        unique = np.unique(self.tagStrings())
        all_tags = []
        for t_string in unique:
            collect_tags = [tag for tag in self.__tags if tag.name() == t_string]
            main_tag = collect_tags[0]
            indices = main_tag.indices()
            for tag in collect_tags[1:]:
                indices = np.concatenate([indices, tag.indices()])
            main_tag.setPicker(indices)
            all_tags.append(main_tag)

        self.__tags = all_tags

    def __getitem__(self, sl):
        constraints = self.constraints

        # Handle ASE bug that numpy arrays cannot slice.
        if isinstance(sl, np.ndarray):
            if sl.dtype=='int':
                pass
            elif sl.dtype=='bool':
                sl = np.arange(len(self))[sl]
            else:
                raise ValueError

            if len(sl) == 0:
                sl = slice(0, 0)
            else:
                sl = list(map(int, sl))

        if isinstance(sl, list) and len(sl) == 0:
            sl = slice(0, 0)

        base = AseAtoms.__getitem__(self, sl)
        if isinstance(base, AseAtom):
            pass
        elif len(constraints) == 1 and isinstance(constraints[0], FixAtoms):
            indices = constraints[0].index
            mask = np.zeros(len(self))
            mask[indices] = True
            mask = mask[sl]
            base.set_constraint(FixAtoms(mask=mask))

        if isinstance(base, AseAtoms):
            base = Atoms(base)
            for tag in self.tags():
                tag = tag.removedAtoms(self, sl)
                tag = tag.copy(base)
                base.__tags.append(tag)

        return base

    def tags(self):
        return list(self.__tags)

    def tagStrings(self):
        return [tag.name() for tag in self.__tags]

    def tag(self, string):
        string_list = self.tagStrings()
        if not string in string_list:
            return

        index = string_list.index(string)
        tag = self.__tags[index]

        return tag

    def removeTag(self, tagname):
        self.__tags = [tag for tag in self.__tags if tag.name() != tagname]

    def purgeTags(self):
        self.__tags = []

    def blocks(self):
        return [tag for tag in self.tags() if isinstance(tag, BlockTag)]

    def blockNames(self):
        return [tag.name() for tag in self.tags() if isinstance(tag, BlockTag)]

    def setBlock(self, name, sub_tags):
        self.__tags.append(BlockTag(self, name, sub_tags))

    def uniqueRegions(self):
        regions = self.regions()
        candidates = np.ones(len(regions), bool)
        for i, candidate in enumerate(regions):
            for j, tester in enumerate(regions[i+1:]):
                if candidate.isTheSame(tester):
                    candidates[i + j + 1] = False

        found = np.array(regions)[candidates]

        result = []
        for region in found:
            names = tuple([region2.name() for region2 in regions \
                     if region.isTheSame(region2)])
            result.append((region, names))

        return result

    def defaulRegion(self):
        return DefaulRegion(self)

    def regionIndices(self):
        region_indices = []
        for region in self.regions():
            region_indices.extend(region.picker())
        region_indices = np.unique(region_indices)

        return region_indices

    def noRegionIndices(self):
        region_indices = self.regionIndices()
        all_indices = np.arange(len(self))
        left_indices = np.unique(set(all_indices) - set(region_indices))
        left_indices = np.array(left_indices, int)

        return left_indices

    def setRegion(self, name, **kwargs):
        self.__tags.append(Region(self, name=name, **kwargs))

    def setTag(self, name, picker=None, classtype=IndexTag, **kwargs):
        self.__tags.append(classtype(device=self, name=name, picker=picker, **kwargs))

    def setDoping(self, name, picker, charge):
        self.__tags.append(DopingTag(
            device=self,
            name=name,
            picker=picker,
            charge=charge,
            ))

    def dopings(self):
        return list([tag for tag in self.__tags if isinstance(tag, DopingTag)])

    def regions(self):
        return list([tag for tag in self.__tags if isinstance(tag, Region)])

    def buffers(self):
        return list([tag for tag in self.__tags if isinstance(tag, Buffer)])

    def regionNames(self):
        return [region.name() for region in self.regions()]

    def region(self, name):
        if isinstance(name, tuple):
            name = name[0]
        for region in self.regions():
            if region.name() == name:
                return region

    def hasOpenBoundaries(self):
        return len(self.electrodes()) > 0

    def electrodes(self):
        return [region for region in self.regions() if region.isElectrode()]

    def electrode(self, name):
        for region in self.electrodes():
            if region.name() == name:
                return region
