
import warnings

from ase.calculators.singlepoint import SinglePointCalculator, all_properties
from ase.constraints import dict2constraint
from ase.atoms import Atoms
from ase.io.aff import affopen, DummyWriter, InvalidAFFError
from ase.io.jsonio import encode, decode
from ase.io.pickletrajectory import PickleTrajectory
from ase.io.trajectory import Trajectory
from ase.io.trajectory import TrajectoryReader
from ase.io.trajectory import write_traj
from ase.parallel import world
from CalcTroll.ASEUtils.Atoms import Atoms


def myread_traj(filename, index):
    trj = TrajectoryReader(filename)
    return trj[index]
