# Written by Mads Engelund, 2017, http://espeem.com
import os
from os.path import join
import unittest
from CalcTroll.Core.Test.Suite import importWalk, findTestCases, ImportTests, TEST_SUITE

test_path = os.path.dirname(__file__)
if test_path == '':
    os.chdir('..')
    test_path = 'CalcTroll'

suite = unittest.TestSuite()
ImportTests.test_paths = [
        join(test_path, 'Core'),
        join(test_path, 'JSON'),
        join(test_path, 'Plugins'),
        join(test_path, 'ASEUtils'),
        ]
suite.addTest(unittest.makeSuite(ImportTests))

for test_path in ImportTests.test_paths:
    importWalk(method=findTestCases, test_path=test_path)
suite.addTest(TEST_SUITE)

runner = unittest.TextTestRunner()
runner.run(suite)
