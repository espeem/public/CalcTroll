# Written by Mads Engelund, 2017, http://espeem.com
import argparse
from CalcTroll.Core.Submission.Workflow import Workflow
host = Workflow().host()

parser = argparse.ArgumentParser(\
"""Cancel all jobs running from subfolders of the current working directory.
Before cancellation all files will be transferred to the home drive of the remote machine.""")
parser.parse_args()

host.cancelJobs()
