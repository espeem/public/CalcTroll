# Written by Mads Engelund, 2017, http://espeem.com
import argparse
from CalcTroll.Core.Submission.Workflow import Workflow

parser = argparse.ArgumentParser(\
"""Cancel all jobs that are current queued.""")
parser.parse_args()

host = Workflow().host()
host.cancelQueued()
