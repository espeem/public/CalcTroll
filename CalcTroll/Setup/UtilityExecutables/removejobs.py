# Written by Mads Engelund, 2017, http://espeem.com
import argparse
from CalcTroll.Core.Submission.Workflow import Workflow
parser = argparse.ArgumentParser(\
"""Cancel all jobs running from subfolders of the current working directory.
All files from the running jobs will be completely removed.""")
parser.parse_args()

host = Workflow().host()
host.removeJobs()

