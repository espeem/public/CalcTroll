# Written by Mads Engelund, 2017, http://espeem.com
import argparse
from CalcTroll.Core.Submission.Workflow import Workflow

parser = argparse.ArgumentParser("Show the the submitted jobs for your current host.")
parser.parse_args()

host = Workflow().host()
host.writeSubmitted()
