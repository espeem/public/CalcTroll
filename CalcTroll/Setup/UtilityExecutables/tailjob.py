# Written by Mads Engelund, 2017, http://espeem.com
import argparse
import sys
from CalcTroll.Core.Submission.Workflow import Workflow

parser = argparse.ArgumentParser(\
"""Show the result of 'tail -f filename' in the main directory of a running job.
The job chosen will be the one running from the current working directory.
""")
parser.add_argument("filename", nargs='?')
args = parser.parse_args()
host = Workflow().host()

host.tailJob(args.filename)
