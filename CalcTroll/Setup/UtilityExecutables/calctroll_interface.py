# Written by Mads Engelund, 2017, http://espeem.com
from CalcTroll import API

# Load all classes that inherit from Plugin and put them in the interface.
class_dict = API.load()
print("""
List of Plugin object in the CalcTroll interface.
(Objects inheriting from specific classes called "Provider" classes)
""")
for key, classes in class_dict.items(): 
    print(key + ':')
    class_names = [cls.className() for cls in classes if cls.className() not in key]
    class_names.sort()
    print(", ".join(class_names))
    print()
