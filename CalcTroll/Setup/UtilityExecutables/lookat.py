# Written by Mads Engelund, 2017, http://espeem.com
import numpy as np
import os
from os.path import join
import argparse
from ase import io
from matplotlib import pylab as plt

from CalcTroll.API import view
from CalcTroll.Core.Submission.Workflow import Workflow
from CalcTroll.Core.Visualizations import dbColorScheme, chargeScheme
from CalcTroll.Plugins.Programs.Siesta.SiestaIO import convertXVtoASE
from CalcTroll.Plugins.Programs.Siesta.SiestaIO import extractMullikenCharge
from CalcTroll.Plugins.Programs.Siesta.SiestaIO import extractVoronoiCharge, extractHirshfeldCharge
from CalcTroll.Plugins.Programs.Siesta.SiestaIO import plotBandStructure
from CalcTroll.ASEUtils.Atoms import Atoms
from CalcTroll.ASEUtils.Trajectory import myread_traj

parser = argparse.ArgumentParser(\
        "Bring up a graphical representation of the data in a file.")
parser.add_argument("filename")
parser.add_argument("-m", "--maya", action="store_true")
args = parser.parse_args()

if args.maya:
    from CalcTroll.Plugins.Visualizations import mview
    from mayavi import mlab
    figure = mlab.figure(bgcolor=(0, 0, 0))

filename = args.filename

if filename[-5:] == '.traj':
    show = myread_traj(filename, -1)
elif filename[-3:] == '.XV':
    show = convertXVtoASE(filename)
elif filename[-4:] == '.xyz':
    show = io.read(filename)
elif filename[-4:] == '.mol':
    show = io.read(filename)
elif filename[-4:] == '.pdb':
    show = io.read(filename)
else:
    show = None

if show is None:
    pass
elif args.maya:
    atoms = Atoms(show)
    mview(atoms=atoms, figure=figure)
else:
    view(show)

def bandstructure(data, ax):
    lines, ticks, EF = data
    up_lines, down_lines = lines
    ticks, tick_labels = ticks
    for x, y in down_lines:
        ax.plot(x, y, 'gray', lw=4)
    for x, y in up_lines:
        ax.plot(x, y, 'k', lw=1)

    ax.set_xlim(0, np.array(x).max())
    ax.set_ylim(EF - 1.5, EF + 1.5)
    for tick in ticks:
        ax.axvline(tick, color='k')
    ax.axhline(EF, linestyle='--')
    ax.set_xticks(ticks)
    ax.set_xticklabels(tick_labels, color='k')


if filename[-4:] == '.out':
    dirname = os.path.dirname(filename)
    xv_file = join(dirname, 'siesta.XV')
    atoms = convertXVtoASE(xv_file)
    charges = extractMullikenCharge(filename)
    chargingScheme(
            figure=figure,
            atoms=atoms,
            charges=charges,
            )

elif filename[-6:] == '.bands':
    data = plotBandStructure(filename)
    ax = plt.gca()
    bandstructure(data=data, ax=ax)
    plt.show()

if args.maya:
    mlab.show()
