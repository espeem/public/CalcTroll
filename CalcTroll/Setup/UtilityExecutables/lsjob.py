# Written by Mads Engelund, 2017, http://espeem.com
import argparse
from CalcTroll.Core.Submission.Workflow import Workflow

parser = argparse.ArgumentParser(\
        "Show the result of performing 'ls -lhtr' in the running directory a job. " +\
        "'ls' will be performed on the job matching the current working directory.")
parser.parse_args()

host = Workflow().host()
host.lsJob()


