# Written by Mads Engelund, 2021, http://espeem.com
import argparse
from CalcTroll.JSON.JsonHandling import queryJSON
from CalcTroll.Core.Submission.Workflow import WORKFLOW

parser = argparse.ArgumentParser("ArgumentParser")
parser.add_argument("filename")
kwargs = vars(parser.parse_args())

with open(kwargs['filename'], 'r') as f:
    json = f.read()

response, script = queryJSON(json)
filename = 'script.py'
with open('response.json', 'w') as f:
    f.write(response)
with open(filename, 'w') as f:
    f.write(script)

host = WORKFLOW.host()
host.submit(filename=filename, loop=True, explain=True, dry_run=False)
