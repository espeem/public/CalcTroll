# Written by Mads Engelund, 2021, http://espeem.com
import argparse
from CalcTroll.JSON.JsonHandling import queryJSON

parser = argparse.ArgumentParser("ArgumentParser")
parser.add_argument("filename")
kwargs = vars(parser.parse_args())

with open(kwargs['filename'], 'r') as f:
    json = f.read()

response, script = queryJSON(json)
with open('response.json', 'w') as f:
    f.write(response)
with open('script.py', 'w') as f:
    f.write(script)
