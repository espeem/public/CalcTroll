# Written by Mads Engelund, 2017, http://espeem.com
import argparse
from CalcTroll.Core.Submission.Workflow import Workflow

parser = argparse.ArgumentParser(\
        "Report all running jobs in the subfolders of the current working directory.")
parser.parse_args()

host = Workflow().host()
host.whatIsRunning()
