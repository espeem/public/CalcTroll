# Written by Mads Engelund, 2017, http://espeem.com
import argparse
from CalcTroll.Core.Submission.Workflow import Workflow

parser = argparse.ArgumentParser(\
"""Copy all jobs running from subfolders of the current working directory.
All files will be copied to the the remote machine data folder from the submitted jobs.""")
parser.add_argument("filename", nargs='?')
args = parser.parse_args()

host = Workflow().host()
host.copyJob(args.filename)
