# Written by Mads Engelund, 2017, http://espeem.com
from CalcTroll.Core.Submission.Workflow import WORKFLOW

host = WORKFLOW.host()
parser = host.submitParser()
kwargs = vars(parser.parse_args())
host.submit(**kwargs)
