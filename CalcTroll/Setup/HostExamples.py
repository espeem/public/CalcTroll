# Written by Mads Engelund, 2017, http://espeem.com
import socket
from ase.units import Ry
from CalcTroll import API
from CalcTroll.Core.Submission.Clusters import CLUSTERS, DEFAULT_CLUSTER_KEY

# This is a plugin that will allow you to play around
# and run the examples on your local machine.
# For proper operation a Host plugin with your
# details must be created.
# See example in Plugins/Servers/Oberon.
SimpleHost = API.load('SimpleHost')

# Set up how your computer should manage calculations.
CLUSTERS[socket.gethostname()] = SimpleHost(
              local_path='/home/appuser',
              data_path='data',
              submit_type=None,
              )

# Create a really, really bad default solver to run the
# examples quicker.
DEFAULT_METHOD = API.load('Siesta')(
        basis_set='SZ',
        spin='non-polarized',
        mesh_cutoff=100*Ry,
        force_tolerance=0.1,
        )
