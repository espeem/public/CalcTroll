# Written by Mads Engelund, 2017, http://espeem.com
import os
import stat
from os.path import join
import sys
import shutil
import getpass
import setuptools

host_filename = 'CalcTroll/HOSTS.py'
packages_filename = 'CalcTroll/PACKAGES.py'
executables = join('CalcTroll', 'Setup', 'UtilityExecutables')
bin_directory = 'bin'
os.chdir(os.path.abspath(os.path.dirname(__file__)))
example_directory = 'ExampleScripts'
PATH = 'CalcTroll/Plugins'

if not os.path.exists(example_directory):
    print('making new examples')
    os.makedirs(example_directory)
    for root, dirs, files in os.walk(PATH):
        head, tail = os.path.split(root)
        if tail == '__pycache__':
            continue

        fs = [f for f in files if f[:8] == 'example_']
        for f in fs:
            shutil.copyfile(join(root, f), join(example_directory, f))

else:
    print('ExampleScripts directory found, will not overwrite...')

if not os.path.exists(host_filename):
    print('making %s example' % host_filename)
    template = join('CalcTroll', 'Setup', 'HostExamples.py')
    with open(template, 'r') as f:
        content = f.read()
    uname = os.uname()[1]
    content = content.replace('COMPUTERNAME', uname)
    home = os.environ['HOME']
    content = content.replace('LOCALPATH', home)

    with open(host_filename, 'w') as f:
        f.write(content)
else:
    print('%s found, will not overwrite...' % host_filename)

if not os.path.exists(packages_filename):
    print('making %s example' % packages_filename)
    template = join('CalcTroll', 'Setup', 'PackagesExample.py')
    with open(template, 'r') as f:
        content = f.read()

    with open(packages_filename, 'w') as f:
        f.write(content)
else:
    print('%s found, will not overwrite...' % packages_filename)

print('making utility executables...')
if not os.path.exists(bin_directory):
    os.makedirs(bin_directory)

proper_files = [filename[:-3] for filename in  os.listdir(executables) if filename[-3:] == '.py']
proper_files.remove('__init__')

for basename in proper_files:
    dst_path = join(bin_directory, basename)
    with open(dst_path, 'w') as dst_file:
        dst_file.write('#!' + sys.executable +'\n')
        dst_file.write('from CalcTroll.Setup.UtilityExecutables import %s' % basename)

    os.chmod(dst_path, stat.S_IEXEC|stat.S_IREAD|stat.S_IWRITE)

with open('README.md', 'r') as fh:
    long_description = fh.read()

setuptools.setup(
    name="calctroll",
    author="Mads Engelund",
    author_email='mads.espeem@espeem.com',
    version="0.0.1",
    description='Workflow management and scripting for atomic-scale simulations.',
    long_description=long_description,
    url='https://gitlab.com/espeem/public/calctroll',
    packages=setuptools.find_packages(),
    classifiers=[
        "Programming Language :: Python :: 3",
        "Operation System :: GNU Linux",
        ],
    python_requires='>=3.6',
    )
