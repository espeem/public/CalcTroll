**********************
Installation
**********************
CalcTroll depends on the Atomic Simulation Environment (ASE) which must be installed to use CalcTroll.
To set up include the root directory in your PYTHONPATH environment.
To get started go to the Examples directory section and investigate the commented scripts.
If the 'ase' directory is empty run the commands: :: 

    git submodule init

    git submodule update

Run setupfile to create utility executables and to create an example:
CalcTrollHosts.py file that you will need to modify for your setup: ::

    python setup.py

Add the relevant directories to the PATH/PYTHONPATH environment: ::

    export $PATH:<Path-to-CalcTroll>/bin

    export $PYTHONPATH:<Path-to-CalcTroll>:<Path-to-CalcTroll>/ase

Go to ExampleScripts and run the examples. You will need to create your own Host plugin, defining how to run calculations a load this plugin in the 
CalcTroll/HOSTS.py file.
