.. CalcTroll documentation master file, created by
   sphinx-quickstart on Mon Sep 12 12:49:23 2016.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to CalcTroll's documentation!
=====================================

Contents:

.. toctree::
   :maxdepth: 2

   intro.rst
   installation.rst
   writing_and_submitting_scripts.rst

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
