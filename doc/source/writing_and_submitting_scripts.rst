**********************************
Writing and submitting scripts
**********************************
You interact with the program by writing python scripts which define your desired calculations.
The easiest way the use CalcTroll is when you can use the predefined physical systems and calculations. 

.. literalinclude:: ../../CalcTroll/Setup/OriginalExampleScripts/stm_expanded.py 
    :language: python

In the following we will expand this script to include more detail.

Importing predefined objects
----------------------------
First, we include all predefined objects start your python script with the following line.

.. literalinclude:: ../../CalcTroll/Setup/OriginalExampleScripts/stm_expanded.py 
    :language: python
    :lines: 2
    
Physical system
---------------
Then we define is the physical system under investigation. 
CalcTroll was originally created to deal with dangling bonds on the Si/Ge(001):H surface and therefore has a lot of predefined systems on this on this topic.

To initiate a system with two neigbouring dangling-bonds, we initialize the predefined 'Dimer2DBDefect' class:

.. literalinclude:: ../../CalcTroll/Setup/OriginalExampleScripts/stm_expanded.py 
    :language: python
    :lines: 4-10

The argument "symbol='Ge'", specify that the element Germanium(Ge) should be used as the substrate. 
A diffent type of arguments are given using the 'parameters' keyword. The normal argument ('symbol') define the physical system, we want to simulate. The arguments given through 'parameters' (layers in the slab, kpts, etc.) is how we make a calculational setup that represents this system.   

STMImage Arguments
------------------
This part of the script sets up the non-default arguments for the STMImage analysis.

.. literalinclude:: ../../CalcTroll/Setup/OriginalExampleScripts/stm_expanded.py 
    :language: python
    :lines: 12-15 

The first line denotes the type of STM image and the second line defined the method used to obtain the image. Using Siesta calculation with the 'KBM' functional, and using the PazSoler method to extend the wavefunctions into vacuum.


Write what the aim is
---------------------
Finally, we plot the the stm image, and if all calculations are not ready a workflow to get to the result will be set up.

.. literalinclude:: ../../CalcTroll/Setup/OriginalExampleScripts/stm_expanded.py 
    :language: python
    :lines: 17-18 
